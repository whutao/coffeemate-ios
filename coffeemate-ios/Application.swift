//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Application
import ApplicationUI
import SwiftUI

// MARK: - Application

@main struct CoffeeMateApplication: App {

	// MARK: Private properties

	@UIApplicationDelegateAdaptor(ApplicationDelegate.self) private var appDelegate

	@Environment(\.scenePhase) private var scenePhase

	// MARK: Body

	var body: some Scene {
		WindowGroup {
			ApplicationView(store: appDelegate.store)
		}
	}

}
