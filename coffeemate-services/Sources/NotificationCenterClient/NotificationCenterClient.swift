//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Foundation

// MARK: - Client

public struct NotificationCenterClient {

	// MARK: Exposed properties

	public let publishNotification: (Notification.Name) -> Void

	public let publisherForNotification: (Notification.Name) -> NotificationCenter.Publisher

}

// MARK: - DependencyKey

extension NotificationCenterClient: DependencyKey {

	public static let liveValue = NotificationCenterClient(
		publishNotification: { notificationName in
			NotificationCenter.default.post(name: notificationName, object: nil)
		},
		publisherForNotification: { notificationName in
			return NotificationCenter.default.publisher(for: notificationName)
		}
	)

}

// MARK: - DependencyValues+NotificationCenterClient

extension DependencyValues {

	public var notificationCenterClient: NotificationCenterClient {
		get { self[NotificationCenterClient.self] }
		set { self[NotificationCenterClient.self] = newValue }
	}

}
