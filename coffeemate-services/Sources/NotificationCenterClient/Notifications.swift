//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Notifications

extension Notification.Name {

	// MARK: Exposed properties

	public static let energyDidReset: Notification.Name = .init("energyDidReset.notification")

}
