//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Foundation

// MARK: - Client

public struct ToastClient: @unchecked Sendable {

	// MARK: Exposed properties

	public var observer: @Sendable () -> AsyncStream<Toast>

	public var present: @Sendable (Toast) async -> Void

}

// MARK: - DependencyKey

extension ToastClient: DependencyKey {

	public static let liveValue: ToastClient = .init(
		observer: {
			return ToastService.shared.toastStream
		},
		present: { toast in
			#if !DEBUG
			if toast.kind == ._debug {
				return
			}
			#endif
			ToastService.shared.append(toast)
		}
	)

}

// MARK: - DependencyValues+ToastClient

extension DependencyValues {

	public var toastClient: ToastClient {
		get { self[ToastClient.self] }
		set { self[ToastClient.self] = newValue }
	}

}
