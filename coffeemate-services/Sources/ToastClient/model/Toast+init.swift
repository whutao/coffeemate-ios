//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Toast inits

extension Toast {

	public static func _debug(_ title: String, subtitle: String? = nil) -> Toast {
		return Toast(id: .init(), title: title, text: subtitle, kind: ._debug)
	}

	public static func info(_ title: String, subtitle: String? = nil) -> Toast {
		return Toast(id: .init(), title: title, text: subtitle, kind: .info)
	}

	public static func error(_ title: String, subtitle: String? = nil) -> Toast {
		return Toast(id: .init(), title: title, text: subtitle, kind: .error)
	}

	public static func warning(_ title: String, subtitle: String? = nil) -> Toast {
		return Toast(id: .init(), title: title, text: subtitle, kind: .warning)
	}

	public static func success(_ title: String, subtitle: String? = nil) -> Toast {
		return Toast(id: .init(), title: title, text: subtitle, kind: .success)
	}

}
