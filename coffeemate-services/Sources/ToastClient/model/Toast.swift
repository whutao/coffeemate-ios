//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct Toast: Equatable, Hashable, Identifiable, Sendable {

	// MARK: Exposed properties

	public let id: UUID

	public let title: String

	public let text: String?

	public let kind: Kind

	// MARK: Init

	public init(
		id: UUID,
		title: String,
		text: String?,
		kind: Kind
	) {
		self.id = id
		self.title = title
		self.text = text
		self.kind = kind
	}

}

// MARK: - Kind

extension Toast {

	public enum Kind: Equatable, Sendable {

		// MARK: Case

		case _debug

		case info

		case error

		case success

		case warning
	}

}
