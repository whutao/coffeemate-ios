//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Service

final class ToastService {

	// MARK: Exposed properties

	static let shared: ToastService = .init()

	private(set) lazy var toastStream: AsyncStream<Toast> = AsyncStream { continuation in
		self.toastStreamContinuation = continuation
	}

	// MARK: Private properties

	private var toastStreamContinuation: AsyncStream<Toast>.Continuation?

	// MARK: Init

	private init() {
		_ = toastStream
	}

	// MARK: Exposed methods

	func append(_ toast: Toast) {
		toastStreamContinuation?.yield(toast)
	}

}
