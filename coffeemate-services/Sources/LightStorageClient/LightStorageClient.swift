//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Extensions
import Foundation
import Models

// MARK: - Client

/// Local storage for *unique* light objects.
///
/// Internal key-value storage maps each stored type to a key,
/// and therefore each type instance is unique in this storage.
///
/// **Avoid storing** primitives here, like `String` or `Int`.
/// Consider some complex singleton types like `OwnProfile`.
public struct LightStorageClient: Sendable {

	// MARK: Private properties

	private let dataForKey: @Sendable (String) -> Data?

	private let setData: @Sendable (Data?, String) -> Void

	private let removeForKey: @Sendable (String) -> Void

	private let removeAllExceptForKeys: @Sendable (Set<String>) -> Void

	// MARK: Exposed properties

	public let removeAll: @Sendable () -> Void

	// MARK: Exposed methods

	public func get<Object: LightStorageStorable>(_ type: Object.Type) -> Object? {
		guard let data = dataForKey(LightStorageClient.uniqueKey(forType: type)) else {
			return nil
		}
		return try? JSONDecoder().decode(Object.self, from: data)
	}

	public func get<Object: LightStorageStorable>(
		_ type: Object.Type,
		or `default`: Object
	) -> Object {
		return get(type.self).unwrapped(or: `default`)
	}

	public func set<Object: LightStorageStorable>(_ object: Object) {
		if let data = try? JSONEncoder().encode(object) {
			setData(data, LightStorageClient.uniqueKey(forType: Object.self))
		} else {
			remove(Object.self)
		}
	}

	public func remove<Object: LightStorageStorable>(_ type: Object.Type) {
		removeForKey(LightStorageClient.uniqueKey(forType: Object.self))
	}

	public func removeAll(exceptFor typesToPersist: [(some LightStorageStorable).Type]) {
		let keysToPersist = Set(typesToPersist.map(LightStorageClient.uniqueKey(forType:)))
		removeAllExceptForKeys(keysToPersist)
	}

	// MARK: Private methods

	private static func uniqueKey<T: LightStorageStorable>(forType type: T.Type) -> String {
		return "lightStorage.\(String(describing: type.self)).UniqueKey"
	}

}

// MARK: - Dependency+Key

extension LightStorageClient: DependencyKey {

	public static let liveValue: LightStorageClient = {
		let defaults: @Sendable () -> UserDefaults = {
			return UserDefaults(suiteName: "coffeemate.lightStorage").forceUnwrapped(
				because: "Suit name must be created or found."
			)
		}
		return LightStorageClient(
			dataForKey: { defaults().data(forKey: $0) },
			setData: { defaults().set($0, forKey: $1) },
			removeForKey: { defaults().removeObject(forKey: $0) },
			removeAllExceptForKeys: { keysToPersist in
				let allKeys = Set(defaults().dictionaryRepresentation().keys)
				let keysToRemove = allKeys.subtracting(keysToPersist)
				keysToRemove.forEach(defaults().removeObject(forKey:))
			},
			removeAll: {
				defaults()
					.dictionaryRepresentation()
					.keys
					.forEach(defaults().removeObject(forKey:))
			}
		)
	}()

	public static let testValue: LightStorageClient = .mock

	public static let noop: LightStorageClient = {
		return LightStorageClient(
			dataForKey: { _ in Data() },
			setData: { _, _ in },
			removeForKey: { _ in },
			removeAllExceptForKeys: { _ in },
			removeAll: { }
		)
	}()

	/// In-memory dictionary that simulate UserDefaults behaviour.
	public static let mock: LightStorageClient = {
		struct Storage: Sendable {
			static var userDefaultsDict: [String: Any] = [:]
		}
		@Sendable func getValue<T>(_ key: String) -> T? {
			return Storage.userDefaultsDict[key] as? T
		}
		@Sendable func removeValue(_ key: String) {
			Storage.userDefaultsDict[key] = nil
		}
		@Sendable func removeAllExpectForKeys(_ keys: Set<String>) {
			Set(Storage.userDefaultsDict.keys)
				.subtracting(keys)
				.forEach { key in
					Storage.userDefaultsDict[key] = nil
				}
		}
		@Sendable func removeAll() {
			Storage.userDefaultsDict = [:]
		}
		@Sendable func setValue<T>(_ value: T, forKey key: String) {
			Storage.userDefaultsDict[key] = value
		}
		return Self(
			dataForKey: { getValue($0) },
			setData: { setValue($0, forKey: $1) },
			removeForKey: { removeValue($0) },
			removeAllExceptForKeys: { removeAllExpectForKeys($0) },
			removeAll: { removeAll() }
		)
	}()

}

// MARK: - DependencyValues+LightStorageClient

extension DependencyValues {

	public var lightStorageClient: LightStorageClient {
		get { self[LightStorageClient.self] }
		set { self[LightStorageClient.self] = newValue }
	}

}
