//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Protocol

public protocol LightStorageStorable: Codable, Sendable {

}
