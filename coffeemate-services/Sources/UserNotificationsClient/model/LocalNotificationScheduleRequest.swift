//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct LocalNotificationScheduleRequest {

	// MARK: Exposed properties

	public let identifier: String

	public let date: Date

	public let title: String

	public let subtitle: String

	public let userInfo: [AnyHashable: Any]

	// MARK: Init

	public init(
		identifier: String,
		date: Date,
		title: String,
		subtitle: String,
		userInfo: [AnyHashable: Any] = [:]
	) {
		self.identifier = identifier
		self.date = date
		self.title = title
		self.subtitle = subtitle
		self.userInfo = userInfo
	}

}
