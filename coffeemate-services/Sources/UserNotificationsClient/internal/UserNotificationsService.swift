//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Constants
import Extensions
import FirebaseMessaging
import LightStorageClient
import Models
import ToastClient
import UIKit

// MARK: - Service

final class UserNotificationsService: NSObject {

	// MARK: Exposed properties

	static let shared = UserNotificationsService()

	// MARK: Private properties

	@Dependency(\.date.now) private var now

	@Dependency(\.lightStorageClient) private var lightStorage

	@Dependency(\.toastClient) private var toastClient

	// MARK: Init

	private override init() {
		super.init()
	}

	// MARK: Exposed methods

	@MainActor func configure() async {
		Messaging.messaging().delegate = self
		UNUserNotificationCenter.current().delegate = self

		UIApplication.shared.registerForRemoteNotifications()

		do {
			let isGranted = try await UNUserNotificationCenter.current().requestAuthorization(
				options: [.alert, .badge, .provisional, .sound]
			)
			if isGranted {
				log.info("Permission for remote notifications is granted.")
			} else {
				log.info("Permission for remote notifications is NOT granted.")
			}
		} catch let error {
			log.error(error)
		}
	}

	func setDeviceToken(_ token: Data) {
		Messaging.messaging().apnsToken = token
	}

	func handleDeviceTokenFailure(_ error: Error) {
		log.error("Device token is not retrieved because \(error).")
	}

	func pendingLocalNotificationExists(withId identifier: String) async -> Bool {
		return await UNUserNotificationCenter.current()
			.pendingNotificationRequests()
			.contains(where: \.identifier == identifier)
	}

	func removePendingLocalNotification(withId identifier: String) {
		UNUserNotificationCenter.current().removePendingNotificationRequests(
			withIdentifiers: [identifier]
		)
	}

	func scheduleLocalNotification(
		withId identifier: String,
		due date: Date,
		title: String,
		subtitle: String,
		userInfo: [AnyHashable: Any] = [:]
	) {
		let timeInterval: TimeInterval = now.distance(to: date)
		guard timeInterval > 0 else {
			log.error("Cannot schedule local notification, time interval is \(timeInterval).")
			return
		}

		let content = UNMutableNotificationContent()
		content.title = title
		content.subtitle = subtitle
		content.sound = .default
		content.userInfo = userInfo

		let trigger = UNTimeIntervalNotificationTrigger(
			timeInterval: timeInterval,
			repeats: false
		)

		let request = UNNotificationRequest(
			identifier: identifier,
			content: content,
			trigger: trigger
		)

		UNUserNotificationCenter.current().add(request)

		log.verbose("Local notification (title: \"\(title)\", subtitle: \"\(subtitle)\") with id=\"\(identifier)\" is scheduled for \(date).")
	}

	func scheduleLocalNotification(_ request: LocalNotificationScheduleRequest) {
		scheduleLocalNotification(
			withId: request.identifier,
			due: request.date,
			title: request.title,
			subtitle: request.subtitle,
			userInfo: request.userInfo
		)
	}

}

// MARK: - UNUserNotificationCenterDelegate

extension UserNotificationsService: UNUserNotificationCenterDelegate {

	// The method will be called on the delegate when the user responded
	// to the notification by opening the application, dismissing the notification
	// or choosing a UNNotificationAction. The delegate must
	// be set before the application returns
	// from application:didFinishLaunchingWithOptions:.
	func userNotificationCenter(
		_ center: UNUserNotificationCenter,
		didReceive response: UNNotificationResponse
	) async {
		log.info("Did receive response \(response).")
	}

	// The method will be called on the delegate only if the application
	// is in the foreground. If the method is not implemented or the handler
	// is not called in a timely manner then the notification will not be presented.
	// The application can choose to have the notification presented as
	// a sound, badge, alert and/or in the notification list.
	// This decision should be based on whether the information
	// in the notification is otherwise visible to the user.
	func userNotificationCenter(
		_ center: UNUserNotificationCenter,
		willPresent notification: UNNotification
	) async -> UNNotificationPresentationOptions {
		log.info("Will present: \(notification).")
		return [.banner, .list, .sound]
	}

}

// MARK: - MessagingDelegate

extension UserNotificationsService: MessagingDelegate {

	func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
		if let fcmToken {
			lightStorage.set(FCMToken(fcmToken))
			log.info("Did receive new FCM token \(fcmToken).")
		} else {
			log.error("Did receive nil FCM token.")
		}
	}

}
