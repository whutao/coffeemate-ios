//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Constants
import Extensions
import FirebaseMessaging
import UIKit

// MARK: - Client

public struct UserNotificationsClient {

	// MARK: Exposed properties

	public let configure: () async -> Void

	public let registerWithDeviceToken: (Data) async -> Void

	public let failToRegisterWithError: (Error) async -> Void

	public let scheduleLocalNotification: (LocalNotificationScheduleRequest) -> Void

}

// MARK: - DependencyKey

extension UserNotificationsClient: DependencyKey {

	public static let liveValue = UserNotificationsClient(
		configure: UserNotificationsService.shared.configure,
		registerWithDeviceToken: UserNotificationsService.shared.setDeviceToken(_:),
		failToRegisterWithError: UserNotificationsService.shared.handleDeviceTokenFailure(_:),
		scheduleLocalNotification: UserNotificationsService.shared.scheduleLocalNotification(_:)
	)

}

// MARK: - DependencyValues+UserNotificationsClient

extension DependencyValues {

	public var userNotificationsClient: UserNotificationsClient {
		get { self[UserNotificationsClient.self] }
		set { self[UserNotificationsClient.self] = newValue }
	}

}
