//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Combine
import ComposableArchitecture
import Foundation

// MARK: - Client

public struct NetworkClient {

	// MARK: Exposed properties

	public let networkDidBecomeReachable: () -> AnyPublisher<Void, Never>

	public let networkDidBecomeUnreachable: () -> AnyPublisher<Void, Never>

}

// MARK: Dependency key

extension NetworkClient: DependencyKey {

	public static var liveValue: NetworkClient {
		return NetworkClient(
			networkDidBecomeReachable: {
				return NetworkService.shared.eventPublisher
					.filter({ $0 == .didBecomeReachable })
					.map({ _ in })
					.eraseToAnyPublisher()
			},
			networkDidBecomeUnreachable: {
				return NetworkService.shared.eventPublisher
					.filter({ $0 == .didBecomeUnreachable })
					.map({ _ in })
					.eraseToAnyPublisher()
			}
		)
	}

}

// MARK: - DependencyValues+ NetworkClient

extension DependencyValues {

	public var networkClient: NetworkClient {
		get { self[NetworkClient.self] }
		set { self[NetworkClient.self] = newValue }
	}

}
