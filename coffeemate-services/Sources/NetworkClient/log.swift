//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import Logger

// MARK: - Logger

let log = makeDefaultLogger(module: "NetworkClient")
