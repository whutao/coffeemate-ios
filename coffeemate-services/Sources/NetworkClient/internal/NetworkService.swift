//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Combine
import Foundation
import Reachability

// MARK: - Base

final class NetworkService: NSObject {

	// MARK: Exposed properties

	static let shared: NetworkService = NetworkService()

	var eventPublisher: AnyPublisher<NetworkEvent, Never> {
		return _eventPublisher.removeDuplicates().eraseToAnyPublisher()
	}

	// MARK: Private properties

	private let _eventPublisher: PassthroughSubject<NetworkEvent, Never>

	private var timer: Timer? {
		willSet {
			timer?.invalidate()
		}
	}

	private lazy var reachability: Reachability = {
		do {
			let reachability = try Reachability()
			NotificationCenter.default.addObserver(
				self,
				selector: #selector(networkStatusChanged),
				name: .reachabilityChanged,
				object: reachability
			)
			try reachability.startNotifier()
			return reachability
		} catch let error {
			fatalError("Reachability initialization failed: \(error).")
		}
	}()

	// MARK: Init

	private override init() {
		_eventPublisher = PassthroughSubject()
		super.init()
	}

	deinit {
		NotificationCenter.default.removeObserver(self)
	}

	// MARK: Private methods

	@objc private func networkStatusChanged(_ sender: Notification) {
		timer = Timer.scheduledTimer(
			withTimeInterval: 3.0,
			repeats: false
		) { [weak self] _ in
			guard let self else {
				return
			}
			switch self.reachability.connection {
			case .cellular, .wifi:
				self._eventPublisher.send(.didBecomeReachable)
			case .unavailable, .none:
				self._eventPublisher.send(.didBecomeUnreachable)
			}
		}
	}

}
