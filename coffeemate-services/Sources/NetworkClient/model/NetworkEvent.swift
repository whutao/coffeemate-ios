//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public enum NetworkEvent: Equatable, Sendable {

	// MARK: Case

	case didBecomeReachable

	case didBecomeUnreachable

}
