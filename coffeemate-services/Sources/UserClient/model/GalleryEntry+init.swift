//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import APIClient
import Foundation
import Models

extension GalleryEntry {

	init(response: GalleryEntryResponse) {
		self.init(
			entryId: response.entryId,
			image: .url(response.imageUrl),
			isOwn: response.isOwn
		)
	}

}
