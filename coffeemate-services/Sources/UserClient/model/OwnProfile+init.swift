//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import APIClient
import Foundation
import Models

extension OwnProfile {

	init(response: OwnProfileResponse) {
		self.init(
			profileId: response.profileId,
			email: response.email,
			nickname: response.nickname,
			gender: response.gender,
			targetGenders: response.targetGenders,
			avatarUrl: response.avatarUrl,
			rating: response.rating,
			about: response.about,
			birthDate: response.birthDate,
			creationDate: response.creationDate,
			isVerified: response.isVerified,
			hobbies: Set(response.hobbies.map(Hobby.init)),
			gallery: response.gallery.map(GalleryEntry.init)
		)
	}

}
