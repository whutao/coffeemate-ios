//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import APIClient
import Combine
import ComposableArchitecture
import Extensions
import GoogleAuthClient
import Foundation
import LightStorageClient
import Models

// MARK: - Helper

private let publishQueue = DispatchQueue(label: "", qos: .utility)

private let signOutEventPublisher: AnyPublisher<Void, Never> = EventPublisher<Void>()
	.subscribe(on: publishQueue)
	.share()
	.eraseToAnyPublisher()

// MARK: - Client

public struct UserClient: @unchecked Sendable {

	// MARK: Exposed properties

	/// Returns the most recent current user access token stored **locally**.
	public var accessToken: @Sendable () -> AccessToken?

	/// Returns the most recent FCM token stored **locally**.
	public var fcmToken: @Sendable () -> FCMToken?

	/// Returns the most recent current user profile stored **locally**.
	public var ownProfile: @Sendable () -> OwnProfile?

	/// Performs a request for own profile using access-token stored **locally**. Stores profile in the local storage.
	public var refreshSession: @Sendable () async throws -> OwnProfile

	/// Triggers sign-in flow that starts with presenting Google sign-in form. Stores profile in the local storage.
	/// - throws: `Error.unexistingAccount` if no account created for this user yet. Then a sign-up form should be completed.
	public var signIn: @Sendable () async throws -> Void

	/// Performs a sign-in flow silently trying to receive Google credentials silently. Stores profile in the local storage.
	/// - throws: `Error.unexistingAccount` if no account created for this user yet. Then a sign-up form should be completed.
	public var signInRestoringGoogleCredentials: @Sendable () async throws -> Bool

	/// Perofms a sing-up request. Stores profile in the local storage.
	public var signUp: @Sendable (SignUpRequest) async throws -> Void

	/// Performs a sign-out request and erases all stored info.
	public var signOut: @Sendable () async throws -> Void

	/// Performs a delete-account request and erases all stored info.
	public var deleteProfile: @Sendable (DeleteOwnProfileRequest) async throws -> Void

	/// Performs profile update request. Stores updated profile in the local storage.
	public var updateProfile: @Sendable (UpdateOwnProfileRequest) async throws -> Void

	/// Performs profile hobby update request.
	public var updateHobbies: @Sendable (UpdateOwnHobbiesRequest) async throws -> Void

	/// Performs profile gallery update request.
	public var updateGalleryWithNewEntries: @Sendable (OwnGalleryPhotoAddRequest) async throws -> Void

	/// Performs profile gallery update request.
	public var deleteEntryFromGallery: @Sendable (OwnGalleryPhotoDeleteRequest) async throws -> Void

	// MARK: - Error

	public enum Error: Swift.Error, Sendable {
		case noAccessTokenFound
		case unexistingAccount
	}

}

// MARK: - DependencyKey

extension UserClient: DependencyKey {

	public static var liveValue: UserClient {
		let getAccessTokenLocally: @Sendable () -> AccessToken? = {
			@Dependency(\.lightStorageClient) var lightStorage
			return lightStorage.get(AccessToken.self)
		}
		let getFCMTokenLocally: @Sendable () -> FCMToken? = {
			@Dependency(\.lightStorageClient) var lightStorage
			return lightStorage.get(FCMToken.self)
		}
		let getOwnProfileLocally: @Sendable () -> OwnProfile? = {
			@Dependency(\.lightStorageClient) var lightStorage
			return lightStorage.get(OwnProfile.self)
		}
		let updateFCMToken: @Sendable (String) async throws -> Void = { token in
			@Dependency(\.apiClient) var apiClient
			try await apiClient.updateFCMToken(.init(fcmToken: token))
		}
		let storeAccessTokenLocally: @Sendable (AccessToken) -> Void = { token in
			@Dependency(\.apiClient) var apiClient
			@Dependency(\.lightStorageClient) var lightStorage
			apiClient.setAccessToken(token.value)
			lightStorage.set(token)
		}
		let storeOwnProfileLocally: @Sendable (OwnProfile) -> Void = { profile in
			@Dependency(\.lightStorageClient) var lightStorage
			lightStorage.set(profile)
		}
		let handleOwnProfileResponse: @Sendable (OwnProfileResponse) -> Void = { response in
			let accessToken = AccessToken(response.accessToken)
			storeAccessTokenLocally(accessToken)
			let ownProfile = OwnProfile(response: response)
			storeOwnProfileLocally(ownProfile)
		}
		let signInUsingGoogleUser: @Sendable (GoogleUser) async throws -> Void = { googleUser in
			@Dependency(\.apiClient) var apiClient
			let signInRequest = SignInWithGoogleRequest(
				socialToken: googleUser.idToken,
				fcmToken: (getFCMTokenLocally()?.value).unwrapped(or: .empty)
			)
			let signInResponse = try await apiClient.signInWithGoogle(signInRequest)
			let accessToken = AccessToken(signInResponse.accessToken)
			storeAccessTokenLocally(accessToken)
			guard signInResponse.isExistingAccount else {
				throw Error.unexistingAccount
			}
			let ownProfileResponse = try await apiClient.ownProfile()
			handleOwnProfileResponse(ownProfileResponse)
			try await updateFCMToken(ownProfileResponse.fcmToken)
		}
		return UserClient(
			accessToken: getAccessTokenLocally,
			fcmToken: getFCMTokenLocally,
			ownProfile: getOwnProfileLocally,
			refreshSession: {
				@Dependency(\.apiClient) var apiClient
				@Dependency(\.lightStorageClient) var lightStorage
				let ownProfileResponse = try await apiClient.ownProfile()
				let ownProfile = OwnProfile(response: ownProfileResponse)
				storeOwnProfileLocally(ownProfile)
				return ownProfile
			},
			signIn: {
				@Dependency(\.apiClient) var apiClient
				@Dependency(\.googleAuthClient) var googleAuth
				let googleUser = try await googleAuth.signIn()
				try await signInUsingGoogleUser(googleUser)
			},
			signInRestoringGoogleCredentials: {
				@Dependency(\.googleAuthClient) var googleAuth
				guard googleAuth.hasPreviousSignIn() else {
					return false
				}
				let googleUser = try await googleAuth.restorePreviousSignIn()
				try await signInUsingGoogleUser(googleUser)
				return true
			},
			signUp: { reqeustBody in
				@Dependency(\.apiClient) var apiClient
				var reqeustBody = reqeustBody
				reqeustBody.payload.fcmToken = (getFCMTokenLocally()?.value).unwrapped(or: .empty)
				let ownProfileResponse = try await apiClient.signUp(reqeustBody)
				handleOwnProfileResponse(ownProfileResponse)
				try await updateFCMToken(ownProfileResponse.fcmToken)
			},
			signOut: {
				@Dependency(\.apiClient) var apiClient
				@Dependency(\.googleAuthClient) var googleAuth
				@Dependency(\.lightStorageClient) var lightStorage
				try await apiClient.signOut()
				googleAuth.signOut()
				apiClient.setAccessToken(nil)
				lightStorage.removeAll(exceptFor: [FCMToken.self])
			},
			deleteProfile: { requestBody in
				@Dependency(\.apiClient) var apiClient
				@Dependency(\.googleAuthClient) var googleAuth
				@Dependency(\.lightStorageClient) var lightStorage
				try await apiClient.deleteOwnProfile(requestBody)
				googleAuth.signOut()
				apiClient.setAccessToken(nil)
				lightStorage.removeAll()
			},
			updateProfile: { requestBody in
				@Dependency(\.apiClient) var apiClient
				let ownProfileResponse = try await apiClient.updateOwnProfile(requestBody)
				handleOwnProfileResponse(ownProfileResponse)
			},
			updateHobbies: { requestBody in
				@Dependency(\.apiClient) var apiClient
				try await apiClient.updateOwnHobbies(requestBody)
			},
			updateGalleryWithNewEntries: { requestBody in
				@Dependency(\.apiClient) var apiClient
				try await apiClient.addPhotoToOwnGallery(requestBody)
			},
			deleteEntryFromGallery: { requestBody in
				@Dependency(\.apiClient) var apiClient
				try await apiClient.deletePhotoFromOwnGallery(requestBody)
			}
		)
	}

}

// MARK: - DependencyValues+UserClient

extension DependencyValues {

	public var userClient: UserClient {
		get { self[UserClient.self] }
		set { self[UserClient.self] = newValue }
	}

}
