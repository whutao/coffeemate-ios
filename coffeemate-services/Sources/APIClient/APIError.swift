//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import Moya

// MARK: - Error

/// Error that raises while working with the network API.
public struct APIError: Error, LocalizedError, Decodable {

	// MARK: Exposed properties

	/// Response status code.
	public let code: Int

	/// Message to display.
	public let message: String

	/// Message to ispect by the developer.
	public let debugMessage: String?

	public var errorDescription: String? {
		return message
	}

	// MARK: Init

	init(
		code: Int,
		message: String,
		debugMessage: String?
	) {
		self.code = code
		self.message = message
		self.debugMessage = debugMessage
	}

	init(fromCode code: Int, message: String? = nil) {
		switch code {
		case 400:
			self.init(
				code: code,
				message: message.unwrapped(or: "Bad request"),
				debugMessage: nil
			)
		case 401:
			self.init(
				code: code,
				message: message.unwrapped(or: "Authorization required"),
				debugMessage: nil
			)
		case 403:
			self.init(
				code: code,
				message: message.unwrapped(or: "Forbidden"),
				debugMessage: nil
			)
		case 404:
			self.init(
				code: code,
				message: message.unwrapped(or: "Not found"),
				debugMessage: nil
			)
		case 426:
			self.init(
				code: code,
				message: message.unwrapped(or: "Upgrade required"),
				debugMessage: nil
			)
		case 429:
			self.init(
				code: code,
				message: message.unwrapped(or: "Too many requests"),
				debugMessage: nil
			)
		case 500:
			self.init(
				code: code,
				message: message.unwrapped(or: "Internal server error"),
				debugMessage: nil
			)
		case 503:
			self.init(
				code: code,
				message: message.unwrapped(or: "Service unavailable"),
				debugMessage: nil
			)
		default:
			self.init(
				code: code,
				message: message.unwrapped(or: "Unknown error."),
				debugMessage: "Unknown error."
			)
		}
	}

}

// MARK: Common HTTP errors

extension APIError {

	// MARK: 4XX

	/// Most probably, body or query parameters are invalid.
	public var isBadRequest: Bool {
		return code == 400
	}

	/// Most probably, access token is invalid and reauthorization is required.
	public var isUnauthorized: Bool {
		return code == 401
	}

	public var isForbidden: Bool {
		return code == 403
	}

	/// Most probably, url is invalid.
	public var isNotFound: Bool {
		return code == 404
	}

	/// New app version should be installed.
	public var isUpgradeRequired: Bool {
		return code == 426
	}

	public var isTooManyRequests: Bool {
		return code == 429
	}

	// MARK: 5XX

	public var isInternalServerError: Bool {
		return code == 500
	}

	public var isServiceUnavailable: Bool {
		return code == 503
	}

}
