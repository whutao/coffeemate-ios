//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import Models

// MARK: - Model

public struct UpdateOwnHobbiesRequest {

	// MARK: Exposed properties

	public var hobbies: Set<Hobby>

	public var parameterDictionary: [String: Any] {
		return [
			"hobbies": hobbies.map(\.hobbyId).sorted()
		]
	}

	// MARK: Init

	public init(hobbies: Set<Hobby>) {
		self.hobbies = hobbies
	}

}
