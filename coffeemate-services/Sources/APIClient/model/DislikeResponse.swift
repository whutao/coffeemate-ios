//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct DislikeResponse: Decodable {

	// MARK: Exposed properties

	public let targetProfileHash: String

	public let isLiked: Bool

	// MARK: Init

	public init(targetProfileHash: String, isLiked: Bool) {
		self.targetProfileHash = targetProfileHash
		self.isLiked = isLiked
	}

}

// MARK: - Coding keys

extension DislikeResponse {

	private enum CodingKeys: String, CodingKey {
		case isLiked = "liked"
		case targetProfileHash = "likedProfileHash"
	}

}
