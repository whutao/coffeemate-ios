//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct OwnGalleryPhotoAddRequest {

	// MARK: Exposed properties

	public let imagesData: [Data]

	// MARK: Init

	public init(imagesData: [Data]) {
		self.imagesData = imagesData
	}

}
