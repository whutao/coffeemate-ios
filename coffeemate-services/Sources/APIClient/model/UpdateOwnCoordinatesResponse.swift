//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct UpdateOwnCoordinatesResponse: Decodable {

	// MARK: Exposed properties

	// MARK: Init

	public init() {

	}

}

// MARK: - Coding keys
//
// extension UpdateOwnCoordinatesResponse {
//	
//	private enum CodingKeys: String, CodingKey {
//		
//	}
//	
// }
