//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct SignInWithGoogleResponse: Decodable {

	// MARK: Exposed properties

	public let accessToken: String

	public let isExistingAccount: Bool

	// MARK: Init

	public init(accessToken: String, isExistingAccount: Bool) {
		self.accessToken = accessToken
		self.isExistingAccount = isExistingAccount
	}

}

// MARK: - Coding keys

extension SignInWithGoogleResponse {

	private enum CodingKeys: String, CodingKey {
		case accessToken = "token"
		case isExistingAccount = "accountExists"
	}

}
