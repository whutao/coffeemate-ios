//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct DislikeRequest {

	// MARK: Exposed properties

	public let targetProfileHash: String

	var parameterDictionary: [String: String] {
		return [
			"interestingProfileHash": targetProfileHash
		]
	}

	// MARK: Init

	public init(targetProfileHash: String) {
		self.targetProfileHash = targetProfileHash
	}

}
