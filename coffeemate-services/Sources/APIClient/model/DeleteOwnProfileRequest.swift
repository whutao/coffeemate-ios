//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct DeleteOwnProfileRequest: Encodable {

	// MARK: Exposed properties

	public let reason: String

	public let feedbackAvailable: Bool

	// MARK: Init

	public init(reason: String, feedbackAvailable: Bool) {
		self.reason = reason
		self.feedbackAvailable = feedbackAvailable
	}

}

// MARK: - Coding keys

extension DeleteOwnProfileRequest {

	private enum CodingKeys: String, CodingKey {
		case reason = "reason"
		case feedbackAvailable = "availableToCallBack"
	}

}
