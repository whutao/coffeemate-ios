//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct OwnGalleryPhotoDeleteRequest {

	// MARK: Exposed properties

	public let galleryEntryId: Int

	public var parameterDictionary: [String: Any] {
		return ["imageId": galleryEntryId]
	}

	// MARK: Init

	public init(galleryEntryId: Int) {
		self.galleryEntryId = galleryEntryId
	}

}
