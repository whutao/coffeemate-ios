//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import CodableWrappers
import Foundation
import Models

// MARK: - Model

public struct SignUpRequest {

	// MARK: Init

	public let imageData: Data

	public var payload: Payload

	public var payloadData: Data {
		do {
			return try JSONEncoder().encode(payload)
		} catch let error {
			log.error("Cannot encode \(payload) due to \(error).")
			return Data()
		}
	}

	// MARK: Init

	public init(
		imageData: Data,
		nickname: String,
		fcmToken: String,
		birthDate: Date,
		ownGender: Gender,
		targetGenders: [Gender],
		about: String,
		hobbies: [Hobby],
		isUserAgreementAccepted: Bool
	) {
		let ownGender: String = {
			switch ownGender {
			case .male:
				return "Man"
			case .female:
				return "Woman"
			case .other:
				return "Minor"
			}
		}()
		self.imageData = imageData
		self.payload = .init(
			nickname: nickname,
			fcmToken: fcmToken,
			birthDate: birthDate,
			ownGender: ownGender,
			targetGenders: Set(targetGenders),
			about: about,
			hobbies: hobbies.map(\.hobbyId),
			isUserAgreementAccepted: true
		)
	}

}

// MARK: - Payload

extension SignUpRequest {

	public struct Payload: Encodable {

		// MARK: Exposed properties

		public let nickname: String

		public var fcmToken: String

		@BirthDateCoding
		public var birthDate: Date

		public let ownGender: String

		@GenderSetCoding
		public var targetGenders: Set<Gender>

		public let about: String

		public let hobbies: [Int]

		public let isUserAgreementAccepted: Bool

		// MARK: Init

		public init(
			nickname: String,
			fcmToken: String,
			birthDate: Date,
			ownGender: String,
			targetGenders: Set<Gender>,
			about: String,
			hobbies: [Int],
			isUserAgreementAccepted: Bool
		) {
			self.nickname = nickname
			self.fcmToken = fcmToken
			self.birthDate = birthDate
			self.ownGender = ownGender
			self.targetGenders = targetGenders
			self.about = about
			self.hobbies = hobbies
			self.isUserAgreementAccepted = isUserAgreementAccepted
		}

		// MARK: - Coding keys

		private enum CodingKeys: String, CodingKey {
			case nickname = "nick"
			case fcmToken = "fcmToken"
			case birthDate = "birthdate"
			case ownGender = "gender"
			case targetGenders = "lookingForGender"
			case about = "about"
			case hobbies = "hobbies"
			case isUserAgreementAccepted = "userAgreementAccepted"
		}

	}

}
