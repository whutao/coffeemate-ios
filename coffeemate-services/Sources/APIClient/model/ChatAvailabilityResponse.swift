//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct ChatAvailabilityResponse: Decodable {

	// MARK: Exposed properties

	public let chatId: Int

	public let companionProfileHash: String

	// MARK: Init

	public init(chatId: Int, companionProfileHash: String) {
		self.chatId = chatId
		self.companionProfileHash = companionProfileHash
	}

}

// MARK: - Coding keys

extension ChatAvailabilityResponse {

	private enum CodingKeys: String, CodingKey {
		case chatId = "chatId"
		case companionProfileHash = "visavisProfileHash"
	}

}
