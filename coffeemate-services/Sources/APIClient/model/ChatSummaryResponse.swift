//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import CodableWrappers
import Foundation

// MARK: - Model

public struct ChatSummaryResponse: Decodable {

	// MARK: Exposed properties

	public let chatId: Int

	@MillisecondsSince1970DateCoding
	public var lastOperationDate: Date

	public let companionPorfileHash: String

	public let companionNickname: String

	public let companionImageUrl: URL

	public let lastMessageType: String?

	public let lastMessagePayload: String?

	@OptionalCoding<APIDateCoding>
	public var lastMessageDateTime: Date?

	public let isHidden: Bool

	public let isIncoming: Bool

	public let isRead: Bool

}

// MARK: - Coding keys

extension ChatSummaryResponse {

	private enum CodingKeys: String, CodingKey {
		case chatId = "chatId"
		case lastOperationDate = "lastChatOperationTime"
		case companionPorfileHash = "visavisProfileHash"
		case companionNickname = "senderNick"
		case companionImageUrl = "senderImageURL"
		case lastMessageType = "lastMessageType"
		case lastMessagePayload = "lastMessagePayload"
		case lastMessageDateTime = "lastMessageDateTime"
		case isHidden = "hidden"
		case isIncoming = "incoming"
		case isRead = "read"
	}

}
