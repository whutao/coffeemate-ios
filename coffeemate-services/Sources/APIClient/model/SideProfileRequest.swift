//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct SideProfileRequest {

	// MARK: Exposed properties

	public let profileHash: String

	var parameterDictionary: [String: String] {
		return [
			"sideUserHash": profileHash
		]
	}

	// MARK: Init

	public init(profileHash: String) {
		self.profileHash = profileHash
	}

}
