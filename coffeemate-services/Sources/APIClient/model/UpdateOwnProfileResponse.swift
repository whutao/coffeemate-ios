//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct UpdateOwnProfileResponse: Decodable {

	// MARK: Exposed properties

	// MARK: Init

	public init() {

	}

}

// MARK: - Coding keys
//
// extension UpdateOwnProfileResponse {
//
//	private enum CodingKeys: String, CodingKey {
//
//	}
//
// }
