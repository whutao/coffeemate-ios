//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import CodableWrappers
import Foundation
import Models

// MARK: - Model

public struct SideProfileResponse: Decodable {

	// MARK: Exposed properties

	public let profileHash: String

	public let nickname: String

	public let age: Int

	@GenderCoding
	public var ownGender: Gender

	@GenderSetCoding
	public var targetGenders: Set<Gender>

	public let avatarUrl: URL

	public let hobbies: [HobbyResponse]

	public let gallery: [GalleryEntryResponse]

	@FallbackDecoding<EmptyString>
	public var about: String

	public let isVerified: Bool

	public let rating: Double

}

// MARK: - Coding keys

 extension SideProfileResponse {

	private enum CodingKeys: String, CodingKey {
		case profileHash = "profileHash"
		case nickname = "nick"
		case age = "age"
		case ownGender = "gender"
		case avatarUrl = "imageURL"
		case hobbies = "hobbies"
		case gallery = "gallery"
		case about = "about"
		case isVerified = "verified"
		case rating = "rating"
		case targetGenders = "lookingFor"
	}

 }
