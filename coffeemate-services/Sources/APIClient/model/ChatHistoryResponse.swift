//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import CodableWrappers
import Extensions
import Foundation

// MARK: - Model

public struct ChatHistoryResponse: Decodable {

	// MARK: Exposed properties

	public let companion: CompanionResponse

	public let messages: [MessageResponse]

	// MARK: Init

	public init(
		companion: CompanionResponse,
		messages: [MessageResponse]
	) {
		self.companion = companion
		self.messages = messages
	}

	public init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		self.companion = try container.decode(CompanionResponse.self, forKey: .companion)
		self.messages = try container.decode(
			Optional<[MessageResponse]>.self,
			forKey: .messages
		)
		.unwrapped(or: [])
	}

	// MARK: - Coding keys

	private enum CodingKeys: String, CodingKey {
		case companion = "visavisInfo"
		case messages = "messages"
	}

}

// MARK: - Model: message

extension ChatHistoryResponse {

	public struct MessageResponse: Decodable {

		// MARK: Exposed properties

		public let messageId: Int

		public let type: String

		public let text: String

		@APIDateCoding
		public var date: Date

		public let senderProfileHash: String

		public let senderNickname: String

		public let senderAvatarUrl: URL

		public let isIncoming: Bool

		public let isRead: Bool

		// MARK: Init

		public init(
			messageId: Int,
			type: String,
			text: String,
			date: Date,
			senderProfileHash: String,
			senderNickname: String,
			senderAvatarUrl: URL,
			isIncoming: Bool,
			isRead: Bool
		) {
			self.messageId = messageId
			self.type = type
			self.text = text
			self.date = date
			self.senderProfileHash = senderProfileHash
			self.senderNickname = senderNickname
			self.senderAvatarUrl = senderAvatarUrl
			self.isIncoming = isIncoming
			self.isRead = isRead
		}

		// MARK: Coding keys

		enum CodingKeys: String, CodingKey {
			case messageId = "id"
			case type = "messageType"
			case text = "payload"
			case date = "dateTime"
			case senderProfileHash = "senderProfileHash"
			case senderNickname = "senderNick"
			case senderAvatarUrl = "senderImageURL"
			case isIncoming = "incoming"
			case isRead = "read"
		}

	}

}

// MARK: - Model: companion

extension ChatHistoryResponse {

	public struct CompanionResponse: Decodable {

		// MARK: Exposed properties

		public let profileHash: String

		public let nickname: String

		public let imageUrl: URL

		// MARK: Init

		public init(profileHash: String, nickname: String, imageUrl: URL) {
			self.profileHash = profileHash
			self.nickname = nickname
			self.imageUrl = imageUrl
		}

		// MARK: Coding keys

		private enum CodingKeys: String, CodingKey {
			case profileHash = "visavisProfileHash"
			case nickname = "visavisNick"
			case imageUrl = "visavisImageURL"
		}

	}

}
