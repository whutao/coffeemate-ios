//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct UpdateOwnCoordinatesRequest: Encodable {

	// MARK: Exposed properties

	public let latitude: Double

	public let longitude: Double

	// MARK: Init

	public init(latitude: Double, longitude: Double) {
		self.latitude = latitude
		self.longitude = longitude
	}

}

// MARK: - Coding keys

extension UpdateOwnCoordinatesRequest {

	private enum CodingKeys: String, CodingKey {
		case latitude = "lat"
		case longitude = "lon"
	}

}
