//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import CodableWrappers
import Foundation
import Models

// MARK: - Model

public struct OwnProfileResponse: Decodable {

	// MARK: Exposed properties

	public let profileId: Int

	public let accessToken: String

	@FallbackDecoding<EmptyString>
	public var fcmToken: String

	public let email: String

	public let nickname: String

	@GenderCoding
	public var gender: Gender

	@GenderSetCoding
	public var targetGenders: Set<Gender>

	public let avatarUrl: URL

	public let rating: Double

	public let about: String

	@BirthDateCoding
	public var birthDate: Date

	@APIDateCoding
	public var creationDate: Date

	public let hobbies: [HobbyResponse]

	public let gallery: [GalleryEntryResponse]

	public let isVerified: Bool

}

// MARK: - Coding keys

extension OwnProfileResponse {

	private enum CodingKeys: String, CodingKey {
		case profileId = "id"
		case fcmToken = "fcmToken"
		case accessToken = "token"
		case avatarUrl = "imageURL"
		case email = "email"
		case nickname = "nick"
		case gender = "gender"
		case birthDate = "birthday"
		case creationDate = "created"
		case hobbies = "hobbies"
		case gallery = "gallery"
		case about = "about"
		case rating = "rating"
		case targetGenders = "lookingFor"
		case isVerified = "verified"
	}

}
