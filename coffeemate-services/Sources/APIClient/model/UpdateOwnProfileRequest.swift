//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import CodableWrappers
import Foundation
import Models

// MARK: - Model

public struct UpdateOwnProfileRequest: Encodable {

	// MARK: Exposed properties

	public var imageData: Data?

	public var payload: Payload?

	public var payloadData: Data? {
		guard let payload else {
			return nil
		}
		do {
			return try JSONEncoder().encode(payload)
		} catch let error {
			log.error("Cannot encode \(payload) due to \(error).")
			return Data()
		}
	}

	// MARK: Init

	public init(
		imageData: Data?,
		payload: Payload?
	) {
		self.imageData = imageData
		self.payload = payload
	}

}

// MARK: - Payload

extension UpdateOwnProfileRequest {

	public struct Payload: Encodable {

		// MARK: Exposed properties

		public let nickname: String

		@BirthDateCoding
		public var birthDate: Date

		@GenderCoding
		public var gender: Gender

		public let about: String

		@GenderSetCoding
		public var targetGenders: Set<Gender>

		// MARK: Init

		public init(
			nickname: String,
			birthDate: Date,
			gender: Gender,
			about: String,
			targetGenders: Set<Gender>
		) {
			self.nickname = nickname
			self.birthDate = birthDate
			self.gender = gender
			self.about = about
			self.targetGenders = targetGenders
		}

		// MARK: Coding keys

		private enum CodingKeys: String, CodingKey {
			case nickname = "nick"
			case birthDate = "birthdate"
			case gender = "gender"
			case about = "about"
			case targetGenders = "lookingForGender"
		}

	}

}
