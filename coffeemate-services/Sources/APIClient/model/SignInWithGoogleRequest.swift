//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct SignInWithGoogleRequest: Encodable {

	// MARK: Exposed properties

	public let socialToken: String

	public let fcmToken: String

	// MARK: Init

	public init(socialToken: String, fcmToken: String) {
		self.socialToken = socialToken
		self.fcmToken = fcmToken
	}

}

// MARK: - Coding keys

extension SignInWithGoogleRequest {

	private enum CodingKeys: String, CodingKey {
		case socialToken = "socialToken"
		case fcmToken = "fcmToken"
	}

}
