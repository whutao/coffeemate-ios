//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct LikeResponse: Decodable {

	// MARK: Exposed properties

	public let targetProfileHash: String

	public let isLikedBack: Bool

	// MARK: Init

	public init(
		targetProfileHash: String,
		isLikedBack: Bool
	) {
		self.targetProfileHash = targetProfileHash
		self.isLikedBack = isLikedBack
	}

}

// MARK: - Coding keys

extension LikeResponse {

	private enum CodingKeys: String, CodingKey {
		case targetProfileHash = "likedProfileHash"
		case isLikedBack = "likedBack"
	}

}
