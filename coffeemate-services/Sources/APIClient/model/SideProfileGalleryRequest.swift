//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct SideProfileGalleryRequest {

	// MARK: Exposed properties

	public let sideProfileHash: String

	public var parameterDictionary: [String: Any] {
		return ["sideProfileHash": sideProfileHash]
	}

	// MARK: Init

	public init(sideProfileHash: String) {
		self.sideProfileHash = sideProfileHash
	}

}
