//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct UpdateFCMTokenRequest: Encodable {

	// MARK: Exposed properties

	public let fcmToken: String

	// MARK: Init

	public init(fcmToken: String) {
		self.fcmToken = fcmToken
	}

}

// MARK: - Coding keys

extension UpdateFCMTokenRequest {

	private enum CodingKeys: String, CodingKey {
		case fcmToken = "fcmToken"
	}

}
