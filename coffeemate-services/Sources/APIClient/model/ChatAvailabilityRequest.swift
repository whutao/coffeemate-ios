//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct ChatAvailabilityRequest {

	// MARK: Exposed properties

	public let companionProfileHash: String

	public var parameterDictionary: [String: Any] {
		return ["visavisId": companionProfileHash]
	}

	// MARK: Init

	public init(companionProfileHash: String) {
		self.companionProfileHash = companionProfileHash
	}

}
