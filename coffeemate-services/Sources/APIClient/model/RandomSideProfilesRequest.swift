//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct RandomSideProfilesRequest {

	// MARK: Exposed properties

	public let searchRadius: Int

	var parameterDictionary: [String: String] {
		return [
			"radiusKm": searchRadius.description
		]
	}

	// MARK: Init

	public init<I: BinaryInteger>(searchRadius: I) {
		self.searchRadius = Int(searchRadius)
	}

}
