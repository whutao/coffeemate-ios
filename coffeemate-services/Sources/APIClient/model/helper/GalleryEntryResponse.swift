//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct GalleryEntryResponse: Decodable {

	// MARK: Exposed properties

	public let entryId: Int

	public let imageUrl: URL

	public let isOwn: Bool

	// MARK: Init

	public init(entryId: Int, imageUrl: URL, isOwn: Bool) {
		self.entryId = entryId
		self.imageUrl = imageUrl
		self.isOwn = isOwn
	}

}

// MARK: - Coding keys

extension GalleryEntryResponse {

	private enum CodingKeys: String, CodingKey {
		case entryId = "id"
		case imageUrl = "imageURL"
		case isOwn = "owner"
	}

}
