//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import CodableWrappers
import Foundation
import Models

// MARK: - Typealias

public typealias GenderCoding = CodingUses<GenderCoder>

// MARK: - Custom strategy

public struct GenderCoder: StaticCoder {

	public static func encode(value: Gender, to encoder: Encoder) throws {
		let stringValue: String = {
			switch value {
			case .male:
				return "Man"
			case .female:
				return "Woman"
			case .other:
				return "Minor"
			}
		}()
		try stringValue.encode(to: encoder)
	}

	public static func decode(from decoder: Decoder) throws -> Gender {
		let stringValue = try String(from: decoder)
		switch stringValue {
		case "Man":
			return .male
		case "Woman":
			return .female
		case "Minor":
			return .other
		default:
			throw DecodingError.dataCorrupted(.init(
				codingPath: decoder.codingPath,
				debugDescription: "Expected to find string value [\"Man\", \"Woman\", \"minor\"], but found \(stringValue)."
			))
		}
	}

}
