//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import CodableWrappers
import ComposableArchitecture
import Foundation

// MARK: - Typealiases

public typealias APIDateCoding = CodingUses<APIDateCoder>

public typealias BirthDateCoding = CodingUses<BirthDateCoder>

// MARK: - Custom strategies

public struct APIDateCoder: StaticCoder, Sendable {

//	@Dependency(\.timeZone) private static var timeZone

	private static let dateFormat = "yyyy-MM-dd HH:mm"

	public static func encode(value: Date, to encoder: Encoder) throws {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = dateFormat
		dateFormatter.timeZone = .gmt

		let stringValue = dateFormatter.string(from: value)
		try stringValue.encode(to: encoder)
	}

	public static func decode(from decoder: Decoder) throws -> Date {
		let stringValue = try String(from: decoder)

		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = dateFormat
		dateFormatter.timeZone = .gmt

		guard let dateValue = dateFormatter.date(from: stringValue) else {
			throw DecodingError.valueNotFound(Date.self, .init(
				codingPath: decoder.codingPath,
				debugDescription: "Expected \(Date.self) but could not convert \(stringValue) to Date."
			))
		}

		return dateValue
	}

}

public struct BirthDateCoder: DateFormatterStaticCoder {

	public static let dateFormatter: DateFormatter = {
		let formatter = DateFormatter()
		formatter.dateFormat = "yyyy-MM-dd"
		return formatter
	}()

}
