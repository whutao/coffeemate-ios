//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import CodableWrappers
import Foundation
import Models

// MARK: - Typealias

public typealias GenderSetCoding = CodingUses<GenderSetCoder>

// MARK: - Custom strategy

public struct GenderSetCoder: StaticCoder {

	public static func encode(value: Set<Gender>, to encoder: Encoder) throws {
		var result = "00000"
		result += value.contains(.other) ? "1" : "0"
		result += value.contains(.female) ? "1" : "0"
		result += value.contains(.male) ? "1" : "0"
		try result.encode(to: encoder)
	}

	public static func decode(from decoder: Decoder) throws -> Set<Gender> {
		var stringValue = try String(from: decoder)

		guard stringValue.count == 8 else {
			throw DecodingError.dataCorrupted(.init(
				codingPath: decoder.codingPath,
				debugDescription: "Expected string value in format \"00000XXX\" but \(stringValue) found."
			))
		}

		var result: Set<Gender> = []

		let maleFlag = stringValue.last
		stringValue = String(stringValue.dropLast(1))
		let femaleFlag = stringValue.last
		stringValue = String(stringValue.dropLast(1))
		let otherFlag = stringValue.last

		if maleFlag == "1" {
			result.insert(.male)
		}
		if femaleFlag == "1" {
			result.insert(.female)
		}
		if otherFlag == "1" {
			result.insert(.other)
		}
		return result
	}

}
