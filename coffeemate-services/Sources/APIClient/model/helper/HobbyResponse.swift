//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct HobbyResponse: Decodable {

	// MARK: Exposed properties

	public let hobbyId: Int

	public let title: String

	public let imageUrl: URL

	public let isPrivileged: Bool

	// MARK: Init

	public init(
		hobbyId: Int,
		title: String,
		imageUrl: URL,
		isPrivileged: Bool
	) {
		self.hobbyId = hobbyId
		self.title = title
		self.imageUrl = imageUrl
		self.isPrivileged = isPrivileged
	}

}

// MARK: - Coding keys

extension HobbyResponse {

	private enum CodingKeys: String, CodingKey {
		case hobbyId = "id"
		case title = "hobby"
		case imageUrl = "iconUrl"
		case isPrivileged = "vipOnly"
	}

}
