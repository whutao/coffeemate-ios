//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct ChatHistoryRequest {

	// MARK: Exposed properties

	public let chatId: Int

	public let companionProfileHash: String

	public var parameterDictionary: [String: Any] {
		return [
			"chatId": chatId,
			"visavisId": companionProfileHash
		]
	}

	// MARK: Init

	public init(
		chatId: Int,
		companionProfileHash: String
	) {
		self.chatId = chatId
		self.companionProfileHash = companionProfileHash
	}

}
