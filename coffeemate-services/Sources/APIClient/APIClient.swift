//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Foundation

// MARK: - Client

public struct APIClient: @unchecked Sendable {

	// MARK: Exposed properties

	// Internal logic

	public var setAccessToken: @Sendable (_ token: String?) -> Void

	// User

	public var signInWithGoogle: @Sendable (SignInWithGoogleRequest) async throws -> SignInWithGoogleResponse

	public var signOut: @Sendable () async throws -> Void

	public var signUp: @Sendable (SignUpRequest) async throws -> OwnProfileResponse

	// Own profile + update

	public var ownProfile: @Sendable () async throws -> OwnProfileResponse

	public var ownProfileGallery: @Sendable () async throws -> [GalleryEntryResponse]

	public var deleteOwnProfile: @Sendable (DeleteOwnProfileRequest) async throws -> Void

	public var deletePhotoFromOwnGallery: @Sendable (OwnGalleryPhotoDeleteRequest) async throws -> Void

	public var addPhotoToOwnGallery: @Sendable (OwnGalleryPhotoAddRequest) async throws -> Void

	public var updateFCMToken: @Sendable (UpdateFCMTokenRequest) async throws -> Void

	public var updateOwnCoordinates: @Sendable (UpdateOwnCoordinatesRequest) async throws -> Void

	public var updateOwnHobbies: @Sendable (UpdateOwnHobbiesRequest) async throws -> Void

	public var updateOwnProfile: @Sendable (UpdateOwnProfileRequest) async throws -> OwnProfileResponse

	// Side profile

	public var sideProfileGallery: @Sendable (SideProfileGalleryRequest) async throws -> [GalleryEntryResponse]

	public var sideProfile: @Sendable (SideProfileRequest) async throws -> SideProfileResponse

	public var randomSideProfiles: @Sendable (RandomSideProfilesRequest) async throws -> [SideProfileResponse]

	// Hobby

	public var hobbyKinds: @Sendable () async throws -> [HobbyResponse]

	// Likes

	public var like: @Sendable (LikeRequest) async throws -> LikeResponse

	public var dislike: @Sendable (DislikeRequest) async throws -> DislikeResponse

	// Chats

	public var chatAvailability: @Sendable (ChatAvailabilityRequest) async throws -> ChatAvailabilityResponse

	public var chatHistory: @Sendable (ChatHistoryRequest) async throws -> ChatHistoryResponse

	public var chatList: @Sendable () async throws -> [ChatSummaryResponse]

}

// MARK: - APIClient+DependencyKey

extension APIClient: DependencyKey {

	public static let liveValue = APIClient(
		setAccessToken: { token in
			APIService.shared.updateToken(token)
		},
		signInWithGoogle: { requestBody in
			let endpoint: APIEndpoint = .signInWithGoogle(requestBody)
			return try await APIService.shared.request(endpoint, decode: SignInWithGoogleResponse.self)
		},
		signOut: {
			let endpoint: APIEndpoint = .signOut
			return try await APIService.shared.request(endpoint)
		},
		signUp: { requestBody in
			let endpoint: APIEndpoint = .signUp(requestBody)
			return try await APIService.shared.request(endpoint, decode: OwnProfileResponse.self)
		},
		ownProfile: {
			let endpoint: APIEndpoint = .ownProfile
			return try await APIService.shared.request(endpoint, decode: OwnProfileResponse.self)
		},
		ownProfileGallery: {
			let endpoint: APIEndpoint = .ownProfileGallery
			return try await APIService.shared.request(endpoint, decode: [GalleryEntryResponse].self)
		},
		deleteOwnProfile: { requestBody in
			let endpoint: APIEndpoint = .deleteOwnProfile(requestBody)
			try await APIService.shared.request(endpoint)
		},
		deletePhotoFromOwnGallery: { requestBody in
			let endpoint: APIEndpoint = .deletePhotoFromOwnGallery(requestBody)
			try await APIService.shared.request(endpoint)
		},
		addPhotoToOwnGallery: { requestBody in
			let endpoint: APIEndpoint = .addPhotoToOwnGallery(requestBody)
			try await APIService.shared.request(endpoint)
		},
		updateFCMToken: { requestBody in
			let endpoint: APIEndpoint = .updateFCMToken(requestBody)
			try await APIService.shared.request(endpoint)
		},
		updateOwnCoordinates: { requestBody in
			let endpoint: APIEndpoint = .updateOwnCoordinates(requestBody)
			try await APIService.shared.request(endpoint)
		},
		updateOwnHobbies: { requestBody in
			let endpoint: APIEndpoint = .updateOwnHobbies(requestBody)
			try await APIService.shared.request(endpoint)
		},
		updateOwnProfile: { requestBody in
			let endpoint: APIEndpoint = .updateOwnProfile(requestBody)
			return try await APIService.shared.request(endpoint, decode: OwnProfileResponse.self)
		},
		sideProfileGallery: { requestBody in
			let endpoint: APIEndpoint = .sideProfileGallery(requestBody)
			return try await APIService.shared.request(endpoint, decode: [GalleryEntryResponse].self)
		},
		sideProfile: { requestBody in
			let endpoint: APIEndpoint = .sideProfile(requestBody)
			return try await APIService.shared.request(endpoint, decode: SideProfileResponse.self)
		},
		randomSideProfiles: { requestBody in
			let endpoint: APIEndpoint = .randomSideProfiles(requestBody)
			return try await APIService.shared.request(endpoint, decode: [SideProfileResponse].self)
		},
		hobbyKinds: {
			let endpoint: APIEndpoint = .hobbyKinds
			return try await APIService.shared.request(endpoint, decode: [HobbyResponse].self)
		},
		like: { requestBody in
			let endpoint: APIEndpoint = .like(requestBody)
			return try await APIService.shared.request(endpoint, decode: LikeResponse.self)
		},
		dislike: { requestBody in
			let endpoint: APIEndpoint = .dislike(requestBody)
			return try await APIService.shared.request(endpoint, decode: DislikeResponse.self)
		},
		chatAvailability: { requestBody in
			let endpoint: APIEndpoint = .chatAvailability(requestBody)
			return try await APIService.shared.request(endpoint, decode: ChatAvailabilityResponse.self)
		},
		chatHistory: { requestBody in
			let endpoint: APIEndpoint = .chatHistory(requestBody)
			return try await APIService.shared.request(endpoint, decode: ChatHistoryResponse.self)
		},
		chatList: {
			let endpoint: APIEndpoint = .chatList
			return try await APIService.shared.request(endpoint, decode: [ChatSummaryResponse].self)
		}
	)

}

// MARK: - DependencyValues+APIClient

extension DependencyValues {

	public var apiClient: APIClient {
		get { self[APIClient.self] }
		set { self[APIClient.self] = newValue }
	}

}
