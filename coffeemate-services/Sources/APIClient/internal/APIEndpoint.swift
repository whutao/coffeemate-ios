//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Constants
import Extensions
import Foundation
import Moya
import SystemInformation

// MARK: - APIEndpoint

/// List of all backend endpoints.
enum APIEndpoint {

	// MARK: Exposed properties

	static let url: URL = Constants.Api.apiBaseUrl

	static var token: String? = nil

	// MARK: Private properties

	private static var sharedHeaders: [String: String?] {
		return [
			"platform": SystemInformation.platform,
			"access-token": token,
			"client_version": SystemInformation.appFlatVersion,
			"language": SystemInformation.languageCode
		]
	}

	// MARK: Cases

	// Auth

	/// GET */api/v1/auth/social_login*.
	case signInWithGoogle(SignInWithGoogleRequest)

	/// POST to */api/v1/auth/logout*.
	case signOut

	/// POST to */api/v1/auth/registration*.
	case signUp(SignUpRequest)

	// Own profile + update

	/// GET */api/v1/auth/self*.
	case ownProfile

	/// GET */api/v1/auth/gallery*.
	case ownProfileGallery

	/// DEL from */api/v1/auth/user*.
	case deleteOwnProfile(DeleteOwnProfileRequest)

	/// DEL from */api/v1/auth/gallery*.
	case deletePhotoFromOwnGallery(OwnGalleryPhotoDeleteRequest)

	/// POST to */api/v1/auth/gallery*.
	case addPhotoToOwnGallery(OwnGalleryPhotoAddRequest)

	/// POST to */api/v1/auth/updateFcmToken*.
	case updateFCMToken(UpdateFCMTokenRequest)

	/// POST to */api/v1/auth/updateLastLocation*.
	case updateOwnCoordinates(UpdateOwnCoordinatesRequest)

	/// POST to */api/v1/auth/hobbies*.
	case updateOwnHobbies(UpdateOwnHobbiesRequest)

	/// POST to */api/v1/auth/self*.
	case updateOwnProfile(UpdateOwnProfileRequest)

	// Side profile

	/// GET */api/v1/auth/gallery*.
	case sideProfileGallery(SideProfileGalleryRequest)

	/// GET */api/v1/auth/sideUserAccount*.
	case sideProfile(SideProfileRequest)

	/// GET */api/v1/auth/sideUserAccounts*.
	case randomSideProfiles(RandomSideProfilesRequest)

	// Hobby

	/// GET */api/v1/auth/hobbies*.
	case hobbyKinds

	// Likes

	/// POST */api/v1/likes/like*.
	case like(LikeRequest)

	/// POST */api/v1/likes/dislike*.
	case dislike(DislikeRequest)

	// Chats

	/// GET */api/v1/chats/chatAvailability*.
	case chatAvailability(ChatAvailabilityRequest)

	/// GET */api/v1/chats/messages*.
	case chatHistory(ChatHistoryRequest)

	/// GET */api/v1/chats/chats*.
	case chatList

}

// MARK: - TargetType

extension APIEndpoint: TargetType {

	var baseURL: URL {
		return APIEndpoint.url
	}

	var path: String {
		switch self {
		case .signInWithGoogle: return "/api/v1/auth/social_login"
		case .signOut: return "/api/v1/auth/logout"
		case .signUp: return "/api/v1/auth/registration"
		case .ownProfile: return "/api/v1/auth/self"
		case .ownProfileGallery: return "/api/v1/auth/gallery"
		case .deleteOwnProfile: return "/api/v1/auth/user"
		case .deletePhotoFromOwnGallery: return "/api/v1/auth/gallery"
		case .addPhotoToOwnGallery: return "/api/v1/auth/gallery"
		case .updateFCMToken: return "/api/v1/auth/updateFcmToken"
		case .updateOwnCoordinates: return "/api/v1/auth/updateLastLocation"
		case .updateOwnHobbies: return "/api/v1/auth/hobbies"
		case .updateOwnProfile: return "/api/v1/auth/self"
		case .sideProfileGallery: return "/api/v1/auth/gallery"
		case .sideProfile: return "/api/v1/auth/sideUserAccount"
		case .randomSideProfiles: return "/api/v1/auth/sideUserAccounts"
		case .hobbyKinds: return "/api/v1/auth/hobbies"
		case .like: return "/api/v1/likes/like"
		case .dislike: return "/api/v1/likes/dislike"
		case .chatAvailability: return "/api/v1/chats/chatAvailability"
		case .chatHistory: return "/api/v1/chats/messages"
		case .chatList: return "/api/v1/chats/chats"
		}
	}

	var method: Moya.Method {
		switch self {
		case .deleteOwnProfile,
			 .deletePhotoFromOwnGallery:
			return .delete
		case .addPhotoToOwnGallery,
			 .updateFCMToken,
			 .updateOwnCoordinates,
			 .updateOwnHobbies,
			 .updateOwnProfile,
			 .signInWithGoogle,
			 .signOut,
			 .signUp,
			 .like,
			 .dislike:
			return .post
		case .ownProfile,
			 .ownProfileGallery,
			 .sideProfileGallery,
			 .sideProfile,
			 .randomSideProfiles,
			 .hobbyKinds,
			 .chatAvailability,
			 .chatHistory,
			 .chatList:
			return .get
		}
	}

	var headers: [String: String]? {
		return APIEndpoint.sharedHeaders.compactMapValues({ $0 })
	}

	var task: Moya.Task {
		switch self {
		case .signOut,
			 .ownProfile,
			 .ownProfileGallery,
			 .hobbyKinds,
			 .chatList:
			return .requestPlain
		case let .signInWithGoogle(body):
			return .requestJSONEncodable(body)
		case let .signUp(body):
			return .uploadMultipart([
				.init(
					provider: .data(body.imageData),
					name: "image",
					fileName: "image",
					mimeType: "image/jpeg"
				),
				.init(
					provider: .data(body.payloadData),
					name: "request",
					fileName: "request.json",
					mimeType: "application/json"
				)
			])
		case let .deleteOwnProfile(body):
			return .requestJSONEncodable(body)
		case let .updateFCMToken(body):
			return .requestJSONEncodable(body)
		case let .updateOwnCoordinates(body):
			return .requestJSONEncodable(body)
		case let .updateOwnHobbies(payload):
			return .requestParameters(
				parameters: payload.parameterDictionary,
				encoding: URLEncoding(arrayEncoding: .noBrackets)
			)
		case let .updateOwnProfile(body):
			var form: [MultipartFormData] = []
			if let imageData = body.imageData {
				form.append(.init(
					provider: .data(imageData),
					name: "image",
					fileName: "image",
					mimeType: "image/jpeg"
				))
			}
			if let payloadData = body.payloadData {
				log.debug(String(data: payloadData, encoding: .utf8)!)
				form.append(.init(
					provider: .data(payloadData),
					name: "request",
					fileName: "request.json",
					mimeType: "application/json"
				))
			}
			return .uploadMultipart(form)
		case let .deletePhotoFromOwnGallery(payload):
			return .requestParameters(
				parameters: payload.parameterDictionary,
				encoding: URLEncoding.default
			)
		case let .sideProfileGallery(payload):
			return .requestParameters(
				parameters: payload.parameterDictionary,
				encoding: URLEncoding.default
			)
		case let .addPhotoToOwnGallery(ownGalleryPhotoAddRequest):
			let form = ownGalleryPhotoAddRequest.imagesData.enumerated().map { index, data in
				MultipartFormData(
					provider: .data(data),
					name: "images",
					fileName: "image_\(index)",
					mimeType: "image/jpeg"
				)
			}
			return .uploadMultipart(form)
		case let .sideProfile(payload):
			return .requestParameters(
				parameters: payload.parameterDictionary,
				encoding: URLEncoding.default
			)
		case let .randomSideProfiles(payload):
			return .requestParameters(
				parameters: payload.parameterDictionary,
				encoding: URLEncoding.default
			)
		case let .like(payload):
			return .requestParameters(
				parameters: payload.parameterDictionary,
				encoding: URLEncoding.default
			)
		case let .dislike(payload):
			return .requestParameters(
				parameters: payload.parameterDictionary,
				encoding: URLEncoding.default
			)
		case let .chatAvailability(payload):
			return .requestParameters(
				parameters: payload.parameterDictionary,
				encoding: URLEncoding.default
			)
		case let .chatHistory(payload):
			return .requestParameters(
				parameters: payload.parameterDictionary,
				encoding: URLEncoding.default
			)
		}
	}

}
