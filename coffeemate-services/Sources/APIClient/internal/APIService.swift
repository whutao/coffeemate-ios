//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import Moya

// MARK: - Service

/// Service for the communication with the backend.
final class APIService {

	// MARK: Exposed properties

	static let shared: APIService = APIService()

	// MARK: Private properties

	/// Queue to perform callbacks.
	private let callbackQueue: DispatchQueue

	/// Moya API.
	private let provider: MoyaProvider<APIEndpoint>

	// MARK: Init

	private init() {
		let loggerPlugin = NetworkLoggerPlugin(configuration: .init(
			output: { _, items in
				log.verbose(items.joined(separator: "\n"))
			},
			logOptions: [
				.requestMethod, .requestHeaders, .requestBody,
				.successResponseBody, .errorResponseBody
			]
		))
		self.provider = MoyaProvider<APIEndpoint>(plugins: [loggerPlugin])
		self.callbackQueue = DispatchQueue(
			label: "api-service.callback-queue",
			qos: .utility
		)
	}

	// MARK: Methods

	func updateToken(_ token: String?) {
		APIEndpoint.token = token
	}

	// MARK: Exposed methods

	func request<ResponseBody: Decodable>(
		_ endpoint: APIEndpoint,
		decode decodeBody: ResponseBody.Type
	) async throws -> ResponseBody {
		return try await withCheckedThrowingContinuation { continuation in
			provider.request(endpoint) { [self] response in
				do {
					let responseBody = try self.process(
						result: response,
						decodeBody: decodeBody.self,
						allowCodes: [200]
					)
					continuation.resume(returning: responseBody)
				} catch let error {
					continuation.resume(throwing: error)
				}
			}
		}
	}

	func request(_ endpoint: APIEndpoint) async throws {
		return try await withCheckedThrowingContinuation { continuation in
			provider.request(endpoint) { [self] response in
				do {
					try self.process(
						result: response,
						allowCodes: [200]
					)
					continuation.resume(returning: ())
				} catch let error {
					continuation.resume(throwing: error)
				}
			}
		}
	}

	// MARK: Private methods

	private func process<ResponseBody: Decodable>(
		result: Result<Response, MoyaError>,
		decodeBody: ResponseBody.Type,
		allowCodes allowedCodes: Set<Int>
	) throws -> ResponseBody {
		switch result {
		case let .success(response):
			if allowedCodes.contains(response.statusCode) {
				return try JSONDecoder().decode(decodeBody.self, from: response.data)
			} else {
				throw extractAPIError(from: response)
			}
		case let .failure(error):
			throw error
		}
	}

	private func process(
		result: Result<Response, MoyaError>,
		allowCodes allowedCodes: Set<Int>
	) throws {
		switch result {
		case let .success(response):
			if allowedCodes.contains(response.statusCode) {
				return
			} else {
				throw extractAPIError(from: response)
			}
		case let .failure(error):
			throw error
		}
	}

	private func extractAPIError(from moyaResponse: Response) -> APIError {
		let statusCode: Int = moyaResponse.statusCode
		let errorMessage: String? = {
			guard let jsonResponse = try? JSONSerialization.jsonObject(
				with: moyaResponse.data,
				options: []
			) else {
				return nil
			}
			guard let message = jsonResponse as? [String: Any] else {
				return nil
			}
			return message["message"] as? String
		}()
		return APIError(fromCode: statusCode, message: errorMessage)
	}

}
