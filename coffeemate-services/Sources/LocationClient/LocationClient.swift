//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Foundation

// MARK: - Client

public struct LocationClient: Sendable {

	// MARK: Exposed properties

	public var observer: @Sendable () -> AsyncStream<LocationEvent>

	public var configure: @Sendable () async -> Void

}

// MARK: - DependencyKey

extension LocationClient: DependencyKey {

	public static let liveValue = LocationClient(
		observer: {
			return LocationService.shared.eventStream
		},
		configure: {
			await LocationService.shared.configure()
		}
	)

}

// MARK: - DependencyValues+LocationClient

extension DependencyValues {

	public var locationClient: LocationClient {
		get { self[LocationClient.self] }
		set { self[LocationClient.self] = newValue }
	}

}
