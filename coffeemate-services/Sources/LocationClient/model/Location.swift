//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import CoreLocation
import Foundation

// MARK: - Model

public struct Location: Equatable, Sendable {

	// MARK: Exposed properties

	public let latitude: Double

	public let longitude: Double

	public var isValid: Bool {
		return CLLocationCoordinate2DIsValid(self.asCLLocationCoordinate2D)
	}

	public var asCLLocationCoordinate2D: CLLocationCoordinate2D {
		return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
	}

	public var asCLLocation: CLLocation {
		return CLLocation(latitude: latitude, longitude: longitude)
	}

	// MARK: Init

	public init<F: BinaryFloatingPoint>(latitude: F, longiture: F) {
		self.latitude = Double(latitude)
		self.longitude = Double(longiture)
	}

	public init(coordinate: CLLocationCoordinate2D) {
		self.init(
			latitude: coordinate.latitude,
			longiture: coordinate.longitude
		)
	}

	// MARK: Exposed methods

	public func distance(from other: Location) -> Double {
		return self.asCLLocation.distance(from: other.asCLLocation)
	}

}
