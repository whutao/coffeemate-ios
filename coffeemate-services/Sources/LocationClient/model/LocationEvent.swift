//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public enum LocationEvent: Sendable {

	// MARK: Case

	case userLocation(Location)

	case error

}
