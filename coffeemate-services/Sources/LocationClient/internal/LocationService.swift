//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import CoreLocation
import Foundation

// MARK: - Service

final class LocationService: NSObject {

	// MARK: Exposed properties

	static let shared = LocationService()

	private(set) lazy var eventStream = AsyncStream<LocationEvent> { continuation in
		self.eventStreamContinuation = continuation
	}

	// MARK: Private properties

	@Dependency(\.suspendingClock) private var clock

	private lazy var locationManager = CLLocationManager()

	private var eventStreamContinuation: AsyncStream<LocationEvent>.Continuation?

	private var userLocationPeriodicalRequestTask: Task<Void, Error>?

	// MARK: Init

	override init() {
		super.init()
	}

	// MARK: Exposed methods

	@MainActor func configure() {
		locationManager.delegate = self
		locationManager.requestWhenInUseAuthorization()
	}

	func oneTimeUserLocationRequest() {
		locationManager.requestLocation()
	}

}

// MARK: CLLocationManagerDelegate

extension LocationService: CLLocationManagerDelegate {

	func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
		log.info("Authorization status changed to \(manager.authorizationStatus).")
		switch manager.authorizationStatus {
		case .authorizedAlways, .authorizedWhenInUse:
			userLocationPeriodicalRequestTask = Task {
				for await _ in clock.timer(interval: .seconds(10)) {
					try Task.checkCancellation()
					locationManager.requestLocation()
				}
			}
			log.info("Did start periodical location requests.")
		case .notDetermined, .denied, .restricted:
			log.error("Cannot start periodical location requests as permission has not been granted.")
		@unknown default:
			log.severe("Cannot start periodical location requests as UNKNOWN authorization status found.")
		}
	}

	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		guard let recentLocation = (locations.first?.coordinate).flatMap(Location.init) else {
			return
		}
		eventStreamContinuation?.yield(.userLocation(recentLocation))
	}

	func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
		// CLError
		log.error("Location manager did fail with \(error).")
	}

}
