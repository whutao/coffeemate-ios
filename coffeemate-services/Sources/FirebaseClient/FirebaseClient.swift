//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import FirebaseCore

// MARK: - Helper

private final class BundleToken {
	static let bundle: Bundle = {
		#if SWIFT_PACKAGE
		return Bundle.module
		#else
		return Bundle(for: BundleToken.self)
		#endif
	}()
}

// MARK: - Client

public struct FirebaseClient {

	// MARK: Exposed properties

	public let configure: () -> Void

}

// MARK: - DependencyKey

extension FirebaseClient: DependencyKey {

	public static let liveValue = FirebaseClient(
		configure: {
			let googlePlistPath = BundleToken.bundle.path(
				forResource: "GoogleService-Info",
				ofType: "plist"
			)!
			let options = FirebaseOptions(
				contentsOfFile: googlePlistPath
			)!
			FirebaseApp.configure(options: options)
		}
	)

}

// MARK: - DependencyValues+FirebaseClient

extension DependencyValues {

	public var firebaseClient: FirebaseClient {
		get { self[FirebaseClient.self] }
		set { self[FirebaseClient.self] = newValue }
	}

}
