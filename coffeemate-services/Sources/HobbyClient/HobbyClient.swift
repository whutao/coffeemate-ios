//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import APIClient
import ComposableArchitecture
import Extensions
import Foundation
import Models

// MARK: - Client

public struct HobbyClient: Sendable {

	// MARK: Exposed properties

	public var allHobbyKinds: @Sendable () async throws -> [Hobby]

}

// MARK: - DependencyKey

extension HobbyClient: DependencyKey {

	public static let liveValue = HobbyClient(
		allHobbyKinds: {
			@Dependency(\.apiClient) var apiClient
			let hobbyKindsResponse = try await apiClient.hobbyKinds()
			return hobbyKindsResponse.map(Hobby.init).sorted(by: \.title)
		}
	)

}

// MARK: - DependencyValues+HobbyClient

extension DependencyValues {

	public var hobbyClient: HobbyClient {
		get { self[HobbyClient.self] }
		set { self[HobbyClient.self] = newValue }
	}

}
