//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import APIClient
import Foundation
import Models

extension Hobby {

	init(response: HobbyResponse) {
		self.init(
			hobbyId: response.hobbyId,
			title: response.title,
			imageUrl: response.imageUrl,
			isPrivileged: response.isPrivileged
		)
	}

}
