//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import APIClient
import ComposableArchitecture
import Extensions
import Foundation
import IdentifiedCollections
import Models

// MARK: - Client

public struct ContactsClient: Sendable {

	// MARK: Exposed properties

	public var sendLike: @Sendable (LikeRequest) async throws -> LikeResponse

	public var sendDislike: @Sendable (DislikeRequest) async throws -> DislikeResponse

	public var sideProfile: @Sendable (SideProfileRequest) async throws -> SideProfile

	public var sideProfileWithGallery: @Sendable (SideProfileRequest) async throws -> SideProfile

	public var randomSideProfiles: @Sendable (RandomSideProfilesRequest) async throws -> [SideProfile]

}

// MARK: - DependencyKey

extension ContactsClient: DependencyKey {

	public static var liveValue: ContactsClient {
		return Self(
			sendLike: { requestBody in
				@Dependency(\.apiClient) var apiClient
				return try await apiClient.like(requestBody)
			},
			sendDislike: { requestBody in
				@Dependency(\.apiClient) var apiClient
				return try await apiClient.dislike(requestBody)
			},
			sideProfile: { requestBody in
				@Dependency(\.apiClient) var apiClient
				let sideProfileResponse = try await apiClient.sideProfile(requestBody)
				return SideProfile(response: sideProfileResponse)
			},
			sideProfileWithGallery: { requestBody in
				@Dependency(\.apiClient) var apiClient
				async let sideProfileResponse = try apiClient.sideProfile(requestBody)
				async let sideProfielGalleryResponse = try apiClient.sideProfileGallery(.init(
					sideProfileHash: requestBody.profileHash
				))
				var sideProfile = try await SideProfile(response: sideProfileResponse)
				sideProfile.gallery = try await IdentifiedArrayOf(uniqueElements: sideProfielGalleryResponse.map(GalleryEntry.init))
				return sideProfile
			},
			randomSideProfiles: { requestBody in
				@Dependency(\.apiClient) var apiClient
				let randomProfilesResponse = try await apiClient.randomSideProfiles(requestBody)
				return randomProfilesResponse.map(SideProfile.init)
			}
		)
	}

}

#if DEBUG
import Mocking

extension ContactsClient {

	public static var previewValue: ContactsClient {
		return Self(
			sendLike: { _ in
				return LikeResponse(targetProfileHash: "aaa", isLikedBack: false)
			},
			sendDislike: { _ in
				return DislikeResponse(targetProfileHash: "ddd", isLiked: false)
			},
			sideProfile: { _ in
				return .mockedDenis()
			},
			sideProfileWithGallery: { _ in
				return .mockedDenis()
			},
			randomSideProfiles: { _ in
				return [.randomMocked(), .randomMocked(), .randomMocked(), .randomMocked()]
			}
		)
	}

}
#endif

// MARK: - DependencyValues+ContactsClient

extension DependencyValues {

	public var contactsClient: ContactsClient {
		get { self[ContactsClient.self] }
		set { self[ContactsClient.self] = newValue }
	}

}
