//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import APIClient
import Foundation
import Models

extension SideProfile {

	init(response: SideProfileResponse) {
		self.init(
			profileHash: response.profileHash,
			avatarUrl: response.avatarUrl,
			nickname: response.nickname,
			age: response.age,
			about: response.about,
			rating: response.rating,
			ownGender: response.ownGender,
			targetGenders: response.targetGenders,
			hobbies: response.hobbies.map(Hobby.init),
			gallery: response.gallery.map(GalleryEntry.init),
			isVerified: response.isVerified
		)
	}

}
