//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import APIClient
import Combine
import ComposableArchitecture
import Foundation
import Mocking
import Models
import UserClient

// MARK: - Helper

private var chatWebSocket: ChatWebSocket?

// MARK: - Client

public struct ChatClient: Sendable {

	// MARK: Exposed properties

	public var chatList: @Sendable () async throws -> [ChatSummary]

	public var chatHistory: @Sendable (ChatHistoryRequest) async throws -> [ChatMessage]

	public var chatAvailability: @Sendable (ChatAvailabilityRequest) async throws -> ChatAvailabilityResponse

	public var chatRoomConnect: @Sendable (ChatRoomConnectRequest) throws -> AnyPublisher<ChatEvent, Never>

	public var chatRoomDisconnectFromAll: @Sendable () -> Void

	public var chatRoomSendTextMessage: @Sendable (ChatRoomSendTextMessageRequest) throws -> Void

	public var chatRoomSendMessageRead: @Sendable (ChatRoomSendMessageReadRequest) throws -> Void

	// MARK: Error

	public enum Error: Swift.Error, Equatable {
		case chatRoomIsNotConnected
		case chatRoomIsAlreadyConnected
		case noAccessTokenFound
	}

}

// MARK: - DependencyKey

extension ChatClient: DependencyKey {

	public static let liveValue: ChatClient = {
		let getAccessToken: () throws -> String = {
			@Dependency(\.userClient) var userClient
			guard let accessToken = userClient.accessToken()?.value else {
				throw Error.noAccessTokenFound
			}
			return accessToken
		}
		let getChatWebSocketForRoom: (ChatID) throws -> ChatWebSocket = { chatId in
			let accessToken = try getAccessToken()
			if let existingWebSocket = chatWebSocket {
				/// If socket for this chat already exists, otherwise disconnect.
				if
					existingWebSocket.chatId == chatId,
					existingWebSocket.accessToken == accessToken
				{
					return existingWebSocket
				} else {
					do {
						try existingWebSocket.disconnect()
					} catch {
						existingWebSocket.forceDisconnect()
					}
				}
			}
			let newWebSocket = ChatWebSocket(
				accessToken: accessToken,
				chatId: chatId
			)
			chatWebSocket = newWebSocket
			return newWebSocket
		}
		return ChatClient(
			chatList: {
				@Dependency(\.apiClient) var apiClient
				let chatSummaryResponses = try await apiClient.chatList()
				return chatSummaryResponses.map(ChatSummary.init)
			},
			chatHistory: { requestBody in
				@Dependency(\.apiClient) var apiClient
				let chatHistoryResponses = try await apiClient.chatHistory(requestBody)
				return chatHistoryResponses.messages.map(ChatMessage.init)
			},
			chatAvailability: { requestBody in
				@Dependency(\.apiClient) var apiClient
				let chatAvailabilityResponse = try await apiClient.chatAvailability(requestBody)
				return chatAvailabilityResponse
			},
			chatRoomConnect: { request in
				let chatWebSocket = try getChatWebSocketForRoom(request.chatId)
				do {
					try chatWebSocket.connect()
				} catch STOMPSocket.Error.alreadyConnectedViaSTOMP {
					// Do nothing.
				} catch let error {
					throw error
				}
				return chatWebSocket.eventPublisher.eraseToAnyPublisher()
			},
			chatRoomDisconnectFromAll: {
				guard let existingWebSocket = chatWebSocket else {
					return
				}
				do {
					try existingWebSocket.disconnect()
				} catch let error {
					existingWebSocket.forceDisconnect()
				}
				chatWebSocket = nil
			},
			chatRoomSendTextMessage: { request in
				let chatWebSocket = try getChatWebSocketForRoom(request.chatId)
				try chatWebSocket.sendTextMessage(text: request.text)
			},
			chatRoomSendMessageRead: { request in
				let chatWebSocket = try getChatWebSocketForRoom(request.chatId)
				try chatWebSocket.sendMessageRead(messageId: request.messageId)
			}
		)
	}()

	#if DEBUG
	public static var previewValue: ChatClient {
		return ChatClient(
			chatList: {
				return [
					.mockedWithLastMessage(),
					.mockedWithoutLastMessage()
				]
			},
			chatHistory: { _ in
				return [
					.mockedFromCurrentSender(),
					.mockedFromSideSender(),
					.mockedFromSideSender(),
					.mockedFromCurrentSender(),
					.mockedFromCurrentSender(),
					.mockedFromSideSender()
				].sorted(by: \.date).reversed()
			},
			chatAvailability: { _ in
				return .init(chatId: 1, companionProfileHash: "658798090")
			},
			chatRoomConnect: { _ in
				return Just(ChatEvent.didConnect).eraseToAnyPublisher()
			},
			chatRoomDisconnectFromAll: { },
			chatRoomSendTextMessage: { _ in },
			chatRoomSendMessageRead: { _ in }
		)
	}
	#endif

}

// MARK: - DependencyValues+ChatClient

extension DependencyValues {

	public var chatClient: ChatClient {
		get { self[ChatClient.self] }
		set { self[ChatClient.self] = newValue }
	}

}
