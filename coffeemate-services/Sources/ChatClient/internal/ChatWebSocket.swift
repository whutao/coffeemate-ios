//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Combine
import Constants
import Extensions
import Foundation
import Models

// MARK: - Service

final class ChatWebSocket {

	// MARK: Exposed properties

	let eventPublisher = PassthroughSubject<ChatEvent, Never>()

	let chatId: Int

	let accessToken: String

	// MARK: Private properties

	private let stompSocket: STOMPSocket

	// MARK: Init

	init(
		accessToken: String,
		chatId: Int,
		connectionUrl: URL = Constants.WebSocket.connectionUrl,
		connectionTimeout: TimeInterval = Constants.WebSocket.connectionTimeout
	) {
		self.accessToken = accessToken
		self.chatId = chatId
		self.stompSocket = STOMPSocket(
			connectionUrl: connectionUrl,
			connectionHeaders: ["access-token": accessToken],
			connectionTimeout: connectionTimeout,
			receivedMessageTypes: [
				IncomingTextMessageBatch.self,
				IncomingMessageRead.self
			]
		)
	}

	func connect() throws {
		stompSocket.eventHandler = { [weak self] _, event in
			guard let self else {
				return
			}
			switch event {
			case .isConnecting:
				self.eventPublisher.send(.isConnecting)
			case .didConnect:
				do {
					try joinChatRoom()
					try subscribeToMessageQueue()
					self.eventPublisher.send(.didConnect)
				} catch let error {
					log.error("Received \(error).")
				}
			case .didDisconnect:
				self.eventPublisher.send(.didDisconnect)
			case let .didReceiveError(description: description):
				log.error(description)
			case let .didReceivePayload(payload, from: destination):
				if Destination(fromString: destination).isNotNil {
					self.processReceivedPayload(payload)
				} else {
					log.error("Received message from unexpected destination \(destination).")
				}
			}
		}
		try stompSocket.connect()
	}

	func forceDisconnect() {
		stompSocket.disconnect(force: true)
	}

	func disconnect() throws {
		stompSocket.disconnect(force: false)
	}

	func sendMessageRead(messageId: ChatMessageID) throws {
		guard let messageId = Int(messageId) else {
			log.severe("Cannot send message read. Message ID \(messageId) is not convertible to Int.")
			return
		}
		let payload = OutgoingMessageRead(
			accessToken: accessToken,
			chatId: chatId,
			messageId: messageId
		)
		try stompSocket.send(payload: payload, to: Destination.room(chatId).asString)
	}

	func sendTextMessage(text: String) throws {
		let payload = OutgoingTextMessage(
			accessToken: accessToken,
			chatId: chatId,
			text: text
		)
		try stompSocket.send(payload: payload, to: Destination.room(chatId).asString)
	}

	// MARK: Private methods

	private func joinChatRoom() throws {
		try stompSocket.send(
			payload: OutgoingServiceMessage.userJoin(
				accessToken: accessToken,
				chatId: chatId
			),
			to: Destination.connect.asString
		)
	}

	private func subscribeToMessageQueue() throws {
		try stompSocket.subscribe(to: Destination.messageQueue.asString)
	}

	private func processReceivedPayload(_ payload: Decodable) {
		if let messageRead = payload as? IncomingMessageRead, messageRead.chatId == chatId {
			eventPublisher.send(.didReceiveChatMessageRead(.init(
				chatId: messageRead.chatId,
				messageId: "\(messageRead.messageId)"
			)))
		} else if let textMessageBatch = payload as? IncomingTextMessageBatch {
			let companion = ChatCompanion(
				profileHash: textMessageBatch.companion.companionProfileHash,
				nickname: textMessageBatch.companion.companionNickname,
				avatarUrl: textMessageBatch.companion.companionImageUrl
			)
			let messages = textMessageBatch.messages.map { incomingMessage in
				ChatMessage(
					messageId: "\(incomingMessage.messageId)",
					date: incomingMessage.date,
					content: .text(incomingMessage.text),
					sender: .init(
						senderId: incomingMessage.senderProfileHash,
						nickname: incomingMessage.senderNickname
					),
					status: incomingMessage.isRead ? .read : .sent
				)
			}
			eventPublisher.send(.didReceiveChatTextMessageBatch(companion, messages))
		} else {
			log.error("Unexpected payload received \(payload).")
		}
	}

}
