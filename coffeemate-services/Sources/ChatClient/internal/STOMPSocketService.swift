//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Constants
import Foundation

// MARK: - Service

final class STOMPSocketService {

	// MARK: Exposed properties

	private(set) lazy var eventStream = AsyncStream<STOMPSocket.Event> { continuation in
		self.eventContinuation = continuation
	}

	// MARK: Private properties

	private let stompSocket: STOMPSocket

	private var eventContinuation: AsyncStream<STOMPSocket.Event>.Continuation?

	// MARK: Init

	init(accessToken: String, receivedMessageTypes: [Decodable.Type]) {
		self.stompSocket = STOMPSocket(
			connectionUrl: Constants.WebSocket.connectionUrl,
			connectionHeaders: ["access-token": accessToken],
			connectionTimeout: 15,
			receivedMessageTypes: receivedMessageTypes
		)
		self.stompSocket.eventHandler = { [weak self] _, event in
			self?.eventContinuation?.yield(event)
		}
	}

	// MARK: Exposed methods

	// MARK: Private properties

}
