//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import APIClient
import Foundation
import Models

extension ChatMessage {

	init(response: ChatHistoryResponse.MessageResponse) {
		self.init(
			messageId: "\(response.messageId)",
			date: response.date,
			content: .text(response.text),
			sender: .init(
				senderId: response.senderProfileHash,
				nickname: response.senderNickname
			),
			status: response.isRead ? .read : .sent
		)
	}

}
