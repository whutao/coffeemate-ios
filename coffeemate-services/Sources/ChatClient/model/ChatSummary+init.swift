//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import APIClient
import Foundation
import Models

extension ChatSummary {

	init(response: ChatSummaryResponse) {
		let lastMessage: ChatSummary.LastMessage? = {
			if let text = response.lastMessagePayload, let date = response.lastMessageDateTime {
				return ChatSummary.LastMessage(
					date: date,
					content: .text(text),
					isIncoming: response.isRead,
					isRead: response.isIncoming
				)
			} else {
				return nil
			}
		}()
		self.init(
			chatId: response.chatId,
			companion: ChatCompanion(
				profileHash: response.companionPorfileHash,
				nickname: response.companionNickname,
				avatarUrl: response.companionImageUrl
			),
			lastMessage: lastMessage,
			isHidden: response.isHidden
		)
	}

}
