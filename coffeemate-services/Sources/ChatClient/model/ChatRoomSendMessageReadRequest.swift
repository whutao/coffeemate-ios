//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import Models

// MARK: - Model

public struct ChatRoomSendMessageReadRequest: Equatable, Sendable {

	// MARK: Exposed properties

	public let chatId: ChatID

	public let messageId: ChatMessageID

	// MARK: Init

	public init(chatId: ChatID, messageId: ChatMessageID) {
		self.chatId = chatId
		self.messageId = messageId
	}

}
