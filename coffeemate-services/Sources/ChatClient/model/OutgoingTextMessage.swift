//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

struct OutgoingTextMessage: Equatable, Encodable, Sendable {

	// MARK: Exposed properties

	let accessToken: String

	let chatId: Int

	let text: String

	let messageType: MessageType

	// MARK: Init

	init(
		accessToken: String,
		chatId: Int,
		text: String
	) {
		self.accessToken = accessToken
		self.chatId = chatId
		self.text = text
		self.messageType = .messageText
	}

	// MARK: Coding keys

	private enum CodingKeys: String, CodingKey {
		case accessToken = "token"
		case chatId = "chatId"
		case text = "message"
		case messageType = "messageType"
	}

}
