//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import Models

// MARK: - Model

public struct ChatRoomConnectRequest: Equatable, Sendable {

	// MARK: Exposed properties

	public let chatId: ChatID

	// MARK: Init

	public init(chatId: ChatID) {
		self.chatId = chatId
	}

}
