//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import Models

// MARK: - Model

public enum ChatEvent: Equatable, Sendable {

	// MARK: Case

	/// Chat web-socket is trying to establish a connection.
	case isConnecting

	/// Chat web-socket is completely connected and ready to send/receive messages.
	case didConnect

	/// Chat web-socket is completely disconnected.
	case didDisconnect

	/// Payload recevied from web-socket.
	case didReceiveChatTextMessageBatch(ChatCompanion, [ChatMessage])

	/// Payload recevied from web-socket.
	case didReceiveChatMessageRead(ChatMessageRead)

}
