//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

enum MessageType: String, Codable, Equatable, Sendable {

	// MARK: Case

	case messageText = "Text"

	case messageRead = "MessageRead"

	case startTyping = "Typing"

	case stopTyping = "StopTyping"

	case userJoin = "UserJoin"

	case explicitPing = "Ping"

}
