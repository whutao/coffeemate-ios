//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

struct OutgoingServiceMessage: Equatable, Encodable, Sendable {

	// MARK: Exposed properties

	let accessToken: String

	let chatId: Int

	let messageType: MessageType

	// MARK: Init

	private init(accessToken: String, chatId: Int, messageType: MessageType) {
		self.accessToken = accessToken
		self.chatId = chatId
		self.messageType = .messageRead
	}

	// MARK: Exposed properties

	static func userJoin(accessToken: String, chatId: Int) -> OutgoingServiceMessage {
		return OutgoingServiceMessage(
			accessToken: accessToken,
			chatId: chatId,
			messageType: .userJoin
		)
	}

	static func startTyping(accessToken: String, chatId: Int) -> OutgoingServiceMessage {
		return OutgoingServiceMessage(
			accessToken: accessToken,
			chatId: chatId,
			messageType: .startTyping
		)
	}

	static func stopTyping(accessToken: String, chatId: Int) -> OutgoingServiceMessage {
		return OutgoingServiceMessage(
			accessToken: accessToken,
			chatId: chatId,
			messageType: .stopTyping
		)
	}

	static func explicitPing(accessToken: String, chatId: Int) -> OutgoingServiceMessage {
		return OutgoingServiceMessage(
			accessToken: accessToken,
			chatId: chatId,
			messageType: .explicitPing
		)
	}

	// MARK: Coding keys

	private enum CodingKeys: String, CodingKey {
		case accessToken = "token"
		case chatId = "chatId"
		case messageType = "messageType"
	}

}
