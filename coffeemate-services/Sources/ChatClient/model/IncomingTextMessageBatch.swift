//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

struct IncomingTextMessageBatch: Decodable, Equatable, Sendable {

	// MARK: Exposed properties

	let type: MessageType

	let companion: IncomingCompanion

	let messages: [IncomingTextMessage]

	// MARK: Coding keys

	private enum CodingKeys: String, CodingKey {
		case type = "messageType"
		case companion = "visavisInfo"
		case messages = "messages"
	}

}
