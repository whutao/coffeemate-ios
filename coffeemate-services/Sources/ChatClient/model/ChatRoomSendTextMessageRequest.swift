//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import Models

// MARK: - Model

public struct ChatRoomSendTextMessageRequest: Equatable, Sendable {

	// MARK: Expsoed properties

	public let chatId: ChatID

	public let text: String

	// MARK: Init

	public init(chatId: ChatID, text: String) {
		self.chatId = chatId
		self.text = text
	}

}
