//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

struct OutgoingMessageRead: Equatable, Encodable, Sendable {

	// MARK: Exposed properties

	let accessToken: String

	let chatId: Int

	let messageId: Int

	let messageType: MessageType

	// MARK: Init

	init(
		accessToken: String,
		chatId: Int,
		messageId: Int
	) {
		self.accessToken = accessToken
		self.chatId = chatId
		self.messageId = messageId
		self.messageType = .messageRead
	}

	// MARK: Coding keys

	private enum CodingKeys: String, CodingKey {
		case accessToken = "token"
		case chatId = "chatId"
		case messageId = "messageId"
		case messageType = "messageType"
	}

}
