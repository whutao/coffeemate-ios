//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

extension ChatWebSocket {

	enum Destination: Equatable, Sendable {

		// MARK: Case

		case connect

		case messageQueue

		case room(Int)

		// MARK: Exposed properties

		var asString: String {
			switch self {
			case .connect:
				return "/app/chat/connect"
			case .messageQueue:
				return "/user/queue/messages"
			case let .room(roomId):
				return "/app/chat/room/\(roomId)"
			}
		}

		// MARK: Init

		init?(fromString string: String) {
			if string == "/app/chat/connect" {
				self = .connect
			} else if string == "/user/queue/messages" {
				self = .messageQueue
			} else if
				string.hasPrefix("/app/chat/room/"),
				let lastComponent = string.split(separator: "/").last,
				let roomId = Int(lastComponent)
			{
				self = .room(roomId)
			} else {
				return nil
			}
		}

	}

}
