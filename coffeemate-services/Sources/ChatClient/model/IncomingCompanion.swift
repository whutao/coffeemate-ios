//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

struct IncomingCompanion: Decodable, Equatable, Sendable {

	// MARK: Exposed properties

	let companionProfileHash: String

	let companionNickname: String

	let companionImageUrl: URL

	// MARK: Coding keys

	private enum CodingKeys: String, CodingKey {
		case companionProfileHash = "visavisProfileHash"
		case companionNickname = "visavisNick"
		case companionImageUrl = "visavisImageURL"
	}

}
