//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import APIClient
import CodableWrappers
import Foundation

// MARK: - Model

struct IncomingTextMessage: Equatable, Decodable, Sendable {

	// MARK: Exposed properties

	let messageId: Int

	let type: MessageType

	let text: String

	@APIDateCoding
	var date: Date

	let senderProfileHash: String

	let senderNickname: String

	let senderImageUrl: URL

	let isIncoming: Bool

	let isRead: Bool

	// MARK: Coding keys

	private enum CodingKeys: String, CodingKey {
		case messageId = "id"
		case type = "messageType"
		case text = "payload"
		case date = "dateTime"
		case senderProfileHash = "senderProfileHash"
		case senderNickname = "senderNick"
		case senderImageUrl = "senderImageURL"
		case isIncoming = "incoming"
		case isRead = "read"
	}

}
