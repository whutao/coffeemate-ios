//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

struct IncomingMessageRead: Equatable, Decodable, Sendable {

	// MARK: Exposed properties

	let chatId: Int

	let messageId: Int

	let messageType: MessageType

	// MARK: Init

	init(
		chatId: Int,
		messageId: Int
	) {
		self.chatId = chatId
		self.messageId = messageId
		self.messageType = .messageRead
	}

	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		let messageType = try container.decode(MessageType.self, forKey: .messageType)
		guard messageType == .messageRead else {
			throw DecodingError.dataCorrupted(.init(
				codingPath: decoder.codingPath,
				debugDescription: "Message is not \"MessageRead\" kind but \(messageType)."
			))
		}
		self.messageType = messageType
		self.chatId = try container.decode(Int.self, forKey: .chatId)
		self.messageId = try container.decode(Int.self, forKey: .messageId)
	}

	// MARK: Coding keys

	private enum CodingKeys: String, CodingKey {
		case chatId = "chatId"
		case messageId = "messageId"
		case messageType = "messageType"
	}

}
