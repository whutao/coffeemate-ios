//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import GoogleSignIn

// MARK: - Model

public struct GoogleUser {

	// MARK: Exposed properties

	/// Google client ID.
	public let idToken: String

	public let email: String

	public let avatarUrl: URL?

	// MARK: Init

	public init(
		idToken: String,
		email: String,
		avatarUrl: URL?
	) {
		self.idToken = idToken
		self.email = email
		self.avatarUrl = avatarUrl
	}

	/// Creates an instance from the Google raw model.
	internal init?(from rawModel: GIDGoogleUser) {
		guard
			let profile = rawModel.profile,
			let idToken = rawModel.idToken?.tokenString
		else {
			return nil
		}
		self.idToken = idToken
		self.avatarUrl = profile.imageURL(withDimension: 256)
		self.email = profile.email
	}

}
