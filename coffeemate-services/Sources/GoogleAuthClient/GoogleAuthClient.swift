//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Constants
import Extensions
import GoogleSignIn
import UIKit

// MARK: - Client

public struct GoogleAuthClient: @unchecked Sendable {

	// MARK: Exposed properties

	/// Extracts currently signed-in user from keychain.
	public let currentUser: @Sendable () -> GoogleUser?

	public let handleUrl: @Sendable (URL) -> Bool

	/// Checks whether there is a signed-in user without presenting the UI again.
	public let hasPreviousSignIn: @Sendable () -> Bool

	public let signIn: @MainActor @Sendable () async throws -> GoogleUser

	public let signOut: @Sendable () -> Void

	/// It attempts to silently sign in the user using the stored credentials from a previous sign-in session.
	///
	/// If the credentials are still valid (e.g., the user did not revoke access), the user is signed
	/// in without the need for any user interaction. If the stored credentials are no
	/// longer valid (e.g., the user revoked access or the token has expired), the
	/// method throws an error.
	public let restorePreviousSignIn: @Sendable () async throws -> GoogleUser

	// MARK: Error

	public enum Error: Swift.Error {

		case cannotDecodeUserModel

		case noCliendIdFound

		case noPresentingViewControllerFound

		case noPreviousSignInFound

		case unknownGoogleResponse

	}

}

// MARK: - Dependency+Key

extension GoogleAuthClient: DependencyKey {

	public static let liveValue: GoogleAuthClient = {
		GIDSignIn.sharedInstance.configuration = GIDConfiguration(
			clientID: Constants.Google.clientId
		)
		return GoogleAuthClient(
			currentUser: {
				return GIDSignIn.sharedInstance.currentUser.flatMap(GoogleUser.init(from:))
			},
			handleUrl: { url in
				return GIDSignIn.sharedInstance.handle(url)
			},
			hasPreviousSignIn: {
				return GIDSignIn.sharedInstance.hasPreviousSignIn()
			},
			signIn: {
				guard let viewController = UIApplication.shared.topViewController else {
					throw Error.noPresentingViewControllerFound
				}
				return try await withCheckedThrowingContinuation { continuation in
					GIDSignIn.sharedInstance.signIn(withPresenting: viewController) { result, error in
						if let result {
							if let googleUser = GoogleUser.init(from: result.user) {
								continuation.resume(returning: googleUser)
							} else {
								continuation.resume(throwing: Error.cannotDecodeUserModel)
							}
						} else if let error {
							continuation.resume(throwing: error)
						} else {
							continuation.resume(throwing: Error.unknownGoogleResponse)
						}
					}
				}
			},
			signOut: {
				GIDSignIn.sharedInstance.signOut()
			},
			restorePreviousSignIn: {
				return try await withCheckedThrowingContinuation { continuation in
					GIDSignIn.sharedInstance.restorePreviousSignIn { user, error in
						if let user {
							if let googleUser = GoogleUser.init(from: user) {
								continuation.resume(returning: googleUser)
							} else {
								continuation.resume(throwing: Error.cannotDecodeUserModel)
							}
						} else if let error {
							continuation.resume(throwing: error)
						} else {
							continuation.resume(throwing: Error.noPreviousSignInFound)
						}
					}
				}
			}
		)
	}()

	public static var testValue: GoogleAuthClient {
		@Sendable func simulateResponseDelay() async throws {
			@Dependency(\.continuousClock) var clock
			try await clock.sleep(for: .milliseconds(300))
		}
		@Sendable func withResponseDelay<Response>(
			_ response: () -> Response
		) async throws -> Response {
			try await simulateResponseDelay()
			return response()
		}
		let makeGoogleUser: @Sendable () -> GoogleUser = {
			return GoogleUser(
				idToken: "0987654321",
				email: "example@mail.com",
				avatarUrl: nil
			)
		}
		return GoogleAuthClient(
			currentUser: {
				return makeGoogleUser()
			},
			handleUrl: { _ in
				return true
			},
			hasPreviousSignIn: {
				return true
			},
			signIn: {
				try await simulateResponseDelay()
				return makeGoogleUser()
			},
			signOut: {

			},
			restorePreviousSignIn: {
				try await simulateResponseDelay()
				return makeGoogleUser()
			}
		)
	}

}

// MARK: - DependencyValues+GoogleAuthClient

extension DependencyValues {

	public var googleAuthClient: GoogleAuthClient {
		get { self[GoogleAuthClient.self] }
		set { self[GoogleAuthClient.self] = newValue }
	}

}
