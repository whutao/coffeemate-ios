// swift-tools-version: 5.8
import PackageDescription

// MARK: - Helper

private enum _PackageTarget: String {

	// Local package dependency targets

	case constants = "Constants"
	case extensions = "Extensions"
	case log = "Log"
	case mocking = "Mocking"
	case models = "Models"
	case systemInformation = "SystemInformation"

	// 3rd party dependency targets

	case codableWrappers = "CodableWrappers"
	case composableArchitecture = "ComposableArchitecture"
	case firebaseMessaging = "FirebaseMessaging"
	case googleSignIn = "GoogleSignIn"
	case identifiedCollections = "IdentifiedCollections"
	case moya = "Moya"
	case reachability = "Reachability"
	case starscream = "Starscream"

	// Targets

	case apiClient = "APIClient"
	case chatClient = "ChatClient"
	case contactsClient = "ContactsClient"
	case countdownTimer = "CountdownTimer"
	case firebaseClient = "FirebaseClient"
	case googleAuthClient = "GoogleAuthClient"
	case hobbyClient = "HobbyClient"
	case lightStorageClient = "LightStorageClient"
	case locationClient = "LocationClient"
	case networkClient = "NetworkClient"
	case notificationCenterClient = "NotificationCenterClient"
	case toastClient = "ToastClient"
	case userClient = "UserClient"
	case userNotificationsClient = "UserNotificationsClient"

	// Exposed properties

	fileprivate var asTargetDependency: Target.Dependency {
		switch self {
		// math-game-common
		case .constants,
			 .extensions,
			 .log,
			 .mocking,
			 .models,
			 .systemInformation:
			return .product(name: rawValue, package: "coffeemate-common")
		// 3rd party
		case .codableWrappers:
			return .product(name: "CodableWrappers", package: "CodableWrappers")
		case .composableArchitecture:
			return .product(name: "ComposableArchitecture", package: "swift-composable-architecture")
		case .firebaseMessaging:
			return .product(name: "FirebaseMessaging", package: "firebase-ios-sdk")
		case .googleSignIn:
			return .product(name: "GoogleSignIn", package: "GoogleSignIn-iOS")
		case .identifiedCollections:
			return .product(
				name: "IdentifiedCollections",
				package: "swift-identified-collections"
			)
		case .moya:
			return .product(name: "Moya", package: "Moya")
		case .starscream:
			return .product(name: "Starscream", package: "Starscream")
		case .reachability:
			return .product(name: "Reachability", package: "Reachability.swift")
		default:
			return .target(name: rawValue)
		}
	}

	var asLibrary: Product {
		return .library(name: name, targets: [name])
	}

	var name: String {
		return rawValue
	}

}

fileprivate extension Target {

	static func target(
		_ target: _PackageTarget,
		dependencies: [_PackageTarget] = [],
		resources: [Resource]? = nil
	) -> Target {
		return .target(
			name: target.name,
			dependencies: dependencies.map(\.asTargetDependency),
			resources: resources
		)
	}

	static func testTarget(
		_ target: _PackageTarget,
		dependencies: [_PackageTarget] = []
	) -> Target {
		return .testTarget(
			name: target.name,
			dependencies: dependencies.map(\.asTargetDependency)
		)
	}
}

fileprivate extension Product {

	static func library(from target: _PackageTarget) -> Product {
		return target.asLibrary
	}

}

// MARK: Package

var package = Package(
	name: "coffeemate-services",
	platforms: [.iOS(.v16)],
	products: [
		.library(from: .apiClient),
		.library(from: .chatClient),
		.library(from: .contactsClient),
		.library(from: .countdownTimer),
		.library(from: .firebaseClient),
		.library(from: .googleAuthClient),
		.library(from: .hobbyClient),
		.library(from: .lightStorageClient),
		.library(from: .locationClient),
		.library(from: .networkClient),
		.library(from: .notificationCenterClient),
		.library(from: .toastClient),
		.library(from: .userClient),
		.library(from: .userNotificationsClient)
	],
	dependencies: [
		.package(name: "coffeemate-common", path: "coffeemate-common"),
		.package(
			url: "https://github.com/GottaGetSwifty/CodableWrappers.git",
			from: Version(2, 0, 7)
		),
		.package(
			url: "https://github.com/firebase/firebase-ios-sdk.git",
			from: Version(10, 13, 0)
		),
		.package(
			url: "https://github.com/pointfreeco/swift-identified-collections.git",
			from: Version(1, 0, 0)
		),
		.package(
			url: "https://github.com/google/GoogleSignIn-iOS.git",
			from: Version(7, 0, 0)
		),
		.package(
			url: "https://github.com/Moya/Moya.git",
			from: Version(15, 0, 3)
		),
		.package(
			url: "https://github.com/daltoniam/Starscream.git",
			from: Version(4, 0, 6)
		),
		.package(
			url: "https://github.com/ashleymills/Reachability.swift",
			from: Version(5, 1, 0)
		),
		.package(
			url: "https://github.com/pointfreeco/swift-composable-architecture.git",
			from: Version(1, 2, 0)
		)
	]
)

// MARK: Targets

package.targets += [
	.target(.apiClient, dependencies: [
		.codableWrappers,
		.constants,
		.composableArchitecture,
		.extensions,
		.log,
		.mocking,
		.moya,
		.systemInformation
	]),
	.target(.chatClient, dependencies: [
		.apiClient,
		.composableArchitecture,
		.constants,
		.extensions,
		.log,
		.models,
		.reachability,
		.starscream,
		.userClient
	]),
	.target(.contactsClient, dependencies: [
		.apiClient,
		.composableArchitecture,
		.extensions,
		.identifiedCollections,
		.log,
		.mocking,
		.models
	]),
	.target(.countdownTimer, dependencies: [
		.composableArchitecture
	]),
	.target(.firebaseClient, dependencies: [
		.composableArchitecture,
		.firebaseMessaging,
		.log
	], resources: [
		.copy("GoogleService-Info.plist")
	]),
	.target(.googleAuthClient, dependencies: [
		.composableArchitecture,
		.constants,
		.extensions,
		.googleSignIn
	]),
	.target(.hobbyClient, dependencies: [
		.apiClient,
		.composableArchitecture,
		.constants,
		.extensions,
		.models
	]),
	.target(.lightStorageClient, dependencies: [
		.composableArchitecture,
		.extensions,
		.models
	]),
	.target(.locationClient, dependencies: [
		.apiClient,
		.composableArchitecture,
		.constants,
		.extensions,
		.log
	]),
	.target(.networkClient, dependencies: [
		.constants,
		.extensions,
		.log,
		.reachability
	]),
	.target(.notificationCenterClient, dependencies: [
		.composableArchitecture
	]),
	.target(.toastClient, dependencies: [
		.composableArchitecture
	]),
	.target(.userClient, dependencies: [
		.apiClient,
		.composableArchitecture,
		.googleAuthClient,
		.lightStorageClient,
		.models,
		.toastClient
	]),
	.target(.userNotificationsClient, dependencies: [
		.composableArchitecture,
		.constants,
		.extensions,
		.models,
		.firebaseMessaging,
		.lightStorageClient,
		.toastClient
	])
]
