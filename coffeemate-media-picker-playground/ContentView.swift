//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import MediaPicker
import SwiftUI

// MARK: - View

public struct ContentView: View {
	
	// MARK: Private properties
	
	@State private var isPickerPresented: Bool = false
	
	// MARK: Body
	
	public var body: some View {
		VStack {
			Spacer()
			
			Button("Open picker") {
				isPickerPresented = true
			}
			
			Spacer()
		}
		.sheet(isPresented: $isPickerPresented) {
			MediaPickerView(isPresented: $isPickerPresented) { media in
				Task {
					await print(media?.url)
				}
			}
		}
	}
	
}

// MARK: - Previews

#if DEBUG
struct ContentView_Preview: PreviewProvider {
	
	static var previews: some View {
		ContentView()
	}
	
}
#endif
