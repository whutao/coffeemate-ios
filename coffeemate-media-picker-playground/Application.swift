//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import SwiftUI

@main struct Application: App {
	
	var body: some Scene {
		WindowGroup {
			ContentView()
		}
	}
	
}
