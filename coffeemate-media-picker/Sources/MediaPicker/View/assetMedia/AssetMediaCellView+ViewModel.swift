//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Photos
import SwiftUI

// MARK: - ViewModel

extension AssetMediaCellView {
	
	internal final class ViewModel: ObservableObject {
		
		// MARK: Exposed properties
		
		@Published private(set) var previewImage: Image?
		
		// MARK: Private properties
		
		private var requestId: PHImageRequestID?
		
		private let assetMedia: AssetMedia
		
		// MARK: Init
		
		internal init(assetMedia: AssetMedia) {
			self.assetMedia = assetMedia
			self.previewImage = nil
			self.requestId = nil
		}
		
		deinit {
			cancelPreviewImageFetchIfNeeded()
		}
		
		// MARK: Exposed methods
		
		internal func fetchPreviewImage(ofSize size: CGSize) {
			requestId = assetMedia.asset.image(size: size) { [weak self] uiImage in
				guard let self else {
					return
				}
				previewImage = uiImage.flatMap(Image.init(uiImage:))
			}
		}
		
		internal func cancelPreviewImageFetchIfNeeded() {
			if let requestId {
				PHCachingImageManager.default().cancelImageRequest(requestId)
			}
		}
		
	}
	
}
