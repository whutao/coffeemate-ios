//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import SwiftUI

// MARK: - View

internal struct AssetMediaCellView: View {
	
	// MARK: Private properties
	
	@StateObject private var viewModel: ViewModel
	
	// MARK: Init
	
	internal init(assetMedia: AssetMedia) {
		self.init(viewModel: ViewModel(assetMedia: assetMedia))
	}
	
	private init(viewModel: ViewModel) {
		self._viewModel = StateObject(wrappedValue: viewModel)
	}
	
	// MARK: Body
	
	internal var body: some View {
		GeometryReader { geometry in
			ThumbnailView(
				image: viewModel.previewImage
			)
			.onAppear {
				viewModel.fetchPreviewImage(ofSize: geometry.size)
			}
			.onDisappear {
				viewModel.cancelPreviewImageFetchIfNeeded()
			}
		}
		.aspectRatio(1, contentMode: .fit)
	}
	
}
