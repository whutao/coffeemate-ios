//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import SwiftUI

// MARK: - View

extension AlbumView {
	
	public struct DefaultEmptyView: View {
		
		// MARK: Body
		
		public var body: some View {
			Color.clear.scaledToFill()
		}
		
	}
	
}
