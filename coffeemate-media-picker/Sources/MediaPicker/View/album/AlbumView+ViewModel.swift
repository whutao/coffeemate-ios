//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Combine
import IdentifiedCollections
import Photos
import SwiftUI

// MARK: - ViewModel

extension AlbumView {
	
	internal final class ViewModel: ObservableObject {
		
		// MARK: Exposed properties
		
		@Published internal private(set) var title: String?
		
		@Published internal private(set) var isLoading: Bool
		
		@Published internal private(set) var assetMedias: IdentifiedArrayOf<AssetMedia>
		
		internal var isAssetMediasEmpty: Bool {
			return assetMedias.isEmpty
		}
		
		// MARK: Private properties
		
		private let assetMediaProvider: AssetMediaProvider
		
		private var assetMediaProviderSubscription: AnyCancellable?
		
		// MARK: Init
		
		internal init(
			assetMediaProvider: AssetMediaProvider,
			title: String? = nil,
			assetMedias: IdentifiedArrayOf<AssetMedia> = [],
			isLoading: Bool = false
		) {
			self.assetMediaProvider = assetMediaProvider
			self.title = title
			self.assetMedias = assetMedias
			self.isLoading = isLoading
		}
		
		deinit {
			assetMediaProviderSubscription?.cancel()
		}
		
		// MARK: Exposed methods
		
		internal func onAppear() {
			isLoading = true
			assetMediaProviderSubscription = assetMediaProvider.medias
				.receive(on: DispatchQueue.main)
				.sink { [weak self] assetMedias in
					self?.assetMedias = assetMedias
					self?.isLoading = false
				}
			assetMediaProvider.refresh()
		}
		
	}
	
}
