//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import SwiftUI

// MARK: - View

public struct AlbumView<
	AlbumEmptyContent: View,
	AlbumLoadingContent: View
>: View {
	
	// MARK: Exposed typealiases
	
	public typealias AlbumEmptyContentBuilder = () -> AlbumEmptyContent
	
	public typealias AlbumLoadingContentBuilder = () -> AlbumLoadingContent
	
	// MARK: Private properties
	
	@EnvironmentObject private var assetMediaSelectionService: AssetMediaSelectionService
	
	@EnvironmentObject private var permissionsService: PermissionsService
	
	@StateObject private var viewModel: ViewModel
	
	private let albumEmptyContentBuilder: AlbumEmptyContentBuilder?
	
	private let albumLoadingContentBuilder: AlbumLoadingContentBuilder?
	
	// MARK: Init
	
	internal init(
		viewModel: ViewModel,
		@ViewBuilder whenEmpty albumEmptyContentBuilder: @escaping AlbumEmptyContentBuilder,
		@ViewBuilder whenLoading albumLoadingContentBuilder: @escaping AlbumLoadingContentBuilder
	) {
		self._viewModel = StateObject(wrappedValue: viewModel)
		self.albumEmptyContentBuilder = albumEmptyContentBuilder
		self.albumLoadingContentBuilder = albumLoadingContentBuilder
	}
	
	// MARK: Body
	
	public var body: some View {
		ScrollView {
			Group {
				if viewModel.isLoading {
					if let albumLoadingContentBuilder {
						albumLoadingContentBuilder()
					} else {
						DefaultLoadingView()
					}
				} else if viewModel.isAssetMediasEmpty {
					if let albumEmptyContentBuilder {
						albumEmptyContentBuilder()
					} else {
						DefaultEmptyView()
					}
				} else {
					MediaLazyGrid(
						items: viewModel.assetMedias,
						content: cellView(for:)
					)
				}
			}
			.frame(maxWidth: .infinity, maxHeight: .infinity)
		}
		.scrollContentBackground(.hidden)
		.onChange(of: viewModel.assetMedias) { assetMedias in
			assetMediaSelectionService.intersectSelectedAssetMedias(with: assetMedias)
		}
		.onAppear(perform: viewModel.onAppear)
	}
	
	// MARK: Private methods
	
	@ViewBuilder private func cellView(for assetMedia: AssetMedia) -> some View {
		Group {
			if assetMediaSelectionService.selectionLimit.allowsOnlyOneSelection {
				AssetMediaCellView(assetMedia: assetMedia)
			} else {
				AssetMediaCellView(assetMedia: assetMedia)
					.if(assetMediaSelectionService.isSelected(assetMedia)) { view in
						view.overlay {
							Rectangle()
								.stroke(Color.white, lineWidth: 3)
						}
					}
			}
		}
		.contentShape(Rectangle())
		.onTapGesture {
			assetMediaSelectionService.toggleSelectionIfPossible(assetMedia)
		}
	}
	
}

// MARK: Convenience init

extension AlbumView where AlbumEmptyContent == EmptyView {
	
	internal init(
		viewModel: ViewModel,
		@ViewBuilder whenLoading albumLoadingContentBuilder: @escaping AlbumLoadingContentBuilder
	) {
		self._viewModel = StateObject(wrappedValue: viewModel)
		self.albumEmptyContentBuilder = nil
		self.albumLoadingContentBuilder = albumLoadingContentBuilder
	}
	
}

extension AlbumView where AlbumLoadingContent == EmptyView {
	
	internal init(
		viewModel: ViewModel,
		@ViewBuilder whenEmpty albumEmptyContentBuilder: @escaping AlbumEmptyContentBuilder
	) {
		self._viewModel = StateObject(wrappedValue: viewModel)
		self.albumEmptyContentBuilder = albumEmptyContentBuilder
		self.albumLoadingContentBuilder = nil
	}
	
}

extension AlbumView where AlbumEmptyContent == EmptyView, AlbumLoadingContent == EmptyView {
	
	internal init(viewModel: ViewModel) {
		self._viewModel = StateObject(wrappedValue: viewModel)
		self.albumEmptyContentBuilder = nil
		self.albumLoadingContentBuilder = nil
	}
	
}
