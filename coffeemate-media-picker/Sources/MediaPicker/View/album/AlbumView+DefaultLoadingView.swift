//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import SwiftUI

// MARK: - View

extension AlbumView {
	
	public struct DefaultLoadingView: View {
		
		// MARK: Body
		
		public var body: some View {
			ZStack {
				Color.clear.scaledToFill()
				
				ProgressView()
			}
		}
		
	}
	
}

