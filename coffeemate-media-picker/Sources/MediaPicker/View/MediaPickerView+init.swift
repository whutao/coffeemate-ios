//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import SwiftUI

// MARK: - MediaPickerView convenience init

extension MediaPickerView {
	
	public init(
		isPresented: Binding<Bool>,
		onSinglePhotoSelectAction selectAction: MediaCompletionClosure? = nil,
		albumEmptyBuilder: @escaping AlbumEmptyBuilder,
		albumLoadingBuilder: @escaping AlbumLoadingBuilder,
		headerCancelBuilder: @escaping HeaderCancelBuilder,
		footerDoneBuilder: @escaping FooterDoneBuilder,
		footerDescriptionBuilder: @escaping FooterDescriptionBuilder,
		permissionRequiredBuilder: @escaping PermissionRequiredBuilder
	) {
		self.init(
			isPresented: isPresented,
			selectionContent: .photo,
			selectionLimit: .limited(1),
			selectAction: { selectedMedias in
				selectAction?(selectedMedias.first)
			},
			albumEmptyBuilder: albumEmptyBuilder,
			albumLoadingBuilder: albumLoadingBuilder,
			headerCancelBuilder: headerCancelBuilder,
			footerDoneBuilder: footerDoneBuilder,
			footerDescriptionBuilder: footerDescriptionBuilder,
			permissionRequiredBuilder: permissionRequiredBuilder
		)
	}
	
}

extension MediaPickerView where
	AlbumEmptyContent == EmptyView,
	AlbumLoadingContent == EmptyView,
	FooterDescriptionContent == EmptyView,
	HeaderCancelContent == EmptyView,
	FooterDoneContent == EmptyView,
	PermissionRequiredContent == EmptyView
{
	
	public init(
		isPresented: Binding<Bool>,
		onSinglePhotoSelectAction selectAction: MediaCompletionClosure? = nil
	) {
		self.init(
			isPresented: isPresented,
			selectionContent: .photo,
			selectionLimit: .limited(1),
			selectAction: { selectedMedias in
				selectAction?(selectedMedias.first)
			},
			albumEmptyBuilder: nil,
			albumLoadingBuilder: nil,
			headerCancelBuilder: nil,
			footerDoneBuilder: nil,
			footerDescriptionBuilder: nil,
			permissionRequiredBuilder: nil
		)
	}
	
}
