//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Combine
import Foundation
import IdentifiedCollections
import SwiftUI

// MARK: - ViewModel

extension MediaPickerView {

	@MainActor internal final class ViewModel: ObservableObject {

		// MARK: Exposed properties

		// MARK: Internal properties

		// MARK: Init

		// MARK: Exposed methods

		// MARK: Private methods

	}

}
