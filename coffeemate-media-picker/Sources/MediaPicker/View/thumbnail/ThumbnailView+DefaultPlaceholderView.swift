//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import SwiftUI

// MARK: - View

extension ThumbnailView {
	
	internal struct DefaultPlaceholderView: View {
		
		// MARK: Body
		
		internal var body: some View {
			Rectangle()
				.fill(.gray.opacity(0.3))
				.aspectRatio(contentMode: .fill)
		}
		
	}
	
}
