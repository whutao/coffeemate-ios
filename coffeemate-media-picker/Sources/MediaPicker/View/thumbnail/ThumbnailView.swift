//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Extensions
import SwiftUI

// MARK: - View

public struct ThumbnailView<PlaceholderContent: View>: View {
	
	// MARK: Typealiases
	
	public typealias PlaceholderContentBuilder = () -> PlaceholderContent
	
	// MARK: Private properties
	
	private let image: Image?
	
	private let imagePlaceholder: PlaceholderContentBuilder?
	
	// MARK: Init
	
	public init(
		image: Image?,
		@ViewBuilder imagePlaceholder: @escaping PlaceholderContentBuilder
	) {
		self.image = image
		self.imagePlaceholder = imagePlaceholder
	}
	
	// MARK: Body
	
	public var body: some View {
		if let image {
			GeometryReader { geometry in
				image
					.resizable()
					.scaledToFill()
					.frame(size: geometry.size)
					.clipped()
			}
		} else if let imagePlaceholder {
			imagePlaceholder()
		} else {
			DefaultPlaceholderView()
		}
	}
	
}

// MARK: - Convenience init

extension ThumbnailView where PlaceholderContent == EmptyView {
	
	public init(image: Image?) {
		self.image = image
		self.imagePlaceholder = nil
	}
	
}
