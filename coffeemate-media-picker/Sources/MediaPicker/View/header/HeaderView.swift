//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import SwiftUI

// MARK: - View

public struct HeaderView<CancelContent: View>: View {
	
	// MARK: Exposed typealiases
	
	public typealias CancelContentBuilder = (@escaping ActionClosure) -> CancelContent
	
	// MARK: Private properties
	
	private let cancelAction: ActionClosure
	
	private let cancelContentBuilder: CancelContentBuilder?
	
	// MARK: Init
	
	internal init(
		cancelAction: @escaping () -> Void,
		@ViewBuilder cancelContentBuilder: @escaping CancelContentBuilder
	) {
		self.cancelAction = cancelAction
		self.cancelContentBuilder = cancelContentBuilder
	}
	
	// MARK: Body
	
	public var body: some View {
		HStack(alignment: .center, spacing: .zero) {
			if let cancelContentBuilder {
				cancelContentBuilder(cancelAction)
			} else {
				DefaultCancelView(action: cancelAction)
			}
			
			Spacer()
		}
		.frame(height: 34)
		.padding(.horizontal, 24)
		.padding(.vertical, 12)
	}
	
}

// MARK: Convenience init

extension HeaderView where CancelContent == EmptyView {
	
	internal init(cancelAction: @escaping () -> Void) {
		self.cancelAction = cancelAction
		self.cancelContentBuilder = nil
	}
	
}
