//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import SwiftUI

// MARK: - View

internal struct MediaLazyGrid<
	Items: RandomAccessCollection,
	Content: View
>: View where Items.Element: Identifiable {
	
	// MARK: Private properties
	
	private let spacing: CGFloat
	
	private var columns: [GridItem] {
		return [GridItem(.adaptive(minimum: 110), spacing: spacing, alignment: .top)]
	}
	
	private let items: Items
	
	private let content: (Items.Element) -> Content
	
	// MARK: Init
	
	internal init(
		items: Items,
		spacing: CGFloat = 1,
		@ViewBuilder content: @escaping (Items.Element) -> Content
	) {
		self.items = items
		self.spacing = spacing
		self.content = content
	}
	
	// MARK: Body
	
	internal var body: some View {
		LazyVGrid(columns: columns, spacing: spacing) {
			ForEach(items) { item in
				content(item)
			}
		}
	}
	
}

// MARK: - Previews

#if DEBUG
private typealias IdentifiableInt = Int

extension IdentifiableInt: Identifiable {
	
	public var id: Int {
		return self
	}
	
}

struct MediaLazyGrid_Preview: PreviewProvider {
	
	static var previews: some View {
		ScrollView {
			MediaLazyGrid(items: Range(1...30), spacing: 1) { index in
				GeometryReader { geometry in
					Text(index.description)
						.font(.system(size: 20, weight: .bold))
						.foregroundColor(.white)
						.frame(size: geometry.size)
						.background {
							Color.brown
						}
				}
				.aspectRatio(1, contentMode: .fill)
			}
		}
	}
	
}
#endif
