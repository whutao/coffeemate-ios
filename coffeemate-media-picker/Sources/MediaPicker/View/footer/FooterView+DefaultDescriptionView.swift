//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import IdentifiedCollections
import SwiftUI

// MARK: - View

extension FooterView {
	
	internal struct DefaultDescriptionView: View {
		
		// MARK: Private properties
		
		private let selectedMedias: IdentifiedArrayOf<AnyMedia>
		
		// MARK: Init
		
		internal init(selectedMedias: IdentifiedArrayOf<AnyMedia>) {
			self.selectedMedias = selectedMedias
		}
		
		// MARK: Body
		
		internal var body: some View {
			Text("Selected \(selectedMedias.count)")
		}
		
	}
	
}
