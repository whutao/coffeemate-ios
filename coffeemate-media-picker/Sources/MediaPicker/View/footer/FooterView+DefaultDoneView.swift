//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import SwiftUI

// MARK: - View

extension FooterView {
	
	internal struct DefaultDoneView: View {
		
		// MARK: Private properties
		
		private let action: ActionClosure
		
		// MARK: Init
		
		internal init(action: @escaping ActionClosure) {
			self.action = action
		}
		
		// MARK: Body
		
		internal var body: some View {
			Button {
				action()
			} label: {
				Text("Done")
			}
		}
		
	}
	
}
