//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import IdentifiedCollections
import SwiftUI

// MARK: - View

public struct FooterView<
	DescriptionContent: View,
	DoneButtonContent: View
>: View {
	
	// MARK: Exposed typealiases
	
	public typealias DescriptionContentBuilder = (IdentifiedArrayOf<AnyMedia>) -> DescriptionContent
	
	public typealias DoneButtonContentBuilder = (@escaping ActionClosure) -> DoneButtonContent
	
	// MARK: Private properties
	
	private let selectedMedias: IdentifiedArrayOf<AnyMedia>
	
	private let doneAction: ActionClosure
	
	private let descriptionContentBuilder: DescriptionContentBuilder?
	
	private let doneButtonContentBuilder: DoneButtonContentBuilder?
	
	// MARK: Init
	
	internal init(
		selectedMedias: IdentifiedArrayOf<AnyMedia>,
		doneAction: @escaping ActionClosure,
		@ViewBuilder descriptionContentBuilder: @escaping DescriptionContentBuilder,
		@ViewBuilder doneButtonContentBuilder: @escaping DoneButtonContentBuilder
	) {
		self.selectedMedias = selectedMedias
		self.doneAction = doneAction
		self.descriptionContentBuilder = descriptionContentBuilder
		self.doneButtonContentBuilder = doneButtonContentBuilder
	}
	
	// MARK: Body
	
	public var body: some View {
		HStack(alignment: .center, spacing: .zero) {
			if let descriptionContentBuilder {
				descriptionContentBuilder(selectedMedias)
			} else {
				DefaultDescriptionView(selectedMedias: selectedMedias)
			}
				
			Spacer(minLength: 30)
			
			if let doneButtonContentBuilder {
				doneButtonContentBuilder(doneAction)
			} else {
				DefaultDoneView(action: doneAction)
			}
		}
		.frame(maxHeight: 80)
		.padding(.horizontal, 16)
		.padding(.vertical, 12)
	}
	
}

// MARK: Convenience init

extension FooterView where DescriptionContent == EmptyView {
	
	internal init(
		selectedMedias: IdentifiedArrayOf<AnyMedia>,
		doneAction: @escaping ActionClosure,
		@ViewBuilder doneButtonContentBuilder: @escaping DoneButtonContentBuilder
	) {
		self.selectedMedias = selectedMedias
		self.doneAction = doneAction
		self.descriptionContentBuilder = nil
		self.doneButtonContentBuilder = doneButtonContentBuilder
	}
	
}

extension FooterView where DoneButtonContent == EmptyView {
	
	internal init(
		selectedMedias: IdentifiedArrayOf<AnyMedia>,
		doneAction: @escaping ActionClosure,
		@ViewBuilder descriptionContentBuilder: @escaping DescriptionContentBuilder
	) {
		self.selectedMedias = selectedMedias
		self.doneAction = doneAction
		self.descriptionContentBuilder = descriptionContentBuilder
		self.doneButtonContentBuilder = nil
	}
	
}

extension FooterView where DescriptionContent == EmptyView, DoneButtonContent == EmptyView {
	
	internal init(
		selectedMedias: IdentifiedArrayOf<AnyMedia>,
		doneAction: @escaping ActionClosure
	) {
		self.selectedMedias = selectedMedias
		self.doneAction = doneAction
		self.descriptionContentBuilder = nil
		self.doneButtonContentBuilder = nil
	}
	
}

// MARK: - Previews

#if DEBUG
struct FooterView_Preview: PreviewProvider {
	
	static var previews: some View {
		FooterView(selectedMedias: [], doneAction: { })
	}
	
}
#endif
