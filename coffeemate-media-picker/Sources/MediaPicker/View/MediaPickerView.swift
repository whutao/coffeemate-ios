//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import SwiftUI

// MARK: - View

public struct MediaPickerView<
	AlbumEmptyContent: View,
	AlbumLoadingContent: View,
	HeaderCancelContent: View,
	FooterDescriptionContent: View,
	FooterDoneContent: View,
	PermissionRequiredContent: View
>: View {
	
	// MARK: Exposed typealiases
	
	public typealias AlbumEmptyBuilder = AlbumView<AlbumEmptyContent, AlbumLoadingContent>.AlbumEmptyContentBuilder
	
	public typealias AlbumLoadingBuilder = AlbumView<AlbumEmptyContent, AlbumLoadingContent>.AlbumLoadingContentBuilder
	
	public typealias HeaderCancelBuilder = HeaderView<HeaderCancelContent>.CancelContentBuilder
	
	public typealias FooterDoneBuilder = FooterView<FooterDoneContent, FooterDescriptionContent>.DoneButtonContentBuilder
	
	public typealias FooterDescriptionBuilder = FooterView<FooterDoneContent, FooterDescriptionContent>.DescriptionContentBuilder
	
	public typealias PermissionRequiredBuilder = () -> PermissionRequiredContent
	
	// MARK: Private properties
	
	@StateObject private var assetMediaSelectionService: AssetMediaSelectionService
	
	@StateObject private var permissionService: PermissionsService
	
	@Binding private var isPresented: Bool
	
	private let configuration: PickerConfiguration
	
	private let selectAction: MediaCollectionCompletionClosure?
	
	private var isFooterVisible: Bool {
		guard assetMediaSelectionService.selectedAnyMedias.isNotEmpty else {
			return false
		}
		guard !configuration.selectionLimit.allowsOnlyOneSelection else {
			return false
		}
		return true
	}
	
	private let albumEmptyBuilder: AlbumEmptyBuilder?
	
	private let albumLoadingBuilder: AlbumLoadingBuilder?
	
	private let headerCancelBuilder: HeaderCancelBuilder?
	
	private let footerDoneBuilder: FooterDoneBuilder?
	
	private let footerDescriptionBuilder: FooterDescriptionBuilder?
	
	private let permissionRequiredBuilder: PermissionRequiredBuilder?
	
	// MARK: Init
	
	internal init(
		isPresented: Binding<Bool>,
		selectionContent: PickerSelectionContent,
		selectionLimit: PickerSelectionLimit,
		selectAction: MediaCollectionCompletionClosure?,
		albumEmptyBuilder: AlbumEmptyBuilder?,
		albumLoadingBuilder: AlbumLoadingBuilder?,
		headerCancelBuilder: HeaderCancelBuilder?,
		footerDoneBuilder: FooterDoneBuilder?,
		footerDescriptionBuilder: FooterDescriptionBuilder?,
		permissionRequiredBuilder: PermissionRequiredBuilder?
	) {
		self._permissionService = StateObject(wrappedValue: .shared)
		self._assetMediaSelectionService = StateObject(wrappedValue: .init(
			selectionLimit: selectionLimit
		))
		self._isPresented = isPresented
		self.configuration = PickerConfiguration(
			selectionContent: selectionContent,
			selectionLimit: selectionLimit
		)
		self.selectAction = selectAction
		self.albumEmptyBuilder = albumEmptyBuilder
		self.albumLoadingBuilder = albumLoadingBuilder
		self.headerCancelBuilder = headerCancelBuilder
		self.footerDoneBuilder = footerDoneBuilder
		self.footerDescriptionBuilder = footerDescriptionBuilder
		self.permissionRequiredBuilder = permissionRequiredBuilder
	}
	
	// MARK: Body
	
	public var body: some View {
		VStack(alignment: .center, spacing: .zero) {
			headerView {
				dismiss()
			}
			
			Group {
				switch permissionService.photoLibraryAction {
				case .authorized, .limited:
					contentView()
				case .unavailable:
					permissionRequiredView()
				case .unknown:
					Color.clear
				}
			}
			.transition(.opacity.animation(.easeInOut))
			
			Group {
				if isFooterVisible {
					footerView {
						selectAction?(assetMediaSelectionService.selectedAnyMedias)
						dismiss()
					}
				}
			}
			.transition(.scale.animation(.easeInOut))
		}
		.environmentObject(assetMediaSelectionService)
		.environmentObject(permissionService)
		.background {
			Color.black.ignoresSafeArea(edges: [.leading, .trailing, .bottom])
		}
		.onChange(of: assetMediaSelectionService.selectedAnyMedias) { selectedMedias in
			if configuration.selectionLimit.allowsOnlyOneSelection {
				if selectedMedias.isNotEmpty {
					selectAction?(assetMediaSelectionService.selectedAnyMedias)
					dismiss()
				}
			}
		}
		.onAppear {
			permissionService.askLibraryPermissionIfNeeded()
		}
	}
	
	// MARK: Exposed methods
	
	@ViewBuilder private func contentView() -> some View {
		if let albumEmptyBuilder, let albumLoadingBuilder {
			AlbumView(
				viewModel: .init(assetMediaProvider: AllAssetMediaProvider(
					selectionContent: configuration.selectionContent,
					permissionsService: permissionService
				)),
				whenEmpty: albumEmptyBuilder,
				whenLoading: albumLoadingBuilder
			)
		} else if let albumEmptyBuilder {
			AlbumView(
				viewModel: .init(assetMediaProvider: AllAssetMediaProvider(
					selectionContent: configuration.selectionContent,
					permissionsService: permissionService
				)),
				whenEmpty: albumEmptyBuilder
			)
		} else if let albumLoadingBuilder {
			AlbumView(
				viewModel: .init(assetMediaProvider: AllAssetMediaProvider(
					selectionContent: configuration.selectionContent,
					permissionsService: permissionService
				)),
				whenLoading: albumLoadingBuilder
			)
		} else {
			AlbumView(viewModel: .init(assetMediaProvider: AllAssetMediaProvider(
				selectionContent: configuration.selectionContent,
				permissionsService: permissionService
			)))
		}
	}
	
	@ViewBuilder private func permissionRequiredView() -> some View {
		if let permissionRequiredBuilder {
			permissionRequiredBuilder()
		} else {
			Color.clear
		}
	}
	
	@ViewBuilder private func headerView(cancelAction: @escaping ActionClosure) -> some View {
		if let headerCancelBuilder {
			HeaderView(
				cancelAction: cancelAction,
				cancelContentBuilder: headerCancelBuilder
			)
		} else {
			HeaderView(cancelAction: cancelAction)
		}
	}
	
	@ViewBuilder private func footerView(doneAction: @escaping ActionClosure) -> some View {
		if let footerDoneBuilder, let footerDescriptionBuilder {
			FooterView(
				selectedMedias: assetMediaSelectionService.selectedAnyMedias,
				doneAction: doneAction,
				descriptionContentBuilder: footerDescriptionBuilder,
				doneButtonContentBuilder: footerDoneBuilder
			)
		} else if let footerDoneBuilder {
			FooterView(
				selectedMedias: assetMediaSelectionService.selectedAnyMedias,
				doneAction: doneAction,
				doneButtonContentBuilder: footerDoneBuilder
			)
		} else if let footerDescriptionBuilder {
			FooterView(
				selectedMedias: assetMediaSelectionService.selectedAnyMedias,
				doneAction: doneAction,
				descriptionContentBuilder: footerDescriptionBuilder
			)
		} else {
			FooterView(
				selectedMedias: assetMediaSelectionService.selectedAnyMedias,
				doneAction: doneAction
			)
		}
	}
	
	private func dismiss() {
		withAnimation {
			isPresented = false
		}
	}
	
}
