//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public enum PickerSelectionLimit: Equatable {
	
	// MARK: Case
	
	case limited(UInt)
	
	case unlimited
	
	// MARK: Exposed properties
	
	public var allowsOnlyOneSelection: Bool {
		if case let .limited(limit) = self, limit == 1 {
			return true
		} else {
			return false
		}
	}
	
}
