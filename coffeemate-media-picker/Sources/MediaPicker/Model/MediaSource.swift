//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import CoreGraphics
import Foundation

// MARK: - Protocol

public protocol MediaSource {
	
	// MARK: Properties
	
	var mediaType: MediaType? { get }
	
	var duration: TimeInterval? { get async }
	
	var url: URL? { get async }
	
	var data: Data? { get async throws }
	
	var thumbnailUrl: URL? { get async }
	
	var thumbnailData: Data? { get async }
	
}
