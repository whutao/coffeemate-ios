//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct AnyMedia: Equatable, Identifiable {
	
	// MARK: Exposed properties
	
	public let id: UUID
	
	public var mediaType: MediaType {
		return source.mediaType.unwrapped(or: .image)
	}
	
	public var duration: CGFloat? {
		get async {
			return await source.duration.flatMap(CGFloat.init)
		}
	}
	
	public var url: URL? {
		get async {
			return await source.url
		}
	}
	
	public var data: Data? {
		get async {
			return try? await source.data
		}
	}
	
	public var thumbnailUrl: URL? {
		get async {
			return await source.thumbnailUrl
		}
	}
	
	public var thumbnailData: Data? {
		get async {
			return await source.thumbnailData
		}
	}
	
	// MARK: Private properties
	
	private let source: MediaSource
	
	// MARK: Init
	
	public init(source: MediaSource) {
		self.id = UUID()
		self.source = source
	}
	
	// MARK: Exposed methods
	
	public static func == (lhs: AnyMedia, rhs: AnyMedia) -> Bool {
		return lhs.id == rhs.id
	}
	
}
