//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct PickerConfiguration {
	
	// MARK: Exposed properties
	
	public let selectionContent: PickerSelectionContent
	
	public let selectionLimit: PickerSelectionLimit
	
	// MARK: Init
	
	public init(
		selectionContent: PickerSelectionContent,
		selectionLimit: PickerSelectionLimit
	) {
		self.selectionContent = selectionContent
		self.selectionLimit = selectionLimit
	}
	
}
