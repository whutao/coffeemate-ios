//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import Photos

// MARK: - Model

internal struct AssetAlbum: Identifiable {
	
	// MARK: Exposed properties
	
	internal var id: String {
		source.localIdentifier
	}
	
	internal var title: String? {
		source.localizedTitle
	}
	
	internal let preview: AssetMedia?
	
	internal let source: PHAssetCollection
	
	// MARK: Init
	
	internal init(source: PHAssetCollection, preview: AssetMedia?) {
		self.source = source
		self.preview = preview
	}
	
}
