//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import Photos

// MARK: - Model

public struct AnyAlbum: Identifiable {
	
	// MARK: Exposed properties
	
	public let id: String
	
	public let title: String?
	
	public let preview: AnyMedia?
	
	// MARK: Init
	
	public init(id: String, title: String?, preview: AnyMedia?) {
		self.id = id
		self.title = title
		self.preview = preview
	}
	
	init(from album: AssetAlbum) {
		self.init(
			id: album.id,
			title: album.title,
			preview: album.preview.flatMap(AnyMedia.init(source:))
		)
	}
	
}
