//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import Photos

// MARK: - Model

internal struct AssetMedia: Equatable, Identifiable {
	
	// MARK: Exposed properties
	
	internal var id: String {
		asset.localIdentifier
	}
	
	internal let asset: PHAsset
	
	// MARK: Init
	
	internal init(asset: PHAsset) {
		self.asset = asset
	}
	
	// MARK: Exposed methods
	
	internal static func == (lhs: AssetMedia, rhs: AssetMedia) -> Bool {
		return lhs.id == rhs.id
	}
	
}

// MARK: - PHAssetMedia+MediaSource

extension AssetMedia: MediaSource {
	
	internal var mediaType: MediaType? {
		switch asset.mediaType {
		case .image:
			return .image
		case .video:
			return .video
		default:
			return nil
		}
	}
	
	internal var duration: TimeInterval? {
		get async {
			return asset.duration
		}
	}
	
	internal var url: URL? {
		get async {
			return await asset.url
		}
	}
	
	internal var data: Data? {
		get async throws {
			return await asset.data
		}
	}
	
	internal var thumbnailUrl: URL? {
		get async {
			guard let url = await url else {
				return nil
			}
			if mediaType == .image {
				return url
			} else if mediaType == .video {
				guard let thumbnailData = AVAsset(url: url).thumbnail() else {
					return nil
				}
				return try? FileManager.default.storeToTemporaryDirectory(
					data: thumbnailData,
					as: .video
				)
			} else {
				return nil
			}
		}
	}
	
	internal var thumbnailData: Data? {
		get async {
			if mediaType == .image {
				return try? await data
			} else if mediaType == .video {
				guard let url = await url else {
					return nil
				}
				return AVAsset(url: url).thumbnail()
			} else {
				return nil
			}
		}
	}
	
}
