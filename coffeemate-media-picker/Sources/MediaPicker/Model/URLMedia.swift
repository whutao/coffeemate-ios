//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import AVFoundation
import Extensions
import SwiftUI
import UniformTypeIdentifiers

// MARK: - Model

internal struct URLMedia: Identifiable {
	
	// MARK: Exposed properties
	
	internal var id: String {
		return _url.absoluteString
	}
	
	// MARK: Private properties
	
	internal let _url: URL
	
	// MARK: Init
	
	internal init(_ url: URL) {
		self._url = url
	}

}

// MARK: - URLMedia+MediaSource

extension URLMedia: MediaSource {
	
	internal var mediaType: MediaType? {
		if _url.isImageFile {
			return .image
		}
		if _url.isVideoFile {
			return .video
		}
		return nil
	}
	
	internal var duration: TimeInterval? {
		get async {
			let asset = AVURLAsset(url: _url)
			return try? await CMTimeGetSeconds(asset.load(.duration))
		}
	}
	
	internal var url: URL? {
		get async {
			return _url
		}
	}
	
	internal var data: Data? {
		get async throws {
			return try? Data(contentsOf: _url)
		}
	}
	
	internal var thumbnailUrl: URL? {
		get async {
			switch mediaType {
			case .image:
				return _url
			case .video:
				return _url.thumbnailUrl
			case .none:
				return nil
			}
		}
	}
	
	internal var thumbnailData: Data? {
		get async {
			switch mediaType {
			case .image:
				return try? Data(contentsOf: _url)
			case .video:
				return _url.thumbnailData
			case .none:
				return nil
			}
		}
	}

}
