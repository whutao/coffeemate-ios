//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct PickerSelectionContent: OptionSet {
	
	// MARK: Exposed properties
	
	public var rawValue: Int
	
	public var allowsPhoto: Bool {
		return self.contains(.photo)
	}
	
	public var allowsVideo: Bool {
		return self.contains(.video)
	}
	
	public static let photo = PickerSelectionContent(rawValue: 1 << 0)
	
	public static let video = PickerSelectionContent(rawValue: 1 << 1)
	
	public static let all = PickerSelectionContent([.photo, .video])
	
	// MARK: Init
	
	public init(rawValue: Int) {
		self.rawValue = rawValue
	}
	
}
