//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public enum MediaType: UInt8, Equatable, Hashable {
	
	// MARK: Case
	
	case image = 0
	
	case video = 1
	
}
