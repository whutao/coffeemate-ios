//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import IdentifiedCollections

// MARK: - Typealias

public typealias MediaCollectionCompletionClosure = (IdentifiedArrayOf<AnyMedia>) -> Void
