//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import IdentifiedCollections
import Photos

// MARK: - PHFetchResult<PHAsset>+filterMap

extension PHFetchResult where ObjectType == PHAsset {
	
	internal func filterMap(
		for selectionContent: PickerSelectionContent
	) -> IdentifiedArrayOf<AssetMedia> {
		guard count != 0 else {
			return []
		}
		var result: IdentifiedArrayOf<AssetMedia> = []
		for index in 0...(count - 1) {
			let asset = self[index]
			if (asset.mediaType == .image && selectionContent.allowsPhoto) {
				result.append(AssetMedia(asset: asset))
			} else if (asset.mediaType == .video && selectionContent.allowsVideo) {
				result.append(AssetMedia(asset: asset))
			}
		}
		return result
	}
	
}

// MARK: - PHFetchResult<PHAssetCollection>+filterMap

extension PHFetchResult where ObjectType == PHAssetCollection {
	
	internal func filterMap(
		for selectionContent: PickerSelectionContent
	) -> IdentifiedArrayOf<AssetAlbum> {
		guard count != 0 else {
			return []
		}
		var result: IdentifiedArrayOf<AssetAlbum> = []
		for index in 0...(count - 1) {
			let collection = self[index]
			let fetchOptions = PHFetchOptions()
			fetchOptions.fetchLimit = 1
			fetchOptions.sortDescriptors = [
				NSSortDescriptor(key: "creationDate", ascending: false)
			]
			let fetchResult = PHAsset.fetchAssets(
				in: collection,
				options: fetchOptions
			)
			guard fetchResult.count != 0 else {
				continue
			}
			#warning("TODO: Collection might not contain preview of this type as a first element.")
			let previewAsset = fetchResult.filterMap(for: selectionContent).first
			let assetAlbum = AssetAlbum(
				source: collection,
				preview: previewAsset
			)
			result.append(assetAlbum)
		}
		return result
	}
	
}
