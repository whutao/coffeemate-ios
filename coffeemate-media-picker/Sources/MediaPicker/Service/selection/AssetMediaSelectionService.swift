//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Combine
import Extensions
import Foundation
import IdentifiedCollections
import Photos

// MARK: - Service

internal final class AssetMediaSelectionService: ObservableObject {

	// MARK: Exposed properties

	@Published internal private(set) var selectedAssetMedias: IdentifiedArrayOf<AssetMedia> = []
	
	internal let selectionLimit: PickerSelectionLimit
	
	internal var selectedAnyMedias: IdentifiedArrayOf<AnyMedia> {
		let anyMedias: [AnyMedia] = selectedAssetMedias.compactMap { assetMedia in
			guard (assetMedia.mediaType).isNotNil else {
				return nil
			}
			return AnyMedia(source: assetMedia)
		}
		return IdentifiedArrayOf(uniqueElements: anyMedias)
	}
	
	internal var numberOfSelectedMedias: Int {
		return selectedAssetMedias.count
	}
	
	// MARK: Private properties

	private var isSelectionLimitReached: Bool {
		switch selectionLimit {
		case let .limited(limit):
			return limit <= numberOfSelectedMedias
		case .unlimited:
			return false
		}
	}

	// MARK: Init
	
	internal init(
		selectionLimit: PickerSelectionLimit,
		selectedAssetMedias: IdentifiedArrayOf<AssetMedia> = []
	) {
		self.selectionLimit = selectionLimit
		self.selectedAssetMedias = selectedAssetMedias
	}

	// MARK: Exposed methods

	internal func isSelected(_ assetMedia: AssetMedia) -> Bool {
		return selectedAssetMedias.contains(assetMedia)
	}

	internal func canToggleSelection(_ assetMedia: AssetMedia) -> Bool {
		return isSelected(assetMedia) || !isSelectionLimitReached
	}

	internal func toggleSelectionIfPossible(_ assetMedia: AssetMedia) {
		if selectedAssetMedias.contains(assetMedia) {
			selectedAssetMedias.remove(assetMedia)
		} else if canToggleSelection(assetMedia) {
			selectedAssetMedias.append(assetMedia)
		}
	}
	 
	internal func intersectSelectedAssetMedias(with assetMedias: IdentifiedArrayOf<AssetMedia>) {
		selectedAssetMedias = selectedAssetMedias.filter(assetMedias.contains(_:))
	}

	internal func removeAll() {
		selectedAssetMedias.removeAll()
	}

}
