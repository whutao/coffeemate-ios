//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Combine
import Foundation
import Photos

// MARK: - Notifications

private let didChangePhotoLibraryPermissionNotification = Notification.Name(rawValue: "DidChangePhotoLibraryPermissionNotification")

private let didChangePhotoLibraryLimitedPhotosNotification = Notification.Name(rawValue: "DidChangePhotoLibraryLimitedPhotosNotification")

private let didChangeCameraPermissionNotification = Notification.Name(rawValue: "DidChangeCameraPermissionNotification")

// MARK: - Publishers

let didChangePhotoLibraryPermissionPublisher: AnyPublisher<Void, Never> = NotificationCenter.default
	.publisher(for: didChangePhotoLibraryPermissionNotification)
	.map({ _ in })
	.share()
	.eraseToAnyPublisher()

let didChangePhotoLibraryLimitedPhotosPublisher: AnyPublisher<Void, Never> = NotificationCenter.default
	.publisher(for: didChangePhotoLibraryLimitedPhotosNotification)
	.map({ _ in })
	.share()
	.eraseToAnyPublisher()

let didChangeCameraPermissionPublisher: AnyPublisher<Void, Never> = NotificationCenter.default
	.publisher(for: didChangeCameraPermissionNotification)
	.map({ _ in })
	.share()
	.eraseToAnyPublisher()

// MARK: - Observer

final class PhotoLibraryChangePermissionObserver: NSObject, PHPhotoLibraryChangeObserver {
	
	// MARK: Init
	
	override init() {
		super.init()
		PHPhotoLibrary.shared().register(self)
	}
	
	deinit {
		PHPhotoLibrary.shared().unregisterChangeObserver(self)
	}
	
	// MARK: Exposed methods
	
	func photoLibraryDidChange(_ changeInstance: PHChange) {
		#warning("TODO: Filter received PHChanges.")
		NotificationCenter.default.post(
			name: didChangePhotoLibraryPermissionNotification,
			object: self
		)
	}

}
