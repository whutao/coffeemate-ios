//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import AVFoundation
import Combine
import Extensions
import Foundation
import Photos

// MARK: - Service

final class PermissionsService: ObservableObject {
	
	// MARK: Exposed types
	
	enum Camera {
		case authorize
		case unavailable
		case unknown
	}
	
	enum PhotoLibrary {
		case authorized
		case limited
		case unavailable
		case unknown
	}
	
	// MARK: Exposed properties
	
	static let shared = PermissionsService()
	
	@Published var cameraAction: Camera = .unknown
	
	@Published var photoLibraryAction: PhotoLibrary = .unknown
	
	// MARK: Private properties
	
	private var disposeBag: Set<AnyCancellable> = []
	
	// MARK: Init
	
	init() {
		disposeBag.collect {
			didChangePhotoLibraryPermissionPublisher.sink { [weak self] in
				self?.checkPhotoLibraryAuthorizationStatus()
			}
			didChangeCameraPermissionPublisher.sink { [weak self] in
				self?.checkCameraAuthorizationStatus()
			}
		}
	}
	
	func askCameraPermissionIfNeeded() {
		checkCameraAuthorizationStatus()
	}
	
	func askLibraryPermissionIfNeeded() {
		checkPhotoLibraryAuthorizationStatus()
	}
	
	func requestPermission(_ completion: @escaping () -> Void) {
		PHPhotoLibrary.requestAuthorization(for: .addOnly) { status in
			if status == .authorized || status == .limited {
				completion()
			}
		}
	}

	// MARK: Private methods
	
	private func checkCameraAuthorizationStatus() {
		handle(camera: AVCaptureDevice.authorizationStatus(for: .video))
	}
	
	private func checkPhotoLibraryAuthorizationStatus() {
		handle(photoLibrary: PHPhotoLibrary.authorizationStatus(for: .readWrite))
	}
	
	private func handle(camera status: AVAuthorizationStatus) {
		var result: Camera? = nil
		#if targetEnvironment(simulator)
		result = .unavailable
		#else
		switch status {
		case .notDetermined:
			AVCaptureDevice.requestAccess(for: .video) { [weak self] _ in
				self?.checkCameraAuthorizationStatus()
			}
		case .restricted:
			result = .unavailable
		case .denied:
			result = .authorize
		case .authorized:
			// Do nothing
			break
		@unknown default:
			result = .unknown
		}
		#endif
		DispatchQueue.main.async { [weak self] in
			self?.cameraAction = result.unwrapped(or: .unknown)
		}
	}
	
	private func handle(photoLibrary status: PHAuthorizationStatus) {
		var result: PhotoLibrary?
		switch status {
		case .notDetermined:
			PHPhotoLibrary.requestAuthorization { [weak self] status in
				self?.handle(photoLibrary: status)
			}
		case .restricted:
			result = .unavailable
		case .denied:
			result = .authorized
		case .authorized:
			result = .authorized
		case .limited:
			result = .limited
		@unknown default:
			result = .unknown
		}
		DispatchQueue.main.async { [weak self] in
			self?.photoLibraryAction = result.unwrapped(or: .unknown)
		}
	}
	
}
