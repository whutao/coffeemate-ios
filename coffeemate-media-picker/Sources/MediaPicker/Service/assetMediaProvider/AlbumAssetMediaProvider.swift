//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Combine
import Extensions
import IdentifiedCollections
import Photos
import SwiftUI

// MARK: - Provider

final class AlbumAssetMediaProvider: AssetMediaProvider {
	
	// MARK: Exposed properties
	
	internal var medias: AnyPublisher<IdentifiedArrayOf<AssetMedia>, Never> {
		return mediasPublisher.eraseToAnyPublisher()
	}
	
	// MARK: Private properties
	
	private let assetAlbum: AssetAlbum
	
	private let permissionsService: PermissionsService
	
	private let selectionContent: PickerSelectionContent
	
	private let mediasPublisher: EventPublisher<IdentifiedArrayOf<AssetMedia>>
	
	// MARK: Init
	
	internal init(
		assetAlbum: AssetAlbum,
		selectionContent: PickerSelectionContent,
		permissionsService: PermissionsService
	) {
		self.assetAlbum = assetAlbum
		self.selectionContent = selectionContent
		self.permissionsService = permissionsService
		self.mediasPublisher = .init()
	}
	
	// MARK: Exposed methods
	
	internal func refresh() {
		permissionsService.requestPermission { [weak self] in
			guard let self else {
				return
			}
			let fetchOptions = PHFetchOptions()
			fetchOptions.sortDescriptors = [
				NSSortDescriptor(key: "creationDate", ascending: false)
			]
			let albumAssets = PHAsset.fetchAssets(
				in: assetAlbum.source,
				options: fetchOptions
			)
			let filteredAssets = albumAssets.filterMap(for: self.selectionContent)
			self.mediasPublisher.send(filteredAssets)
		}
	}
	
	internal func cancel() {
		fatalError("Not implemented.")
	}
	
}
