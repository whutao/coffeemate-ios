//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Combine
import Foundation
import IdentifiedCollections
import Photos

// MARK: - Protocol

internal protocol AssetMediaProvider {
	
	// MARK: Properties
	
	var medias: AnyPublisher<IdentifiedArrayOf<AssetMedia>, Never> { get }
	
	// MARK: Methods
	
	func refresh()
	
	func cancel()
	
}
