//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Combine
import Extensions
import Foundation
import IdentifiedCollections
import Photos

// MARK: - Provider

final class AllAssetMediaProvider: AssetMediaProvider {
	
	// MARK: Exposed properties
	
	internal var medias: AnyPublisher<IdentifiedArrayOf<AssetMedia>, Never> {
		return mediasPublisher.eraseToAnyPublisher()
	}
	
	// MARK: Private properties
	
	private let permissionsService: PermissionsService
	
	private let selectionContent: PickerSelectionContent
	
	private let mediasPublisher: EventPublisher<IdentifiedArrayOf<AssetMedia>>
	
	// MARK: Init
	
	internal init(
		selectionContent: PickerSelectionContent,
		permissionsService: PermissionsService
	) {
		self.selectionContent = selectionContent
		self.permissionsService = permissionsService
		self.mediasPublisher = .init()
	}
	
	// MARK: Exposed methods
	
	internal func refresh() {
		permissionsService.requestPermission { [weak self] in
			guard let self else {
				return
			}
			let allPhotosOptions = PHFetchOptions()
			allPhotosOptions.sortDescriptors = [
				NSSortDescriptor(key: "creationDate", ascending: false)
			]
			let allAssets = PHAsset.fetchAssets(with: allPhotosOptions)
			let filteredAssets = allAssets.filterMap(for: self.selectionContent)
			self.mediasPublisher.send(filteredAssets)
		}
	}
	
	internal func cancel() {
		fatalError("Not implemented.")
	}

}
