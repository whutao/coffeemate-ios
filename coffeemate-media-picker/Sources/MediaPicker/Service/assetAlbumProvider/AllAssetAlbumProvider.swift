//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Combine
import Extensions
import Foundation
import IdentifiedCollections
import Photos

// MARK: - Provider

internal final class AllAssetAlbumProvider: AssetAlbumProvider {
	
	// MARK: Exposed properties
	
	internal var albums: AnyPublisher<IdentifiedArrayOf<AssetAlbum>, Never> {
		return albumsPublisher.eraseToAnyPublisher()
	}
	
	// MARK: Private properties
	
	private let permissionsService: PermissionsService
	
	private let albumTypes: [PHAssetCollectionType] = [.smartAlbum, .album]
	
	private let albumsPublisher: EventPublisher<IdentifiedArrayOf<AssetAlbum>>
	
	private let pickerSelectionContent: PickerSelectionContent
	
	private var fetchAlbumsSubscription: AnyCancellable?
	
	// MARK: Init
	
	internal init(
		pickerSelectionContent: PickerSelectionContent = .all,
		permissionsService: PermissionsService
	) {
		self.albumsPublisher = .init()
		self.pickerSelectionContent = pickerSelectionContent
		self.permissionsService = permissionsService
	}
	
	deinit {
		fetchAlbumsSubscription?.cancel()
	}
	
	// MARK: Exposed methods
	
	internal func refresh() {
		permissionsService.requestPermission { [weak self] in
			self?.fetchAlbums()
		}
	}
	
	internal func cancel() {
		fetchAlbumsSubscription?.cancel()
	}
	
	// MARK: Private methods
	
	public func fetchAlbums() {
		fetchAlbumsSubscription = albumTypes.publisher
			.map(fetchAssetAlbums(ofType:))
			.scan([], +)
			.receive(on: DispatchQueue.main)
			.sink { [weak self] in
				self?.albumsPublisher.send($0)
			}
	}
	
	private func fetchAssetAlbums(ofType type: PHAssetCollectionType) -> IdentifiedArrayOf<AssetAlbum> {
		let allAlbumsFetchOptions = PHFetchOptions()
		allAlbumsFetchOptions.includeAssetSourceTypes = [
			.typeUserLibrary,
			.typeiTunesSynced,
			.typeCloudShared
		]
		allAlbumsFetchOptions.sortDescriptors = [
			NSSortDescriptor(key: "localizedTitle", ascending: true)
		]
		
		let collections = PHAssetCollection.fetchAssetCollections(
			with: type,
			subtype: .any,
			options: allAlbumsFetchOptions
		)
		return collections.filterMap(for: pickerSelectionContent)
	}

}
