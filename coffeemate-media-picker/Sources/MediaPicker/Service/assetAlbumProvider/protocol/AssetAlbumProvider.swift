//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Combine
import Foundation
import IdentifiedCollections

// MARK: - Protocol

internal protocol AssetAlbumProvider {
	
	// MARK: Properties
	
	var albums: AnyPublisher<IdentifiedArrayOf<AssetAlbum>, Never> { get }
	
	// MARK: Methods
	
	func refresh()
	
	func cancel()
	
}
