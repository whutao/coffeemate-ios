// swift-tools-version: 5.8
import PackageDescription

let package = Package(
	name: "coffeemate-media-picker",
	platforms: [.iOS(.v16)],
	products: [
		.library(
			name: "MediaPicker",
			targets: ["MediaPicker"]
		),
	],
	dependencies: [
		.package(name: "coffeemate-common", path: "coffeemate-common"),
		.package(url: "https://github.com/pointfreeco/swift-identified-collections", from: Version(1, 0, 0))
	],
	targets: [
		.target(
			name: "MediaPicker",
			dependencies: [
				.product(name: "Extensions", package: "coffeemate-common"),
				.product(name: "IdentifiedCollections", package: "swift-identified-collections")
			]
		),
	]
)
