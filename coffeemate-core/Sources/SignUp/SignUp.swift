//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import APIClient
import ComposableArchitecture
import Constants
import Extensions
import Foundation
import Localizable
import UserClient

// MARK: - Reducer

public struct SignUp: Reducer {

	// MARK: Private properties

	@Dependency(\.userClient) private var userClient

	// MARK: Init

	public init() {

	}

	// MARK: Body

	public var body: some ReducerOf<Self> {
		Scope(state: \.currentStep, action: /Action.step) {
			Scope(state: /State.Step.info, action: /Action.Step.info) {
				SignUpInfoStep()
			}
			Scope(state: /State.Step.gender, action: /Action.Step.gender) {
				SignUpGenderStep()
			}
			Scope(state: /State.Step.hobby, action: /Action.Step.hobby) {
				SignUpHobbyStep()
			}
			Scope(state: /State.Step.about, action: /Action.Step.about) {
				SignUpAboutStep()
			}
		}
		Reduce { state, action in
			switch action {

			// Delegate

			case .delegate:
				return .none

			// Step

			case let .step(.info(.delegate(.shouldNavigateForward(output)))):
				state.infoStepOutput = output
				return .send(._private(.navigateToNextStep))

			case let .step(.gender(.delegate(.shouldNavigateForward(output)))):
				state.genderStepOutput = output
				return .send(._private(.navigateToNextStep))

			case let .step(.hobby(.delegate(.shouldNavigateForward(output)))):
				state.hobbyStepOutput = output
				return .send(._private(.navigateToNextStep))

			case let .step(.about(.delegate(.shouldNavigateForward(output)))):
				state.aboutStepOutput = output
				return .send(._private(.signUp))

			case .step:
				return .none

			// Private

			case ._private(.navigateToNextStep):
				state.setNextStepIfPossible()
				return .none

			case ._private(.navigateToPreviousStep):
				state.setPreviousStepIfPossible()
				return .none

			case ._private(.signUp):
				guard let infoOutput = state.infoStepOutput else {
					return .none
				}
				guard let genderOutput = state.genderStepOutput else {
					return .none
				}
				guard let hobbyOutput = state.hobbyStepOutput else {
					return .none
				}
				guard let aboutOutput = state.aboutStepOutput else {
					return .none
				}
				return .run { send in
					await send(._private(.setIsPerformingSignUp(true)))
					let signUpRequest = SignUpRequest(
						imageData: infoOutput.avatarImageCompressedData,
						nickname: infoOutput.nickname,
						fcmToken: .empty,
						birthDate: infoOutput.birthDate,
						ownGender: genderOutput.ownGender,
						targetGenders: Array(genderOutput.targetGenders),
						about: aboutOutput.aboutText,
						hobbies: hobbyOutput.hobbies,
						isUserAgreementAccepted: true
					)
					do {
						try await userClient.signUp(signUpRequest)
						await send(._private(.setIsPerformingSignUp(false)))
						await send(.delegate(.didSignUp))
					} catch let error {
						await send(._private(.setIsPerformingSignUp(false)))
						log.error("Sign-up failed due to \(error).")
					}
				}

			case let ._private(.setIsPerformingSignUp(isPerformingSignUp)):
				state.isPerformingSignUp = isPerformingSignUp
				return .none
			}
		}
	}

}

// MARK: - Action

extension SignUp {

	public enum Action: Equatable, Sendable {

		// MARK: Case

		case delegate(Delegate)

		case step(Step)

		case _private(Private)

	}

}

// MARK: - Action: delegate

extension SignUp.Action {

	public enum Delegate: Equatable, Sendable {

		// MARK: Case

		case didSignUp

	}

}

// MARK: - Action: step

extension SignUp.Action {

	public enum Step: Equatable, Sendable {

		// MARK: Case

		case about(SignUpAboutStep.Action)

		case info(SignUpInfoStep.Action)

		case gender(SignUpGenderStep.Action)

		case hobby(SignUpHobbyStep.Action)

	}

}

// MARK: - Action: private

extension SignUp.Action {

	public enum Private: Equatable, Sendable {

		// MARK: Case

		case navigateToNextStep

		case navigateToPreviousStep

		case signUp

		case setIsPerformingSignUp(Bool)

	}

}

// MARK: - State

extension SignUp {

	public struct State: Equatable, Sendable {

		// MARK: Exposed properties

		public var currentStepOrder: Int {
			switch currentStep {
			case .info:
				return 0
			case .gender:
				return 1
			case .hobby:
				return 2
			case .about:
				return 3
			}
		}

		public let numberOfSteps: Int = 4

		public var stepCounterDescription: String {
			return L10n.signUpFooterStepCounterDescription(
				currentStepOrder + 1,
				numberOfSteps
			)
		}

		public var headerTitle: String = L10n.signUpHeaderTitle

		public var aboutStepOutput: SignUpAboutStep.State.Output?

		public var infoStepOutput: SignUpInfoStep.State.Output?

		public var genderStepOutput: SignUpGenderStep.State.Output?

		public var hobbyStepOutput: SignUpHobbyStep.State.Output?

		public var currentStep: Step

		public var isPerformingSignUp: Bool

		// MARK: Init

		public init(
			currentStep: Step? = nil,
			isPerformingSignUp: Bool = false,
			aboutStepOutput: SignUpAboutStep.State.Output? = nil,
			infoStepOutput: SignUpInfoStep.State.Output? = nil,
			genderStepOutput: SignUpGenderStep.State.Output? = nil,
			hobbyStepOutput: SignUpHobbyStep.State.Output? = nil
		) {
			self.currentStep = currentStep.unwrapped(or: .info(.init(
				avatarImageData: infoStepOutput?.avatarImageCompressedData,
				birthDate: infoStepOutput?.birthDate,
				nickname: (infoStepOutput?.nickname).unwrapped(or: .empty)
			)))
			self.isPerformingSignUp = isPerformingSignUp
			self.aboutStepOutput = aboutStepOutput
			self.infoStepOutput = infoStepOutput
			self.genderStepOutput = genderStepOutput
			self.hobbyStepOutput = hobbyStepOutput
		}

		// MARK: Exposed methods

		public mutating func setNextStepIfPossible() {
			switch currentStep {
			case .info:
				currentStep = .gender(.init(
					ownGender: genderStepOutput?.ownGender,
					targetGenders: (genderStepOutput?.targetGenders).unwrapped(or: [])
				))
			case .gender:
				currentStep = .hobby(.init(
					selectedHobbies: (hobbyStepOutput?.hobbies).unwrapped(or: []),
					maxSelectedHobbies: Constants.ProfileEditor.maxSelectedHobbies
				))
			case .hobby:
				currentStep = .about(.init(
					aboutText: (aboutStepOutput?.aboutText).unwrapped(or: .empty)
				))
			case .about:
				log.error("Business logic error. Trying to set next step but the \"about\" is the last one.")
			}
		}

		public mutating func setPreviousStepIfPossible() {
			switch currentStep {
			case .info:
				log.error("Business logic error. Trying to set previous step but the \"info\" is the first one.")
			case .gender:
				currentStep = .info(.init(
					avatarImageData: infoStepOutput?.avatarImageCompressedData,
					birthDate: infoStepOutput?.birthDate,
					nickname: (infoStepOutput?.nickname).unwrapped(or: .empty)
				))
			case .hobby:
				currentStep = .gender(.init(
					ownGender: genderStepOutput?.ownGender,
					targetGenders: (genderStepOutput?.targetGenders).unwrapped(or: [])
				))
			case .about:
				currentStep = .hobby(.init(
					selectedHobbies: (hobbyStepOutput?.hobbies).unwrapped(or: []),
					maxSelectedHobbies: Constants.ProfileEditor.maxSelectedHobbies
				))
			}
		}

	}

}

// MARK: - State: step

extension SignUp.State {

	public enum Step: Equatable, Sendable {

		// MARK: Case

		case about(SignUpAboutStep.State)

		case info(SignUpInfoStep.State)

		case gender(SignUpGenderStep.State)

		case hobby(SignUpHobbyStep.State)

	}

}
