//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Foundation
import HobbyClient
import IdentifiedCollections
import Localizable
import Models

// MARK: - Reducer

public struct SignUpHobbyStep: Reducer {

	// MARK: Private properties

	@Dependency(\.hobbyClient) private var hobbyClient

	// MARK: Init

	public init() {

	}

	// MARK: Body

	public var body: some ReducerOf<Self> {
		Reduce { state, action in
			switch action {

			// Delegate

			case .delegate:
				return .none

			// View

			case .view(.didAppearFirstTime):
				return .send(._private(.initialize))

			case .view(.didTapNextButton):
				do {
					let stepOutput = try state.stepOutput()
					return .send(.delegate(.shouldNavigateForward(stepOutput)))
				} catch let error {
					log.error("Cannot proceed in hobbies step: \(error).")
					return .none
				}

			case let .view(.didSelectHobbies(hobbies)):
				return .send(._private(.setSelectedHobbies(hobbies)))

			// Private

			case let ._private(.setSelectedHobbies(hobbies)):
				state.selectedHobbies = hobbies
				return .none

			case let ._private(.setAllHobbies(hobbies)):
				state.allHobbies = IdentifiedArray(uniqueElements: hobbies)
				return .none

			case let ._private(.setIsLoadingAllHobbies(isLoadingAllHobbies)):
				state.isLoadingAllHobbies = isLoadingAllHobbies
				return .none

			case ._private(.initialize):
				guard state.allHobbies.isEmpty else {
					return .none
				}
				return .run { send in
					await send(._private(.setIsLoadingAllHobbies(true)))
					do {
						let allHobbies = try await hobbyClient.allHobbyKinds()
						await send(._private(.setAllHobbies(IdentifiedArray(uniqueElements: allHobbies))))
					} catch let error {
						log.error("Cannot set all hobbies due to \(error).")
					}
					await send(._private(.setIsLoadingAllHobbies(false)))
				}
			}
		}
	}

}

// MARK: - Action

extension SignUpHobbyStep {

	public enum Action: Equatable, Sendable {

		// MARK: Case

		case delegate(Delegate)

		case view(View)

		case _private(Private)

	}

}

// MARK: - Action: delegate

extension SignUpHobbyStep.Action {

	public enum Delegate: Equatable, Sendable {

		// MARK: Case

		case shouldNavigateForward(SignUpHobbyStep.State.Output)

	}

}

// MARK: - Action: view

extension SignUpHobbyStep.Action {

	public enum View: Equatable, Sendable {

		// MARK: Case

		case didAppearFirstTime

		case didTapNextButton

		case didSelectHobbies(IdentifiedArrayOf<Hobby>)

	}

}

// MARK: - Action: private

extension SignUpHobbyStep.Action {

	public enum Private: Equatable, Sendable {

		// MARK: Case

		case initialize

		case setSelectedHobbies(IdentifiedArrayOf<Hobby>)

		case setAllHobbies(IdentifiedArrayOf<Hobby>)

		case setIsLoadingAllHobbies(Bool)

	}

}

// MARK: - State

extension SignUpHobbyStep {

	public struct State: Equatable, Sendable {

		// MARK: Exposed properties

		public var allHobbies: IdentifiedArrayOf<Hobby>

		public var selectedHobbies: IdentifiedArrayOf<Hobby>

		public let maxSelectedHobbies: Int

		public var isLoadingAllHobbies: Bool

		public var isNextButtonEnabled: Bool {
			return (try? stepOutput()).isNotNil
		}

		public let headerTitle: String = L10n.signUpHobbyStepHeaderTitle

		public let headerDescription: String = L10n.signUpHobbyStepHeaderDescription

		public let nextButtonTitle: String = L10n.signUpHobbyStepNextButtonTitle

		public var selectedHobbyCountDescription: String {
			return L10n.signUpHobbyStepHobbyPickerNumberOfSelectHobbies(
				selectedHobbies.count,
				maxSelectedHobbies
			)
		}

		// MARK: Init

		/// If `allHobbies` are provided, they will not be fetched on view appear.
		public init(
			allHobbies: [Hobby] = [],
			selectedHobbies: [Hobby] = [],
			maxSelectedHobbies: Int,
			isLoadingAllHobbies: Bool = false
		) {
			self.allHobbies = IdentifiedArray(uniqueElements: allHobbies)
			self.selectedHobbies = IdentifiedArray(uniqueElements: selectedHobbies)
			self.maxSelectedHobbies = maxSelectedHobbies
			self.isLoadingAllHobbies = isLoadingAllHobbies
		}

		// MARK: Exposed methods

		public func stepOutput() throws -> Output {
			guard !selectedHobbies.isEmpty else {
				throw Error.noHobbySelected
			}
			guard selectedHobbies.count <= maxSelectedHobbies else {
				throw Error.tooManyHobbies
			}
			return Output(hobbies: Array(selectedHobbies))
		}

	}

}

// MARK: - State: error

extension SignUpHobbyStep.State {

	public enum Error: Swift.Error, Equatable {

		// MARK: Case

		case noHobbySelected

		case tooManyHobbies

	}

}

// MARK: - State: output

extension SignUpHobbyStep.State {

	public struct Output: Equatable, Sendable {

		// MARK: Exposed properties

		public let hobbies: [Hobby]

		// MARK: Init

		public init(hobbies: [Hobby]) {
			self.hobbies = hobbies
		}

	}

}
