//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Constants
import Foundation
import Localizable

// MARK: - Reducer

public struct SignUpAboutStep: Reducer {

	// MARK: Init

	public init() {

	}

	// MARK: Body

	public var body: some ReducerOf<Self> {
		Reduce { state, action in
			switch action {

			// Delegate

			case .delegate:
				return .none

			// View

			case .view(.didTapNextButton):
				do {
					let stepOutput = try state.stepOutput()
					return .send(.delegate(.shouldNavigateForward(stepOutput)))
				} catch let error {
					log.error("Cannot proceed in about step: \(error).")
					return .none
				}

			case let .view(.didUpdateAboutText(text)):
				return .send(._private(.updateAboutText(text)))

			// Private

			case let ._private(.updateAboutText(text)):
				state.aboutText = text.stringPrefix(state.aboutTextCharacterLimit)
				return .none
			}
		}
	}

}

// MARK: - Action

extension SignUpAboutStep {

	public enum Action: Equatable, Sendable {

		// MARK: Case

		case delegate(Delegate)

		case view(View)

		case _private(Private)

	}

}

// MARK: - Action: delegate

extension SignUpAboutStep.Action {

	public enum Delegate: Equatable, Sendable {

		// MARK: Case

		case shouldNavigateForward(SignUpAboutStep.State.Output)

	}

}

// MARK: - Action: view

extension SignUpAboutStep.Action {

	public enum View: Equatable, Sendable {

		// MARK: Case

		case didTapNextButton

		case didUpdateAboutText(String)

	}

}

// MARK: - Action: private

extension SignUpAboutStep.Action {

	public enum Private: Equatable, Sendable {

		// MARK: Case

		case updateAboutText(String)

	}

}

// MARK: - State

extension SignUpAboutStep {

	public struct State: Equatable, Sendable {

		// MARK: Exposed properties

		public var aboutText: String

		public var characterLimitDescription: String {
			return L10n.signUpAboutStepAboutTextViewNumberOfCharacters(
				aboutText.count,
				aboutTextCharacterLimit
			)
		}

		public var isNextButtonEnabled: Bool {
			return (try? stepOutput()).isNotNil
		}

		public let aboutTextCharacterLimit: Int = Constants.ProfileEditor.maxAboutCharactersCount

		public let headerTitle: String = L10n.signUpAboutStepHeaderTitle

		public let headerDescription: String = L10n.signUpAboutStepHeaderDescription

		public let nextButtonTitle: String = L10n.signUpAboutStepNextButtonTitle

		// MARK: Init

		public init(aboutText: String) {
			self.aboutText = aboutText
		}

		// MARK: Exposed methods

		public func stepOutput() throws -> Output {
			guard aboutText.trimmingCharacters(in: .whitespacesAndNewlines).isNotEmpty else {
				throw Error.aboutTextIsInvalid
			}
			return Output(
				aboutText: aboutText.trimmingCharacters(in: .whitespacesAndNewlines)
			)
		}

	}

}

// MARK: - State: error

extension SignUpAboutStep.State {

	public enum Error: Swift.Error, Equatable {

		// MARK: Case

		case aboutTextIsInvalid

	}

}

// MARK: - State: output

extension SignUpAboutStep.State {

	public struct Output: Equatable, Sendable {

		// MARK: Exposed properties

		public let aboutText: String

		// MARK: Init

		public init(aboutText: String) {
			self.aboutText = aboutText
		}

	}

}
