//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Foundation
import Localizable
import Models

// MARK: - Reducer

public struct SignUpGenderStep: Reducer {

	// MARK: Init

	public init() {

	}

	// MARK: Body

	public var body: some ReducerOf<Self> {
		Reduce { state, action in
			switch action {

			// Delegate

			case .delegate:
				return .none

			// View

			case .view(.didTapNextButton):
				do {
					let stepOutput = try state.stepOutput()
					return .send(.delegate(.shouldNavigateForward(stepOutput)))
				} catch let error {
					log.error("Cannot proceed in gender step: \(error).")
					return .none
				}

			case let .view(.didSelectOwnGender(gender)):
				return .send(._private(.selectOwnGender(gender)))

			case let .view(.didSelectTargetGenders(genders)):
				return .send(._private(.selectTargetGenders(genders)))

			// Private

			case let ._private(.selectOwnGender(gender)):
				state.ownGender = gender
				return .none

			case let ._private(.selectTargetGenders(genders)):
				state.targetGenders = genders
				return .none
			}
		}
	}

}

// MARK: - Action

extension SignUpGenderStep {

	public enum Action: Equatable, Sendable {

		// MARK: Case

		case delegate(Delegate)

		case view(View)

		case _private(Private)

	}

}

// MARK: - Action: delegate

extension SignUpGenderStep.Action {

	public enum Delegate: Equatable, Sendable {

		// MARK: Case

		case shouldNavigateForward(SignUpGenderStep.State.Output)

	}

}

// MARK: - Action: view

extension SignUpGenderStep.Action {

	public enum View: Equatable, Sendable {

		// MARK: Case

		case didTapNextButton

		case didSelectOwnGender(Gender?)

		case didSelectTargetGenders(Set<Gender>)

	}

}

// MARK: - Action: private

extension SignUpGenderStep.Action {

	public enum Private: Equatable, Sendable {

		// MARK: Case

		case selectOwnGender(Gender?)

		case selectTargetGenders(Set<Gender>)

	}

}

// MARK: - State

extension SignUpGenderStep {

	public struct State: Equatable, Sendable {

		// MARK: Exposed properties

		public var ownGender: Gender?

		public var targetGenders: Set<Gender>

		public var isNextButtonEnabled: Bool {
			return (try? stepOutput()).isNotNil
		}

		public let ownGenderPickerTitle: String = L10n.signUpGenderStepOwnGenderPickerTitle

		public let targetGenderPickerTitle: String = L10n.signUpGenderStepTargetGenderPickerTitle

		public let nextButtonTitle: String = L10n.signUpGenderStepNextButtonTitle

		// MARK: Init

		public init(
			ownGender: Gender?,
			targetGenders: Set<Gender>
		) {
			self.ownGender = ownGender
			self.targetGenders = targetGenders
		}

		// MARK: Exposed methods

		public func stepOutput() throws -> Output {
			guard let ownGender else {
				throw Error.ownGenderIsNotSelected
			}
			guard !targetGenders.isEmpty else {
				throw Error.targetGendersAreNotSelected
			}
			return Output(ownGender: ownGender, targetGenders: targetGenders)
		}

	}

}

// MARK: - State: error

extension SignUpGenderStep.State {

	public enum Error: Swift.Error, Equatable {

		// MARK: Case

		case ownGenderIsNotSelected

		case targetGendersAreNotSelected

	}

}

// MARK: - State: output

extension SignUpGenderStep.State {

	public struct Output: Equatable, Sendable {

		// MARK: Exposed properties

		public let ownGender: Gender

		public let targetGenders: Set<Gender>

		// MARK: Init

		public init(
			ownGender: Gender,
			targetGenders: Set<Gender>
		) {
			self.ownGender = ownGender
			self.targetGenders = targetGenders
		}

	}

}
