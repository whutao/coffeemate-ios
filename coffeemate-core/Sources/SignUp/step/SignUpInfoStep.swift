//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Constants
import Extensions
import Foundation
import Localizable

// MARK: - Reducer

public struct SignUpInfoStep: Reducer {

	// MARK: Init

	public init() {

	}

	// MARK: Body

	public var body: some ReducerOf<Self> {
		Reduce { state, action in
			switch action {

			// Delegate

			case .delegate:
				return .none

			// View

			case .view(.didTapNextButton):
				do {
					let stepOutput = try state.stepOutput()
					return .send(.delegate(.shouldNavigateForward(stepOutput)))
				} catch let error {
					log.error("Cannot proceed in info step: \(error).")
					return .none
				}

			case let .view(.didUpdateAvatarImage(data)):
				return .send(._private(.setAvatarImage(data)))

			case let .view(.didUpdateBirthDate(date)):
				return .send(._private(.setBirthDate(date)))

			case let .view(.didUpdateNickname(text)):
				return .send(._private(.setNickname(text)))

			// Private

			case let ._private(.setAvatarImage(data)):
				state.avatarImageData = data
				return .none

			case let ._private(.setBirthDate(date)):
				state.birthDate = date
				return .none

			case let ._private(.setNickname(text)):
				state.nickname = text
				return .none
			}
		}
	}

}

// MARK: - Action

extension SignUpInfoStep {

	public enum Action: Equatable, Sendable {

		// MARK: Case

		case delegate(Delegate)

		case view(View)

		case _private(Private)

	}

}

// MARK: - Action: delegate

extension SignUpInfoStep.Action {

	public enum Delegate: Equatable, Sendable {

		// MARK: Case

		case shouldNavigateForward(SignUpInfoStep.State.Output)

	}

}

// MARK: - Action: view

extension SignUpInfoStep.Action {

	public enum View: Equatable, Sendable {

		// MARK: Case

		case didTapNextButton

		case didUpdateAvatarImage(Data?)

		case didUpdateBirthDate(Date?)

		case didUpdateNickname(String)

	}

}

// MARK: - Action: private

extension SignUpInfoStep.Action {

	public enum Private: Equatable, Sendable {

		// MARK: Case

		case setAvatarImage(Data?)

		case setBirthDate(Date?)

		case setNickname(String)

	}

}

// MARK: - State

extension SignUpInfoStep {

	public struct State: Equatable, Sendable {

		// MARK: Exposed properties

		public var avatarImageData: Data?

		public var birthDate: Date?

		public var nickname: String

		public var isNextButtonEnabled: Bool {
			return (try? stepOutput()).isNotNil
		}

		public let avatarMaxSizeInKB: UInt = Constants.ProfileEditor.maxImageSizeInKB

		public let birthDateRange: ClosedRange<Date>

		public let headerTitle: String = L10n.signUpInfoStepHeaderDescription

		public let photoPickerTitle: String = L10n.signUpInfoStepPhotoPickerTitle

		public let birthDateDisplayFormat: String = Constants.ProfileEditor.birthDateDisplayFormat

		public let birthDatePlaceholder: String = L10n.signUpInfoStepBirthDatePickerPlaceholder

		public let birthDateDoneButtonTitle: String = L10n.signUpInfoStepBirthDatePickerDone

		public let nicknameFieldTitle: String = L10n.signUpInfoStepNicknameTextFieldTitle

		public var nicknameFieldFooter: String {
			return L10n.signUpInfoStepNicknameTextFieldFooter(
				nickname.count,
				nicknameFieldCharacterLimit
			)
		}

		public let nicknameFieldCharacterLimit: Int = Constants.ProfileEditor.maxNicknameCharactersCount

		public let nextButtonTitle: String = L10n.signUpInfoStepNextButtonTitle

		// MARK: Init

		public init(
			avatarImageData: Data?,
			birthDate: Date?,
			nickname: String
		) {
			@Dependency(\.calendar) var calendar
			@Dependency(\.date.now) var now
			let toDate = now
				.rounded(to: .day, using: calendar)?
				.subtracting(.year, value: Constants.ProfileEditor.minAllowedAge)
			let fromDate = now
				.rounded(to: .day, using: calendar)?
				.subtracting(.year, value: Constants.ProfileEditor.maxAllowedAge)
			self.birthDateRange = ClosedRange(uncheckedBounds: (
				fromDate.unwrapped(or: .distantPast),
				toDate.unwrapped(or: now)
			))
			self.avatarImageData = avatarImageData
			self.birthDate = birthDate
			self.nickname = nickname
		}

		// MARK: Exposed methods

		public func stepOutput() throws -> Output {
			guard let avatarImageData else {
				throw Error.avatarImageIsNotPicked
			}
			guard let birthDate, birthDateRange.contains(birthDate) else {
				throw Error.birthDateIsNotPicked
			}
			guard nickname.trimmingCharacters(in: .whitespacesAndNewlines).isNotEmpty else {
				throw Error.nicknameIsInvalid
			}
			return Output(
				avatarImageCompressedData: avatarImageData,
				birthDate: birthDate,
				nickname: nickname.trimmingCharacters(in: .whitespacesAndNewlines)
			)
		}

	}

}

// MARK: - State: error

extension SignUpInfoStep.State {

	public enum Error: Swift.Error, Equatable {

		// MARK: Case

		case avatarImageIsNotPicked

		case birthDateIsInvalid

		case birthDateIsNotPicked

		case nicknameIsInvalid

	}

}

// MARK: - State: output

extension SignUpInfoStep.State {

	public struct Output: Equatable, Sendable {

		// MARK: Exposed properties

		public let avatarImageCompressedData: Data

		public let birthDate: Date

		public let nickname: String

		// MARK: Init

		public init(
			avatarImageCompressedData: Data,
			birthDate: Date,
			nickname: String
		) {
			self.avatarImageCompressedData = avatarImageCompressedData
			self.birthDate = birthDate
			self.nickname = nickname
		}

	}

}
