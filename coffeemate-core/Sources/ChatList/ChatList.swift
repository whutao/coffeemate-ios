//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ChatClient
import ComposableArchitecture
import CountdownTimer
import Extensions
import Foundation
import IdentifiedCollections
import Localizable
import Models

// MARK: - Reducer

public struct ChatList: Reducer {

	// MARK: Private properties

	@Dependency(\.chatClient) private var chatClient

	// MARK: Cancel ID

	public enum CancelID: Hashable {
		case chatListLoad
		case chatListRefreshTimer
	}

	// MARK: Init

	public init() {

	}

	// MARK: Body

	public var body: some ReducerOf<Self> {
		Reduce { state, action in
			switch action {

			// Delegate

			case .delegate:
				return .none

			// View

			case .view(.didAppear):
				return .concatenate(
					.send(._private(.refreshChatSummaries)),
					.publisher {
						Timer.publish(
							every: 10.0,
							on: RunLoop.main,
							in: .common
						)
						.autoconnect()
						.map { _ in ._private(.refreshChatSummaries) }
						.receive(on: DispatchQueue.main)
					}
					.cancellable(id: CancelID.chatListRefreshTimer, cancelInFlight: true)
				)

			case .view(.didTapVisibilityButton):
				return .send(._private(.changeVisibility(state.visibility.opposite)))

			case let .view(.didSelectChatEntry(chatEntryId)):
				guard let chatSummary = state.entries[id: chatEntryId]?.chatSummary else {
					log.error("Cannot find chat summary \(chatEntryId).")
					return .none
				}
				return .send(.delegate(.shouldOpenChat(chatSummary)))

			// Private

			case let ._private(.changeVisibility(visibility)):
				state.visibility = visibility
				return .none

			case ._private(.refreshChatSummaries):
				return .run { send in
					await send(._private(.setIsPerformingChatSummariesLoad(true)))
					let chatSummaries = try await chatClient.chatList()
					await send(._private(.setChatSummaries(chatSummaries)))
					await send(._private(.setIsPerformingChatSummariesLoad(false)))
				} catch: { error, send in
					await send(._private(.setIsPerformingChatSummariesLoad(false)))
					log.error("Cannot load chat summaries due to \(error).")
				}
				.cancellable(id: CancelID.chatListLoad, cancelInFlight: true)

			case let ._private(.setChatSummaries(chatSummaries)):
				state.entries = IdentifiedArray(uniqueElements: chatSummaries.map(State.Entry.init))
				return .none

			case let ._private(.setIsPerformingChatSummariesLoad(isPerformingChatSummariesLoad)):
				state.isPerformingChatSummariesLoad = isPerformingChatSummariesLoad
				return .none
			}
		}
	}

}

// MARK: - Action

extension ChatList {

	public enum Action: Equatable, Sendable {

		// MARK: Case

		case delegate(Delegate)

		case view(View)

		case _private(Private)

	}

}

// MARK: - Action: delegate

extension ChatList.Action {

	public enum Delegate: Equatable, Sendable {

		// MARK: Case

		case shouldOpenChat(ChatSummary)

	}

}

// MARK: - Action: view

extension ChatList.Action {

	public enum View: Equatable, Sendable {

		// MARK: Case

		case didAppear

		case didTapVisibilityButton

		case didSelectChatEntry(ChatID)

	}

}

// MARK: - Action: private

extension ChatList.Action {

	public enum Private: Equatable, Sendable {

		// MARK: Case

		case changeVisibility(ChatList.State.Visibility)

		case refreshChatSummaries

		case setChatSummaries([ChatSummary])

		case setIsPerformingChatSummariesLoad(Bool)

	}

}

// MARK: - State

extension ChatList {

	public struct State: Equatable, Sendable {

		// MARK: Exposed properties

		public var entries: IdentifiedArrayOf<Entry>

		public var visibility: Visibility

		public var areChatSummariesEmpty: Bool {
			return entries.isEmpty
		}

		public var isPerformingChatSummariesLoad: Bool

		public var isChangeVisibilityAvailable: Bool

		public let emptyChatListPlaceholder: String = L10n.chatListEmptyPlaceholder

		public let headerTitle: String = L10n.chatListHeaderTitle

		// MARK: Init

		public init(
			entries: IdentifiedArrayOf<Entry> = [],
			visibility: Visibility = .normal,
			isChangeVisibilityAvailable: Bool = false,
			isPerformingChatListLoad: Bool = false
		) {
			self.entries = entries
			self.visibility = visibility
			self.isChangeVisibilityAvailable = isChangeVisibilityAvailable
			self.isPerformingChatSummariesLoad = isPerformingChatListLoad
		}

	}

}

// MARK: - State: visibility

extension ChatList.State {

	public enum Visibility: Equatable, Sendable {

		// MARK: Case

		case normal

		case hidden

		// MARK: Exposed properties

		var opposite: Visibility {
			switch self {
			case .normal:
				return .hidden
			case .hidden:
				return .normal
			}
		}

	}

}

// MARK: - State: entry

extension ChatList.State {

	public struct Entry: Equatable, Identifiable, Sendable {

		// MARK: Exposed properties

		public var id: ChatID {
			return chatSummary.id
		}

		public var chatSummary: ChatSummary

		public var avatarUrl: URL {
			return chatSummary.companion.avatarUrl
		}

		public var nickname: String {
			return chatSummary.companion.nickname
		}

		public var lastMessageDate: Date? {
			return chatSummary.lastMessage?.date
		}

		public var lastMessageDisplayText: String? {
			switch chatSummary.lastMessage?.content {
			case let .text(text):
				return text
			case .none:
				return nil
			}
		}

		public let lastMessageEmptyPlaceholder: String = L10n.chatListChatSummaryLastMessageEmptyPlaceholder

		// MARK: Init

		public init(chatSummary: ChatSummary) {
			self.chatSummary = chatSummary
		}

	}

}
