//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Foundation
import Localizable
import ToastClient
import UserClient

// MARK: - Reducer

public struct SignIn: Reducer {

	// MARK: Private properties

	@Dependency(\.toastClient) private var toastClient

	@Dependency(\.userClient) private var userClient

	// MARK: Init

	public init() {

	}

	// MARK: Body

	public var body: some ReducerOf<Self> {
		Reduce { state, action in
			switch action {

			// Delegate

			case .delegate:
				return .none

			// View

			case .view(.didTapGoogleButton):
				return .send(._private(.signIn))

			// Private

			case let ._private(.setIsPerformingSignIn(isPerformingSignIn)):
				state.isPerformingSignIn = isPerformingSignIn
				return .none

			case ._private(.signIn):
				return .run { send in
					await send(._private(.setIsPerformingSignIn(true)))
					do {
						try await userClient.signIn()
						await send(._private(.setIsPerformingSignIn(false)))
						await send(.delegate(.didSignInForExistingAccount))
					} catch UserClient.Error.unexistingAccount {
						await send(._private(.setIsPerformingSignIn(false)))
						await send(.delegate(.didSignInForNewAccount))
					} catch let error {
						log.error("Cannot sign in due to \(error).")
						await send(._private(.setIsPerformingSignIn(false)))
					}
				}
			}
		}
	}

}

// MARK: - Action

extension SignIn {

	public enum Action: Equatable, Sendable {

		// MARK: Case

		case delegate(Delegate)

		case view(View)

		case _private(Private)

	}

}

// MARK: - Action: delegate

extension SignIn.Action {

	public enum Delegate: Equatable, Sendable {

		// MARK: Case

		case didSignInForExistingAccount

		case didSignInForNewAccount

	}

}

// MARK: - Action: view

extension SignIn.Action {

	public enum View: Equatable, Sendable {

		// MARK: Case

		case didTapGoogleButton

	}

}

// MARK: - Action: private

extension SignIn.Action {

	public enum Private: Equatable, Sendable {

		// MARK: Case

		case setIsPerformingSignIn(Bool)

		case signIn

	}

}

// MARK: - State

extension SignIn {

	public struct State: Equatable, Sendable {

		// MARK: Exposed properties

		public let headerTitle: String = L10n.signInHeaderTitle

		public let headerDescription: String = L10n.signInHeaderDescription

		public let footerDescription: String = L10n.signInFooterMarkdownText

		public var isPerformingSignIn: Bool

		// MARK: Init

		public init(isPerformingSignIn: Bool = false) {
			self.isPerformingSignIn = isPerformingSignIn
		}

	}

}
