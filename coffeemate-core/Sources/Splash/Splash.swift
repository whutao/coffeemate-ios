//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import CountdownTimer
import ComposableArchitecture
import Foundation

// MARK: - Reducer

public struct Splash: Reducer {

	// MARK: Init

	public init() {

	}

	// MARK: Body

	public var body: some ReducerOf<Self> {
		Scope(state: \.timer, action: /Action.timer) {
			CountdownTimer()
		}
		Reduce { _, action in
			switch action {
			case .delegate:
				return .none

			case .timer(.delegate(.didExpire)):
				return .send(.delegate(.shouldClose))

			case .timer:
				return .none

			case .view(.didAppearFirstTime):
				return .send(._private(.resumeCountdown))

			case ._private(.pauseCountdown):
				return .send(.timer(.pause))

			case ._private(.resumeCountdown):
				return .send(.timer(.resume))
			}
		}
	}

}

// MARK: - Action

extension Splash {

	public enum Action: Equatable, Sendable {

		// MARK: Case

		case delegate(Delegate)

		case timer(CountdownTimer.Action)

		case view(View)

		case _private(Private)

	}

}

// MARK: - Action: delegate

extension Splash.Action {

	public enum Delegate: Equatable, Sendable {

		// MARK: Case

		case shouldClose

	}

}

// MARK: - Action: view

extension Splash.Action {

	public enum View: Equatable, Sendable {

		// MARK: Case

		case didAppearFirstTime

	}

}

// MARK: - Action: private

extension Splash.Action {

	public enum Private: Equatable, Sendable {

		// MARK: Case

		case pauseCountdown

		case resumeCountdown

	}

}

// MARK: - State

extension Splash {

	public struct State: Equatable, Sendable {

		// MARK: Title

		public var timer: CountdownTimer.State

		public let title: String

		public let isShowingLoadingSpinner: Bool

		// MARK: Init

		public init(
			timer: CountdownTimer.State,
			title: String,
			isShowingLoadingSpinner: Bool = true
		) {
			self.timer = timer
			self.title = title
			self.isShowingLoadingSpinner = isShowingLoadingSpinner
		}

	}

}
