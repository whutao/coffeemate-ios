//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Constants
import Extensions
import Foundation
import Localizable
import Log
import Models
import UserClient
import SystemInformation

// MARK: - Reducer

public struct ProfileEditorRedactor: Reducer {

	// MARK: Init

	public init() {

	}

	// MARK: Body

	public var body: some ReducerOf<Self> {
		Reduce { state, action in
			switch action {

			// Delegate

			case .delegate:
				return .none

			// View

			case .view(.didTapDeleteAccount):
				return .send(.delegate(.shouldDeleteProfile))

			case .view(.didTapSaveButton):
				return .send(._private(.saveProfile))

			case let .view(.didUpdateAbout(about)):
				return .send(._private(.setAbout(about)))

			case let .view(.didUpdateAvatarImage(data)):
				return .send(._private(.setAvatarImage(data)))

			case let .view(.didUpdateNickname(nickname)):
				return .send(._private(.setNickname(nickname)))

			case let .view(.didUpdateBirthDate(birthDate)):
				return .send(._private(.setBirthDate(birthDate)))

			case let .view(.didUpdateOwnGender(gender)):
				return .send(._private(.setOwnGender(gender)))

			case let .view(.didUpdateTargetGenders(targetGenders)):
				return .send(._private(.setTargetGenders(targetGenders)))

			case let .view(.didUpdateSelectedHobbies(selectedHobbies)):
				return .send(._private(.setSelectedHobbies(selectedHobbies)))

			case let .view(.didAddNewGalleryEntry(imageData: data)):
				return .send(._private(.addNewGalleryEntry(data)))

			case let .view(.didTapRemoveGalleryEntry(entry)):
				return .send(._private(.removeGalleryEntry(entry)))

			case let .view(.didSetActiveGalleryEntry(entry)):
				return .send(._private(.setActiveGalleryEntry(entry)))

			// Private

			case ._private(.saveProfile):
				do {
					let output = try state.output()
					return .send(.delegate(.shouldSaveProfile(editorOutput: output)))
				} catch let error {
					#warning("TODO: Show warning toast to a user based on the field error kind.")
					log.error("Cannot save profile due to \(error).")
					return .none
				}

			case let ._private(.setAbout(about)):
				state.about = about.stringPrefix(state.aboutFieldCharacterLimit)
				return .none

			case let ._private(.setAvatarImage(image)):
				state.avatarImage = image
				return .none

			case let ._private(.setNickname(nickname)):
				state.nickname = nickname.stringPrefix(state.nicknameFieldCharacterLimit)
				return .none

			case let ._private(.setOwnGender(gender)):
				state.ownGender = gender
				return .none

			case let ._private(.setTargetGenders(targetGenders)):
				state.targetGenders = targetGenders
				return .none

			case let ._private(.setBirthDate(date)):
				state.birthDate = date
				return .none

			case let ._private(.setSelectedHobbies(selectedHobbies)):
				state.selectedHobbies = selectedHobbies
				return .none

			case let ._private(.setActiveGalleryEntry(entry)):
				state.activeGalleryEntry = entry
				return .none

			case let ._private(.removeGalleryEntry(galleryEditorEntry)):
				guard
					let removedEntryIndex = state.gallery.firstIndex(of: galleryEditorEntry),
					let removedEntry = state.gallery.remove(galleryEditorEntry)
				else {
					log.error("Entry \(galleryEditorEntry) not found and cannot be removed.")
					return .none
				}
				if case let .existingEntry(galleryEntry) = removedEntry {
					state.galleryEntriesToRemove.append(galleryEntry)
				}
				if state.activeGalleryEntry?.id == removedEntry.id {
					if state.gallery.isEmpty {
						return .send(._private(.setActiveGalleryEntry(nil)))
					} else {
						let clampedIndex = clamp(removedEntryIndex, to: ClosedRange(state.gallery.indices))
						return .send(._private(.setActiveGalleryEntry(state.gallery[safe: clampedIndex])))
					}
				} else {
					return .none
				}

			case let ._private(.addNewGalleryEntry(imageData)):
				let randomId: GalleryEntryID = {
					let existingIds = Set(state.gallery.map(\.id) + state.galleryEntriesToRemove.map(\.id))
					while true {
						let candidateId: GalleryEntryID = .random(in: -10_000...10_000)
						if existingIds.contains(candidateId) {
							continue
						} else {
							return candidateId
						}
					}
				}()
				let newGalleryEditorEntry: State.GalleryPickerEntry = .newEntry(randomId, imageData)
				state.gallery.append(newGalleryEditorEntry)
				return .send(._private(.setActiveGalleryEntry(newGalleryEditorEntry)))
			}
		}
	}

}

// MARK: - Action

extension ProfileEditorRedactor {

	public enum Action: Equatable, Sendable {

		// MARK: Case

		case delegate(Delegate)

		case view(View)

		case _private(Private)

	}

}

// MARK: - Action: delegate

extension ProfileEditorRedactor.Action {

	public enum Delegate: Equatable, Sendable {

		// MARK: Case

		case shouldDeleteProfile

		case shouldSaveProfile(editorOutput: ProfileEditorRedactor.State.Output)

	}

}

// MARK: - Action: view

extension ProfileEditorRedactor.Action {

	public enum View: Equatable, Sendable {

		// MARK: Case

		case didTapSaveButton

		case didTapDeleteAccount

		case didUpdateAbout(String)

		case didUpdateAvatarImage(ImageRepresentation?)

		case didUpdateBirthDate(Date?)

		case didUpdateNickname(String)

		case didUpdateOwnGender(Gender?)

		case didUpdateTargetGenders(Set<Gender>)

		case didUpdateSelectedHobbies(IdentifiedArrayOf<Hobby>)

		case didTapRemoveGalleryEntry(ProfileEditorRedactor.State.GalleryPickerEntry)

		case didAddNewGalleryEntry(imageData: Data)

		case didSetActiveGalleryEntry(ProfileEditorRedactor.State.GalleryPickerEntry?)

	}

}

// MARK: - Action: delegate

extension ProfileEditorRedactor.Action {

	public enum Private: Equatable, Sendable {

		// MARK: Case

		case saveProfile

		case setAbout(String)

		case setAvatarImage(ImageRepresentation?)

		case setBirthDate(Date?)

		case setNickname(String)

		case setOwnGender(Gender?)

		case setTargetGenders(Set<Gender>)

		case setSelectedHobbies(IdentifiedArrayOf<Hobby>)

		case setActiveGalleryEntry(ProfileEditorRedactor.State.GalleryPickerEntry?)

		case removeGalleryEntry(ProfileEditorRedactor.State.GalleryPickerEntry)

		case addNewGalleryEntry(Data)

	}

}

// MARK: - State

extension ProfileEditorRedactor {

	public struct State: Equatable, Sendable {

		// MARK: Exposed properties

		// Primary properties

		public var about: String

		public var avatarImage: ImageRepresentation?

		public var birthDate: Date?

		public var nickname: String

		public var ownGender: Gender?

		public var targetGenders: Set<Gender>

		public var allHobbies: IdentifiedArrayOf<Hobby>

		public var selectedHobbies: IdentifiedArrayOf<Hobby>

		public var activeGalleryEntry: GalleryPickerEntry?

		public var gallery: IdentifiedArrayOf<GalleryPickerEntry>

		public var galleryEntriesToRemove: IdentifiedArrayOf<GalleryEntry>

		public var isSaveButtonEnabled: Bool {
			return (try? output()).isNotNil
		}

		// Secondary properties

		public let photoPickerTitle: String = L10n.signUpInfoStepPhotoPickerTitle

		public let pickedPhotoMaxSizeInKB: UInt = Constants.ProfileEditor.maxImageSizeInKB

		public let nicknameFieldTitle: String = L10n.ownProfileEditorNicknameTextFieldTitle

		public let nicknameFieldCharacterLimit: Int = Constants.ProfileEditor.maxNicknameCharactersCount

		public var nicknameFieldFooter: String {
			return L10n.ownProfileEditorNicknameTextFieldFooter(
				nickname.count,
				nicknameFieldCharacterLimit
			)
		}

		public let ownGenderPickerTitle: String = L10n.ownProfileEditorOwnGenderPickerTitle

		public let birthDatePickerTitle: String = L10n.ownProfileEditorBirthDatePickerTitle

		public let birthDateRange: ClosedRange<Date>

		public let birthDateDisplayFormat: String = Constants.ProfileEditor.birthDateDisplayFormat

		public let birthDatePlaceholder: String = L10n.ownProfileEditorBirthDatePickerPlaceholder

		public let birthDateDoneButtonTitle: String = L10n.ownProfileEditorBirthDatePickerDone

		public let targetGendersPickerTitle: String = L10n.ownProfileEditorTargetGendersPickerTitle

		public let hobbiesPickerSelectionLimit: Int = Constants.ProfileEditor.maxSelectedHobbies

		public let hobbiesPickerTitle: String = L10n.ownProfileEditorHobbyPickerTitle

		public var hobbiesPickerFooter: String {
			return L10n.ownProfileEditorHobbyPickerNumberOfSelectHobbies(
				selectedHobbies.count,
				hobbiesPickerSelectionLimit
			)
		}

		public let galleryPickerTitle: String = L10n.ownProfileEditorGalleryPickerTitle

		public let galleryPickerEntryLimit: Int = Constants.ProfileEditor.maxGalleryEntryCount

		public var galleryPiclerFooter: String {
			return L10n.ownProfileEditorGalleryPickerFooter(
				gallery.count,
				galleryPickerEntryLimit
			)
		}

		public let aboutFieldCharacterLimit: Int = Constants.ProfileEditor.maxAboutCharactersCount

		public let aboutFieldTitle: String = L10n.ownProfileEditorAboutTextFieldTitle

		public var aboutFieldFooterLabel: String {
			return L10n.ownProfileEditorAboutTextFieldFooter(
				about.count,
				aboutFieldCharacterLimit
			)
		}

		public let saveButtonTitle: String = L10n.ownProfileEditorSaveButtonTitle

		public let deleteAccountButtonTitle: String = L10n.ownProfileEditorDeleteAccountButtonTitle

		public var versionFooterTitle: String {
			let version = "\(SystemInformation.appVersion):\(SystemInformation.buildNumber)"
			let buildMode = (SystemInformation.isDebugBuild ? "debug" : "release")
			return L10n.versionDescription("\(version).\(buildMode)")
		}

		// MARK: Init

		public init(
			ownProfile: OwnProfile,
			allHobbies: IdentifiedArrayOf<Hobby>
		) {
			@Dependency(\.calendar) var calendar
			@Dependency(\.date.now) var now
			let toDate = now
				.rounded(to: .day, using: calendar)?
				.subtracting(.year, value: Constants.ProfileEditor.minAllowedAge)
			let fromDate = now
				.rounded(to: .day, using: calendar)?
				.subtracting(.year, value: Constants.ProfileEditor.maxAllowedAge)
			self.birthDateRange = ClosedRange(uncheckedBounds: (
				fromDate.unwrapped(or: .distantPast),
				toDate.unwrapped(or: now)
			))
			self.avatarImage = .url(ownProfile.avatarUrl)
			self.nickname = ownProfile.nickname
			self.birthDate = ownProfile.birthDate
			self.allHobbies = allHobbies
			self.selectedHobbies = IdentifiedArrayOf(
				uniqueElements: ownProfile.hobbies
			)
			self.gallery = IdentifiedArrayOf(
				uniqueElements: ownProfile.gallery.map(GalleryPickerEntry.existingEntry)
			)
			self.activeGalleryEntry = ownProfile.gallery.first.flatMap(GalleryPickerEntry.existingEntry)
			self.galleryEntriesToRemove = []
			self.ownGender = ownProfile.gender
			self.targetGenders = ownProfile.targetGenders
			self.about = ownProfile.about
		}

		// MARK: Exposed methods

		public func output() throws -> Output {
			guard let ownGender else {
				throw Error.ownGenderNotSelected
			}
			guard let birthDate, birthDateRange.contains(birthDate) else {
				throw Error.invalidBirthdate
			}
			guard !targetGenders.isEmpty else {
				throw Error.noTargetGendersSelected
			}
			guard nickname.trimmingCharacters(in: .whitespacesAndNewlines).isNotEmpty else {
				throw Error.nicknameEmpty
			}
			guard about.trimmingCharacters(in: .whitespacesAndNewlines).isNotEmpty else {
				throw Error.aboutEmpty
			}
			let avatarImageData: Data? = {
				if case let .data(imageData) = avatarImage {
					return imageData
				} else {
					return nil
				}
			}()
			let galleryImagesToAdd: [Data] = gallery.compactMap { entry in
				if case let .newEntry(_, data) = entry {
					return data
				} else {
					return nil
				}
			}
			return Output(
				avatarImageData: avatarImageData,
				birthDate: birthDate,
				nickname: nickname.trimmingCharacters(in: .whitespacesAndNewlines),
				about: about.trimmingCharacters(in: .whitespacesAndNewlines),
				hobbies: selectedHobbies,
				ownGender: ownGender,
				targetGenders: targetGenders,
				galleryEntriesToRemove: Array(galleryEntriesToRemove),
				galleryImagesToAdd: galleryImagesToAdd
			)
		}

	}

}

// MARK: - State: error

extension ProfileEditorRedactor.State {

	public enum Error: Swift.Error, Equatable {

		// MARK: Case

		case ownGenderNotSelected

		case nicknameEmpty

		case aboutEmpty

		case noTargetGendersSelected

		case invalidBirthdate

	}

}

// MARK: - State: output

extension ProfileEditorRedactor.State {

	public struct Output: Equatable, Sendable {

		// MARK: Exposed properties

		public var avatarImageData: Data?

		public var birthDate: Date

		public var nickname: String

		public var about: String

		public var ownGender: Gender

		public var targetGenders: Set<Gender>

		public var hobbies: IdentifiedArrayOf<Hobby>

		public var galleryEntriesToRemove: [GalleryEntry]

		public var galleryImagesToAdd: [Data]

		// MARK: Init

		public init(
			avatarImageData: Data?,
			birthDate: Date,
			nickname: String,
			about: String,
			hobbies: IdentifiedArrayOf<Hobby>,
			ownGender: Gender,
			targetGenders: Set<Gender>,
			galleryEntriesToRemove: [GalleryEntry],
			galleryImagesToAdd: [Data]
		) {
			self.avatarImageData = avatarImageData
			self.birthDate = birthDate
			self.nickname = nickname
			self.hobbies = hobbies
			self.ownGender = ownGender
			self.about = about
			self.targetGenders = targetGenders
			self.galleryEntriesToRemove = galleryEntriesToRemove
			self.galleryImagesToAdd = galleryImagesToAdd
		}

	}

}

// MARK: - State: entry

extension ProfileEditorRedactor.State {

	public enum GalleryPickerEntry: Equatable, Identifiable, Sendable {

		// MARK: Case

		case existingEntry(GalleryEntry)

		case newEntry(GalleryEntryID, Data)

		// MARK: Exposed properties

		public var id: GalleryEntryID {
			switch self {
			case let .existingEntry(entry):
				return entry.entryId
			case let .newEntry(entryId, _):
				return entryId
			}
		}

		// MARK: Exposed methods

		public static func == (_ lhs: GalleryPickerEntry, _ rhs: GalleryPickerEntry) -> Bool {
			return lhs.id == rhs.id
		}

	}

}
