//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import APIClient
import ComposableArchitecture
import Constants
import Extensions
import Foundation
import HobbyClient
import Localizable
import Log
import Models
import SideProfileOverview
import SystemInformation
import ToastClient
import UserClient

// MARK: - Reducer

public struct ProfileEditor: Reducer {

	// MARK: Private properties

	@Dependency(\.calendar) private var calendar

	@Dependency(\.date.now) private var now

	@Dependency(\.hobbyClient) private var hobbyClient

	@Dependency(\.toastClient) private var toastClient

	@Dependency(\.userClient) private var userClient

	// MARK: Cancel ID

	private enum CancelID: Hashable {
		case ownProfileLoad
	}

	// MARK: Init

	public init() {

	}

	// MARK: Body

	public var body: some ReducerOf<Self> {
		Reduce { state, action in
			switch action {

			// Delegate

			case .delegate:
				return .none

			// Editor

			case .ownProfileEditorRedactor(.delegate(.shouldDeleteProfile)):
				return .send(._private(.showDeleteProfileDialog))

			case let .ownProfileEditorRedactor(.delegate(.shouldSaveProfile(editorOutput: output))):
				return .send(._private(.saveEditedProfile(output)))

			case .ownProfileEditorRedactor:
				return .none

			// Profile overview

			case .ownProfileOverview:
				return .none

			// Profile delete dialog

			case .ownProfileDeleteDialog(.presented(.delegate(.didCancelAccountDelete))):
				state.ownProfileDeleteDialog = nil
				return .none

			case let .ownProfileDeleteDialog(.presented(.delegate(.didConfirmAccountDelete(output)))):
				state.ownProfileDeleteDialog = nil
				return .run { send in
					await send(._private(.setIsPerformingProfileDelete(true)))
					try await userClient.deleteProfile(.init(
						reason: output.reason,
						feedbackAvailable: output.isFeedbackAllowed
					))
					await send(._private(.setIsPerformingProfileDelete(false)))
					await send(.delegate(.didDeleteProfile))
				} catch: { error, send in
					await send(._private(.setIsPerformingProfileDelete(false)))
					log.error("Cannot sign-out due to \(error).")
				}

			case .ownProfileDeleteDialog:
				return .none

			// View

			case .view(.didAppearFirstTime):
				return .send(._private(.initialize))

			case .view(.didTapBackButton):
				return .send(.delegate(.shouldClose))

			case .view(.didTapPreviewButton):
				return .send(._private(.openOwnProfilePreview))

			// Private

			case ._private(.initialize):
				return .send(._private(.refreshOwnProfile))

			case ._private(.refreshOwnProfile):
				let existingAllHobbies = state.editorRedactor?.allHobbies
				return .run { send in
					async let ownProfile = try userClient.refreshSession()
					async let allHobbies = try {
						if let existingAllHobbies {
							return existingAllHobbies
						} else {
							return try await IdentifiedArray(uniqueElements: hobbyClient.allHobbyKinds())
						}
					}()
					let editorState = try await ProfileEditorRedactor.State(
						ownProfile: ownProfile,
						allHobbies: allHobbies
					)
					await send(._private(.setOwnProfileEditor(editorState)))
				} catch: { error, _ in
					log.error("Cannot initialize own profile screen due to \(error).")
				}
				.cancellable(id: CancelID.ownProfileLoad, cancelInFlight: true)

			case ._private(.showDeleteProfileDialog):
				state.ownProfileDeleteDialog = .init()
				return .none

			case ._private(.openOwnProfilePreview):
				guard let ownProfile = userClient.ownProfile() else {
					log.error("Cannot find own profile locally.")
					return .none
				}
				var profileOverviewState = SideProfileOverview.State(ownProfile: ownProfile)
				if let editedAvatar = state.editorRedactor?.avatarImage {
					profileOverviewState.avatarImage = editedAvatar
				}
				if let editedNickname = state.editorRedactor?.nickname {
					profileOverviewState.nickname = editedNickname
				}
				if let editedAge = state.editorRedactor?.birthDate?.age(now: now, using: calendar) {
					profileOverviewState.age = editedAge
				}
				if let editedAbout = state.editorRedactor?.about {
					profileOverviewState.about = editedAbout
				}
				if let editedOwnGender = state.editorRedactor?.ownGender {
					profileOverviewState.gender = editedOwnGender
				}
				if let editedTargetGenders = state.editorRedactor?.targetGenders {
					profileOverviewState.targetGenders = editedTargetGenders
				}
				if let editedHobbies = state.editorRedactor?.selectedHobbies {
					profileOverviewState.hobbies = editedHobbies
				}
				if let editedGallery = state.editorRedactor?.gallery {
					let galleryEntries = editedGallery.map { editorEntry in
						switch editorEntry {
						case let .existingEntry(existingEntry):
							return existingEntry
						case let .newEntry(entryId, imageData):
							return GalleryEntry(
								entryId: entryId,
								image: .data(imageData),
								isOwn: true
							)
						}
					}
					profileOverviewState.galleryEntries = IdentifiedArrayOf(uniqueElements: galleryEntries)
				}
				state.ownProfileOverview = profileOverviewState
				return .none

			case let ._private(.saveEditedProfile(output)):
				guard let existingOwnProfile = userClient.ownProfile() else {
					log.error("Own profile cannot be found locally.")
					return .none
				}
				let updateHobbiesRequest: UpdateOwnHobbiesRequest? = {
					let existingHobbies = Set(existingOwnProfile.hobbies)
					let newHobbies = Set(output.hobbies)
					guard existingHobbies != newHobbies else {
						return nil
					}
					return UpdateOwnHobbiesRequest(hobbies: newHobbies)
				}()
				let updateProfileInfoRequest: UpdateOwnProfileRequest? = {
					var request = UpdateOwnProfileRequest(
						imageData: output.avatarImageData,
						payload: nil
					)
					if
						existingOwnProfile.nickname == output.nickname,
						existingOwnProfile.birthDate == output.birthDate,
						existingOwnProfile.gender == output.ownGender,
						existingOwnProfile.about == output.about,
						existingOwnProfile.targetGenders == output.targetGenders
					{
						// Do nothing if every field is the same.
					} else {
						request.payload = .init(
							nickname: output.nickname,
							birthDate: output.birthDate,
							gender: output.ownGender,
							about: output.about,
							targetGenders: output.targetGenders
						)
					}
					if request.imageData == nil, request.payload == nil {
						return nil
					} else {
						return request
					}
				}()
				let updateGalleryAddRequest: OwnGalleryPhotoAddRequest? = {
					if output.galleryImagesToAdd.isNotEmpty {
						return OwnGalleryPhotoAddRequest(imagesData: output.galleryImagesToAdd)
					} else {
						return nil
					}
				}()
				let updateGalleryRemoveRequests: [OwnGalleryPhotoDeleteRequest] = {
					return output.galleryEntriesToRemove
						.map(\.entryId)
						.map(OwnGalleryPhotoDeleteRequest.init)
				}()
				return .run { send in
					await send(._private(.setIsPerformingProfileSave(true)))
					if let updateHobbiesRequest {
						try await userClient.updateHobbies(updateHobbiesRequest)
					}
					if let updateGalleryAddRequest {
						try await userClient.updateGalleryWithNewEntries(updateGalleryAddRequest)
					}
					for updateGalleryRemoveRequest in updateGalleryRemoveRequests {
						try await userClient.deleteEntryFromGallery(updateGalleryRemoveRequest)
					}
					if let updateProfileInfoRequest {
						try await userClient.updateProfile(updateProfileInfoRequest)
					}
					await send(._private(.refreshOwnProfile))
					await toastClient.present(.success(
						L10n.ownProfileEditorToastChangesSavedTitle
					))
					await send(._private(.setIsPerformingProfileSave(false)))
				} catch: { _, send in
					await send(._private(.refreshOwnProfile))
					await toastClient.present(.error(
						L10n.ownProfileEditorToastChangesNotSavedTitle,
						subtitle: L10n.ownProfileEditorToastChangesNotSavedDescription
					))
					await send(._private(.setIsPerformingProfileSave(false)))
				}

			case let ._private(.setOwnProfileEditor(editorState)):
				state.editorRedactor = editorState
				return .none

			case let ._private(.setIsPerformingProfileDelete(isPerformingProfileDelete)):
				state.isPerformingProfileDelete = isPerformingProfileDelete
				return .none

			case let ._private(.setIsPerformingProfileSave(isPerformingProfileUpdate)):
				state.isPerformingProfileUpdate = isPerformingProfileUpdate
				return .none
			}
		}
		.ifLet(\.editorRedactor, action: /Action.ownProfileEditorRedactor) {
			ProfileEditorRedactor()
		}
		.ifLet(\.$ownProfileDeleteDialog, action: /Action.ownProfileDeleteDialog) {
			ProfileEditorDelete()
		}
		.ifLet(\.$ownProfileOverview, action: /Action.ownProfileOverview) {
			SideProfileOverview()
		}
	}

}

// MARK: - Action

extension ProfileEditor {

	public enum Action: Equatable, Sendable {

		// MARK: Case

		case delegate(Delegate)

		case ownProfileEditorRedactor(ProfileEditorRedactor.Action)

		case ownProfileDeleteDialog(PresentationAction<ProfileEditorDelete.Action>)

		case ownProfileOverview(PresentationAction<SideProfileOverview.Action>)

		case view(View)

		case _private(Private)

	}

}

// MARK: - Action: delegate

extension ProfileEditor.Action {

	public enum Delegate: Equatable, Sendable {

		// MARK: Case

		case didDeleteProfile

		case shouldClose

	}

}

// MARK: - Action: view

extension ProfileEditor.Action {

	public enum View: Equatable, Sendable {

		// MARK: Case

		case didAppearFirstTime

		case didTapPreviewButton

		case didTapBackButton

	}

}

// MARK: - Action: delegate

extension ProfileEditor.Action {

	public enum Private: Equatable, Sendable {

		// MARK: Case

		case initialize

		case refreshOwnProfile

		case showDeleteProfileDialog

		case openOwnProfilePreview

		case saveEditedProfile(ProfileEditorRedactor.State.Output)

		case setOwnProfileEditor(ProfileEditorRedactor.State)

		case setIsPerformingProfileSave(Bool)

		case setIsPerformingProfileDelete(Bool)

	}

}

// MARK: - State

extension ProfileEditor {

	public struct State: Equatable, Sendable {

		// MARK: Exposed properties

		public var isPerformingProfileDelete: Bool

		public var isPerformingProfileUpdate: Bool

		public var areUserInteractionsEnabled: Bool {
			guard !isPerformingProfileDelete else {
				return false
			}
			guard !isPerformingProfileUpdate else {
				return false
			}
			return true
		}

		public let headerTitle: String = L10n.ownProfileEditorHeaderTitle

		public var editorRedactor: ProfileEditorRedactor.State?

		@PresentationState public var ownProfileOverview: SideProfileOverview.State?

		@PresentationState public var ownProfileDeleteDialog: ProfileEditorDelete.State?

		// MARK: Init

		public init(
			editorRedactor: ProfileEditorRedactor.State? = nil,
			ownProfileOverview: SideProfileOverview.State? = nil,
			ownProfileDeleteDialog: ProfileEditorDelete.State? = nil
		) {
			self.isPerformingProfileDelete = false
			self.isPerformingProfileUpdate = false
			self.editorRedactor = editorRedactor
			self.ownProfileDeleteDialog = ownProfileDeleteDialog
			self.ownProfileOverview = ownProfileOverview
		}

	}

}
