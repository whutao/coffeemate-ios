//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Constants
import Extensions
import Foundation
import Localizable

// MARK: - Reducer

public struct ProfileEditorDelete: Reducer {

	// MARK: Init

	public init() {

	}

	// MARK: Body

	public var body: some ReducerOf<Self> {
		Reduce { state, action in
			switch action {

			// Delegate

			case .delegate:
				return .none

			// View

			case .view(.didTapCancelButton):
				return .send(.delegate(.didCancelAccountDelete))

			case .view(.didTapConfirmButton):
				return .send(.delegate(.didConfirmAccountDelete(state.output())))

			case let .view(.didChangeReasonText(reason)):
				return .send(._private(.updateReasonText(reason)))

			case let .view(.didSelectFeedbackAllowed(isFeedbackAllowed)):
				return .send(._private(.setFeedbackAllowed(isFeedbackAllowed)))

			// Private

			case let ._private(.setFeedbackAllowed(isFeedbackAllowed)):
				state.isFeedbackAllowed = isFeedbackAllowed
				return .none

			case let ._private(.updateReasonText(reason)):
				state.reason = reason
				return .none
			}
		}
	}

}

// MARK: - Action

extension ProfileEditorDelete {

	public enum Action: Equatable, Sendable {

		// MARK: Case

		case delegate(Delegate)

		case view(View)

		case _private(Private)

	}

}

// MARK: - Action: delegate

extension ProfileEditorDelete.Action {

	public enum Delegate: Equatable, Sendable {

		// MARK: Case

		case didCancelAccountDelete

		case didConfirmAccountDelete(ProfileEditorDelete.State.Output)

	}

}

// MARK: - Action: view

extension ProfileEditorDelete.Action {

	public enum View: Equatable, Sendable {

		// MARK: Case

		case didChangeReasonText(String)

		case didSelectFeedbackAllowed(Bool)

		case didTapCancelButton

		case didTapConfirmButton

	}

}

// MARK: - Action: private

extension ProfileEditorDelete.Action {

	public enum Private: Equatable, Sendable {

		// MARK: Case

		case setFeedbackAllowed(Bool)

		case updateReasonText(String)

	}

}

// MARK: - State

extension ProfileEditorDelete {

	public struct State: Equatable, Sendable {

		// MARK: Private properties

		public var reason: String

		public var isFeedbackAllowed: Bool

		public let confirmButtonTitle: String = L10n.ownProfileDeleteDialogConfirmButtonTitle

		public let reasonFieldTitle: String = L10n.ownProfileDeleteDialogReasonFieldTitle

		public var reasonFieldFooter: String {
			return L10n.ownProfileDeleteDialogReasonFieldFooter(
				reason.count,
				Constants.ProfileEditor.maxDeleteProfileReasonCharactersCount
			)
		}

		public let feedbackAllowedFieldTitle: String = L10n.ownProfileDeleteDialogFeedbackFieldTitle

		// MARK: Init

		public init(
			reason: String = .empty,
			isFeedbackAllowed: Bool = true
		) {
			self.reason = reason
			self.isFeedbackAllowed = isFeedbackAllowed
		}

		// MARK: Exposed properties

		public func output() -> Output {
			return Output(reason: reason, isFeedbackAllowed: isFeedbackAllowed)
		}

	}

}

// MARK: - State: output

extension ProfileEditorDelete.State {

	public struct Output: Equatable, Sendable {

		// MARK: Exposed properties

		public var reason: String

		public var isFeedbackAllowed: Bool

		// MARK: Init

		public init(reason: String, isFeedbackAllowed: Bool) {
			self.reason = reason
			self.isFeedbackAllowed = isFeedbackAllowed
		}

	}

}
