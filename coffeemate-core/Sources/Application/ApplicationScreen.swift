//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Home
import SignIn
import SignUp
import Splash

// MARK: - Reducer

public struct ApplicationScreen: Reducer {

	// MARK: Init

	public init() {

	}

	// MARK: Body

	public var body: some ReducerOf<Self> {
		EmptyReducer()
			.ifCaseLet(/State.signIn, action: /Action.signIn) {
				SignIn()
			}
			.ifCaseLet(/State.signUp, action: /Action.signUp) {
				SignUp()
			}
			.ifCaseLet(/State.home, action: /Action.home) {
				Home()
			}
			.ifCaseLet(/State.splash, action: /Action.splash) {
				Splash()
			}
	}

}

// MARK: - Action

extension ApplicationScreen {

	public enum Action: Equatable, Sendable {

		// MARK: Case

		case signIn(SignIn.Action)

		case signUp(SignUp.Action)

		case home(Home.Action)

		case splash(Splash.Action)

	}

}

// MARK: - State

extension ApplicationScreen {

	public enum State: Equatable, Sendable {

		// MARK: Case

		case signIn(SignIn.State)

		case signUp(SignUp.State)

		case home(Home.State)

		case splash(Splash.State)

	}

}
