//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import APIClient
import ComposableArchitecture
import Extensions
import FirebaseClient
import GoogleAuthClient
import UIKit

// MARK: - AppDelegate

public final class ApplicationDelegate: NSObject, UIApplicationDelegate {

	// MARK: Private properties

	@Dependency(\.firebaseClient) private var firebase

	@Dependency(\.googleAuthClient) private var googleAuth

	// MARK: Exposed properties

	public private(set) lazy var store: StoreOf<Application> = Store(
		initialState: .setupDuringSplash(),
		reducer: Application.init
	)

	// MARK: Exposed methods

	public func application(
		_ application: UIApplication,
		didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil
	) -> Bool {
		firebase.configure()
		store.send(.delegate(.didFinishLaunching))
		return true
	}

	public func application(
		_ app: UIApplication,
		open url: URL,
		options: [UIApplication.OpenURLOptionsKey: Any] = [:]
	) -> Bool {
		if googleAuth.handleUrl(url) {
			return true
		} else {
			return false
		}
	}

	public func application(
		_ application: UIApplication,
		didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data
	) {
		store.send(.delegate(.didRegisterForRemoteNotifications(deviceToken: deviceToken)))
	}

	public func application(
		_ application: UIApplication,
		didFailToRegisterForRemoteNotificationsWithError error: Error
	) {
		store.send(.delegate(.didFailToRegisterForRemoteNotifications(error: error)))
	}

	public func applicationDidReceiveMemoryWarning(_ application: UIApplication) {
		store.send(.delegate(.didReceiveMemoryWarning))
	}

}
