//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Extensions
import Foundation
import ToastClient

// MARK: - Reducer

public struct ApplicationToast: Reducer {

	// MARK: Private properties

	@Dependency(\.toastClient) private var toastClient

	// MARK: CancelID

	private enum CancelID: Hashable {
		case toastObserving
	}

	// MARK: Init

	public init() {

	}

	// MARK: Body

	public var body: some ReducerOf<Self> {
		Reduce { state, action in
			switch action {

			// Exposed

			case .enable:
				return .send(._private(.startToastObserving))

			case .disable:
				return .send(._private(.stopToastObserving))

			// View

			case .view(.didDismissCurrentToast):
				return .send(._private(.removeCurrentToastIfPossible))

			// Private

			case ._private(.startToastObserving):
				return .run { send in
					log.info("Toasts are being observed from now.")
					for await toast in toastClient.observer() {
						await send(._private(.appendToast(toast)))
					}
				}
				.cancellable(id: CancelID.toastObserving, cancelInFlight: true)

			case ._private(.stopToastObserving):
				log.info("Toasts are not being observed anymore.")
				return .cancel(id: CancelID.toastObserving)

			case let ._private(.appendToast(toast)):
				state.toasts.append(toast)
				log.verbose("Toast \"\(toast)\" has been added to the queue.")
				return .none

			case ._private(.removeCurrentToastIfPossible):
				if state.toasts.isNotEmpty {
					state.toasts.removeFirst()
				}
				return .none
			}
		}
	}

}

// MARK: - Action

extension ApplicationToast {

	public enum Action: Equatable, Sendable {

		// MARK: Case

		case disable

		case enable

		case view(View)

		case _private(Private)

	}

}

// MARK: - Action: private

extension ApplicationToast.Action {

	public enum Private: Equatable, Sendable {

		// MARK: Case

		case appendToast(Toast)

		case startToastObserving

		case stopToastObserving

		case removeCurrentToastIfPossible

	}

}

// MARK: - Action: view

extension ApplicationToast.Action {

	public enum View: Equatable, Sendable {

		// MARK: Case

		case didDismissCurrentToast

	}

}

// MARK: - State

extension ApplicationToast {

	public struct State: Equatable, Sendable {

		// MARK: Exposed properties

		public var isPresenting: Bool {
			return toasts.isNotEmpty
		}

		public var currentToast: Toast? {
			return toasts.first
		}

		public var toastDuration: TimeInterval {
			return 5.0
		}

		public var toasts: [Toast]

		// MARK: Init

		public init(toasts: [Toast] = []) {
			self.toasts = toasts
		}

	}

}
