//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import APIClient
import ComposableArchitecture
import Constants
import CountdownTimer
import Extensions
import Home
import SignIn
import SignUp
import Splash
import SwiftUI
import SystemInformation
import ToastClient
import UserClient
import UserNotificationsClient

// MARK: - Reducer

public struct Application: Reducer {

	// MARK: Private properties

	@Dependency(\.apiClient) private var apiClient

	@Dependency(\.locationClient) private var locationClient

	@Dependency(\.toastClient) private var toastClient

	@Dependency(\.userClient) private var userClient

	@Dependency(\.userNotificationsClient) private var userNotifications

	// MARK: Init

	public init() {

	}

	// MARK: Body

	public var body: some ReducerOf<Self> {
		Scope(state: \.location, action: /Action.location) {
			ApplicationLocation()
		}
		Scope(state: \.screen, action: /Action.screen) {
			ApplicationScreen()
		}
		Scope(state: \.toast, action: /Action.toast) {
			ApplicationToast()
		}
		Reduce { state, action in
			switch action {

			// Delegate

			case .delegate(.didFinishLaunching):
				return .send(._private(.setupOnLaunch))

			case let .delegate(.didRegisterForRemoteNotifications(deviceToken: token)):
				return .run { _ in
					await userNotifications.registerWithDeviceToken(token)
				}

			case let .delegate(.didFailToRegisterForRemoteNotifications(error: error)):
				return .run { _ in
					await userNotifications.failToRegisterWithError(error)
				}

			case .delegate(.didReceiveMemoryWarning):
				log.warning("Did receive memory warning.")
				return .none

			case .delegate:
				return .none

			// Location

			case .location:
				return .none

			// Screen

			case .screen(.splash(.delegate(.shouldClose))):
				return .send(._private(.setSplashFinished))

			case .screen(.signUp(.delegate(.didSignUp))):
				state.screen = .home(.init())
				log.info("Home screen is selected after successful sign-up.")
				return .none

			case .screen(.signIn(.delegate(.didSignInForNewAccount))):
				state.screen = .signUp(.init())
				log.info("Sign-up screen is selected after sign-in attempt.")
				return .none

			case .screen(.signIn(.delegate(.didSignInForExistingAccount))):
				state.screen = .home(.init())
				log.info("Home screen is selected after successful sign-in.")
				return .none

			case .screen(.home(.delegate(.didSignOut))):
				state.screen = .signIn(.init())
				log.info("Sign-in screen is selected after successful sign-out.")
				return .send(._private(.resetRecentLocation))

			case .screen(.home(.delegate(.didDeleteProfile))):
				state.screen = .signIn(.init())
				log.info("Sign-in screen is selected after successful profile delete.")
				return .send(._private(.resetRecentLocation))

			case .screen:
				return .none

			// Toast

			case .toast:
				return .none

			// Private

			case ._private(.setSetupOnLaunchFinished):
				state.isSetupOnLaunchFinished = true
				log.info("Setup on launch has successfully finished.")
				if state.canSetScreenAfterLaunch {
					return .send(._private(.setScreenAfterLaunch))
				} else {
					return .none
				}

			case ._private(.setSplashFinished):
				state.isSplashFinished = true
				log.info("Splash has successfully finished.")
				if state.canSetScreenAfterLaunch {
					return .send(._private(.setScreenAfterLaunch))
				} else {
					return .none
				}

			case let ._private(.setPreferredScreenAfterLaunch(screen)):
				state.preferredScreenAfterLaunch = screen
				return .none

			case ._private(.setScreenAfterLaunch):
				switch state.preferredScreenAfterLaunch {
				case .home:
					state.screen = .home(.init())
					log.info("Home screen is selected after launch.")
				case .signIn:
					state.screen = .signIn(.init())
					log.info("Sign-in screen is selected after launch.")
				case .signUp:
					state.screen = .signUp(.init())
					log.info("Sign-up screen is selected after launch.")
				}
				return .none

			case ._private(.setupOnLaunch):
				return .run { send in
					/// Enable showing global toasts if needed.
					if Constants.areToastsEnabled {
						await send(.toast(.enable))
					}
					/// Configure location client.
					await locationClient.configure()
					/// Enable global location observing.
					await send(.location(.enable))
					/// Set up notifications client.
					await userNotifications.configure()
					/// Set up credentials.
					if let accessToken = userClient.accessToken() {
						log.info("Existing access token has been found on launch.")
						apiClient.setAccessToken(accessToken.value)
						do {
							_ = try await userClient.refreshSession()
							log.info("Session has been successfully refreshed on launch.")
							await send(._private(.setPreferredScreenAfterLaunch(.home)))
						} catch UserClient.Error.unexistingAccount {
							log.info("Session has been successfully refreshed BUT an account should be created.")
							await send(._private(.setPreferredScreenAfterLaunch(.signUp)))
						} catch let error {
							if let error = error as? APIError, error.isUnauthorized {
								log.info("Access token expired and explicit sing-in required.")
							} else {
								log.error("Cannot refresh session due to \(error).")
							}
						}
					} else {
						log.info("Existing access token has NOT been found on launch.")
						/// If session no access token found,
						/// but session is restored using Google credentials,
						/// set up-to-date access token to the API client.
						log.info("Trying to sign-in using existing Google credentials.")
						do {
							if try await userClient.signInRestoringGoogleCredentials() {
								log.info("Signed-in using Google credentials.")
								await send(._private(.setPreferredScreenAfterLaunch(.home)))
							} else {
								log.info("No Google credentials found.")
							}
						} catch UserClient.Error.unexistingAccount {
							log.info("Session has been successfully restored BUT an account should be created.")
							await send(._private(.setPreferredScreenAfterLaunch(.signUp)))
						} catch let error {
							log.error("Cannot restore session using Google sign-in due to \(error).")
						}
					}
					await send(._private(.setSetupOnLaunchFinished))
				}

			case ._private(.resetRecentLocation):
				state.location.recentUserLocation = nil
				return .none
			}
		}
	}

}

// MARK: - Action

extension Application {

	public enum Action: Equatable {

		// MARK: Case

		case delegate(Delegate)

		case location(ApplicationLocation.Action)

		case screen(ApplicationScreen.Action)

		case toast(ApplicationToast.Action)

		case _private(Private)

	}

}

// MARK: - Action: delegate

extension Application.Action {

	public enum Delegate: Equatable, Sendable {

		// MARK: Case

		case didFinishLaunching

		case didRegisterForRemoteNotifications(deviceToken: Data)

		case didFailToRegisterForRemoteNotifications(error: Error)

		case didReceiveMemoryWarning

		// MARK: Equatable

		public static func == (
			lhs: Application.Action.Delegate,
			rhs: Application.Action.Delegate
		) -> Bool {
			switch (lhs, rhs) {
			case (.didFinishLaunching, .didFinishLaunching),
				(.didReceiveMemoryWarning, .didReceiveMemoryWarning),
				(.didRegisterForRemoteNotifications, .didRegisterForRemoteNotifications),
				(.didFailToRegisterForRemoteNotifications, .didFailToRegisterForRemoteNotifications):
				return true
			default:
				return false
			}
		}

	}

}

// MARK: - Action: private

extension Application.Action {

	public enum Private: Equatable, Sendable {

		// MARK: Case

		case setupOnLaunch

		case setSplashFinished

		case setSetupOnLaunchFinished

		case setScreenAfterLaunch

		case setPreferredScreenAfterLaunch(Application.State.PreferredScreenAfterLaunch)

		case resetRecentLocation

	}

}

// MARK: - State

extension Application {

	public struct State: Equatable {

		// MARK: Exposed properties

		public var canSetScreenAfterLaunch: Bool {
			return isSetupOnLaunchFinished && isSplashFinished
		}

		public var isSetupOnLaunchFinished: Bool

		public var isSplashFinished: Bool

		public var preferredScreenAfterLaunch: PreferredScreenAfterLaunch

		public var location: ApplicationLocation.State

		public var screen: ApplicationScreen.State

		public var toast: ApplicationToast.State

		// MARK: Init

		private init(
			isSetupOnLaunchFinished: Bool,
			isSplashFinished: Bool,
			preferredScreenAfterLaunch: PreferredScreenAfterLaunch,
			location: ApplicationLocation.State,
			screen: ApplicationScreen.State,
			toast: ApplicationToast.State
		) {
			self.isSetupOnLaunchFinished = isSetupOnLaunchFinished
			self.isSplashFinished = isSplashFinished
			self.location = location
			self.screen = screen
			self.toast = toast
			self.preferredScreenAfterLaunch = preferredScreenAfterLaunch
		}

		// MARK: Exposed properties

		public static func setupDuringSplash(
			location: ApplicationLocation.State = .init(),
			toast: ApplicationToast.State = .init()
		) -> State {
			return State(
				isSetupOnLaunchFinished: false,
				isSplashFinished: false,
				preferredScreenAfterLaunch: .signIn,
				location: location,
				screen: .splash(.init(
					timer: .init(totalTicks: Constants.Splash.splashDurationInSeconds),
					title: "COFFEE MATE"
				)),
				toast: toast
			)
		}

	}

}

// MARK: - State: PreferredScreenAfterLaunch

extension Application.State {

	public enum PreferredScreenAfterLaunch: Equatable, Sendable {

		// MARK: Case

		case home

		case signIn

		case signUp

	}

}
