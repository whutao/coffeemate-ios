//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import APIClient
import ComposableArchitecture
import Constants
import Foundation
import LocationClient

// MARK: - Reducer

public struct ApplicationLocation: Reducer {

	// MARK: Private properties

	@Dependency(\.apiClient) private var apiClient

	@Dependency(\.locationClient) private var locationClient

	// MARK: Cancel ID

	private enum CancelID: Hashable {
		case locationObserving
	}

	// MARK: Init

	public init() {

	}

	// MARK: Body

	public var body: some ReducerOf<Self> {
		Reduce { state, action in
			switch action {
			case .enable:
				return .send(._private(.startLocationObserving))

			case .disable:
				return .send(._private(.stopLocationObserving))

			case ._private(.startLocationObserving):
				return .run { send in
					log.info("User locations are being observed from now.")
					for await locationEvent in locationClient.observer() {
						if case let .userLocation(location) = locationEvent {
							await send(._private(.processLocation(location)))
						}
					}
				}
				.cancellable(id: CancelID.locationObserving, cancelInFlight: true)

			case ._private(.stopLocationObserving):
				return .cancel(id: CancelID.locationObserving)

			case let ._private(.setIsUpdatingUserLocation(isUpdatingUserLocation)):
				state.isUpdatingUserLocation = isUpdatingUserLocation
				return .none

			case let ._private(.setRecentLocation(location)):
				state.recentUserLocation = location
				return .none

			case let ._private(.processLocation(location)):
				guard !state.isUpdatingUserLocation else {
					return .none
				}
				if let previousLocation = state.recentUserLocation, previousLocation.distance(from: location) < state.minDistanceDifference {
					log.verbose("Location will not be updated since has not changed a lot.")
					return .none
				} else {
					return .run { send in
						await send(._private(.setIsUpdatingUserLocation(true)))
						do {
							try await apiClient.updateOwnCoordinates(.init(
								latitude: location.latitude,
								longitude: location.longitude
							))
							await send(._private(.setRecentLocation(location)))
						} catch let error {
							log.error("Cannot update user location due to \(error).")
						}
						await send(._private(.setIsUpdatingUserLocation(false)))
					}
				}
			}
		}
	}

}

// MARK: - Action

extension ApplicationLocation {

	public enum Action: Equatable, Sendable {

		// MARK: Case

		case disable

		case enable

		case _private(Private)

	}

}

// MARK: - Action: private

extension ApplicationLocation.Action {

	public enum Private: Equatable, Sendable {

		// MARK: Case

		case processLocation(Location)

		case setIsUpdatingUserLocation(Bool)

		case setRecentLocation(Location?)

		case startLocationObserving

		case stopLocationObserving

	}

}

// MARK: - State

extension ApplicationLocation {

	public struct State: Equatable, Sendable {

		// MARK: Exposed properties

		public var isUpdatingUserLocation: Bool

		public var recentUserLocation: Location?

		public let minDistanceDifference: Double = Constants.minDistanceDifferenceInMetersForLocationUpdate

		// MARK: Init

		public init(
			isUpdatingUserLocation: Bool = false,
			recentUserLocation: Location? = nil
		) {
			self.isUpdatingUserLocation = isUpdatingUserLocation
			self.recentUserLocation = recentUserLocation
		}

	}

}
