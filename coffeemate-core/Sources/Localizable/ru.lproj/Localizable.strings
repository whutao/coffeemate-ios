/*
 Localizable.strings
 */

// MARK: Common

"timeAgo.minutes" = "%d мин назад";
"timeAgo.hours" = "%d часов назад";
"timeAgo.days" = "%d дней назад";
"timeAgo.weeks" = "%d недель назад";
"timeAgo.months" = "%d месяцев назад";
"timeAgo.years" = "%d лет назад";

"version.description" = "Версия %s";

"imageCropper.navigationBar.cancel" = "Отмена";
"imageCropper.navigationBar.done" = "Далее";

"mediaPicker.photoLibrary.cancel" = "Отмена";
"mediaPicker.photoLibrary.done" = "Далее";
"mediaPicker.photoLibrary.emptyPlaceholder" = "Пусто";
"mediaPicker.photoLibrary.selectedMediasDescription" = "Выбрано %d";
"mediaPicker.photoLibrary.permissionRequired.description" = "Чтобы выбрать фото, пожалуйста, предоставьте доступ к медиатеке.";
"mediaPicker.photoLibrary.permissionRequired.button" = "Перейти в настройки приложения";

// MARK: Chat room

"chatRoom.navigationBar.companionOnline" = "Онлайн";
"chatRoom.input.placeholder" = "Набирает...";

// MARK: Chat list

"chatList.header.title" = "Собеседники";
"chatList.empty.placeholder" = "Список чатов пуст";
"chatList.chatSummary.lastMessageEmpty.placeholder" = "Сообщений нет";

// MARK: Contacts

"contacts.toast.cannotLoadNearbyContacts.title" = "Не удается загрузить находящихся поблизости людей";
"contacts.toast.cannotLoadProfile.title" = "Не удалось загрузить профиль";
"contacts.toast.cannotOpenChat.title" = "Не удается открыть чат";
"contacts.toast.cannotSendLike.title" = "Не удается отправить лайк";
"contacts.toast.cannotSendDislike.title" = "Не удается отправить неприязнь";
"contacts.toast.itsAMatch.title" = "Совпадение!";
"contacts.toast.itsAMatch.description" = "В чатах появится новый коллега по кофе";

"contacts.empty.title" = "На данный момент это все";
"contacts.empty.description" = "В вашем регионе не осталось людей. Повторите попытку позже или обновите.";
"contacts.empty.refreshButtonTitle" = "Обновить";

// MARK: Match

"match.header.title" = "Совпадение!";
"match.sendMessageButton.title" = "Открыть чат";

// MARK: Profile

"profile.toast.signOutFailed.title" = "Не удалось выполнить выход из системы";

"profile.header.title" = "Ваш профиль";

// MARK: Own profile editor

"ownProfileEditor.toast.changesSaved.title" = "Сохранено";
"ownProfileEditor.toast.changesNotSaved.title" = "Ошибка сохранения";
"ownProfileEditor.toast.changesNotSaved.description" = "Попробуйте позже";

"ownProfileEditor.header.title" = "Редактор";
"ownProfileEditor.nicknameTextField.title" = "Никнейм";
"ownProfileEditor.nicknameTextField.footer" = "Количество символов %d/%d";
"ownProfileEditor.birthDatePicker.title" = "Дата рождения";
"ownProfileEditor.birthDatePicker.done" = "Готово";
"ownProfileEditor.birthDatePicker.placeholder" = "Выбрать";
"ownProfileEditor.ownGenderPicker.title" = "Я";
"ownProfileEditor.hobbyPicker.title" = "Интересы";
"ownProfileEditor.hobbyPicker.numberOfSelectHobbies" = "Выбрано %d из %d";
"ownProfileEditor.galleryPicker.title" = "Галерея";
"ownProfileEditor.galleryPicker.footer" = "Выбрано %d из %d";
"ownProfileEditor.targetGendersPicker.title" = "Ищу";
"ownProfileEditor.aboutTextField.title" = "О себе";
"ownProfileEditor.aboutTextField.footer" = "Количество символов %d/%d";
"ownProfileEditor.saveButton.title" = "Сохранить";
"ownProfileEditor.deleteAccountButton.title" = "Удалить аккаунт";

"ownProfileDeleteDialog.reasonField.title" = "Почему вы покидаете нас? :(";
"ownProfileDeleteDialog.reasonField.footer" = "Количество символов %d/%d";
"ownProfileDeleteDialog.feedbackField.title" = "Можем ли мы связаться с вами по электронной почте?";
"ownProfileDeleteDialog.confirmButton.title" = "Подтвердить";

// MARK: Side profile overview

"sideProfileOverview.profileVerified" = "Верифицированный профиль";
"sideProfileOverview.targetGenders.title" = "Ищу";
"sideProfileOverview.about.title" = "О себе";
"sideProfileOverview.about.readMore" = "Читать далее";
"sideProfileOverview.about.readLess" = "Read less";
"sideProfileOverview.hobbies.title" = "Интересы";
"sideProfileOverview.gallety.title" = "Галерея";

// MARK: Sign In

"signIn.header.title" = "Давайте начнем";
"signIn.header.description" = "Зарегистрируйтесь в своем аккаунте Google, чтобы ознакомиться со всеми ценными функциями и пообщаться со своими новыми коллегами по кофе";
"signIn.footer.markdownText" = "2023 Coffee Mate LTD. Все права защищены. Используя это приложение, вы подтверждаете свое согласие с нашими [Условиями использования](https://www.apple.com/in/) и [Политикой конфиденциальности](https://www.apple.com/in/).";

// MARK: Sign Up

"signUp.header.title" = "Создание профиля";
"signUp.footer.stepCounter.description" = "Шаг %d из %d";

"signUp.infoStep.header.description" = "Заполните короткую анкету, чтобы начать пить кофе с друзьями";
"signUp.infoStep.photoPicker.title" = "Фото профиля";
"signUp.infoStep.nicknameTextField.title" = "Никнейм";
"signUp.infoStep.nicknameTextField.footer" = "Количество символов %d/%d";
"signUp.infoStep.birthDatePicker.placeholder" = "Дата рождения";
"signUp.infoStep.birthDatePicker.done" = "Готово";
"signUp.infoStep.nextButton.title" = "Далее";

"signUp.genderStep.ownGenderPicker.title" = "Я";
"signUp.genderStep.targetGenderPicker.title" = "Ищу";
"signUp.genderStep.nextButton.title" = "Далее";

"signUp.hobbyStep.header.title" = "Мои интересы";
"signUp.hobbyStep.header.description" = "Выберите несколько ваших интересов и дайте всем знать, чем вы увлечены";
"signUp.hobbyStep.hobbyPicker.numberOfSelectHobbies" = "Выбрано %d из %d";
"signUp.hobbyStep.nextButton.title" = "Далее";

"signUp.aboutStep.header.title" = "Расскажите немного о себе";
"signUp.aboutStep.header.description" = "Благодаря этой информации люди узнают вас лучше и поймут, насколько вы им интересны";
"signUp.aboutStep.aboutTextView.numberOfCharacters" = "Количество символов %d/%d";
"signUp.aboutStep.nextButton.title" = "Создать";
