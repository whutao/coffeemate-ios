//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ChatList
import ChatRoom
import ComposableArchitecture
import Contacts
import Foundation
import Models
import Profile
import ProfileEditor
import SideProfileOverview

// MARK: - Reducer

public struct Home: Reducer {

	// MARK: Init

	public init() {

	}

	// MARK: Body

	public var body: some ReducerOf<Self> {
		Scope(state: \.activeTab, action: /Action.tab) {
			Scope(state: /State.Tab.chatList, action: /Action.Tab.chatList) {
				ChatList()
			}
			Scope(state: /State.Tab.contacts, action: /Action.Tab.contacts) {
				Contacts()
			}
			Scope(state: /State.Tab.profile, action: /Action.Tab.profile) {
				Profile()
			}
		}
		Reduce { state, action in
			switch action {

			// Delegate

			case .delegate:
				return .none

			// Path

			case .path(.element(id: _, action: .chatRoom(.delegate(.shouldClose)))):
				state.path = .init()
				return .none

			case .path(.element(id: _, action: .profileEditor(.delegate(.shouldClose)))):
				state.path = .init()
				return .none

			case .path(.element(id: _, action: .profileEditor(.delegate(.didDeleteProfile)))):
				return .send(.delegate(.didDeleteProfile))

			case .path:
				return .none

			// Tab

			case let .tab(.chatList(.delegate(.shouldOpenChat(chatSummary)))):
				return .send(._private(.openChatRoom(.init(
					chatId: chatSummary.chatId,
					chatCompanion: chatSummary.companion
				))))

			case let .tab(.contacts(.delegate(.shouldOpenChatRoom(chatRoom)))):
				return .send(._private(.openChatRoom(chatRoom)))

			case .tab(.profile(.delegate(.didSignOut))):
				return .send(.delegate(.didSignOut))

			case let .tab(.profile(.delegate(.shouldOpenEditor(editorState)))):
				return .send(._private(.openProfileEditor(editorState)))

			case .tab:
				return .none

			// View

			case .view(.didAppearFirstTime):
				return .none

			case let .view(.didSelectTabItem(tabItem)):
				if state.activeTabBarItem == tabItem {
					return .none
				} else {
					return .send(._private(.setTabItemActive(tabItem)))
				}

			// Private

			case let ._private(.openChatRoom(chatRoom)):
				state.path.append(.chatRoom(chatRoom))
				return .none

			case let ._private(.openProfileEditor(profileEditor)):
				state.path.append(.profileEditor(profileEditor))
				return .none

			case let ._private(.setTabItemActive(tabItem)):
				switch tabItem {
				case .chat:
					state.activeTab = .chatList(.init())
					return .none
				case .contacts:
					state.activeTab = .contacts(.init())
					return .cancel(id: ChatList.CancelID.chatListRefreshTimer)
				case .profile:
					state.activeTab = .profile(.init())
					return .cancel(id: ChatList.CancelID.chatListRefreshTimer)
				}
			}
		}
		.forEach(\.path, action: /Action.path) {
			Scope(state: /State.Path.chatRoom, action: /Action.Path.chatRoom) {
				ChatRoom()
			}
			Scope(state: /State.Path.profileEditor, action: /Action.Path.profileEditor) {
				ProfileEditor()
			}
			Scope(state: /State.Path.sideProfileOverview, action: /Action.Path.sideProfileOverview) {
				SideProfileOverview()
			}
		}
	}

}

// MARK: - Action

extension Home {

	public enum Action: Equatable, Sendable {

		// MARK: Case

		case delegate(Delegate)

		case path(StackAction<Home.State.Path, Home.Action.Path>)

		case tab(Tab)

		case view(View)

		case _private(Private)

	}

}

// MARK: - Action: delegate

extension Home.Action {

	public enum Delegate: Equatable, Sendable {

		// MARK: Case

		case didDeleteProfile

		case didSignOut

	}

}

// MARK: - Action: view

extension Home.Action {

	public enum View: Equatable, Sendable {

		// MARK: Case

		case didAppearFirstTime

		case didSelectTabItem(HomeTabBarItem)

	}

}

// MARK: - Action: private

extension Home.Action {

	public enum Private: Equatable, Sendable {

		// MARK: Case

		case openChatRoom(ChatRoom.State)

		case openProfileEditor(ProfileEditor.State)

		case setTabItemActive(HomeTabBarItem)

	}

}

// MARK: - Action: path

extension Home.Action {

	public enum Path: Equatable, Sendable {

		// MARK: Case

		case chatRoom(ChatRoom.Action)

		case profileEditor(ProfileEditor.Action)

		case sideProfileOverview(SideProfileOverview.Action)

	}

}

// MARK: - Action: tab

extension Home.Action {

	public enum Tab: Equatable, Sendable {

		// MARK: Case

		case chatList(ChatList.Action)

		case contacts(Contacts.Action)

		case profile(Profile.Action)

	}

}

// MARK: - State

extension Home {

	public struct State: Equatable, Sendable {

		// MARK: Exposed properties

		public var activeTab: Tab

		public var activeTabBarItem: HomeTabBarItem {
			switch activeTab {
			case .chatList:
				return .chat
			case .contacts:
				return .contacts
			case .profile:
				return .profile
			}
		}

		public var path: StackState<Path>

		public let tabBarItems: [HomeTabBarItem]

		// MARK: Init

		public init(
			activeTab: Tab = .contacts(.init()),
			tabBarItems: [HomeTabBarItem] = [.chat, .contacts, .profile],
			path: StackState<Path> = StackState()
		) {
			self.activeTab = activeTab
			self.tabBarItems = tabBarItems
			self.path = path
		}

	}

}

// MARK: - State: path

extension Home.State {

	public enum Path: Equatable, Sendable {

		// MARK: Case

		case chatRoom(ChatRoom.State)

		case profileEditor(ProfileEditor.State)

		case sideProfileOverview(SideProfileOverview.State)

	}

}

// MARK: - State: tab

extension Home.State {

	public enum Tab: Equatable, Sendable {

		// MARK: Case

		case chatList(ChatList.State)

		case contacts(Contacts.State)

		case profile(Profile.State)

	}

}
