//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import Log

// MARK: - Logger

let log = makeDefaultLogger(module: "ChatRoom")
