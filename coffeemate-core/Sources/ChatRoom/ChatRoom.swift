//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ChatClient
import ComposableArchitecture
import ContactsClient
import Extensions
import Foundation
import Localizable
import Models
import SideProfileOverview
import UserClient

// MARK: - Reducer

public struct ChatRoom: Reducer {

	// MARK: Private properties

	@Dependency(\.chatClient) private var chatClient

	@Dependency(\.contactsClient) private var contactsClient

	@Dependency(\.mainQueue) private var mainQueue

	@Dependency(\.userClient) private var userClient

	// MARK: Cancel ID

	private enum CancelID: Hashable, Sendable {
		case chatEventObserving
	}

	// MARK: Init

	public init() {

	}

	// MARK: Body

	public var body: some ReducerOf<Self> {
		Reduce { state, action in
			switch action {

			// Delegate

			case .chat(.isConnecting):
				return .none

			case .chat(.didConnect):
				let chatId = state.chatId
				let chatCompanionProfileHash = state.chatCompanion.profileHash
				return .run { send in
					let chatHistory = try await chatClient.chatHistory(.init(
						chatId: chatId,
						companionProfileHash: chatCompanionProfileHash
					))
					await send(._private(.setChatHistoryMessages(chatHistory)))
				}

			case .chat(.didDisconnect):
				return .none

			case let .chat(.didReceiveTextMessages(chatMessages)):
				return .send(._private(.appendChatHistoryMessages(chatMessages)))

			case let .chat(.didReceiveMessageRead(chatMessageRead)):
				return .send(._private(.applyMessageRead(chatMessageRead)))

			// Delegate

			case .delegate:
				return .none

			// Side profile overview

			case .sideProfileOverview:
				return .none

			// View

			case .view(.didTapBackButton):
				return .concatenate(
					.send(._private(.disconnectFromChatRoom)),
					.send(.delegate(.shouldClose))
				)

			case .view(.didAppearFirstTime):
				return .send(._private(.connectToChatRoom))

			case .view(.didEnterForeground):
				return .send(._private(.connectToChatRoom))

			case .view(.didEnterBackground):
				return .send(._private(.disconnectFromChatRoom))

			case let .view(.didGenerateTextMessage(text)):
				let trimmedText = text.trimmingCharacters(in: .whitespacesAndNewlines)
				return .send(._private(.sendTextMessage(trimmedText)))

			case .view(.didTapCompanionAvatar):
				return .send(._private(.showCompationProfileOverview))

			// Private

			case let ._private(.sendTextMessage(text)):
				do {
					try chatClient.chatRoomSendTextMessage(.init(
						chatId: state.chatId,
						text: text
					))
				} catch let error {
					log.error("Cannot send message due to \(error).")
				}
				return .none

			case let ._private(.sendMessageRead(messageId)):
				do {
					try chatClient.chatRoomSendMessageRead(.init(
						chatId: state.chatId,
						messageId: messageId
					))
				} catch let error {
					log.error("Cannot send message due to \(error).")
				}
				return .none

			case ._private(.connectToChatRoom):
				do {
					let chatRoomId = state.chatId
					let chatPublisher = try chatClient.chatRoomConnect(.init(chatId: chatRoomId))
					return .publisher {
						chatPublisher.map { event in
							switch event {
							case .isConnecting:
								return .chat(.isConnecting)
							case .didConnect:
								return .chat(.didConnect)
							case .didDisconnect:
								return .chat(.didDisconnect)
							case let .didReceiveChatTextMessageBatch(_, textMessages):
								return .chat(.didReceiveTextMessages(textMessages))
							case let .didReceiveChatMessageRead(messageRead):
								return .chat(.didReceiveMessageRead(messageRead))
							}
						}
						.receive(on: mainQueue)
					}
					.cancellable(id: CancelID.chatEventObserving, cancelInFlight: true)
				} catch let error {
					log.error("Failed to connect to chat room due to \(error).")
					return .none
				}

			case ._private(.disconnectFromChatRoom):
				chatClient.chatRoomDisconnectFromAll()
				return .none

			case let ._private(.applyMessageRead(messageRead)):
				state.chatHistory.forEachElement(
					suchThat: \.messageId == messageRead.messageId
				) { message in
					message.status = .read
				}
				return .none

			case let ._private(.appendChatHistoryMessages(chatMessages)):
				state.chatHistory.append(contentsOf: chatMessages)
				return .none

			case let ._private(.setChatHistoryMessages(chatHistory)):
				state.chatHistory = chatHistory
				return .none

			case ._private(.showCompationProfileOverview):
				let companionProfileHash = state.chatCompanion.profileHash
				return .run { send in
					await send(._private(.setIsCompanionAvatarInteractionEnabled(false)))
					let sideProfile = try await contactsClient.sideProfile(.init(
						profileHash: companionProfileHash
					))
					await send(._private(.showSideProfileOverview(sideProfile)))
					await send(._private(.setIsCompanionAvatarInteractionEnabled(true)))
				} catch: { error, send in
					log.error("Cannot load side profile due to \(error).")
					await send(._private(.setIsCompanionAvatarInteractionEnabled(true)))
				}

			case let ._private(.showSideProfileOverview(sideProfile)):
				state.sideProfileOverview = .init(
					sideProfile: sideProfile,
					matchedHobbies: (userClient.ownProfile()?.hobbies).unwrapped(or: []),
					areActionEnabled: false
				)
				return .none

			case let ._private(.setIsCompanionAvatarInteractionEnabled(isEnabled)):
				state.isCompanionAvatarInteractionEnabled = isEnabled
				return .none
			}
		}
		.ifLet(\.$sideProfileOverview, action: /Action.sideProfileOverview) {
			SideProfileOverview()
		}
	}

}

// MARK: - Action

extension ChatRoom {

	public enum Action: Equatable, Sendable {

		// MARK: Case

		case chat(Chat)

		case delegate(Delegate)

		case sideProfileOverview(PresentationAction<SideProfileOverview.Action>)

		case view(View)

		case _private(Private)

	}

}

// MARK: - Action: delegate

extension ChatRoom.Action {

	public enum Delegate: Equatable, Sendable {

		// MARK: Case

		case shouldClose

	}

}

// MARK: - Action: chat

extension ChatRoom.Action {

	public enum Chat: Equatable, Sendable {

		// MARK: Case

		case isConnecting

		case didConnect

		case didDisconnect

		case didReceiveMessageRead(ChatMessageRead)

		case didReceiveTextMessages([ChatMessage])

	}

}

// MARK: - Action: view

extension ChatRoom.Action {

	public enum View: Equatable, Sendable {

		// MARK: Case

		case didAppearFirstTime

		case didEnterForeground

		case didEnterBackground

		case didTapBackButton

		case didTapCompanionAvatar

		case didGenerateTextMessage(String)

	}

}

// MARK: - Action: private

extension ChatRoom.Action {

	public enum Private: Equatable, Sendable {

		// MARK: Case

		case connectToChatRoom

		case disconnectFromChatRoom

		case appendChatHistoryMessages([ChatMessage])

		case applyMessageRead(ChatMessageRead)

		case setChatHistoryMessages([ChatMessage])

		case sendTextMessage(String)

		case sendMessageRead(ChatMessageID)

		case showCompationProfileOverview

		case showSideProfileOverview(SideProfile)

		case setIsCompanionAvatarInteractionEnabled(Bool)

	}

}

// MARK: - State

extension ChatRoom {

	public struct State: Equatable, Sendable {

		// MARK: State

		public let chatId: Int

		public var chatCompanion: ChatCompanion

		public var chatHistory: [ChatMessage]

		public var isCompanionAvatarInteractionEnabled: Bool

		@PresentationState public var sideProfileOverview: SideProfileOverview.State?

		public let inputPlaceholder: String = L10n.chatRoomInputPlaceholder

		// MARK: Init

		public init(
			chatId: Int,
			chatCompanion: ChatCompanion,
			chatHistory: [ChatMessage] = [],
			sideProfileOverview: SideProfileOverview.State? = nil,
			isCompanionAvatarInteractionEnabled: Bool = true
		) {
			self.chatId = chatId
			self.chatCompanion = chatCompanion
			self.chatHistory = chatHistory
			self.sideProfileOverview = sideProfileOverview
			self.isCompanionAvatarInteractionEnabled = isCompanionAvatarInteractionEnabled
		}

	}

}
