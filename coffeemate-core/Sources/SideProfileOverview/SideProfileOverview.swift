//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Extensions
import Foundation
import Localizable
import Models

// MARK: - Reducer

public struct SideProfileOverview: Reducer {

	// MARK: Init

	public init() {

	}

	// MARK: Body

	public var body: some ReducerOf<Self> {
		Reduce { state, action in
			switch action {

			// Delegate

			case .delegate:
				return .none

			// View

			case let .view(.didSetActiveGalleryEntry(entry)):
				return .send(._private(.setActiveGalleryEntry(entry)))

			case .view(.didTapGalleryEntry):
				return .send(._private(.setIsShowingGallery(true)))

			case .view(.didCloseGallery):
				return .send(._private(.setIsShowingGallery(false)))

			case .view(.didTapSendMessageButton):
				let hash = state.profileHashOrId
				return .send(.delegate(.shouldOpenChat(contactProfileHash: hash)))

			case .view(.didTapLikeButton):
				let hash = state.profileHashOrId
				return .send(.delegate(.shouldSendLike(contactProfileHash: hash)))

			case .view(.didTapDislikeButton):
				let hash = state.profileHashOrId
				return .send(.delegate(.shouldSendDislike(contactProfileHash: hash)))

			// Private

			case let ._private(.setActiveGalleryEntry(entry)):
				state.activeGalleryEntry = entry
				return .none

			case let ._private(.setIsShowingGallery(isShowing)):
				state.isShowingGallery = isShowing
				return .none
			}
		}
	}

}

// MARK: - Action

extension SideProfileOverview {

	public enum Action: Equatable, Sendable {

		// MARK: Case

		case delegate(Delegate)

		case view(View)

		case _private(Private)

	}

}

// MARK: - Action: delegate

extension SideProfileOverview.Action {

	public enum Delegate: Equatable, Sendable {

		// MARK: Case

		case shouldOpenChat(contactProfileHash: String)

		case shouldSendLike(contactProfileHash: String)

		case shouldSendDislike(contactProfileHash: String)

	}

}

// MARK: - Action: view

extension SideProfileOverview.Action {

	public enum View: Equatable, Sendable {

		// MARK: Case

		case didCloseGallery

		case didTapLikeButton

		case didTapDislikeButton

		case didTapSendMessageButton

		case didTapGalleryEntry(GalleryEntry)

		case didSetActiveGalleryEntry(GalleryEntry?)

	}

}

// MARK: - Action: private

extension SideProfileOverview.Action {

	public enum Private: Equatable, Sendable {

		// MARK: Case

		case setIsShowingGallery(Bool)

		case setActiveGalleryEntry(GalleryEntry?)

	}

}

// MARK: - State

extension SideProfileOverview {

	public struct State: Equatable, Sendable {

		// MARK: Exposed properties

		public var isShowingGallery: Bool

		public let profileHashOrId: String

		public let areActionEnabled: Bool

		public var avatarImage: ImageRepresentation

		public var age: Int

		public var nickname: String

		public var gender: Gender

		public var targetGenders: Set<Gender>

		public var hobbies: IdentifiedArrayOf<Hobby>

		public let matchedHobbies: IdentifiedArrayOf<Hobby>

		public var galleryEntries: IdentifiedArrayOf<GalleryEntry>

		public var activeGalleryEntry: GalleryEntry?

		public var about: String

		public var isVerified: Bool

		public var isAboutExpanded: Bool

		public let targetGendersFieldTitle: String = L10n.sideProfileOverviewTargetGendersTitle

		public let galleryFieldTitle: String = L10n.sideProfileOverviewGalletyTitle

		public let aboutFieldTitle: String = L10n.sideProfileOverviewAboutTitle

		public let hobbiesFieldTitle: String = L10n.sideProfileOverviewHobbiesTitle

		public let aboutExpand: String = L10n.sideProfileOverviewAboutReadMore

		public let aboutCollapse: String = L10n.sideProfileOverviewAboutReadLess

		public let verifiedProfileBadgeTitle: String = L10n.sideProfileOverviewProfileVerified

		// MARK: Init

		public init(
			profileHashOrId: String,
			avatarImage: ImageRepresentation,
			nickname: String,
			age: Int,
			gender: Gender,
			targetGenders: Set<Gender>,
			about: String,
			hobbies: IdentifiedArrayOf<Hobby>,
			matchedHobbies: IdentifiedArrayOf<Hobby>,
			galleryEntries: IdentifiedArrayOf<GalleryEntry>,
			isVerified: Bool,
			areActionEnabled: Bool,
			isAboutExpanded: Bool = false
		) {
			self.profileHashOrId = profileHashOrId
			self.about = about
			self.isAboutExpanded = isAboutExpanded
			self.age = age
			self.avatarImage = avatarImage
			self.nickname = nickname
			self.hobbies = hobbies
			self.matchedHobbies = matchedHobbies
			self.gender = gender
			self.targetGenders = targetGenders
			self.galleryEntries = galleryEntries
			self.activeGalleryEntry = galleryEntries.first
			self.isVerified = isVerified
			self.areActionEnabled = areActionEnabled
			self.isShowingGallery = false
		}

		public init(
			sideProfile: SideProfile,
			matchedHobbies: Set<Hobby>,
			areActionEnabled: Bool = true
		) {
			self.init(
				profileHashOrId: sideProfile.profileHash,
				avatarImage: .url(sideProfile.avatarUrl),
				nickname: sideProfile.nickname,
				age: sideProfile.age,
				gender: sideProfile.ownGender,
				targetGenders: sideProfile.targetGenders,
				about: sideProfile.about,
				hobbies: sideProfile.hobbies,
				matchedHobbies: IdentifiedArrayOf(uniqueElements: matchedHobbies),
				galleryEntries: sideProfile.gallery,
				isVerified: sideProfile.isVerified,
				areActionEnabled: areActionEnabled
			)
		}

		public init(ownProfile: OwnProfile) {
			@Dependency(\.date.now) var now
			@Dependency(\.calendar) var calendar
			self.init(
				profileHashOrId: "\(ownProfile.profileId)",
				avatarImage: .url(ownProfile.avatarUrl),
				nickname: ownProfile.nickname,
				age: ownProfile.birthDate.age(now: now, using: calendar),
				gender: ownProfile.gender,
				targetGenders: ownProfile.targetGenders,
				about: ownProfile.about,
				hobbies: IdentifiedArrayOf(uniqueElements: ownProfile.hobbies),
				matchedHobbies: [],
				galleryEntries: IdentifiedArrayOf(uniqueElements: ownProfile.gallery),
				isVerified: ownProfile.isVerified,
				areActionEnabled: false
			)
		}

	}

}
