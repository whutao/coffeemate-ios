//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import APIClient
import ChatRoom
import ChatClient
import ComposableArchitecture
import Extensions
import Foundation
import Localizable
import Models
import ToastClient

// MARK: - Reducer

public struct Match: Reducer {

	// MARK: Private properties

	@Dependency(\.chatClient) private var chatClient

	@Dependency(\.toastClient) private var toastClient

	// MARK: Init

	public init() {

	}

	// MARK: Body

	public var body: some ReducerOf<Self> {
		Reduce { state, action in
			switch action {

			// Delegate

			case .delegate:
				return .none

			// View

			case .view(.didTapSendMessageButton):
				return .send(._private(.openChatSummaryWithSideProfile))

			// Private

			case let ._private(.addInProgressOperations(operations)):
				state.inProgressOperation.insert(operations)
				return .none

			case let ._private(.removeInProgressOperations(operations)):
				state.inProgressOperation.remove(operations)
				return .none

			case ._private(.openChatSummaryWithSideProfile):
				let sideProfile = state.sideProfile
				return .run { send in
					await send(._private(.addInProgressOperations(.chatSummaryLoad)))
					let chatAvailability = try await chatClient.chatAvailability(.init(
						companionProfileHash: sideProfile.profileHash
					))
					let chatRoom = ChatRoom.State(
						chatId: chatAvailability.chatId,
						chatCompanion: .init(from: sideProfile)
					)
					await send(.delegate(.shouldOpenChatRoom(chatRoom)))
					await send(._private(.removeInProgressOperations(.chatSummaryLoad)))
				} catch: { error, send in
					log.error("Failed to open chat due to \(error).")
					if let error = error as? APIError {
						await toastClient.present(.error(
							L10n.contactsToastCannotOpenChatTitle,
							subtitle: error.message
						))
					} else {
						await toastClient.present(.error(
							L10n.contactsToastCannotOpenChatTitle,
							subtitle: error.localizedDescription
						))
					}
					await send(._private(.removeInProgressOperations(.chatSummaryLoad)))
				}
			}
		}
	}

}

// MARK: - Action

extension Match {

	public enum Action: Equatable, Sendable {

		// MARK: Case

		case delegate(Delegate)

		case view(View)

		case _private(Private)

	}

}

// MARK: - Action: delegate

extension Match.Action {

	public enum Delegate: Equatable, Sendable {

		// MARK: Case

		case shouldOpenChatRoom(ChatRoom.State)

	}

}

// MARK: - Action: view

extension Match.Action {

	public enum View: Equatable, Sendable {

		// MARK: Case

		case didTapSendMessageButton

	}

}

// MARK: - Action: private

extension Match.Action {

	public enum Private: Equatable, Sendable {

		// MARK: Case

		case openChatSummaryWithSideProfile

		case addInProgressOperations(Match.State.InProgressOperations)

		case removeInProgressOperations(Match.State.InProgressOperations)

	}

}

// MARK: - State

extension Match {

	public struct State: Equatable, Sendable {

		// MARK: Exposed properties

		public var inProgressOperation: InProgressOperations

		public let ownProfile: OwnProfile

		public let sideProfile: SideProfile

		public var headerTitle: String {
			return L10n.matchHeaderTitle
		}

		public var chatButtonTitle: String {
			return L10n.matchSendMessageButtonTitle
		}

		// MARK: Init

		public init(
			ownProfile: OwnProfile,
			sideProfile: SideProfile,
			inProgressOperation: InProgressOperations = []
		) {
			self.ownProfile = ownProfile
			self.sideProfile = sideProfile
			self.inProgressOperation = inProgressOperation
		}

	}

}

// MARK: - State: In-progress operation

extension Match.State {

	public struct InProgressOperations: OptionSet, Equatable, Sendable {

		// MARK: Exposed properties

		public let rawValue: UInt

		public static let chatSummaryLoad = InProgressOperations(rawValue: 1 << 0)

		// MARK: Init

		public init(rawValue: UInt) {
			self.rawValue = rawValue
		}

	}

}
