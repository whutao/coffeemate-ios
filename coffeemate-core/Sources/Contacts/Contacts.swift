//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import APIClient
import ChatRoom
import ChatClient
import ComposableArchitecture
import Constants
import ContactsClient
import Foundation
import Localizable
import Match
import Models
import SideProfileOverview
import ToastClient
import UserClient

// MARK: - Reducer

public struct Contacts: Reducer {

	// MARK: Private properties

	@Dependency(\.continuousClock) private var clock

	@Dependency(\.chatClient) private var chatClient

	@Dependency(\.contactsClient) private var contactsClient

	@Dependency(\.toastClient) private var toastClient

	@Dependency(\.userClient) private var userClient

	// MARK: Cancel ID

	private enum CancelID: Hashable {
		case sideContactsLoad
	}

	// MARK: Init

	public init() {

	}

	// MARK: Body

	public var body: some ReducerOf<Self> {
		Reduce { state, action in
			switch action {

			// Contact

			case let .contact(.delegate(.shouldSendDislike(contactProfileHash: hash))):
				return .send(._private(.sendDislike(profileHash: hash)))

			case let .contact(.delegate(.shouldSendLike(contactProfileHash: hash))):
				return .send(._private(.sendLike(profileHash: hash)))

			case let .contact(.delegate(.shouldOpenProfile(contactProfileHash: hash))):
				return .send(._private(.openSideProfileOverview(profileHash: hash)))

			case let .contact(.delegate(.shouldOpenChat(contactProfileHash: hash))):
				return .send(._private(.openChatWithContact(profileHash: hash)))

			case .contact:
				return .none

			// Delegate

			case .delegate:
				return .none

			// Match

			case let .match(.presented(.delegate(.shouldOpenChatRoom(chatRoom)))):
				return .concatenate(
					.send(._private(.setMatch(nil))),
					.send(.delegate(.shouldOpenChatRoom(chatRoom)))
				)

			case .match:
				return .none

			// Side profile overview

			case let .sideProfileOverview(.presented(.delegate(.shouldOpenChat(hash)))):
				return .send(._private(.openChatWithContact(profileHash: hash)))

			case let .sideProfileOverview(.presented(.delegate(.shouldSendLike(hash)))):
				return .send(._private(.sendLike(profileHash: hash)))

			case let .sideProfileOverview(.presented(.delegate(.shouldSendDislike(hash)))):
				return .send(._private(.sendDislike(profileHash: hash)))

			case .sideProfileOverview:
				return .none

			// View

			case .view(.didAppearFirstTime):
				return .send(._private(.initialize))

			case .view(.didTapEmptyPlaceholderRefreshButton):
				return .send(._private(.refreshContacts))

			// Private

			case ._private(.initialize):
				guard state.contactsRefreshTimesCount == 0 else {
					return .none
				}
				return .send(._private(.refreshContacts))

			case ._private(.refreshContacts):
				let searchRadius = state.contactsSearchRadiusInKm
				let newContactsRefreshTimesCount = state.contactsRefreshTimesCount + 1
				return .run { send in
					await send(._private(.addInProgressOperations(.refreshContacts)))
					let sideProfiles = try await contactsClient.randomSideProfiles(.init(
						searchRadius: searchRadius
					))
					await send(._private(.setFoundSideProfiles(sideProfiles)))
					await send(._private(.setContactsRefreshTimesCount(newContactsRefreshTimesCount)))
					await send(._private(.removeInProgressOperations(.refreshContacts)))
				} catch: { error, send in
					log.error("Cannot load profiles due to \(error).")
					await toastClient.present(.error(
						L10n.contactsToastCannotLoadNearbyContactsTitle,
						subtitle: error.localizedDescription
					))
					await send(._private(.removeInProgressOperations(.refreshContacts)))
				}
				.cancellable(id: CancelID.sideContactsLoad, cancelInFlight: true)

			case let ._private(.sendLike(profileHash: hash)):
				return .run { send in
					await send(._private(.addInProgressOperations(.sendLike)))
					let likeResponse = try await contactsClient.sendLike(.init(
						targetProfileHash: hash
					))
					await send(._private(.setSideProfileOverview(nil)))
					if likeResponse.isLikedBack {
						let ownProfile = try await userClient.refreshSession()
						let sideProfile = try await contactsClient.sideProfile(.init(
							profileHash: hash
						))
						await send(._private(.setMatch(.init(
							ownProfile: ownProfile,
							sideProfile: sideProfile
						))))
					}
					await send(._private(.setNextActiveContact))
					await send(._private(.removeInProgressOperations(.sendLike)))
				} catch: { error, send in
					log.error("Cannot send like due to \(error).")
					await toastClient.present(.error(
						L10n.contactsToastCannotSendLikeTitle,
						subtitle: error.localizedDescription
					))
					await send(._private(.removeInProgressOperations(.sendLike)))
				}

			case let ._private(.sendDislike(profileHash: hash)):
				return .run { send in
					await send(._private(.addInProgressOperations(.sendDislike)))
					let dislikeResponse = try await contactsClient.sendDislike(.init(targetProfileHash: hash))
					await send(._private(.setSideProfileOverview(nil)))
					await send(._private(.setNextActiveContact))
					await send(._private(.removeInProgressOperations(.sendDislike)))
				} catch: { error, send in
					log.error("Cannot send dislike due to \(error).")
					await toastClient.present(.error(
						L10n.contactsToastCannotSendDislikeTitle,
						subtitle: error.localizedDescription
					))
					await send(._private(.removeInProgressOperations(.sendDislike)))
				}

			case let ._private(.openSideProfileOverview(profileHash: hash)):
				return .run { send in
					await send(._private(.addInProgressOperations(.loadSideProfile)))
					let sideProfile = try await contactsClient.sideProfileWithGallery(.init(
						profileHash: hash
					))
					let ownHobbies = (userClient.ownProfile()?.hobbies).unwrapped(or: Set())
					await send(._private(.setSideProfileOverview(.init(
						sideProfile: sideProfile,
						matchedHobbies: ownHobbies
					))))
					await send(._private(.removeInProgressOperations(.loadSideProfile)))
				} catch: { error, send in
					log.error("Failed to fetch side user profile due to \(error).")
					await toastClient.present(.error(
						L10n.contactsToastCannotLoadProfileTitle,
						subtitle: error.localizedDescription
					))
					await send(._private(.removeInProgressOperations(.loadSideProfile)))
				}

			case let ._private(.openChatWithContact(profileHash: hash)):
				return .run { send in
					await send(._private(.addInProgressOperations(.loadChat)))
					let chatAvailability = try await chatClient.chatAvailability(.init(
						companionProfileHash: hash
					))
					let sideProfile = try await contactsClient.sideProfile(.init(
						profileHash: hash
					))
					let chatRoom = ChatRoom.State(
						chatId: chatAvailability.chatId,
						chatCompanion: .init(from: sideProfile)
					)
					await send(._private(.setSideProfileOverview(nil)))
					await send(.delegate(.shouldOpenChatRoom(chatRoom)))
					await send(._private(.removeInProgressOperations(.loadChat)))
				} catch: { error, send in
					log.error("Failed to open chat due to \(error).")
					if let error = error as? APIError {
						await toastClient.present(.error(
							L10n.contactsToastCannotOpenChatTitle,
							subtitle: error.message
						))
					} else {
						await toastClient.present(.error(
							L10n.contactsToastCannotOpenChatTitle,
							subtitle: error.localizedDescription
						))
					}
					await send(._private(.removeInProgressOperations(.loadChat)))
				}

			case let ._private(.setSideProfileOverview(sideProfileOveview)):
				state.sideProfileOverview = sideProfileOveview
				return .none

			case let ._private(.setFoundSideProfiles(profiles)):
				state.pendingContacts = profiles.map { sideProfile in
					return Contact.State(
						isOptionsButtonVisible: false,
						sideProfile: sideProfile
					)
				}
				state.activeContact = state.pendingContacts.first
				return .none

			case ._private(.setNextActiveContact):
				state.pendingContacts = Array(state.pendingContacts.dropFirst())
				state.activeContact = state.pendingContacts.first
				return .none

			case let ._private(.setMatch(matchState)):
				state.match = matchState
				return .none

			case let ._private(.setContactsRefreshTimesCount(contactsRefreshTimesCount)):
				state.contactsRefreshTimesCount = contactsRefreshTimesCount
				return .none

			case let ._private(.addInProgressOperations(operations)):
				state.inProgressOperations.insert(operations)
				return .none

			case let ._private(.removeInProgressOperations(operations)):
				state.inProgressOperations.remove(operations)
				return .none
			}
		}
		.ifLet(\.activeContact, action: /Action.contact) {
			Contact()
		}
		.ifLet(\.$match, action: /Action.match) {
			Match()
		}
		.ifLet(\.$sideProfileOverview, action: /Action.sideProfileOverview) {
			SideProfileOverview()
		}
	}

}

// MARK: - Action

extension Contacts {

	public enum Action: Equatable, Sendable {

		// MARK: Case

		case contact(Contact.Action)

		case delegate(Delegate)

		case match(PresentationAction<Match.Action>)

		case sideProfileOverview(PresentationAction<SideProfileOverview.Action>)

		case view(View)

		case _private(Private)

	}

}

// MARK: - Action: delegate

extension Contacts.Action {

	public enum Delegate: Equatable, Sendable {

		// MARK: Case

		case shouldOpenChatRoom(ChatRoom.State)

	}

}

// MARK: - Action: view

extension Contacts.Action {

	public enum View: Equatable, Sendable {

		// MARK: Case

		case didAppearFirstTime

		case didTapEmptyPlaceholderRefreshButton

	}

}

// MARK: - Action: private

extension Contacts.Action {

	public enum Private: Equatable, Sendable {

		// MARK: Case

		case initialize

		case refreshContacts

		case openSideProfileOverview(profileHash: String)

		case openChatWithContact(profileHash: String)

		case sendLike(profileHash: String)

		case sendDislike(profileHash: String)

		case setNextActiveContact

		case setFoundSideProfiles([SideProfile])

		case setMatch(Match.State?)

		case setSideProfileOverview(SideProfileOverview.State?)

		case setContactsRefreshTimesCount(UInt)

		case addInProgressOperations(Contacts.State.InProgressOperations)

		case removeInProgressOperations(Contacts.State.InProgressOperations)

	}

}

// MARK: - State

extension Contacts {

	public struct State: Equatable, Sendable {

		// MARK: Exposed properties

		public var contactsRefreshTimesCount: UInt

		public var isUserInteractionEnabled: Bool {
			return inProgressOperations.isEmpty
		}

		public var inProgressOperations: InProgressOperations

		public var activeContact: Contact.State?

		public var pendingContacts: [Contact.State]

		public let contactsSearchRadiusInKm: UInt = Constants.sideContactsSearchRadiusInKm

		@PresentationState public var match: Match.State?

		@PresentationState public var sideProfileOverview: SideProfileOverview.State?

		public var contactsEmptyTitle: String {
			return L10n.contactsEmptyTitle
		}

		public var contactsEmptyDescription: String {
			return L10n.contactsEmptyDescription
		}

		public var contactsEmptyRefreshButtonTitle: String {
			return L10n.contactsEmptyRefreshButtonTitle
		}

		// MARK: Init

		public init(
			activeContact: Contact.State? = nil,
			pendingContacts: [Contact.State] = [],
			contactsRefreshTimesCount: UInt = 0,
			inProgressOperations: InProgressOperations = [],
			match: Match.State? = nil,
			sideProfileOverview: SideProfileOverview.State? = nil
		) {
			self.activeContact = activeContact
			self.pendingContacts = pendingContacts
			self.sideProfileOverview = sideProfileOverview
			self.inProgressOperations = inProgressOperations
			self.match = match
			self.contactsRefreshTimesCount = contactsRefreshTimesCount
		}

	}

}

// MARK: - State: InProgressOperations

extension Contacts.State {

	public struct InProgressOperations: OptionSet, Equatable, Sendable {

		// MARK: Exposed properties

		public let rawValue: UInt

		public static let refreshContacts = InProgressOperations(rawValue: 1 << 0)

		public static let sendLike = InProgressOperations(rawValue: 1 << 1)

		public static let sendDislike = InProgressOperations(rawValue: 1 << 2)

		public static let loadSideProfile = InProgressOperations(rawValue: 1 << 3)

		public static let loadChat = InProgressOperations(rawValue: 1 << 4)

		// MARK: Init

		public init(rawValue: UInt) {
			self.rawValue = rawValue
		}

	}

}
