//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Extensions
import Localizable
import Models
import SwiftUI

// MARK: - Reducer

public struct Contact: Reducer {

	// MARK: Init

	public init() {

	}

	// MARK: Body

	public var body: some ReducerOf<Self> {
		Reduce { state, action in
			switch action {

			// Delegate

			case .delegate:
				return .none

			// View

			case .view(.didTapDislikeButton):
				let hash = state.sideProfile.profileHash
				return .send(.delegate(.shouldSendDislike(contactProfileHash: hash)))

			case .view(.didTapLikeButton):
				let hash = state.sideProfile.profileHash
				return .send(.delegate(.shouldSendLike(contactProfileHash: hash)))

			case .view(.didTapProfileButton):
				let hash = state.sideProfile.profileHash
				return .send(.delegate(.shouldOpenProfile(contactProfileHash: hash)))

			case .view(.didTapOptionsButton):
				#warning("Options button actions.")
				log.debug("Options button tapped.")
				return .none

			case .view(.didTapSendMessageButton):
				let hash = state.sideProfile.profileHash
				return .send(.delegate(.shouldOpenChat(contactProfileHash: hash)))

			// Private

			case ._private:
				return .none
			}
		}
	}

}

// MARK: - Action

extension Contact {

	public enum Action: Equatable, Sendable {

		// MARK: Case

		case delegate(Delegate)

		case view(View)

		case _private(Private)

	}

}

// MARK: - Action: delegate

extension Contact.Action {

	public enum Delegate: Equatable, Sendable {

		// MARK: Case

		case shouldOpenChat(contactProfileHash: String)

		case shouldOpenProfile(contactProfileHash: String)

		case shouldSendDislike(contactProfileHash: String)

		case shouldSendLike(contactProfileHash: String)

	}

}

// MARK: - Action: view

extension Contact.Action {

	public enum View: Equatable, Sendable {

		// MARK: Case

		case didTapSendMessageButton

		case didTapDislikeButton

		case didTapLikeButton

		case didTapProfileButton

		case didTapOptionsButton

	}

}

// MARK: - Action: private

extension Contact.Action {

	public enum Private: Equatable, Sendable {

		// MARK: Case

	}

}

// MARK: - State

extension Contact {

	public struct State: Equatable, Sendable {

		// MARK: Exposed properties

		public let isOptionsButtonVisible: Bool

		public let sideProfile: SideProfile

		// MARK: Init

		public init(
			isOptionsButtonVisible: Bool,
			sideProfile: SideProfile
		) {
			self.isOptionsButtonVisible = isOptionsButtonVisible
			self.sideProfile = sideProfile
		}

	}

}
