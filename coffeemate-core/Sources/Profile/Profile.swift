//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import APIClient
import ComposableArchitecture
import Constants
import Extensions
import Foundation
import Localizable
import Log
import Models
import ProfileEditor
import SideProfileOverview
import SystemInformation
import ToastClient
import UserClient

// MARK: - Reducer

public struct Profile: Reducer {

	// MARK: Private properties

	@Dependency(\.toastClient) private var toastClient

	@Dependency(\.userClient) private var userClient

	// MARK: Cancel ID

	private enum CancelID: Hashable {
		case ownProfileLoad
	}

	// MARK: Init

	public init() {

	}

	// MARK: Body

	public var body: some ReducerOf<Self> {
		Reduce { state, action in
			switch action {

			// Delegate

			case .delegate:
				return .none

			// Profile overview

			case .ownProfileOverview:
				return .none

			// View

			case .view(.didAppear):
				return .send(._private(.refreshOwnProfile))

			case .view(.didTapSignOutButton):
				return .send(._private(.signOut))

			case .view(.didTapEditButton):
				return .send(._private(.openProfileEditor))

			// Private

			case ._private(.refreshOwnProfile):
				return .run { send in
					let ownProfile = try await userClient.refreshSession()
					let profileOverview = SideProfileOverview.State(ownProfile: ownProfile)
					await send(._private(.setOwnProfileOverview(profileOverview)))
				} catch: { error, _ in
					log.error("Cannot refresh own profile screen due to \(error).")
				}
				.cancellable(id: CancelID.ownProfileLoad, cancelInFlight: true)

			case let ._private(.setOwnProfileOverview(ownProfileOverview)):
				state.ownProfileOverview = ownProfileOverview
				return .none

			case ._private(.signOut):
				return .run { send in
					await send(._private(.setIsPerformingSignOut(true)))
					try await userClient.signOut()
					await send(._private(.setIsPerformingSignOut(false)))
					await send(.delegate(.didSignOut))
				} catch: { error, send in
					await toastClient.present(.error(
						L10n.profileToastSignOutFailedTitle,
						subtitle: "\(error.localizedDescription)"
					))
					await send(._private(.setIsPerformingSignOut(false)))
				}

			case let ._private(.setIsPerformingSignOut(isPerformingSignOut)):
				state.isPerformingSignOut = isPerformingSignOut
				return .none

			case ._private(.openProfileEditor):
				return .send(.delegate(.shouldOpenEditor(ProfileEditor.State())))
			}
		}
		.ifLet(\.ownProfileOverview, action: /Action.ownProfileOverview) {
			SideProfileOverview()
		}
	}

}

// MARK: - Action

extension Profile {

	public enum Action: Equatable, Sendable {

		// MARK: Case

		case delegate(Delegate)

		case ownProfileOverview(SideProfileOverview.Action)

		case view(View)

		case _private(Private)

	}

}

// MARK: - Action: delegate

extension Profile.Action {

	public enum Delegate: Equatable, Sendable {

		// MARK: Case

		case didSignOut

		case shouldOpenEditor(ProfileEditor.State)

	}

}

// MARK: - Action: view

extension Profile.Action {

	public enum View: Equatable, Sendable {

		// MARK: Case

		case didAppear

		case didTapEditButton

		case didTapSignOutButton

	}

}

// MARK: - Action: delegate

extension Profile.Action {

	public enum Private: Equatable, Sendable {

		// MARK: Case

		case refreshOwnProfile

		case openProfileEditor

		case signOut

		case setOwnProfileOverview(SideProfileOverview.State?)

		case setIsPerformingSignOut(Bool)

	}

}

// MARK: - State

extension Profile {

	public struct State: Equatable, Sendable {

		// MARK: Exposed properties

		public var isPerformingSignOut: Bool

		public var areUserInteractionsEnabled: Bool {
			guard !isPerformingSignOut else {
				return false
			}
			return true
		}

		public let headerTitle: String = L10n.profileHeaderTitle

		public var ownProfileOverview: SideProfileOverview.State?

		// MARK: Init

		public init(
			ownProfileOverview: SideProfileOverview.State? = nil
		) {
			self.ownProfileOverview = ownProfileOverview
			self.isPerformingSignOut = false
		}

	}

}
