// swift-tools-version: 5.8
import PackageDescription

// MARK: - Helper

private enum _PackageTarget: String {

	// Local package dependency targets

	case constants = "Constants"
	case extensions = "Extensions"
	case log = "Log"
	case models = "Models"
	case systemInformation = "SystemInformation"

	case apiClient = "APIClient"
	case chatClient = "ChatClient"
	case contactsClient = "ContactsClient"
	case countdownTimer = "CountdownTimer"
	case firebaseClient = "FirebaseClient"
	case googleAuthClient = "GoogleAuthClient"
	case hobbyClient = "HobbyClient"
	case lightStorageClient = "LightStorageClient"
	case locationClient = "LocationClient"
	case networkClient = "NetworkClient"
	case notificationCenterClient = "NotificationCenterClient"
	case toastClient = "ToastClient"
	case userClient = "UserClient"
	case userNotificationsClient = "UserNotificationsClient"

	// 3rd party dependency targets

	case composableArchitecture = "ComposableArchitecture"
	case identifiedCollections = "IdentifiedCollections"

	// Targets

	case alert = "Alert"
	case application = "Application"
	case chatList = "ChatList"
	case chatRoom = "ChatRoom"
	case contacts = "Contacts"
	case home = "Home"
	case localizable = "Localizable"
	case match = "Match"
	case profile = "Profile"
	case profileEditor = "ProfileEditor"
	case sideProfileOverview = "SideProfileOverview"
	case signIn = "SignIn"
	case signUp = "SignUp"
	case splash = "Splash"

	// Exposed properties

	fileprivate var asTargetDependency: Target.Dependency {
		switch self {
		// coffemate-common
		case .constants,
			 .extensions,
			 .log,
			 .models,
			 .systemInformation:
			return .product(name: rawValue, package: "coffeemate-common")
		// coffemate-services
		case .apiClient,
			 .chatClient,
			 .contactsClient,
			 .countdownTimer,
			 .firebaseClient,
			 .googleAuthClient,
			 .hobbyClient,
			 .lightStorageClient,
			 .locationClient,
			 .networkClient,
			 .notificationCenterClient,
			 .toastClient,
			 .userClient,
			 .userNotificationsClient:
			return .product(name: rawValue, package: "coffeemate-services")
		// 3rd party
		case .composableArchitecture:
			return .product(
				name: "ComposableArchitecture",
				package: "swift-composable-architecture"
			)
		case .identifiedCollections:
			return .product(
				name: "IdentifiedCollections",
				package: "swift-identified-collections"
			)
		default:
			return .target(name: rawValue)
		}
	}

	var asLibrary: Product {
		return .library(name: name, targets: [name])
	}

	var name: String {
		return rawValue
	}

}

fileprivate extension Target {

	static func target(
		_ target: _PackageTarget,
		dependencies: [_PackageTarget] = [],
		resources: [Resource]? = nil
	) -> Target {
		return .target(
			name: target.name,
			dependencies: dependencies.map(\.asTargetDependency),
			resources: resources
		)
	}

	static func testTarget(
		_ target: _PackageTarget,
		dependencies: [_PackageTarget] = []
	) -> Target {
		return .testTarget(
			name: target.name,
			dependencies: dependencies.map(\.asTargetDependency)
		)
	}
}

fileprivate extension Product {

	static func library(from target: _PackageTarget) -> Product {
		return target.asLibrary
	}

}

// MARK: Package

var package = Package(
	name: "coffemate-core",
	defaultLocalization: .init("en"),
	platforms: [.iOS(.v16)],
	products: [
		.library(from: .alert),
		.library(from: .application),
		.library(from: .chatList),
		.library(from: .chatRoom),
		.library(from: .contacts),
		.library(from: .home),
		.library(from: .localizable),
		.library(from: .match),
		.library(from: .profile),
		.library(from: .profileEditor),
		.library(from: .sideProfileOverview),
		.library(from: .signIn),
		.library(from: .signUp),
		.library(from: .splash)
	],
	dependencies: [
		.package(name: "coffeemate-common", path: "coffeemate-common"),
		.package(name: "coffeemate-services", path: "coffeemate-services"),
		.package(
			url: "https://github.com/pointfreeco/swift-identified-collections.git",
			from: Version(1, 0, 0)
		),
		.package(
			url: "https://github.com/pointfreeco/swift-composable-architecture.git",
			from: Version(1, 1, 0)
		)
	]
)

// MARK: Targets

package.targets += [
	.target(.alert, dependencies: [
		.composableArchitecture,
		.extensions
	]),
	.target(.application, dependencies: [
		.composableArchitecture,
		.countdownTimer,
		.firebaseClient,
		.googleAuthClient,
		.home,
		.locationClient,
		.signIn,
		.signUp,
		.splash,
		.systemInformation,
		.toastClient,
		.userClient,
		.userNotificationsClient
	]),
	.target(.chatList, dependencies: [
		.chatClient,
		.composableArchitecture,
		.constants,
		.contacts,
		.countdownTimer,
		.extensions,
		.identifiedCollections,
		.localizable,
		.log,
		.models
	]),
	.target(.chatRoom, dependencies: [
		.chatClient,
		.composableArchitecture,
		.constants,
		.contactsClient,
		.extensions,
		.identifiedCollections,
		.localizable,
		.log,
		.models,
		.sideProfileOverview,
		.toastClient,
		.userClient
	]),
	.target(.contacts, dependencies: [
		.chatRoom,
		.chatClient,
		.composableArchitecture,
		.contactsClient,
		.identifiedCollections,
		.localizable,
		.log,
		.match,
		.models,
		.sideProfileOverview,
		.toastClient,
		.userClient
	]),
	.target(.home, dependencies: [
		.chatList,
		.chatRoom,
		.constants,
		.contacts,
		.extensions,
		.localizable,
		.log,
		.models,
		.profile,
		.profileEditor,
		.sideProfileOverview,
		.toastClient,
		.userClient
	]),
	.target(.localizable, resources: [
		.copy("en.lproj/Localizable.strings")
	]),
	.target(.match, dependencies: [
		.apiClient,
		.chatClient,
		.chatRoom,
		.composableArchitecture,
		.constants,
		.extensions,
		.log,
		.localizable,
		.models,
		.toastClient
	]),
	.target(.profile, dependencies: [
		.composableArchitecture,
		.extensions,
		.hobbyClient,
		.localizable,
		.log,
		.models,
		.profileEditor,
		.sideProfileOverview,
		.systemInformation,
		.userClient
	]),
	.target(.profileEditor, dependencies: [
		.composableArchitecture,
		.extensions,
		.hobbyClient,
		.localizable,
		.log,
		.models,
		.sideProfileOverview,
		.systemInformation,
		.userClient
	]),
	.target(.sideProfileOverview, dependencies: [
		.composableArchitecture,
		.extensions,
		.localizable,
		.log,
		.models,
		.userClient
	]),
	.target(.signIn, dependencies: [
		.composableArchitecture,
		.localizable,
		.log,
		.toastClient,
		.userClient
	]),
	.target(.signUp, dependencies: [
		.composableArchitecture,
		.identifiedCollections,
		.hobbyClient,
		.localizable,
		.log,
		.userClient,
		.toastClient
	]),
	.target(.splash, dependencies: [
		.composableArchitecture,
		.countdownTimer
	])
]
