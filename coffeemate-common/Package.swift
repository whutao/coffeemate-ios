// swift-tools-version: 5.8
import PackageDescription

let package = Package(
	name: "coffeemate-common",
	platforms: [.iOS(.v16)],
	products: [
		.library(name: "Constants", targets: ["Constants"]),
		.library(name: "Extensions", targets: ["Extensions"]),
		.library(name: "Log", targets: ["Log"]),
		.library(name: "Mocking", targets: ["Mocking"]),
		.library(name: "Models", targets: ["Models"]),
		.library(name: "SystemInformation", targets: ["SystemInformation"])
	],
	dependencies: [
		.package(
			url: "https://github.com/pointfreeco/swift-composable-architecture.git",
			from: Version(1, 1, 0)
		),
		.package(
			url: "https://github.com/pointfreeco/swift-identified-collections.git",
			from: Version(1, 0, 0)
		),
		.package(
			url: "https://github.com/whutao/swift-logger.git",
			from: Version(1, 0, 1)
		)
	],
	targets: [
		.target(name: "Constants", dependencies: ["Models"]),
		.target(name: "Extensions", dependencies: [
			.product(name: "ComposableArchitecture", package: "swift-composable-architecture")
		]),
		.target(
			name: "Log",
			dependencies: [
				.product(name: "Logger", package: "swift-logger")
			]
		),
		.target(name: "Mocking", dependencies: ["Extensions", "Models"]),
		.target(name: "Models", dependencies: [
			"Extensions",
			.product(
				name: "IdentifiedCollections",
				package: "swift-identified-collections"
			)
		]),
		.target(name: "SystemInformation", dependencies: ["Extensions"])
	]
)
