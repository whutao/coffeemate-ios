//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import UIKit

extension UIApplication {

	/// Returns the current key window of the app if present.
	public var currentKeyWindow: UIWindow? {
		return UIApplication.shared.connectedScenes
			.first(where: { $0 is UIWindowScene && $0.activationState == .foregroundActive })
			.flatMap({ $0 as? UIWindowScene })?
			.windows
			.first(where: \.isKeyWindow)
	}

}
