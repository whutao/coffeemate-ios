//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Photos
import UIKit

// MARK: - PHAsset+image

extension PHAsset {

	public func image(size: CGSize, completion: @escaping (UIImage?) -> Void) -> PHImageRequestID {
		let scaledSize = CGSize(
			width: size.width * UIScreen.main.scale,
			height: size.height * UIScreen.main.scale
		)

		let options = PHImageRequestOptions()
		options.isNetworkAccessAllowed = true
		options.deliveryMode = .opportunistic

		return PHCachingImageManager.default().requestImage(
			for: self,
			targetSize: scaledSize,
			contentMode: .aspectFill,
			options: options,
			resultHandler: { image, _ in
				completion(image)
			}
		)
	}

}
