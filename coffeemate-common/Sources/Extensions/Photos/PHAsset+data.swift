//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import Photos

extension PHAsset {

	/// Fetches and returns image or video data from this PHAsset. Any faced error is converted into a `nil` output.
	public var data: Data? {
		get async {
			return await withCheckedContinuation { continuation in
				if mediaType == .image {
					let options = PHImageRequestOptions()
					options.isNetworkAccessAllowed = true
					options.deliveryMode = .highQualityFormat
					options.isSynchronous = true

					PHCachingImageManager.default().requestImageDataAndOrientation(
						for: self,
						options: options
					) { imageData, _, _, info in
						guard info?.keys.contains(PHImageResultIsDegradedKey) == true else {
							fatalError("PHImageManager with `options.isSynchronous = true` should call result ONE time.")
						}
						continuation.resume(returning: imageData)
					}
				} else if mediaType == .video {
					let options = PHVideoRequestOptions()
					options.isNetworkAccessAllowed = true
					options.deliveryMode = .highQualityFormat
					options.version = .current

					PHCachingImageManager.default().requestAVAsset(
						forVideo: self,
						options: options
					) { avAsset, _, _ in
						if let asset = avAsset as? AVURLAsset {
							let videoData = try? Data(contentsOf: asset.url)
							continuation.resume(returning: videoData)
						} else {
							continuation.resume(returning: nil)
						}
					}
				} else {
					continuation.resume(returning: nil)
				}
			}
		}
	}

}
