//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import Photos

// MARK: - PHRequester

private actor PHRequesterForURL {

	// MARK: Request kind

	enum RequestKind {
		case contentEditing(identifier: PHContentEditingInputRequestID)
		case imageRequest(identifier: PHImageRequestID)
	}

	// MARK: Exposed properties

	var currentRequest: RequestKind?

	// MARK: Exposed methods

	func store(_ request: RequestKind) {
		self.currentRequest = request
	}

	func cancel(asset: PHAsset) {
		if case let .contentEditing(identifier: identifier) = currentRequest {
			asset.cancelContentEditingInputRequest(identifier)
		} else if case let .imageRequest(identifier: identifier) = currentRequest {
			PHCachingImageManager.default().cancelImageRequest(identifier)
		}
	}

}

extension PHAsset {

	private func cancellableRequestForUrl(completion: @escaping (URL?) -> Void) -> PHRequesterForURL.RequestKind? {
		if mediaType == .image {
			let options = PHContentEditingInputRequestOptions()
			options.canHandleAdjustmentData = { _ in
				return true
			}
			return .contentEditing(identifier: requestContentEditingInput(
				with: options,
				completionHandler: { (contentEditingInput, _) in
					completion(contentEditingInput?.fullSizeImageURL)
				}
			))
		} else if mediaType == .video {
			let options = PHVideoRequestOptions()
			options.version = .current
			options.isNetworkAccessAllowed = true
			options.deliveryMode = .highQualityFormat
			return .imageRequest(identifier: PHCachingImageManager.default().requestAVAsset(
				forVideo: self,
				options: options
			) { avAsset, _, _ in
				let asset = avAsset as? AVURLAsset
				completion(asset?.url)
			})
		} else {
			return nil
		}
	}

}

// MARK: - PHAsset+url

extension PHAsset {

	public var url: URL? {
		get async {
			let requester = PHRequesterForURL()
			return await withTaskCancellationHandler {
				await withCheckedContinuation { continuation in
					let request = cancellableRequestForUrl { url in
						continuation.resume(returning: url)
					}
					if let request = request {
						Task {
							await requester.store(request)
						}
					}
				}
			} onCancel: {
				Task {
					await requester.cancel(asset: self)
				}
			}
		}
	}

}
