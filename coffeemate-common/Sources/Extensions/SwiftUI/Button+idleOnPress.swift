//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import SwiftUI

// MARK: - Style

private struct IdleOnPressButtonStyle: ButtonStyle {

	// MARK: - Exposed methods

	fileprivate func makeBody(configuration: Configuration) -> some View {
		configuration.label
	}

}

// MARK: - Button+scaleOnPress

extension Button {

	@ViewBuilder public func idleOnPress() -> some View {
		buttonStyle(IdleOnPressButtonStyle())
	}

}
