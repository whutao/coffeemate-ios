//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import SwiftUI

// MARK: - Modifier

private struct DisabledAndFaintModifier: ViewModifier {

	// MARK: Properties

	private let opacity: Double

	private let condition: Bool

	// MARK: Init

	init(condition: Bool, opacity: Double) {
		self.opacity = opacity
		self.condition = condition
	}

	// MARK: Exposed methods

	func body(content: Content) -> some View {
		content
			.disabled(condition)
			.opacity(condition ? opacity : 1.0)
	}

}

// MARK: - View+disabledAndFaintIf

extension View {

	@ViewBuilder public func disabledAndFaintIf(
		_ condition: @autoclosure () -> Bool,
		opacity: Double = 0.7
	) -> some View {
		modifier(DisabledAndFaintModifier(condition: condition(), opacity: opacity))
	}

}
