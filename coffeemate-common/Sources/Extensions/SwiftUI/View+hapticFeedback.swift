//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import SwiftUI

extension View {

	public func sendHapticFeedback(_ style: UIImpactFeedbackGenerator.FeedbackStyle) {
		UIImpactFeedbackGenerator(style: style).impactOccurred()
	}

}
