//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import SwiftUI

// MARK: - View+if

extension View {

	/// Applies a transform to this view if a specified condition holds.
	@ViewBuilder public func `if`<Content: View>(
		_ condition: @autoclosure () -> Bool,
		transform: (Self) -> Content
	) -> some View {
		if condition() {
			transform(self)
		} else {
			self
		}
	}

	/// Applies a transform to this view if a specified condition does **not** hold.
	@ViewBuilder public func ifNot<Content: View>(
		_ condition: @autoclosure () -> Bool,
		transform: (Self) -> Content
	) -> some View {
		if condition() {
			self
		} else {
			transform(self)
		}
	}

}
