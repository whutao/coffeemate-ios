//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import SwiftUI

// MARK: - Extension

extension View {

	@ViewBuilder public func frame(
		size: CGSize,
		alignment: Alignment = .center
	) -> some View {
		frame(
			width: size.width,
			height: size.height,
			alignment: alignment
		)
	}

}
