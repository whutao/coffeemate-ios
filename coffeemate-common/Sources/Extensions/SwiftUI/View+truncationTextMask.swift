//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import SwiftUI

// MARK: - Modifier

private struct TruncationTextMaskModifier: ViewModifier {

	// MARK: Private properties

	@Environment(\.layoutDirection) private var layoutDirection

	private let size: CGSize

	private let enabled: Bool

	// MARK: Init

	init(size: CGSize, enabled: Bool) {
		self.size = size
		self.enabled = enabled
	}

	// MARK: Exposed methods

	func body(content: Content) -> some View {
		if enabled {
			content.mask(
				VStack(spacing: .zero) {
					Rectangle()
					HStack(spacing: .zero) {
						Rectangle()
						HStack(spacing: .zero) {
							LinearGradient(
								gradient: .init(stops: [
									Gradient.Stop(color: .black, location: 0),
									Gradient.Stop(color: .clear, location: 0.9)
								]),
								startPoint: layoutDirection == .rightToLeft ? .trailing : .leading,
								endPoint: layoutDirection == .rightToLeft ? .leading : .trailing
							)
							.frame(size: size)

							Rectangle()
								.foregroundColor(.clear)
								.frame(width: size.width)
						}
					}
					.frame(height: size.height)
				}
			)
		} else {
			content
				.fixedSize(horizontal: false, vertical: true)
		}
	}
}

// MARK: - View extension

extension View {

	public func truncationMask(size: CGSize, enabled: Bool = true) -> some View {
		modifier(TruncationTextMaskModifier(size: size, enabled: enabled))
	}

}
