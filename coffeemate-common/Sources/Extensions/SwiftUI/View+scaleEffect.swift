//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import SwiftUI

extension View {

	@ViewBuilder public func scaleEffect(
		amount: CGFloat,
		anchor: UnitPoint = .center
	) -> some View {
		scaleEffect(x: amount, y: amount, anchor: anchor)
	}

}
