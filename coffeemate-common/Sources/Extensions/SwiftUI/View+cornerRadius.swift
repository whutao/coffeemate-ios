//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import SwiftUI

// MARK: - Shape

private struct RoundedCorner: Shape {

	// MARK: Private properties

	private var radius: CGFloat = .infinity

	private var corners: UIRectCorner = .allCorners

	// MARK: Init

	init(radius: CGFloat, corners: UIRectCorner) {
		self.radius = radius
		self.corners = corners
	}

	// MARK: Exposed properties

	func path(in rect: CGRect) -> Path {
		let path = UIBezierPath(
			roundedRect: rect,
			byRoundingCorners: corners,
			cornerRadii: CGSize(width: radius, height: radius)
		)
		return Path(path.cgPath)
	}

}

// MARK: - View extension

extension View {

	public func cornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
		clipShape(RoundedCorner(radius: radius, corners: corners))
	}

}
