//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import SwiftUI

// MARK: - Focusable

public enum Focusable: Hashable {

	// MARK: Case

	case uuid(UUID)

}

// MARK: - Global focus state

public final class GlobalFocusState: ObservableObject {

	// MARK: Exposed properties

	@Published public var focus: Focusable?

	// MARK: Init

	public init(focus: Focusable? = nil) {
		self.focus = focus
	}

}

// MARK: - Custom focus modifier

private struct CustomFocusModifier<T: Hashable>: ViewModifier {

	// MARK: Private properties

	@Binding private var binding: T

	@FocusState private var focus: Bool

	private var value: T

	// MARK: Init

	init(_ binding: Binding<T>, equals value: T) {
		self._binding = binding
		self.value = value
		self.focus = (binding.wrappedValue == value)
	}

	func body(content: Content) -> some View {
		content
			.focused($focus, equals: true)
			.onChange(of: binding) { newBinding in
				focus = (newBinding == value)
			}
			.onChange(of: focus) { isFocused in
				if isFocused {
					binding = value
				}
			}
	}
}

// MARK: - View extension

extension View {

	public func customFocus<Value>(
		_ binding: Binding<Value>,
		equals value: Value
	) -> some View where Value: Hashable {
		modifier(CustomFocusModifier(binding, equals: value))
	}

}
