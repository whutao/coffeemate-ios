//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

// https://www.fivestars.blog/articles/swiftui-share-layout-information/

import SwiftUI

// MARK: - Modifier

private struct SizePreferenceKey: PreferenceKey {

	static var defaultValue: CGSize = .zero

	static func reduce(value: inout CGSize, nextValue: () -> CGSize) {

	}

}

// MARK: - View extension

extension View {

	public func readSize(onChange: @escaping (CGSize) -> Void) -> some View {
		background(
			GeometryReader { geometryProxy in
				Color.clear.preference(
					key: SizePreferenceKey.self,
					value: geometryProxy.size
				)
			}
		)
		.onPreferenceChange(SizePreferenceKey.self, perform: onChange)
	}

}
