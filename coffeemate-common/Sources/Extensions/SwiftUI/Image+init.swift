//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import SwiftUI

// MARK: - Image+init

extension Image {

	/// Creates an image from raw data.
	public init?(data: Data) {
		if let uiImage = UIImage(data: data) {
			self.init(uiImage: uiImage)
		} else {
			return nil
		}
	}

}
