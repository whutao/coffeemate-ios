//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import SwiftUI

// MARK: - Style

private struct ScalableOnPressButtonStyle: ButtonStyle {

	// MARK: - Private properties

	fileprivate var scaleAmount: CGFloat

	// MARK: - Exposed methods

	fileprivate func makeBody(configuration: Configuration) -> some View {
		configuration.label
			.scaleEffect(configuration.isPressed ? scaleAmount : 1.0)
	}

}

// MARK: - Button+scaleOnPress

extension Button {

	@ViewBuilder public func scaleOnPress(
		by value: CGFloat = 0.96
	) -> some View {
		buttonStyle(ScalableOnPressButtonStyle(scaleAmount: value))
	}

}
