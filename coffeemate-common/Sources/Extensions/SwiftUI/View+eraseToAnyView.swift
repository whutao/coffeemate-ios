//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import SwiftUI

extension View {

	@ViewBuilder public func eraseToAnyView() -> AnyView {
		AnyView(self)
	}

}
