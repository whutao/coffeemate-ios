//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import SwiftUI

// MARK: - View

public struct ForEachDevice<Content: View>: View {

	// MARK: Private properties

	private let devices: [PreviewDevice]

	@ViewBuilder private let content: () -> Content

	// MARK: Init

	public init(
		_ devices: [PreviewDevice],
		@ViewBuilder content: @escaping () -> Content
	) {
		self.devices = devices
		self.content = content
	}

	// MARK: Body

	public var body: some View {
		ForEach(devices, id: \.rawValue) { device in
			content()
				.previewDevice(device)
				.previewDisplayName(device.rawValue)
		}
	}

}

// MARK: - Preview

#if DEBUG
struct ForEachDevice_Preview: PreviewProvider {

	static var previews: some View {
		ForEachDevice([.iPhone8, .iPhone13mini]) {
			Rectangle()
				.fill(.red)
				.ignoresSafeArea()
		}
	}

}
#endif
