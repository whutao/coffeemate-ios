//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import SwiftUI

// MARK: - Helper

private struct BlinkForeverContainerView<Content: View>: View {

	// MARK: Private properties

	@State private var isAnimating: Bool = false

	let content: () -> Content

	// MARK: Init

	init(content: @escaping () -> Content) {
		self.content = content
	}

	// MARK: Body

	var body: some View {
		content()
			.opacity(isAnimating ? 0.3 : 1.0)
			.animation(
				.timingCurve(0.5, 0.35, 0.25, 1, duration: 1.5)
				.repeatForever(autoreverses: true),
				value: isAnimating
			)
			.onAppear {
				isAnimating = true
			}
			.onDisappear {
				isAnimating = false
			}
	}

}

// MARK: - View+blinkForever

extension View {

	@ViewBuilder public func blinkForever() -> some View {
		BlinkForeverContainerView {
			self
		}
	}

}

// MARK: - Previews

#if DEBUG
struct BlinkForeverContainerView_Preview: PreviewProvider {

	static var previews: some View {
		ZStack {
			Color.gray
				.ignoresSafeArea()

			Text("Hwllo")
				.foregroundColor(.white)
				.font(.system(size: 30, weight: .bold))
				.blinkForever()
		}
	}

}
#endif
