//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import CoreGraphics
import Foundation

// MARK: - Extension

extension CGSize {

	@inlinable @inline(__always) public static func square(_ side: CGFloat) -> CGSize {
		return CGSize(width: side, height: side)
	}

}
