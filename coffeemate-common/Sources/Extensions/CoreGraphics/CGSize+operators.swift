//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 / Present
//
//  All Rights Reserved.
//

import CoreGraphics

// MARK: "+"

extension CGSize {

	public static func + (_ lhs: CGSize, _ rhs: CGSize) -> CGSize {
		return CGSize(
			width: lhs.width + rhs.width,
			height: lhs.height + rhs.height
		)
	}

}

// MARK: "*"

extension CGSize {

	public static func * <I: BinaryInteger>(_ lhs: CGSize, rhs: I) -> CGSize {
		return CGSize(
			width: lhs.width * CGFloat(rhs),
			height: lhs.height * CGFloat(rhs)
		)
	}

	public static func * <I: BinaryInteger>(_ lhs: I, rhs: CGSize) -> CGSize {
		return CGSize(
			width: CGFloat(lhs) * rhs.width,
			height: CGFloat(lhs) * rhs.height
		)
	}

	public static func * <F: BinaryFloatingPoint>(_ lhs: CGSize, rhs: F) -> CGSize {
		return CGSize(
			width: lhs.width * CGFloat(rhs),
			height: lhs.height * CGFloat(rhs)
		)
	}

	public static func * <F: BinaryFloatingPoint>(_ lhs: F, rhs: CGSize) -> CGSize {
		return CGSize(
			width: CGFloat(lhs) * rhs.width,
			height: CGFloat(lhs) * rhs.height
		)
	}

}

// MARK: "/"

extension CGSize {

	public static func / <I: BinaryInteger>(_ lhs: CGSize, rhs: I) -> CGSize {
		return CGSize(
			width: lhs.width / CGFloat(rhs),
			height: lhs.height / CGFloat(rhs)
		)
	}

	public static func / <I: BinaryInteger>(_ lhs: I, rhs: CGSize) -> CGSize {
		return CGSize(
			width: CGFloat(lhs) / rhs.width,
			height: CGFloat(lhs) / rhs.height
		)
	}

	public static func / <F: BinaryFloatingPoint>(_ lhs: CGSize, rhs: F) -> CGSize {
		return CGSize(
			width: lhs.width / CGFloat(rhs),
			height: lhs.height / CGFloat(rhs)
		)
	}

	public static func / <F: BinaryFloatingPoint>(_ lhs: F, rhs: CGSize) -> CGSize {
		return CGSize(
			width: CGFloat(lhs) / rhs.width,
			height: CGFloat(lhs) / rhs.height
		)
	}

}
