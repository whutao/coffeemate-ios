//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import CoreGraphics
import Foundation

// MARK: - CGRect+init

extension CGRect {

	public init(center: CGPoint, size: CGSize) {
		let originX = center.x - size.width / 2
		let originY = center.y - size.height / 2
		self.init(origin: CGPoint(x: originX, y: originY), size: size)
	}

}
