//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import CoreGraphics
import Foundation
import ImageIO.CGImageDestination
import UniformTypeIdentifiers

// MARK: - CGImage+jpegData

extension CGImage {

	/// Extracrts JPEG image data from CGImage instance.
	public var jpegData: Data? {
		let jpegIdentifier = UTType.jpeg.identifier as CFString
		guard
			let mutableData = CFDataCreateMutable(nil, 0),
			let destination = CGImageDestinationCreateWithData(mutableData, jpegIdentifier, 1, nil)
		else {
			return nil
		}
		CGImageDestinationAddImage(destination, self, nil)
		guard CGImageDestinationFinalize(destination) else {
			return nil
		}
		return mutableData as Data
	}

}
