//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import CoreGraphics
import Foundation

// MARK: - CGRect+center

extension CGRect {

	public var center: CGPoint {
		return CGPoint(x: midX, y: midY)
	}

}
