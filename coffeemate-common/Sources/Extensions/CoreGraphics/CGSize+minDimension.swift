//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import CoreGraphics
import Foundation

extension CGSize {

	@inlinable @inline(__always) public var minDimension: CGFloat {
		return min(width, height)
	}

	@inlinable @inline(__always) public var maxDimension: CGFloat {
		return max(width, height)
	}

}
