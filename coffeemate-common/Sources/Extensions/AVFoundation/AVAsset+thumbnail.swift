//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import AVFoundation
import CoreMedia
import Foundation

// MARK: - AVAsset+thumbnail

extension AVAsset {

	/// Returns a JPEG image data for a video asset at specified time.
	public func thumbnail(at cmTime: CMTime = CMTimeMake(value: 1, timescale: 60)) -> Data? {
		let imageGenerator = AVAssetImageGenerator(asset: self)
		imageGenerator.appliesPreferredTrackTransform = true
		let thumbnailImage = try? imageGenerator.copyCGImage(
			at: cmTime,
			actualTime: nil
		)
		return thumbnailImage?.jpegData
	}

}
