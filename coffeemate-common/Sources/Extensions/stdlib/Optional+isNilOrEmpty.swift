//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

// MARK: String

import Foundation

extension Optional where Wrapped == String {

	/// Returns `true` if an optional contains a non-empty string value, `false` otherwise.
	///
	/// A convenient way to write, for example:
	/// ```
	/// let optionalString: String? = ""
	///
	/// print(optionalString.isNilOrEmpty)
	/// ```
	/// instead of:
	/// ```
	/// let optionalString: String? = ""
	///
	/// print(optionalString == nil || optionalString == "")
	/// ```
	@inline(__always) public var isNilOrEmpty: Bool {
		if let string = self {
			return string.isEmpty
		}
		return true
	}

}

// MARK: Collection

extension Optional where Wrapped: Collection {

	/// Returns `true` if an optional contains a non-empty string value, `false` otherwise.
	///
	/// A convenient way to write, for example:
	/// ```
	/// let optionalCollection: Array<Int>? = []
	///
	/// print(optionalCollection.isNilOrEmpty)
	/// ```
	/// instead of:
	/// ```
	/// let optionalCollection: Array<Int>? = []
	///
	/// print(optionalCollection == nil || optionalCollection == [])
	/// ```
	@inline(__always) public var isNilOrEmpty: Bool {
		if let collection = self {
			return collection.isEmpty
		}
		return true
	}

}
