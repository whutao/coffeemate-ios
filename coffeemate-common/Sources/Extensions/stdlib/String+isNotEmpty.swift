//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

extension String {

	/// Returns `true` if an string contains a non-empty value, `false` otherwise.
	@inline(__always) public var isNotEmpty: Bool {
		return !isEmpty
	}

}
