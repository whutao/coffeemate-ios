//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

extension String {

	@inline(__always) public func firstLetters(count: UInt = 1) -> String {
		return String(prefix(Int(count)))
	}

}
