//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

extension String {

	/// Returns a substring, up to the specified maximum length, containing the initial part of the string.
	@inline(__always) public func stringPrefix<I: BinaryInteger>(_ maxLength: I) -> String {
		return String(prefix(Int(maxLength)))
	}

}
