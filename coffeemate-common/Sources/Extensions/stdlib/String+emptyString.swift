//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

extension String {

	/// Empty string aka `""`.
	@inline(__always) public static let empty: String = ""

}
