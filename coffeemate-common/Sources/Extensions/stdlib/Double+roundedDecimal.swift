//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

extension Double {

	/// Convert `Double` to `Decimal`, rounding it to `scale` decimal places.
	///
	/// - Parameters:
	///   - scale: How many decimal places to round to. Defaults to `0`.
	///   - mode:  The preferred rounding mode. Defaults to `.plain`.
	/// - Returns: The rounded `Decimal` value.

	public func roundedDecimal(
		to scale: Int = 0,
		mode: NSDecimalNumber.RoundingMode = .plain
	) -> Decimal {
		var decimalValue = Decimal(self)
		var result = Decimal()
		NSDecimalRound(&result, &decimalValue, scale, mode)
		return result
	}

}
