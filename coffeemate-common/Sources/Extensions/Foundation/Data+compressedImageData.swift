//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import UIKit

// MARK: - Data+compressedImageData

extension Data {

	/// If this instance contains image data, returns the compressed version of it to fit the specified number of KB.
	public func compressedImageData(toFitKB sizeThreshold: UInt) -> Data? {
		guard let image = UIImage(data: self) else {
			return nil
		}
		guard var imageData = image.jpegData(compressionQuality: 1.0) else {
			return nil
		}

		var compressionQuality: CGFloat = 0.9

		let sizeThresholdInBytes = sizeThreshold * 1024

		while imageData.count > sizeThresholdInBytes, compressionQuality > 0.01 {
			if let compressedData = image.jpegData(compressionQuality: compressionQuality) {
				compressionQuality = compressionQuality * 0.8
				imageData = compressedData
			}
		}

		return imageData
	}

}
