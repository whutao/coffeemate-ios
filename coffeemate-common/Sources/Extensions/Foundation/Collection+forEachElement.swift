//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

extension MutableCollection {

	/// Applies a mutation for each element that satisfies the given predicate.
	public mutating func forEachElement(
		suchThat predicate: (Element) -> Bool,
		apply modification: (Element) -> Element
	) {
		for index in indices {
			let existingElement = self[index]
			if predicate(existingElement) {
				self[index] = modification(existingElement)
			}
		}
	}

	/// Applies a mutation for each element that satisfies the given predicate.
	public mutating func forEachElement(
		suchThat predicate: (Element) -> Bool,
		apply modification: (inout Element) -> Void
	) {
		for index in indices {
			var existingElement = self[index]
			if predicate(existingElement) {
				modification(&existingElement)
				self[index] = existingElement
			}
		}
	}

}
