//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

extension Sequence {

	/// Performs ``forEach`` operation but waits for each element using `await`.
	public func asyncForEach(_ operation: (Element) async throws -> Void) async rethrows {
		for element in self {
			try await operation(element)
		}
	}

}
