//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import UniformTypeIdentifiers

// MARK: - FileManager+temporary

extension FileManager {

	/// Returns the URL of the temporary directory.
	public var temporaryDirectory: URL {
		return URL(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true)
	}

	/// Returns a temporary file URL inside a temporary directory.
	public func temporaryFileUrl(nameWithoutExtension name: String, contentType: UTType) -> URL {
		return temporaryDirectory.appendingPathComponent(name, conformingTo: contentType)
	}

	public func randomTemporaryFileUrl(for contentType: UTType) -> URL {
		return temporaryFileUrl(
			nameWithoutExtension: UUID().uuidString,
			contentType: contentType
		)
	}

	public func storeToTemporaryDirectory(data: Data, as contentType: UTType) throws -> URL {
		let temporaryUrl = randomTemporaryFileUrl(for: contentType)
		try data.write(to: temporaryUrl)
		return temporaryUrl
	}

	public func copyToRandomTemporaryUrl(sourceUrl: URL) throws -> URL {
		let contentType = sourceUrl.contentType.unwrapped(or: .data)
		let temporaryUrl = randomTemporaryFileUrl(for: contentType)
		try copyItem(at: sourceUrl, to: temporaryUrl)
		return temporaryUrl
	}

}
