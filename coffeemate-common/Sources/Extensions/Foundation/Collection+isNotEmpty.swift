//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

extension Collection {

	/// A Boolean value indicating whether the collection is not empty.
	public var isNotEmpty: Bool {
		return !isEmpty
	}

}
