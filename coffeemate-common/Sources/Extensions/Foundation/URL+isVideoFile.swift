//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import UniformTypeIdentifiers

extension URL {

	/// Returns a Boolean value that indicates whether a URL content type conforms to image.
	public var isVideoFile: Bool {
		let contentType = UTType(filenameExtension: pathExtension)
		return (contentType?.conforms(to: .video)).unwrapped(or: false)
	}

}
