//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import AVFoundation
import Foundation

// MARK: - URL+thumbnailData

extension URL {

	/// Returns a JPEG image data for a video URL.
	public var thumbnailData: Data? {
		return AVAsset(url: self).thumbnail()
	}

}
