//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

extension Collection {

	/// Returns the element at the specified index if it is within bounds, otherwise nil.
	public subscript (safe index: Index) -> Element? {
		return indices.contains(index) ? self[index] : nil
	}

}
