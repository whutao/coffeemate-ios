//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

extension Date {

	/// Calculate age given a date instance as a birthdate.
	public func age(
		now: Date = Date(),
		using calendar: Calendar = .current
	) -> Int {
		return calendar
			.dateComponents([.year], from: self, to: now)
			.year
			.unwrapped(or: .zero)
	}

}
