//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import UniformTypeIdentifiers

// MARK: - URL+contentType

extension URL {

	/// Creates a type based on a filename extension and an existing supertype that it conforms to.
	public var contentType: UTType? {
		return UTType(filenameExtension: pathExtension)
	}

}
