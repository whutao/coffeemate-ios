//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

extension Date {

	/// Rounds date down to a specified date component.
	/// Allowed components are `.year, .month, .day, .hour, .minute, .second`.
	public func rounded(
		to component: Calendar.Component,
		using calendar: Calendar = .current
	) -> Date? {
		var dateComponents = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self)
		switch component {
		case .year:
			dateComponents.month = 0
			dateComponents.day = 0
			dateComponents.hour = 0
			dateComponents.minute = 0
			dateComponents.second = 0
		case .month:
			dateComponents.day = 0
			dateComponents.hour = 0
			dateComponents.minute = 0
			dateComponents.second = 0
		case .day:
			dateComponents.hour = 0
			dateComponents.minute = 0
			dateComponents.second = 0
		case .hour:
			dateComponents.minute = 0
			dateComponents.second = 0
		case .minute:
			dateComponents.minute = 0
		case .second:
			dateComponents.second = 0
		default:
			return nil
		}
		return calendar.date(from: dateComponents).unwrapped(or: self)
	}

	/// Get date minus some specified component.
	/// Allowed components are `.year, .month, .day, .hour, .minute, .second`.
	public func subtracting(
		_ component: Calendar.Component,
		value: Int,
		using calendar: Calendar = .current
	) -> Date? {
		var dateComponents = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self)
		switch component {
		case .year:
			dateComponents.year = dateComponents.year.flatMap({ $0 - value })
		case .month:
			dateComponents.month = dateComponents.month.flatMap({ $0 - value })
		case .day:
			dateComponents.day = dateComponents.day.flatMap({ $0 - value })
		case .hour:
			dateComponents.hour = dateComponents.hour.flatMap({ $0 - value })
		case .minute:
			dateComponents.minute = dateComponents.year.flatMap({ $0 - value })
		case .second:
			dateComponents.second = dateComponents.second.flatMap({ $0 - value })
		default:
			return nil
		}
		return calendar.date(from: dateComponents)
	}

}
