//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Combine
import Foundation

// MARK: - DisposeBag

public typealias _DisposeBag = Set<AnyCancellable>

extension _DisposeBag {

	@resultBuilder public struct CancellableBuilder {

		public static func buildBlock(_ cancellables: AnyCancellable...) -> [AnyCancellable] {
			return cancellables
		}

	}

	/// Adds input cancellables into the bag.
	public mutating func collect(
		@CancellableBuilder _ cancellables: () -> [AnyCancellable]
	) {
		formUnion(cancellables())
	}

}
