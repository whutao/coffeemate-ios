//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Combine
import Foundation

// MARK: - Publisher

/// Alias for `PassthroughSubject` that does not publish errors.
public typealias EventPublisher<Value> = PassthroughSubject<Value, Never>
