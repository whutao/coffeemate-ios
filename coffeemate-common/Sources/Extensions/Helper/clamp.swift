//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: Clampings

@inline(__always) public func clamp<C: Comparable>(
	_ value: C,
	to range: ClosedRange<C>
) -> C {
	return min(range.upperBound, max(range.lowerBound, value))
}

@inline(__always) public func clamp<C: Comparable>(
	_ value: C,
	to range: PartialRangeThrough<C>
) -> C {
	return min(range.upperBound, value)
}
