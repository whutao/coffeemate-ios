//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Combine
import Foundation

// MARK: - Publisher

/// Alias for `CurrentValueSubject` that does not publish errors.
public typealias ValuePublisher<Value> = CurrentValueSubject<Value, Never>
