//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Foundation

// MARK: - Method

/// Executes an operation in the current task subject to a timeout.
///
/// - Parameters:
///   - duration: The duration during which an`operation` is allowed to run before timing out.
///   - timeoutError: The custom error that thrown after the timeout expires.
///   - operation: The async operation to perform.
/// - Returns: Returns the result of `operation` if it completed in time.
/// - Throws: Throws provided error if the timeout expires before `operation` completes.
///   If `operation` throws an error before the timeout expires, that error is propagated to the caller.
public func withTimeout<Result, TimeoutError: Error>(
	_ duration: Duration,
	throwingTimeoutError timeoutError: TimeoutError,
	operation: @escaping @Sendable () async throws -> Result
) async throws -> Result {
	return try await withThrowingTaskGroup(of: Result.self) { group in
		@Dependency(\.continuousClock) var clock

		group.addTask {
			return try await operation()
		}

		group.addTask {
			try await clock.sleep(for: duration)
			try Task.checkCancellation()
			throw timeoutError
		}

		// It is guaranteed that when the execution reaches this line,
		// the operation task is finished first, otherwise
		// timeout error will be thrown from the timeout task.
		let result = try await group.next()!

		// Cancel remaining task.
		group.cancelAll()

		return result
	}
}
