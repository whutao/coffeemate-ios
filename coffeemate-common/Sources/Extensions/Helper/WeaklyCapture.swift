//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// swiftlint:disable identifier_name

/** Weak capture taking nothing, returning nothing. */
public func weakly<T: AnyObject>(
	_ object: T,
	_ f: @escaping (T) -> () -> Void
) -> () -> Void {
	{ [weak object] in
		if let object = object {
			return f(object)()
		}
	}
}

/** Weak capture returning nothing. */
public func weakly<T: AnyObject, A>(
	_ object: T,
	_ f: @escaping (T) -> (A) -> Void
) -> (A) -> Void {
	{ [weak object] value in
		if let object = object {
			return f(object)(value)
		}
	}
}

public func weakly<T: AnyObject, A, U>(
	_ object: T,
	_ keyPath: KeyPath<T, U>,
	_ f: @escaping (U) -> (A) -> Void
) -> (A) -> Void {
	{ [weak object] element in
		guard let object = object else { return }
		let childObject = object[keyPath: keyPath]
		return f(childObject)(element)
	}
}

public func weakly<T: AnyObject, U>(
	_ object: T,
	_ keyPath: KeyPath<T, U?>,
	_ f: @escaping (U) -> () -> Void
) -> () -> Void {
	{ [weak object] in
		guard let object = object,
			  let childObject = object[keyPath: keyPath]
		else {
			return
		}
		return f(childObject)()
	}
}

/** Weak capture returning a result. */
public func weakly<T: AnyObject, A, R>(
	_ object: T,
	_ f: @escaping (T) -> (A) -> R
) -> (A) -> R? {
	{ [weak object] value in
		object.map({ f($0)(value) })
	}
}

public func weakly<T: AnyObject, A, B>(
	_ object: T,
	_ f: @escaping (T) -> (A, B) -> Void
) -> (A, B) -> Void {
	{ [weak object] a, b in
		object.map({ f($0)(a, b) })
	}
}

public func weakly<T: AnyObject, A, R>(
	_ object: T,
	_ f: @escaping (T) -> (A) -> R,
	_ onNil: R
) -> (A) -> R {
	{ [weak object] value in
		if let object = object {
			return f(object)(value)
		}
		return onNil
	}
}

public func weakly<T: AnyObject, A, B, C>(
	_ object: T,
	_ f: @escaping (T) -> (A, B, C) -> Void
) -> (A, B, C) -> Void {
	{ [weak object] a, b, c in
		object.map({ f($0)(a, b, c) })
	}
}

public func weakly<T: AnyObject, A, B, C, R>(
	_ object: T,
	_ f: @escaping (T) -> (A, B, C) -> R,
	_ onNil: R
) -> (A, B, C) -> R {
	{ [weak object] a, b, c in
		if let object = object {
			return f(object)(a, b, c)
		}
		return onNil
	}
}

public func weakly<T: AnyObject, U>(
	_ object: T,
	_ keyPath: KeyPath<T, U>,
	_ f: @escaping (U) -> () -> Void
) -> () -> Void {
	{ [weak object] in
		guard let object = object else { return }
		let childObject = object[keyPath: keyPath]
		return f(childObject)()
	}
}

/** Unowned capture, accepting () and returting a result. */
public func unownedly<T: AnyObject, R>(
	_ object: T,
	_ f: @escaping (T) -> () -> R
) -> () -> R {
	{ [unowned object] in
		f(object)()
	}
}

/** Unowned capture returing a result. */
public func unownedly<T: AnyObject, A, R>(
	_ object: T,
	_ f: @escaping (T) -> (A) -> R
) -> (A) -> R {
	{ [unowned object] value in
		f(object)(value)
	}
}

public func unownedly<T: AnyObject, A, B, C, R>(
	_ object: T,
	_ f: @escaping (T) -> (A, B, C) -> R
) -> (A, B, C) -> R {
	{ [unowned object] a, b, c in
		f(object)(a, b, c)
	}
}

public func unownedly<T: AnyObject, A, B, C, D, R>(
	_ object: T,
	_ f: @escaping (T) -> (A, B, C, D) -> R
) -> (A, B, C, D) -> R {
	{ [unowned object] a, b, c, d in
		f(object)(a, b, c, d)
	}
}

public func unownedly<T: AnyObject, A, B, R>(
	_ object: T,
	_ f: @escaping (T) -> (A, B) -> R
) -> (A, B) -> R {
	{ [unowned object] a, b in
		f(object)(a, b)
	}
}

public func setter<Object: AnyObject, Value>(
	for object: Object,
	property: ReferenceWritableKeyPath<Object, Value>
) -> (Value) -> Void {
	{ [weak object] value in
		object?[keyPath: property] = value
	}
}

// swiftlint:enable identifier_name
