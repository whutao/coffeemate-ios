//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Extensions
import Foundation

// MARK: - Model

public enum SystemInformation {

	// MARK: Exposed properties

	/// Returns `true` if *XCTestConfigurationFilePath* is found in the environment.
	public static let isRunningTests: Bool = {
		return ProcessInfo.processInfo.environment["XCTestConfigurationFilePath"].isNotNil
	}()

	/// Returns the current platform.
	public static let platform: String = "iOS"

	/// Returns the current device language.
	public static var languageCode: String {
		let language = Locale.autoupdatingCurrent.language
		let languageIdentifier = (language.languageCode?.identifier).unwrapped(or: "en")
		let languageIdentifierUppercased = languageIdentifier.uppercased()
		let allowedLanguageIdentifiers = Set(["EN", "RU"])
		if allowedLanguageIdentifiers.contains(languageIdentifierUppercased) {
			return languageIdentifierUppercased
		} else {
			return "EN"
		}
	}

	/// Returns the current version obtained from the project settings. Example: "1.1.7".
	public static let appVersion: String = {
		let plistValue = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString")
		let stringValue = plistValue as? String
		return stringValue.unwrapped(or: .empty)
	}()

	/// Returns the current version obtained from the project settings in the flat form. Example: "1.1.7" -> "11007".
	public static let appFlatVersion: String = {
		let components = appVersion.split(separator: ".").map(String.init).compactMap(Int.init)
		guard
			let major = components[safe: 0],
			let minor = components[safe: 1],
			let patch = components[safe: 2]
		else {
			return "10000"
		}
		return String(("0000" + String(describing: major * 10000 + minor * 1000 + patch)).suffix(5))
	}()

	/// Returns the current build number obtained from the project settings. Example: "44".
	public static let buildNumber: String = {
		let plistValue = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion")
		let stringValue = plistValue as? String
		return stringValue.unwrapped(or: .empty)
	}()

	/// Returns true if a current build is a DEBUG one.
	public static let isDebugBuild: Bool = {
		#if DEBUG
		return true
		#else
		return false
		#endif
	}()

}
