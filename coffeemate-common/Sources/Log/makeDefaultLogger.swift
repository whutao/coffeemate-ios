//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import Logger

// MARK: - Logger instance

/// Create new prefixed(by module name) logger with console destination.
public func makeDefaultLogger(module: String) -> LoggerProtocol {
	let modulePrefix: [LogFormatElement] = [
		.leftParanthesis, .text(module), .rightParanthesis, .space(),
		.logLevelSymbol, .space(),
		.logLevel, .space(),
		.leftBracket, .date, .rightBracket, .space(),
		.leftParanthesis,
		.fileName, .comma, .space(),
		.functionName, .comma, .space(),
		.text("line"), .space(), .lineNumber,
		.rightParanthesis,
		.space(), .dash, .space(), .text("\""), .logString, .text("\"")
	]
	let format = LogFormat(arguments: modulePrefix)
	let logger = Logger(logFormat: format, dateFormat: "hh:mm:ss dd MMM")
	logger.isEnabled = true
	logger.addDestination(ConsoleDestination())
	return logger
}
