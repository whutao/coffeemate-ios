//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Lorem

/// Generator for "Lorem" texts.
public enum Lorem {

	// MARK: Private properties

	private static let allWords: [String] = [
		"orci", "risus", "mauris", "vulputate", "dictumst",
		"ultrices", "suscipit", "vel", "quis", "sagittis",
		"nisi", "tincidunt", "hendrerit", "aliquet", "ligula",
		"purus", "integer", "volutpat", "lectus", "molestie",
		"finibus", "sed", "est", "vitae", "egestas", "neque",
		"semper", "phasellus", "vivamus", "elementum",
		"dignissim", "hac", "proin", "nulla", "donec",
		"turpis", "sapien", "dictum", "posuere", "ex", "at",
		"adipiscing", "feugiat", "tempus", "sollicitudin",
		"felis", "duis", "justo", "interdum", "ornare", "habitasse",
		"gravida", "bibendum", "metus", "iaculis", "consequat",
		"primis", "elit", "pharetra", "nisl", "viverra",
		"ullamcorper", "etiam", "platea", "tristique", "cras",
		"lobortis", "eleifend", "mollis", "rutrum", "convallis",
		"nec", "pretium", "varius", "lorem", "in", "vehicula",
		"imperdiet", "fermentum", "tellus", "quam", "libero",
		"ante", "tortor", "porta", "sodales", "amet", "luctus",
		"massa", "nibh", "rhoncus", "non", "malesuada", "porttitor",
		"maximus", "morbi", "vestibulum", "erat", "condimentum",
		"suspendisse", "auctor", "id", "ac", "venenatis", "efficitur",
		"fusce", "blandit", "diam", "enim", "nullam", "accumsan", "odio",
		"lacinia", "magna", "leo", "laoreet", "lacus", "pellentesque",
		"mi", "velit", "euismod", "praesent", "scelerisque", "sit",
		"eget", "dolor", "dapibus", "fames", "faucibus", "nam", "augue",
		"curabitur", "pulvinar", "et", "eu", "cursus", "urna", "sem",
		"a", "maecenas", "consectetur", "ipsum", "quisque", "eros",
		"arcu", "tempor", "aliquam", "facilisis", "nunc", "fringilla",
		"commodo", "facilisi", "dui", "placerat", "mattis",
		"ultricies", "aenean", "ut"
	]

	// MARK: Exposed methods

	/// Random sentence of *length* words.
	public static func sentence(ofLength length: Int) -> String {
		let rawSentance: String = Range(0...length)
			.map { _ in word }
			.joined(separator: " ")

		return rawSentance.capitalizeFirstLetter + "."
	}

	/// Random paragraph with *length* sentences.
	public static func paragraph(ofLength length: Int) -> String {
		return Range(0...length)
			.map { _ in sentence(ofLength: .random(in: 3...16)) }
			.joined(separator: " ")
	}

	// MARK: Exposed properites

	// swiftlint:disable force_unwrapping

	/// Random "Lorem Ipsum" lowercased word.
	public static var word: String {
		return allWords.randomElement()!
	}

	// swiftlint:enable force_unwrapping

	/// Random "Lorem Ipsum" capitalized word.
	public static var capitalizedWord: String {
		return word.capitalizeFirstLetter
	}

	/// Random fullname consisting of "Lorem Ipsum" words.
	public static var fullname: String {
		return capitalizedWord + " " + capitalizedWord
	}

	/// Random email consisting of "Lorem Ipsum" words.
	public static var email: String {
		return word + "_" + word + "@" + word + ".com"
	}

	/// Random "Lorem Ipsum" sentence that contains from 3 to 5 words.
	public static var shortSentence: String {
		return sentence(ofLength: .random(in: 3...5))
	}

	/// Random "Lorem Ipsum" sentence that contains from 6 to 10 words.
	public static var sentence: String {
		return sentence(ofLength: .random(in: 6...10))
	}

	/// Random "Lorem Ipsum" sentence that contains from 11 to 16 words.
	public static var longSentence: String {
		return sentence(ofLength: .random(in: 11...16))
	}

	/// Random "Lorem Ipsum" paragraph that contains from 3 to 5 senences.
	public static var shortParagraph: String {
		return paragraph(ofLength: .random(in: 3...5))
	}

	/// Random "Lorem Ipsum" paragraph that contains from 6 to 10 senences.
	public static var paragraph: String {
		return paragraph(ofLength: .random(in: 6...10))
	}

	/// Random "Lorem Ipsum" paragraph that contains from 11 to 16 senences.
	public static var longParagraph: String {
		return paragraph(ofLength: .random(in: 11...16))
	}

}

// MARK: - Helper

extension String {

	fileprivate var capitalizeFirstLetter: String {
		return prefix(1).capitalized + dropFirst()
	}

}
