//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import Models

// MARK: - Hobby + mocked

#if DEBUG
private let hobbyTitles: [String] = [
	"insta_blogger", "party", "poetry", "theatre"
]
private var hobbyIndices: ClosedRange = ClosedRange(0..<hobbyTitles.count)

extension Hobby {

	private static func mocked(index: Int, isPrivileged: Bool = false) -> Hobby {
		return Hobby(
			hobbyId: index,
			title: hobbyTitles[index],
			imageUrl: URL(string: "https://api.coffeemates.space:443/images/hobbies/\(hobbyTitles[index]).svg")!,
			isPrivileged: isPrivileged
		)
	}

	public static func allRandomMocked() -> [Hobby] {
		return hobbyIndices.map { mocked(index: $0) }
	}

	public static func randomMock(isPrivileged: Bool = false) -> Hobby {
		return mocked(index: hobbyIndices.randomElement()!, isPrivileged: isPrivileged)
	}

}
#endif
