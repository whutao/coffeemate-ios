//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import Models

#if DEBUG
extension OwnProfile {

	public static func mockedJulia() -> OwnProfile {
		return OwnProfile(
			profileId: 567,
			email: "julia@mail.ru",
			nickname: "Julia",
			gender: .female,
			targetGenders: [.female, .male],
			avatarUrl: AvatarURL.ladybug,
			rating: 4.8,
			about: "Хочу стать президентом мира и первым указом выдать каждому по котенку",
			birthDate: Date(timeIntervalSince1970: 1_026_513_795),
			creationDate: Date().addingTimeInterval(-1_000_000),
			isVerified: false,
			hobbies: Set([.randomMock(), .randomMock(), .randomMock()]),
			gallery: [.randomMockedOwn(), .randomMockedOwn()]
		)
	}

}
#endif
