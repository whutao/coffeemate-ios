//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import Models

#if DEBUG
extension ChatSender {

	public static var mockedCurrentSender: ChatSender {
		return ChatSender(senderId: "djiodojd", nickname: "Roma")
	}

	public static var mockedSideSender: ChatSender {
		return ChatSender(senderId: "mnne299m", nickname: "Julia")
	}

}
#endif
