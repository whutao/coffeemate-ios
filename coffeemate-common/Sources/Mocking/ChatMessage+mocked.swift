//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import Models

#if DEBUG
extension ChatMessage {

	public static func mockedFromCurrentSender() -> ChatMessage {
		return ChatMessage(
			messageId: UUID().uuidString,
			date: Date().addingTimeInterval(.random(in: -100_000...0)),
			content: .text([Lorem.capitalizedWord, Lorem.shortSentence, Lorem.shortParagraph].randomElement()!),
			sender: .mockedCurrentSender,
			status: .sent
		)
	}

	public static func mockedFromSideSender() -> ChatMessage {
		return ChatMessage(
			messageId: UUID().uuidString,
			date: Date().addingTimeInterval(.random(in: -100_000...0)),
			content: .text([Lorem.capitalizedWord, Lorem.shortSentence, Lorem.shortParagraph].randomElement()!),
			sender: .mockedSideSender,
			status: .sent
		)
	}

}
#endif
