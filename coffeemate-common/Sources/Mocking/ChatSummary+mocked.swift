//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import Models

#if DEBUG
extension ChatSummary {

	public static func mockedWithLastMessage() -> ChatSummary {
		return ChatSummary(
			chatId: 4,
			companion: .mocked(),
			lastMessage: .init(
				date: Date().addingTimeInterval(-10_000),
				content: .text(Lorem.longSentence),
				isIncoming: true,
				isRead: false
			),
			isHidden: false
		)
	}

	public static func mockedWithoutLastMessage() -> ChatSummary {
		return ChatSummary(
			chatId: 4,
			companion: .mocked(),
			lastMessage: nil,
			isHidden: false
		)
	}

}
#endif
