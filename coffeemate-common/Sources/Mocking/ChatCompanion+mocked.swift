//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import Models

#if DEBUG
extension ChatCompanion {

	public static func mocked() -> ChatCompanion {
		return ChatCompanion(
			profileHash: "ino2hio3nsb981",
			nickname: "Julia",
			avatarUrl: AvatarURL.ladybug
		)
	}

	public static func randomMocked() -> ChatCompanion {
		return ChatCompanion(
			profileHash: UUID().uuidString,
			nickname: Bool.random() ? Lorem.fullname : Lorem.shortSentence,
			avatarUrl: AvatarURL.ladybug
		)
	}

}
#endif
