//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Avatar URL

public enum AvatarURL {

	// MARK: Exposed properties

	public static let ladybug: URL = URL(string: "https://upload.wikimedia.org/wikipedia/ru/f/f1/Miraculous_Ladybug.png?20200817132031")!

	public static let stalin: URL = URL(string: "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Joseph_Stalin%2C_1950_%28cropped%29.jpg/440px-Joseph_Stalin%2C_1950_%28cropped%29.jpg")!

}
