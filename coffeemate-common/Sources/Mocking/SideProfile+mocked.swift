//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import Models

#if DEBUG
extension SideProfile {

	public static func mockedDenis() -> SideProfile {
		return SideProfile(
			profileHash: "abobaboba",
			avatarUrl: AvatarURL.stalin,
			nickname: "Denis",
			age: 20,
			about: "I love caramel liqueur lattes. If you are ready to campaign in Barcelona, you love literature - feel free to let me know. I love caramel liqueur lattes. If you are ready to campaign in Barcelona, you love literature - feel free to let me know. I love caramel liqueur lattes. If you are ready to campaign in Barcelona, you love literature - feel free to let me know.",
			rating: 3.5,
			ownGender: .male,
			targetGenders: [.female, .male, .other],
			hobbies: Hobby.allRandomMocked(),
			gallery: [.randomMockedSide(), .randomMockedSide(), .randomMockedSide()],
			isVerified: true
		)
	}

	public static func randomMocked() -> SideProfile {
		return SideProfile(
			profileHash: UUID().uuidString,
			avatarUrl: AvatarURL.stalin,
			nickname: Lorem.fullname,
			age: .random(in: 14...120),
			about: [Lorem.word, Lorem.shortParagraph].randomElement()!,
			rating: .random(in: 0...5),
			ownGender: .male,
			targetGenders: [.female, .male, .other],
			hobbies: Hobby.allRandomMocked(),
			gallery: [.randomMockedSide(), .randomMockedSide(), .randomMockedSide()],
			isVerified: .random()
		)
	}

}
#endif
