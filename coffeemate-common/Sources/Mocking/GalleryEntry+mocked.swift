//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import Models

#if DEBUG
extension GalleryEntry {

	public static func randomMockedOwn() -> GalleryEntry {
		return GalleryEntry(
			entryId: .random(in: Int.min...Int.max),
			image: .url(AvatarURL.stalin),
			isOwn: true
		)
	}

	public static func randomMockedSide() -> GalleryEntry {
		return GalleryEntry(
			entryId: .random(in: Int.min...Int.max),
			image: .url(AvatarURL.stalin),
			isOwn: true
		)
	}

}
#endif
