//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation
import Models

// MARK: - Constants

public enum Constants {

	// MARK: Other

	public static let areToastsEnabled: Bool = true

	public static let minDistanceDifferenceInMetersForLocationUpdate: Double = 1_000

	public static let sideContactsSearchRadiusInKm: UInt = 5000

	// MARK: API

	public enum Api {
		public static let apiBaseUrl = URL(string: "https://api.coffeemates.space:8443")!
	}

	// MARK: Google

	public enum Google {
		public static let clientId: String = "367822453540-dcn3l7c9im9cjuaf1aseg8d5j8untq4v.apps.googleusercontent.com"
	}

	// MARK: ProfileEditor

	public enum ProfileEditor {
		public static let birthDateDisplayFormat: String = "dd.MM.yyyy"
		public static let minAllowedAge: Int = 14
		public static let maxAllowedAge: Int = 114
		public static let maxAboutCharactersCount: Int = 2000
		public static let maxDeleteProfileReasonCharactersCount: Int = 2000
		public static let maxSelectedHobbies: Int = 7
		public static let maxGalleryEntryCount: Int = 10
		public static let maxImageSizeInKB: UInt = 980
		public static let maxNicknameCharactersCount: Int = 20
	}

	// MARK: Splash

	public enum Splash {
		public static let splashDurationInSeconds: UInt = 3
	}

	// MARK: Web socket

	public enum WebSocket {
		public static let autoPingInterval: TimeInterval = 10
		public static let connectionTimeout: TimeInterval = 15
		public static let connectionUrl = URL(string: "wss://api.coffeemates.space:8443/wschat/websocket")!
	}

}
