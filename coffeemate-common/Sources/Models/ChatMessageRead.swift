//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct ChatMessageRead: Codable, Equatable, Sendable {

	// MARK: Exposed properties

	public let chatId: Int

	public let messageId: ChatMessageID

	// MARK: Init

	public init(chatId: Int, messageId: ChatMessageID) {
		self.chatId = chatId
		self.messageId = messageId
	}

}
