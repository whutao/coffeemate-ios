//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct ChatCompanion: Codable, Equatable, Identifiable, Sendable {

	// MARK: Exposed properties

	public var id: String {
		return profileHash
	}

	public let profileHash: String

	public let nickname: String

	public let avatarUrl: URL

	// MARK: Init

	public init(
		profileHash: String,
		nickname: String,
		avatarUrl: URL
	) {
		self.profileHash = profileHash
		self.nickname = nickname
		self.avatarUrl = avatarUrl
	}

	public init(from sideProfile: SideProfile) {
		self.init(
			profileHash: sideProfile.profileHash,
			nickname: sideProfile.nickname,
			avatarUrl: sideProfile.avatarUrl
		)
	}

}
