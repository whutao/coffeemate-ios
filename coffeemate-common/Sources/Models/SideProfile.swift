//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Extensions
import Foundation
import IdentifiedCollections

// MARK: - Model

public struct SideProfile: Equatable, Identifiable, Hashable, Codable, Sendable {

	// MARK: Exposed properties

	public var id: String {
		return profileHash
	}

	public let profileHash: String

	public let avatarUrl: URL

	public let nickname: String

	public let age: Int

	public let about: String

	public let rating: Double

	public let ownGender: Gender

	public var targetGenders: Set<Gender>

	public let hobbies: IdentifiedArrayOf<Hobby>

	public var gallery: IdentifiedArrayOf<GalleryEntry>

	public let isVerified: Bool

	// MARK: Init

	public init(
		profileHash: String,
		avatarUrl: URL,
		nickname: String,
		age: Int,
		about: String,
		rating: Double,
		ownGender: Gender,
		targetGenders: Set<Gender>,
		hobbies: [Hobby],
		gallery: [GalleryEntry],
		isVerified: Bool
	) {
		self.profileHash = profileHash
		self.avatarUrl = avatarUrl
		self.nickname = nickname
		self.age = age
		self.about = about
		self.rating = rating
		self.ownGender = ownGender
		self.targetGenders = targetGenders
		self.hobbies = IdentifiedArrayOf(uniqueElements: hobbies.sorted(by: \.title))
		self.gallery = IdentifiedArrayOf(uniqueElements: gallery)
		self.isVerified = isVerified
	}

}
