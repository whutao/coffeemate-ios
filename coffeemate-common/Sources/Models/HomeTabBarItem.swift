//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public enum HomeTabBarItem: String, Identifiable, Equatable, Sendable {

	// MARK: Exposed properties

	public var id: String {
		return rawValue
	}

	// MARK: Case

	case chat

	case contacts

	case profile

}
