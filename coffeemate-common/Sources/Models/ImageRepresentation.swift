//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public enum ImageRepresentation: Equatable, Hashable, Codable, Sendable {

	// MARK: Case

	/// Image data stored locally.
	case data(Data)

	/// URL to a remote image.
	case url(URL)

}
