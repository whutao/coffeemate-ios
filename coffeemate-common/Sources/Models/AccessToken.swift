//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct AccessToken: Equatable, Codable, Hashable, Sendable {

	// MARK: Exposed properties

	public let value: String

	// MARK: Init

	public init(_ value: String) {
		self.value = value
	}

}
