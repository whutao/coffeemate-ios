//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct ChatMessage: Codable, Equatable, Sendable {

	// MARK: Exposed properties

	public let messageId: ChatMessageID

	public let date: Date

	public var content: Content

	public var sender: ChatSender

	public var status: Status

	// MARK: Init

	public init(
		messageId: ChatMessageID,
		date: Date,
		content: ChatMessage.Content,
		sender: ChatSender,
		status: Status
	) {
		self.messageId = messageId
		self.date = date
		self.content = content
		self.sender = sender
		self.status = status
	}

}

// MARK: - Model: kind

extension ChatMessage {

	public enum Content: Codable, Equatable, Sendable {

		// MARK: Case

		case text(String)

	}

}

// MARK: - Model: status

extension ChatMessage {

	public enum Status: Codable, Equatable, Sendable {

		// MARK: Case

		case sending

		case sent

		case read

	}

}
