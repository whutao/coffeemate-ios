//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public enum Gender: UInt8, Equatable, Identifiable, Codable, Sendable {

	// MARK: Exposed properties

	public var id: UInt8 {
		return rawValue
	}

	// MARK: Case

	case male = 1

	case female = 2

	case other = 3

}
