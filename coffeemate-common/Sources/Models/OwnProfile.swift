//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct OwnProfile: Equatable, Codable, Sendable {

	// MARK: Exposed properties

	public let profileId: Int

	public let email: String

	public let nickname: String

	public let gender: Gender

	public let targetGenders: Set<Gender>

	public let avatarUrl: URL

	public let rating: Double

	public let about: String

	public let birthDate: Date

	public let creationDate: Date

	public let isVerified: Bool

	public let hobbies: Set<Hobby>

	public let gallery: [GalleryEntry]

	// MARK: Init

	public init(
		profileId: Int,
		email: String,
		nickname: String,
		gender: Gender,
		targetGenders: Set<Gender>,
		avatarUrl: URL,
		rating: Double,
		about: String,
		birthDate: Date,
		creationDate: Date,
		isVerified: Bool,
		hobbies: Set<Hobby> = [],
		gallery: [GalleryEntry] = []
	) {
		self.profileId = profileId
		self.email = email
		self.nickname = nickname
		self.gender = gender
		self.targetGenders = targetGenders
		self.avatarUrl = avatarUrl
		self.rating = rating
		self.about = about
		self.birthDate = birthDate
		self.creationDate = creationDate
		self.hobbies = hobbies
		self.gallery = gallery
		self.isVerified = isVerified
	}

}
