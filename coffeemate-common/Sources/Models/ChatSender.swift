//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct ChatSender: Codable, Equatable, Identifiable, Sendable {

	// MARK: Exposed properties

	public var id: ChatSenderID {
		return senderId
	}

	public let senderId: ChatSenderID

	public let nickname: String

	// MARK: Init

	public init(
		senderId: ChatSenderID,
		nickname: String
	) {
		self.senderId = senderId
		self.nickname = nickname
	}

}
