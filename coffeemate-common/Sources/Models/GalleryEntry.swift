//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct GalleryEntry: Equatable, Identifiable, Hashable, Codable, Sendable {

	// MARK: Exposed properties

	public var id: GalleryEntryID {
		return entryId
	}

	public let entryId: GalleryEntryID

	public let image: ImageRepresentation

	public let isOwn: Bool

	// MARK: Init

	public init(
		entryId: GalleryEntryID,
		image: ImageRepresentation,
		isOwn: Bool
	) {
		self.entryId = entryId
		self.image = image
		self.isOwn = isOwn
	}

	public static func == (_ lhs: GalleryEntry, _ rhs: GalleryEntry) -> Bool {
		return lhs.entryId == rhs.entryId
	}

}
