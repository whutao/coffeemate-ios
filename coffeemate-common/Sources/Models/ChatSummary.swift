//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public struct ChatSummary: Codable, Equatable, Identifiable, Sendable {

	// MARK: - Exposed properties

	public var id: ChatID {
		return chatId
	}

	public let chatId: ChatID

	public let companion: ChatCompanion

	public let lastMessage: LastMessage?

	public let isHidden: Bool

	// MARK: Init

	public init(
		chatId: ChatID,
		companion: ChatCompanion,
		lastMessage: LastMessage?,
		isHidden: Bool
	) {
		self.chatId = chatId
		self.companion = companion
		self.lastMessage = lastMessage
		self.isHidden = isHidden
	}

}

// MARK: - Model: last message

extension ChatSummary {

	public struct LastMessage: Codable, Equatable, Sendable {

		// MARK: Exposed properties

		public let date: Date

		public let content: ChatMessage.Content

		public let isIncoming: Bool

		public let isRead: Bool

		// MARK: Init

		public init(
			date: Date,
			content: ChatMessage.Content,
			isIncoming: Bool,
			isRead: Bool
		) {
			self.date = date
			self.isIncoming = isIncoming
			self.content = content
			self.isRead = isRead
		}

	}

}
