//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Token

public struct FCMToken: Equatable, Codable, Sendable {

	// MARK: Exposed properties

	public let value: String

	// MARK: Init

	public init(_ value: String) {
		self.value = value
	}

}
