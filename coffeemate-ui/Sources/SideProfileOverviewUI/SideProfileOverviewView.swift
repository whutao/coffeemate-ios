//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Kingfisher
import Models
import SideProfileOverview
import SwiftUI
import ViewComponents

// MARK: - View

public struct SideProfileOverviewView: View {

	// MARK: Private properties

	private let scrollTopMostAnchorID: UUID = .init()

	private let scrollCoordinateSpace: UUID = .init()

	private let store: StoreOf<SideProfileOverview>

	@ObservedObject private var viewStore: ViewStore<ViewState, SideProfileOverview.Action>

	@ObservedObject private var fullScreenGalleryViewStore: ViewStore<FullScreenGalleryViewState, SideProfileOverview.Action>

	@State private var scrollOffset: CGPoint = .zero

	// MARK: Init

	public init(store: StoreOf<SideProfileOverview>) {
		UIScrollView.appearance().alwaysBounceVertical = false
		self.store = store
		self._viewStore = .init(initialValue: .init(store, observe: ViewState.init))
		self._fullScreenGalleryViewStore = .init(initialValue: .init(store, observe: FullScreenGalleryViewState.init))
	}

	// MARK: Body

	public var body: some View {
		GeometryReader { geometry in
			ZStack {
				ZStack(alignment: .top) {
					Asset.Color.alabaster.swiftUIColor

					ImageView(
						imageRepresentation: viewStore.avatarImage,
						ifInterpretedAsKFImage: { kfImage in
							kfImage
								.resizable()
								.downsampling(size: .init(
									width: 800,
									height: 800
								))
								.placeholder {
									ZStack {
										Asset.Color.black.swiftUIColor

										LoadingSpinnerView(tintColor: Asset.Color.keppel.swiftUIColor)
											.adaptiveFrame(size: .square(60))
									}
								}
						},
						ifInterpretedAsImage: { image in
							image
								.resizable()
						}
					)
					.aspectRatio(contentMode: .fill)
					.frame(
						width: geometry.size.width,
						height: min(geometry.size.height, geometry.size.width + max(0, scrollOffset.y))
					)
					.clipped()
				}

				ScrollViewReader { scroll in
					ScrollView(.vertical, showsIndicators: false) {
						PositionObservingView(
							coordinateSpace: .named(scrollCoordinateSpace),
							position: $scrollOffset
						) {
							VStack(alignment: .center, spacing: .zero) {
								Spacer()
									.frame(
										width: geometry.size.width,
										height: max(0, geometry.size.width - 130)
									)
									.id(scrollTopMostAnchorID)

								Group {
									if viewStore.areActionEnabled {
										SideProfileOverviewActionBarView(
											messageAction: { viewStore.send(.view(.didTapSendMessageButton)) },
											dislikeAction: { viewStore.send(.view(.didTapDislikeButton)) },
											likeAction: { viewStore.send(.view(.didTapLikeButton)) }
										)
									} else {
										HStack {
											Spacer()
										}
										.adaptiveFrame(height: 66)
									}
								}
								.adaptivePadding(.horizontal, 40)
								.padding(.vertical, 20)
								.background {
									LinearGradient(
										stops: [
											.init(color: Color.clear, location: 0),
											.init(color: Asset.Color.black.swiftUIColor, location: 0.8),
											.init(color: Asset.Color.black.swiftUIColor, location: 1)
										],
										startPoint: .top,
										endPoint: .bottom
									)
									.padding(.vertical, -20)
								}

								VStack(alignment: .center, spacing: .zero) {
									Spacer()
										.adaptiveFrame(height: 20)

									SideProfileOverviewInfoView(store: store)

									Spacer()
										.adaptiveFrame(height: 20)

									Button {
										withAnimation {
											scroll.scrollTo(scrollTopMostAnchorID)
										}
									} label: {
										Asset.Image.Icon.arrowUp.swiftUIImage
											.resizable()
											.aspectRatio(contentMode: .fit)
											.foregroundColor(Asset.Color.keppel.swiftUIColor)
											.adaptiveFrame(size: .square(20))
											.adaptivePadding(8)
											.background {
												Asset.Color.alabaster.swiftUIColor
											}
											.clipShape(Capsule())
											.overlay {
												Capsule()
													.adaptiveStroke(Asset.Color.keppel.swiftUIColor, lineWidth: 1)
											}
											.contentShape(Capsule())
									}
									.scaleOnPress()
									.adaptiveFrame(size: .square(46))

									Spacer()
										.frame(height: 80)
								}
								.background {
									Asset.Color.alabaster.swiftUIColor
								}
								.cornerRadius(20, corners: [.topLeft, .topRight])
							}
						}
					}
					.coordinateSpace(name: scrollCoordinateSpace)
				}
			}
		}
		.sheet(isPresented: fullScreenGalleryViewStore.binding(
			get: \.isPresented,
			send: { _ in .view(.didCloseGallery) }
		)) {
			GalleryFullScreenView(
				selectedEntry: fullScreenGalleryViewStore.binding(
					get: \.activeGalleryEntry,
					send: { .view(.didSetActiveGalleryEntry($0)) }
				),
				galleryEntries: fullScreenGalleryViewStore.galleryEntries,
				closeAction: {
					fullScreenGalleryViewStore.send(.view(.didCloseGallery))
				}
			)
		}
		.ignoresSafeArea()
	}

}

// MARK: - ViewState

extension SideProfileOverviewView {

	struct ViewState: Equatable, Sendable {

		// MARK: Exposed properties

		let avatarImage: ImageRepresentation

		let areActionEnabled: Bool

		let isShowingGallery: Bool

		// MARK: Init

		init(sideProfileOverviewState: SideProfileOverview.State) {
			self.avatarImage = sideProfileOverviewState.avatarImage
			self.areActionEnabled = sideProfileOverviewState.areActionEnabled
			self.isShowingGallery = sideProfileOverviewState.isShowingGallery
		}

	}

}

// MARK: - FullscreenGalleryViewState

extension SideProfileOverviewView {

	struct FullScreenGalleryViewState: Equatable, Sendable {

		// MARK: Exposed properties

		let isPresented: Bool

		let activeGalleryEntry: GalleryEntry?

		let galleryEntries: IdentifiedArrayOf<GalleryEntry>

		// MARK: Init

		init(sideProfileOverviewState: SideProfileOverview.State) {
			self.activeGalleryEntry = sideProfileOverviewState.activeGalleryEntry
			self.galleryEntries = sideProfileOverviewState.galleryEntries
			self.isPresented = sideProfileOverviewState.isShowingGallery
		}

	}

}

// MARK: - Previews

#if DEBUG
import Mocking

struct SideProfileOverviewView_Preview: PreviewProvider {

	static var previews: some View {
//		SideProfileOverviewView(store: Store(
//			initialState: .init(
//				sideProfile: .mockedDenis(),
//				matchedHobbies: [.randomMock(), .randomMock()]
//			),
//			reducer: SideProfileOverview.init
//		))
		Color.clear.sheet(isPresented: .constant(true)) {
			SideProfileOverviewView(store: Store(
				initialState: .init(
					sideProfile: .mockedDenis(),
					matchedHobbies: [.randomMock(), .randomMock()]
				),
				reducer: SideProfileOverview.init
			))
		}
	}

}
#endif
