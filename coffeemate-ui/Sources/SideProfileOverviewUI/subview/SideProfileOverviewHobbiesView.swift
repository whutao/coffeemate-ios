//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import IdentifiedCollections
import Models
import SideProfileOverview
import SwiftUI
import ViewComponents

// MARK: - View

struct SideProfileOverviewHobbiesView: View {

	// MARK: Private properties

	private let store: StoreOf<SideProfileOverview>

	@ObservedObject private var viewStore: ViewStore<ViewState, SideProfileOverview.Action>

	// MARK: Init

	init(store: StoreOf<SideProfileOverview>) {
		self.store = store
		self._viewStore = ObservedObject(initialValue: ViewStore(store, observe: ViewState.init))
	}

	// MARK: Body

	var body: some View {
		HStack(alignment: .center, spacing: .zero) {
			WrappedVStack(
				alignment: .leading,
				horizontalSpacing: 8,
				verticalSpacing: 6
			) {
				ForEach(viewStore.hobbies) { hobby in
					HobbyView(
						hobby: hobby,
						tintColor: tintColor(hobby),
						isAccented: isMatched(hobby)
					)
				}
			}

			Spacer()
		}
	}

	// MARK: Private methods

	private func isMatched(_ hobby: Hobby) -> Bool {
		return viewStore.matchedHobbies.contains(hobby)
	}

	private func isAccented(_ hobby: Hobby) -> Bool {
		return isMatched(hobby)
	}

	private func tintColor(_ hobby: Hobby) -> SwiftUI.Color {
		if isMatched(hobby) {
			return Asset.Color.keppel.swiftUIColor
		} else {
			return Asset.Color.mineShaft.swiftUIColor.opacity(0.3)
		}
	}

}

// MARK: - ViewState

extension SideProfileOverviewHobbiesView {

	struct ViewState: Equatable, Sendable {

		// MARK: Exposed properties

		let hobbies: IdentifiedArrayOf<Hobby>

		let matchedHobbies: IdentifiedArrayOf<Hobby>

		// MARK: Init

		init(sideProfileState: SideProfileOverview.State) {
			self.hobbies = sideProfileState.hobbies
			self.matchedHobbies = sideProfileState.matchedHobbies
		}

	}

}

// MARK: - Previews

#if DEBUG
import Mocking

struct SideProfileOverviewHobbiesView_Preview: PreviewProvider {

	static var previews: some View {
		EmptyView()
	}

}
#endif
