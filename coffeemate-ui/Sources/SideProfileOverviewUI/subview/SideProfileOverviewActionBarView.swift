//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import SwiftUI
import ViewComponents

// MARK: - View

struct SideProfileOverviewActionBarView: View {

	// MARK: Private properties

	private let messageAction: () -> Void

	private let dislikeAction: () -> Void

	private let likeAction: () -> Void

	// MARK: Init

	init(
		messageAction: @escaping () -> Void = {},
		dislikeAction: @escaping () -> Void = {},
		likeAction: @escaping () -> Void = {}
	) {
		self.messageAction = messageAction
		self.dislikeAction = dislikeAction
		self.likeAction = likeAction
	}

	// MARK: Body

	var body: some View {
		HStack(alignment: .center, spacing: .zero) {
			Group {
				button(
					image: Asset.Image.Icon.cross.swiftUIImage,
					color: Asset.Color.comegranate.swiftUIColor,
					action: dislikeAction
				)
				.adaptiveFrame(size: .square(46))

				button(
					image: Asset.Image.Icon.coffee.swiftUIImage,
					color: Asset.Color.monteCarlo.swiftUIColor,
					action: likeAction
				)
				.adaptiveFrame(size: .square(66))

				button(
					image: Asset.Image.Icon.paperplane.swiftUIImage,
					color: Asset.Color.goldSand.swiftUIColor,
					action: messageAction
				)
				.adaptiveFrame(size: .square(46))
			}
			.frame(maxWidth: .infinity)
		}
	}

	// MARK: Private properties

	@ViewBuilder private func button(
		image: SwiftUI.Image,
		color: SwiftUI.Color,
		action: @escaping () -> Void
	) -> some View {
		Button {
			action()
		} label: {
			TransparentCircleView(
				image: image,
				color: color
			)
		}
		.scaleOnPress()
	}

}

// MARK: - Previews

#if DEBUG
struct SideProfileOverviewActionBarView_Preview: PreviewProvider {

	static var previews: some View {
		ZStack {
			Color.black
				.ignoresSafeArea()

			SideProfileOverviewActionBarView()
				.padding()
		}
	}

}
#endif
