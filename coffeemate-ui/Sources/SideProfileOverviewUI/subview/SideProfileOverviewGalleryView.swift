//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import IdentifiedCollections
import Models
import SideProfileOverview
import SwiftUI
import ViewComponents

// MARK: - View

struct SideProfileOverviewGalleryView: View {

	// MARK: Private properties

	private let store: StoreOf<SideProfileOverview>

	@ObservedObject private var viewStore: ViewStore<ViewState, SideProfileOverview.Action>

	// MARK: Init

	init(store: StoreOf<SideProfileOverview>) {
		self.store = store
		self._viewStore = ObservedObject(initialValue: ViewStore(store, observe: ViewState.init))
	}

	// MARK: Body

	var body: some View {
		GalleryCarouselView(
			entries: viewStore.galleryEntries,
			selectedEntry: viewStore.binding(
				get: \.activeGalleryEntry,
				send: { .view(.didSetActiveGalleryEntry($0)) }
			),
			entryTapAction: { entry in
				viewStore.send(.view(.didTapGalleryEntry(entry)))
			}
		)
	}

}

// MARK: - ViewState

extension SideProfileOverviewGalleryView {

	struct ViewState: Equatable, Sendable {

		// MARK: Exposed properties

		let activeGalleryEntry: GalleryEntry?

		let galleryEntries: IdentifiedArrayOf<GalleryEntry>

		// MARK: Init

		init(sideProfileState: SideProfileOverview.State) {
			self.galleryEntries = sideProfileState.galleryEntries
			self.activeGalleryEntry = sideProfileState.activeGalleryEntry
		}

	}

}

// MARK: - Previews

#if DEBUG
import Mocking

struct SideProfileOverviewGalleryView_Preview: PreviewProvider {

	static var previews: some View {
		EmptyView()
	}

}
#endif
