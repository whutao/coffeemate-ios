//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import IdentifiedCollections
import Models
import SideProfileOverview
import SwiftUI
import ViewComponents

// MARK: - View

struct SideProfileOverviewInfoView: View {

	// MARK: Private properties

	private let store: StoreOf<SideProfileOverview>

	@ObservedObject private var viewStore: ViewStore<ViewState, SideProfileOverview.Action>

	// MARK: Init

	init(store: StoreOf<SideProfileOverview>) {
		self.store = store
		self._viewStore = ObservedObject(initialValue: ViewStore(store, observe: ViewState.init))
	}

	// MARK: Body

	var body: some View {
		VStack(alignment: .center, spacing: .zero) {
			Group {
				VStack(alignment: .leading, spacing: .zero) {
					HStack(alignment: .center, spacing: .zero) {
						Text("\(viewStore.nickname), \(viewStore.age)")
							.foregroundColor(Asset.Color.black.swiftUIColor)
							.adaptiveFont(.primary, .medium, ofSize: 24)
							.lineLimit(1)
							.minimumScaleFactor(0.5)

						Spacer()
							.adaptiveFrame(width: 10)

						TransparentGenderView(gender: viewStore.ownGender)
							.adaptiveFrame(size: .square(20))

						Spacer()
					}

					if viewStore.isVerified {
						Spacer()
							.adaptiveFrame(height: 4)

						HStack(alignment: .center, spacing: .zero) {
							Text(viewStore.verifiedProfileBadgeTitle)
								.adaptiveFont(.primary, .bold, ofSize: 10)

							Spacer()
								.adaptiveFrame(width: 6)

							Asset.Image.Icon.verified.swiftUIImage
								.resizable()
								.aspectRatio(contentMode: .fit)
								.adaptiveFrame(size: .square(10))
						}
						.foregroundColor(Asset.Color.alabaster.swiftUIColor)
						.adaptivePadding(.vertical, 4)
						.adaptivePadding(.horizontal, 6)
						.background {
							Asset.Color.lightningYellow.swiftUIColor
						}
						.clipShape(RoundedRectangle(cornerRadius: 6))
					}
				}

				if !viewStore.targetGenders.isEmpty {
					SideProfileOverviewSectionView(title: viewStore.targetGendersFieldTitle) {
						HStack(alignment: .center, spacing: 18) {
							ForEach(viewStore.targetGenders.sorted(by: \.rawValue)) { gender in
								TransparentGenderView(gender: gender)
									.adaptiveFrame(size: .square(30))
							}

							Spacer()
						}
					}
				}

				if viewStore.about.isNotEmpty {
					SideProfileOverviewSectionView(title: viewStore.aboutFieldTitle) {
						ExpandableText(
							text: viewStore.about,
							lineLimit: 4,
							moreButtonText: viewStore.aboutExpand,
							moreButtonColor: Asset.Color.keppel.swiftUIColor
						)
					}
				}

				if !viewStore.areHobbiesEmpty {
					SideProfileOverviewSectionView(title: viewStore.hobbiesFieldTitle) {
						SideProfileOverviewHobbiesView(store: store)
					}
				}
			}
			.adaptivePadding(.horizontal, 34)
			.adaptivePadding(.vertical, 14)

			if !viewStore.isGalleryEmpty {
				SideProfileOverviewSectionView(title: viewStore.galleryFieldTitle, titlePadding: 34) {
					SideProfileOverviewGalleryView(store: store)
				}
				.adaptivePadding(.vertical, 14)
			}
		}
	}

}

// MARK: - ViewState

extension SideProfileOverviewInfoView {

	struct ViewState: Equatable, Sendable {

		// MARK: Exposed properties

		let nickname: String

		let age: Int

		let isVerified: Bool

		let ownGender: Gender

		let targetGenders: IdentifiedArrayOf<Gender>

		let about: String

		let aboutExpand: String

		let areHobbiesEmpty: Bool

		let isGalleryEmpty: Bool

		let verifiedProfileBadgeTitle: String

		let targetGendersFieldTitle: String

		let aboutFieldTitle: String

		let hobbiesFieldTitle: String

		let galleryFieldTitle: String

		// MARK: Init

		init(sideProfileOverviewState: SideProfileOverview.State) {
			self.isVerified = sideProfileOverviewState.isVerified
			self.nickname = sideProfileOverviewState.nickname
			self.age = sideProfileOverviewState.age
			self.ownGender = sideProfileOverviewState.gender
			self.targetGenders = IdentifiedArrayOf(uniqueElements: sideProfileOverviewState.targetGenders)
			self.about = sideProfileOverviewState.about
			self.aboutExpand = sideProfileOverviewState.aboutExpand
			self.areHobbiesEmpty = sideProfileOverviewState.hobbies.isEmpty
			self.isGalleryEmpty = sideProfileOverviewState.galleryEntries.isEmpty
			self.verifiedProfileBadgeTitle = sideProfileOverviewState.verifiedProfileBadgeTitle
			self.targetGendersFieldTitle = sideProfileOverviewState.targetGendersFieldTitle
			self.aboutFieldTitle = sideProfileOverviewState.aboutFieldTitle
			self.hobbiesFieldTitle = sideProfileOverviewState.hobbiesFieldTitle
			self.galleryFieldTitle = sideProfileOverviewState.galleryFieldTitle
		}

	}

}

// MARK: - Previews

#if DEBUG
import Mocking

struct SideProfileOverviewInfoView_Preview: PreviewProvider {

	static var previews: some View {
		ScrollView {
			SideProfileOverviewInfoView(store: Store(
				initialState: .init(
					sideProfile: .mockedDenis(),
					matchedHobbies: [.randomMock(), .randomMock()]
				),
				reducer: SideProfileOverview.init
			))
		}
	}

}
#endif
