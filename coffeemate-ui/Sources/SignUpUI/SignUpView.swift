//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import SignUp
import SwiftUI
import ViewComponents

// MARK: - View

public struct SignUpView: View {

	// MARK: Private properties

	private let store: StoreOf<SignUp>

	// MARK: Init

	public init(store: StoreOf<SignUp>) {
		self.store = store
	}

	// MARK: Body

	public var body: some View {
		WithViewStore(store, observe: { $0 }) { viewStore in
			SignUpNavigationContainerView(text: viewStore.headerTitle) {
				VStack(alignment: .center, spacing: .zero) {
					currentStepView(store: store)
						.frame(maxHeight: .infinity)
						.adaptivePadding(.top, 24)
						.adaptivePadding(.bottom, 24)

					VStack(alignment: .center, spacing: .zero) {
						StepCounterView(
							currentStep: viewStore.currentStepOrder,
							stepCount: viewStore.numberOfSteps
						)

						Spacer()
							.adaptiveFrame(height: 12)

						Text(viewStore.stepCounterDescription)
							.foregroundColor(Asset.Color.gunsmoke.swiftUIColor)
							.adaptiveFont(.primary, .regular, ofSize: 12)

						Spacer()
							.adaptiveFrame(height: 6)
					}
					.frame(maxWidth: .infinity)
				}
			}
		}
	}

	// MARK: Private properties

	@ViewBuilder private func currentStepView(store: StoreOf<SignUp>) -> some View {
		SwitchStore(store.scope(
			state: \.currentStep,
			action: SignUp.Action.step
		)) { currentStepState in
			Group {
				switch currentStepState {
				case .info:
					CaseLet(
						/SignUp.State.Step.info,
						 action: SignUp.Action.Step.info,
						 then: SignUpInfoStepView.init
					)
				case .gender:
					CaseLet(
						/SignUp.State.Step.gender,
						 action: SignUp.Action.Step.gender,
						 then: SignUpGenderStepView.init
					)
				case .hobby:
					CaseLet(
						/SignUp.State.Step.hobby,
						 action: SignUp.Action.Step.hobby,
						 then: SignUpHobbyStepView.init
					)
				case .about:
					CaseLet(
						/SignUp.State.Step.about,
						 action: SignUp.Action.Step.about,
						 then: SignUpAboutStepView.init
					)
				}
			}
			.transition(.opacity.animation(.easeInOut))
		}

	}

}

// MARK: - Previews

#if DEBUG
struct SignUpView_Preview: PreviewProvider {

	static var previews: some View {
		SignUpView(store: Store(
			initialState: .init(),
			reducer: SignUp.init
		))
	}

}
#endif
