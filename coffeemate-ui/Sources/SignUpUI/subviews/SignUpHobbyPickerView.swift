//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import IdentifiedCollections
import Models
import SignUp
import SwiftUI
import ViewComponents

// MARK: - View

struct SignUpHobbyPickerView: View {

	// MARK: Private properties

	private let store: StoreOf<SignUpHobbyStep>

	// MARK: Init

	init(store: StoreOf<SignUpHobbyStep>) {
		self.store = store
	}

	// MARK: Body

	var body: some View {
		WithViewStore(store, observe: { $0 }) { viewStore in
			HobbyPickerView(
				allHobbies: viewStore.allHobbies,
				selectedHobbies: viewStore.binding(
					get: \.selectedHobbies,
					send: { .view(.didSelectHobbies($0)) }
				),
				selectionLimit: viewStore.maxSelectedHobbies
			)
		}
	}

}

// MARK: - Previews

#if DEBUG
struct SignUpHobbyPickerView_Preview: PreviewProvider {

	static var previews: some View {
		ZStack {
			Color.gray.ignoresSafeArea()

			SignUpHobbyPickerView(store: Store(
				initialState: .init(
					selectedHobbies: [],
					maxSelectedHobbies: 20
				),
				reducer: SignUpHobbyStep.init
			))
			.background {
				Color.white
			}
			.padding(.vertical, 100)
			.padding(.horizontal, 24)
		}
	}

}
#endif
