//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import SwiftUI
import ViewComponents

// MARK: - View

struct SignUpNavigationContainerView<Content: View>: View {

	// MARK: Private properties

	private let text: String

	private let content: () -> Content

	init(
		text: String,
		@ViewBuilder content: @escaping () -> Content
	) {
		self.text = text
		self.content = content
	}

	// MARK: Body

	var body: some View {
		ZStack(alignment: .top) {
			Asset.Color.alabaster.swiftUIColor
				.ignoresSafeArea()

			ConvexNavigationContainerView { _ in
				Text(text)
					.lineLimit(1)
					.multilineTextAlignment(.center)
					.foregroundColor(Asset.Color.alabaster.swiftUIColor)
					.adaptiveFont(.primary, .medium, ofSize: 20)
					.frame(maxWidth: .infinity, maxHeight: .infinity)
			} content: {
				content()
			}
		}
	}

}

// MARK: - Previews

#if DEBUG
struct SignUpNavigationContainerView_Preview: PreviewProvider {

	static var previews: some View {
		SignUpNavigationContainerView(text: "Create account") {
			VStack {
				Color.red
			}
		}
	}

}
#endif
