//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import SignUp
import SwiftUI
import ViewComponents

// MARK: - View

struct SignUpAboutStepView: View {

	// MARK: Private properties

	@StateObject private var globalFocusState = GlobalFocusState()

	private let aboutFieldId: UUID

	private let store: StoreOf<SignUpAboutStep>

	// MARK: Init

	init(store: StoreOf<SignUpAboutStep>) {
		self.aboutFieldId = UUID()
		self.store = store
	}

	// MARK: Body

	var body: some View {
		WithViewStore(store, observe: { $0 }) { viewStore in
			GeometryReader { geometry in
				VStack(alignment: .center, spacing: .zero) {
					Group {
						Text(viewStore.headerTitle)
							.foregroundColor(Asset.Color.black.swiftUIColor)
							.adaptiveFont(.primary, .medium, ofSize: 16)

						Spacer()
							.adaptiveFrame(height: 12)

						Text(viewStore.headerDescription)
							.foregroundColor(Asset.Color.black.swiftUIColor)
							.adaptiveFont(.primary, .regular, ofSize: 14)

						Spacer()
							.adaptiveFrame(height: 20)

						MultiLineTextView(
							footer: .text(viewStore.characterLimitDescription),
							text: viewStore.binding(
								get: \.aboutText,
								send: { .view(.didUpdateAboutText($0)) }
							)
						)
						.customFocus($globalFocusState.focus, equals: .uuid(aboutFieldId))
						.onTapGesture {
							globalFocusState.focus = .uuid(aboutFieldId)
						}

						Spacer()
							.frame(maxHeight: 30)
					}
					.adaptivePadding(.horizontal, 12)

					OpaqueButton(title: viewStore.nextButtonTitle) {
						endEditing()
						viewStore.send(.view(.didTapNextButton))
					}
					.adaptiveFrame(height: 40)
					.disabledAndFaintIf(!viewStore.isNextButtonEnabled)
					.adaptivePadding(.horizontal, 46)
					.frame(height: (0.13) * geometry.size.height)
				}
			}
		}
		.adaptivePadding(.horizontal, 24)
		.onTapGesture {
			globalFocusState.focus = nil
		}
	}

}

// MARK: - Previews

#if DEBUG
import Mocking

struct SignUpAboutStepView_Preview: PreviewProvider {

	static var previews: some View {
		ZStack {
			Color.gray.ignoresSafeArea()

			SignUpAboutStepView(store: Store(
				initialState: .init(aboutText: ""),
				reducer: SignUpAboutStep.init
			))
			.background {
				Color.white
			}
			.padding(.vertical, 100)
			.padding(.horizontal, 24)
		}
	}

}
#endif
