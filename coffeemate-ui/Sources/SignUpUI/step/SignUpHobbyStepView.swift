//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import SignUp
import SwiftUI
import ViewComponents

// MARK: - View

struct SignUpHobbyStepView: View {

	// MARK: Private properties

	private let store: StoreOf<SignUpHobbyStep>

	// MARK: Init

	init(store: StoreOf<SignUpHobbyStep>) {
		self.store = store
	}

	// MARK: Body

	var body: some View {
		WithViewStore(store, observe: { $0 }) { viewStore in
			GeometryReader { geometry in
				VStack(alignment: .center, spacing: .zero) {
					Group {
						Text(viewStore.headerTitle)
							.foregroundColor(Asset.Color.black.swiftUIColor)
							.adaptiveFont(.primary, .medium, ofSize: 16)

						Spacer()
							.adaptiveFrame(height: 12)

						Text(viewStore.headerDescription)
							.foregroundColor(Asset.Color.black.swiftUIColor)
							.adaptiveFont(.primary, .regular, ofSize: 14)

						Spacer()
							.adaptiveFrame(height: 24)

						Group {
							if viewStore.isLoadingAllHobbies {
								LoadingSpinnerView(
									tintColor: Asset.Color.alabaster.swiftUIColor
								)
								.adaptiveFrame(size: .square(40))
							} else {
								SignUpHobbyPickerView(store: store)
							}
						}
						.transition(.opacity.animation(.easeInOut))
						.frame(maxWidth: .infinity, maxHeight: .infinity)
					}
					.adaptivePadding(.horizontal, 10)

					Spacer()
						.adaptiveFrame(height: 14)

					Text(viewStore.selectedHobbyCountDescription)
						.lineLimit(1)
						.adaptiveFont(.primary, .regular, ofSize: 14)
						.foregroundColor(Asset.Color.gunsmoke.swiftUIColor.opacity(0.7))

					Spacer()
						.adaptiveFrame(height: 14)

					OpaqueButton(title: viewStore.nextButtonTitle) {
						viewStore.send(.view(.didTapNextButton))
					}
					.adaptiveFrame(height: 40)
					.disabledAndFaintIf(!viewStore.isNextButtonEnabled)
					.adaptivePadding(.horizontal, 46)
					.frame(height: (0.13) * geometry.size.height)
				}
			}
			.onFirstAppear {
				viewStore.send(.view(.didAppearFirstTime))
			}
		}
		.adaptivePadding(.horizontal, 24)
	}

}

// MARK: - Previews

#if DEBUG
import Mocking
import Models

struct SignUpHobbyStepView_Preview: PreviewProvider {

	static func randomHobby() -> Hobby {
		return Hobby(
			hobbyId: .random(in: -10000000...10000000),
			title: Lorem.capitalizedWord,
			imageUrl: URL(string: "https://api.coffeemates.space:443/images/hobbies/party.svg")!,
			isPrivileged: .random()
		)
	}

	static func allHobbies() -> [Hobby] {
		return Range(0...60).map { _ in randomHobby() }
	}

	static var previews: some View {
		ZStack {
			Color.gray.ignoresSafeArea()

			SignUpHobbyStepView(store: Store(
				initialState: .init(
					allHobbies: allHobbies(),
					maxSelectedHobbies: 7
				),
				reducer: SignUpHobbyStep.init
			))
			.background {
				Color.white
			}
			.padding(.vertical, 100)
			.padding(.horizontal, 24)
		}
	}

}
#endif
