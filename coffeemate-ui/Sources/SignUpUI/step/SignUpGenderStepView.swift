//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import SignUp
import SwiftUI
import ViewComponents

// MARK: - View

struct SignUpGenderStepView: View {

	// MARK: Private properties

	private let store: StoreOf<SignUpGenderStep>

	// MARK: Init

	init(store: StoreOf<SignUpGenderStep>) {
		self.store = store
	}

	// MARK: Body

	var body: some View {
		WithViewStore(store, observe: { $0 }) { viewStore in
			GeometryReader { geometry in
				VStack(alignment: .center, spacing: .zero) {
					VStack(spacing: .zero) {
						VStack(spacing: .zero) {
							Text(viewStore.ownGenderPickerTitle)
								.lineLimit(1)
								.foregroundColor(Asset.Color.black.swiftUIColor)
								.adaptiveFont(.primary, .medium, ofSize: 16)
								.frame(maxWidth: .infinity, maxHeight: .infinity)

							GenderPickerView(
								style: .ownGender,
								fromUniqueGenders: [.female, .male, .other],
								selectedGender: viewStore.binding(
									get: \.ownGender,
									send: { .view(.didSelectOwnGender($0)) }
								)
								.removeDuplicates()
							)
							.frame(maxWidth: .infinity, maxHeight: .infinity)
						}
						.frame(height: (0.25) * geometry.size.height)

						VStack(spacing: .zero) {
							Text(viewStore.targetGenderPickerTitle)
								.lineLimit(1)
								.foregroundColor(Asset.Color.black.swiftUIColor)
								.adaptiveFont(.primary, .medium, ofSize: 16)
								.frame(maxWidth: .infinity, maxHeight: .infinity)

							GenderPickerView(
								style: .targetGender,
								fromUniqueGenders: [.female, .male, .other],
								selectedGenders: viewStore.binding(
									get: \.targetGenders,
									send: { .view(.didSelectTargetGenders($0)) }
								)
							)
							.frame(maxWidth: .infinity, maxHeight: .infinity)
						}
						.frame(height: (0.25) * geometry.size.height)
					}
					.adaptivePadding(.horizontal, 56)
					.frame(height: (0.5) * geometry.size.height)

					VStack(spacing: .zero) {
						Spacer()

						OpaqueButton(title: viewStore.nextButtonTitle) {
							viewStore.send(.view(.didTapNextButton))
						}
						.adaptiveFrame(height: 40)
						.disabledAndFaintIf(!viewStore.isNextButtonEnabled)
						.adaptivePadding(.horizontal, 46)
						.frame(height: (0.13) * geometry.size.height)
					}
					.frame(height: (0.5) * geometry.size.height)
				}
			}
		}
		.adaptivePadding(.horizontal, 24)
	}

}

// MARK: - Previews

#if DEBUG
struct SignUpGenderStepView_Preview: PreviewProvider {

	static var previews: some View {
		ZStack {
			Color.gray.ignoresSafeArea()

			SignUpGenderStepView(store: Store(
				initialState: .init(
					ownGender: nil,
					targetGenders: []
				),
				reducer: SignUpGenderStep.init
			))
			.background {
				Color.white
			}
			.padding(.vertical, 100)
			.padding(.horizontal, 24)
		}
	}

}
#endif
