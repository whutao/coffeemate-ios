//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import SignUp
import SwiftUI
import ViewComponents

// MARK: - View

struct SignUpInfoStepView: View {

	// MARK: Private properties

	@StateObject private var globalFocusState = GlobalFocusState()

	@ObservedObject private var viewStore: ViewStoreOf<SignUpInfoStep>

	private let nicknameFieldId: UUID = UUID()

	private let store: StoreOf<SignUpInfoStep>

	// MARK: Init

	init(store: StoreOf<SignUpInfoStep>) {
		self.store = store
		self._viewStore = .init(initialValue: .init(store, observe: { $0 }))
	}

	// MARK: Body

	var body: some View {
		ScrollView(showsIndicators: false) {
			Group {
				VStack(alignment: .center, spacing: .zero) {
					Text(viewStore.headerTitle)
						.lineLimit(2)
						.foregroundColor(Asset.Color.black.swiftUIColor)
						.adaptiveFont(.primary, .regular, ofSize: 14)

					Spacer()
						.adaptiveFrame(height: 14)

					GradientSeparatorView()
				}
				.adaptiveFrame(height: 60)

				AvatarImagePickerView(
					selectedImageData: viewStore.binding(
						get: \.avatarImageData,
						send: { .view(.didUpdateAvatarImage($0)) }
					),
					selectedImageMaxSizeInKB: viewStore.avatarMaxSizeInKB
				)
				.adaptiveFrame(height: 124)

				Spacer()
					.adaptiveFrame(height: 12)

				Group {
					SingleLineTextView(
						title: .text(viewStore.nicknameFieldTitle),
						footer: .text(viewStore.nicknameFieldFooter),
						placeholder: .empty,
						text: viewStore.binding(
							get: \.nickname,
							send: { .view(.didUpdateNickname($0)) }
						)
					)
					.customFocus($globalFocusState.focus, equals: .uuid(nicknameFieldId))
					.onTapGesture {
						globalFocusState.focus = .uuid(nicknameFieldId)
					}

					Spacer()
						.adaptiveFrame(height: 12)

					BirthDatePickerView(
						selectedDate: viewStore.binding(
							get: \.birthDate,
							send: { .view(.didUpdateBirthDate($0)) }
						),
						dateRange: viewStore.birthDateRange,
						dateDisplayFormat: viewStore.birthDateDisplayFormat,
						placeholder: viewStore.birthDatePlaceholder,
						doneButtonTitle: viewStore.birthDateDoneButtonTitle
					)
					.adaptiveFrame(height: 48)
				}
				.adaptivePadding(.horizontal, 16)

				Spacer()
					.adaptiveFrame(height: 30)

				OpaqueButton(title: viewStore.nextButtonTitle) {
					viewStore.send(.view(.didTapNextButton))
				}
				.adaptiveFrame(height: 40)
				.disabledAndFaintIf(!viewStore.isNextButtonEnabled)
				.adaptivePadding(.horizontal, 46)
				.adaptiveFrame(height: 48)

				Spacer()
					.adaptiveFrame(height: 80)
			}
			.adaptivePadding(.horizontal, 24)
		}
		.onTapGesture {
			globalFocusState.focus = nil
		}
		.ignoresSafeArea()
	}

}

// MARK: - Previews

#if DEBUG
struct SignUpInfoStepView_Preview: PreviewProvider {

	static var previews: some View {
		ForEachDevice([.iPhoneSE3rdGeneration, .iPhone14]) {
			ZStack {
				Color.gray.ignoresSafeArea()

				SignUpInfoStepView(store: Store(
					initialState: .init(
						avatarImageData: nil,
						birthDate: nil,
						nickname: .empty
					),
					reducer: SignUpInfoStep.init
				))
				.background {
					Color.white
				}
				.adaptivePadding(.vertical, 80)
			}
		}
	}

}
#endif
