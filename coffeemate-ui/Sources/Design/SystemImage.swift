//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import SwiftUI

// MARK: - Images

public enum SystemImage {

	// MARK: Exposed properties

	public static let checkmark = Image(systemName: "checkmark")

	public static let exit = Image(systemName: "rectangle.portrait.and.arrow.right")

}
