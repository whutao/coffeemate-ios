//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import SwiftUI

// MARK: - Modifier

struct AdaptivePaddingModifier: ViewModifier {

	// MARK: Private properties

	@Dependency(\.adaptiveSize) private var adaptiveSize

	private var adaptiveIncrement: CGFloat {
		switch adaptiveSize {
		case .small:
			return 0
		case .medium:
			return 1
		case .large:
			return 2
		}
	}

	private let edges: Edge.Set

	private let length: CGFloat?

	// MARK: Init

	init(edges: Edge.Set, length: CGFloat?) {
		self.edges = edges
		self.length = length
	}

	// MARK: Body

	func body(content: Content) -> some View {
		content
			.padding(edges, length.flatMap({ $0 + adaptiveIncrement }))
	}

}

// MARK: - View+adaptiveFrame

extension View {

	@ViewBuilder public func adaptivePadding(
		_ edges: Edge.Set = .all,
		_ length: CGFloat? = nil
	) -> some View {
		modifier(AdaptivePaddingModifier(edges: edges, length: length))
	}

	@ViewBuilder public func adaptivePadding(
		_ length: CGFloat? = nil
	) -> some View {
		modifier(AdaptivePaddingModifier(edges: .all, length: length))
	}

}
