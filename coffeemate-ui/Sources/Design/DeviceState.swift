//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import SwiftUI

// MARK: - Device

public struct DeviceState {

	// MARK: Static properties

	public static let `default` = DeviceState(
		idiom: UIDevice.current.userInterfaceIdiom,
		orientation: UIDevice.current.orientation,
		previousOrientation: UIDevice.current.orientation
	)

	// MARK: Exposed properties

	public var idiom: UIUserInterfaceIdiom

	public var orientation: UIDeviceOrientation

	public var previousOrientation: UIDeviceOrientation

}

// MARK: - Mofifier

struct DeviceStateModifier: ViewModifier {

	// MARK: Private properties

	@State private var state: DeviceState = .default

	// MARK: Init

	init() {

	}

	// MARK: Body

	func body(content: Content) -> some View {
		content
			.onAppear()
			.onReceive(NotificationCenter.default.publisher(
				for: UIDevice.orientationDidChangeNotification
			)) { _ in
				state.previousOrientation = state.orientation
				state.orientation = UIDevice.current.orientation
			}
			.environment(\.deviceState, state)
	}

}

// MARK: - View+DeviceStateModifier

extension View {

	/// Should be applied directly and only to the application view to
	/// observe device state changes and pass them to the environment.
	@ViewBuilder public func deviceStateObserver() -> some View {
		modifier(DeviceStateModifier())
	}

}

// MARK: - EnvironmentKey

private struct DeviceStateKey: EnvironmentKey {

	static var defaultValue: DeviceState {
		return .default
	}

}

// MARK: - EnvironmentValues+EnvironmentKey

extension EnvironmentValues {

	public var deviceState: DeviceState {
		get { self[DeviceStateKey.self] }
		set { self[DeviceStateKey.self] = newValue }
	}

}
