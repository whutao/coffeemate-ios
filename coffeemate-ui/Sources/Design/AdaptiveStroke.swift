//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import SwiftUI

// MARK: - Shape+adaptiveStroke

extension Shape {

	@ViewBuilder public func adaptiveStroke<S: ShapeStyle>(
		_ content: S,
		lineWidth: CGFloat
	) -> some View {
		@Dependency(\.adaptiveSize) var adaptiveSize
		stroke(content, lineWidth: lineWidth + {
			switch adaptiveSize {
			case .small:
				return 0
			case .medium:
				return 1
			case .large:
				return 2
			}
		}())
	}

}
