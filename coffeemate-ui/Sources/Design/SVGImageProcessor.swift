//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Kingfisher
import SVGKit
import UIKit

// MARK: - Processor

public struct SVGImageProcessor: ImageProcessor {

	// MARK: Exposed properties

	public let identifier: String = "com.wimmi.coffeemate-ios.svg-processor"

	// MARK: Init

	public init() {

	}

	// MARK: Exposed methods

	public func process(item: ImageProcessItem, options: KingfisherParsedOptionsInfo) -> KFCrossPlatformImage? {
		switch item {
		case let .image(image):
			return image
		case let .data(data):
			return SVGKImage(data: data)?.uiImage
		}
	}

}

// MARK: - KFImage extension

extension KFImage {

	/// Appends an SVG image processor.
	public func processAsSVG() -> KFImage {
		return appendProcessor(SVGImageProcessor())
	}

}
