//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Splash
import SwiftUI
import ViewComponents

// MARK: - View

public struct SplashView: View {

	// MARK: Private properties

	@Dependency(\.mainQueue) private var mainQueue

	@State private var canShowLoadingSpinner: Bool = false

	private let store: StoreOf<Splash>

	// MARK: Init

	public init(store: StoreOf<Splash>) {
		self.store = store
	}

	// MARK: Body

	public var body: some View {
		WithViewStore(store, observe: { $0 }) { viewStore in
			ZStack {
				Asset.Image.Splash.background.swiftUIImage
					.resizable()
					.aspectRatio(contentMode: .fill)
					.overlay {
						Color.black
					}
					.opacity(0.6)
					.blur(radius: 3.5)
					.padding(-20)
					.ignoresSafeArea()

				VStack(spacing: .zero) {
					Text(viewStore.title)
						.adaptiveFont(.primary, .bold, ofSize: 32)

					Spacer()
						.adaptiveFrame(height: 64)

					Group {
						if canShowLoadingSpinner, viewStore.isShowingLoadingSpinner {
							LoadingSpinnerView(
								tintColor: Asset.Color.alabaster.swiftUIColor
							)
							.transition(.opacity)
							.animation(.easeInOut, value: canShowLoadingSpinner)
						} else {
							Spacer()
						}
					}
					.adaptiveFrame(size: .square(70))
				}
				.foregroundColor(Asset.Color.alabaster.swiftUIColor)
				.adaptivePadding(.bottom, 70)
			}
			.onFirstAppear {
				mainQueue.schedule(after: .init(.now() + 0.5)) {
					withAnimation {
						canShowLoadingSpinner = true
					}
				}
				viewStore.send(.view(.didAppearFirstTime))
			}
		}
	}

}

// MARK: - Previews

#if DEBUG
struct SplashView_Preview: PreviewProvider {

	static var previews: some View {
		SplashView(store: Store(
			initialState: Splash.State(
				timer: .init(totalTicks: 10),
				title: "COFFEE MATE"
			),
			reducer: { Splash() }
		))
	}

}
#endif
