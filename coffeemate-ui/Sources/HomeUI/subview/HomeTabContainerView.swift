//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import Home
import Models
import SwiftUI
import ViewComponents

// MARK: - View

struct HomeTabContainerView<Content: View>: View {

	// MARK: Private properties

	@ObservedObject private var viewStore: ViewStore<ViewState, Home.Action>

	private let store: StoreOf<Home>

	private let content: () -> Content

	// MARK: Init

	init(
		store: StoreOf<Home>,
		@ViewBuilder content: @escaping () -> Content
	) {
		self._viewStore = .init(initialValue: .init(store, observe: ViewState.init))
		self.store = store
		self.content = content
	}

	// MARK: Body

	var body: some View {
		VStack(spacing: .zero) {
			content()
				.frame(maxWidth: .infinity, maxHeight: .infinity)
				.padding(.bottom, -20)

			HomeTabBarView(
				tabBarItems: viewStore.tabItems,
				selectedItem: viewStore.binding(
					get: \.activeTabItem,
					send: { .view(.didSelectTabItem($0)) }
				)
			)
		}
		.ignoresSafeArea(.keyboard)
	}

}

// MARK: - ViewState

extension HomeTabContainerView {

	struct ViewState: Equatable, Sendable {

		// MARK: Exposed properties

		let tabItems: [HomeTabBarItem]

		let activeTabItem: HomeTabBarItem

		// MARK: Init

		init(homeState: Home.State) {
			self.tabItems = homeState.tabBarItems
			self.activeTabItem = homeState.activeTabBarItem
		}

	}

}

// MARK: - Previews

#if DEBUG
struct HomeTabContainerView_Preview: PreviewProvider {

	static var previews: some View {
		HomeTabContainerView(store: Store(
			initialState: .init(),
			reducer: Home.init
		)) {
			Color.red.edgesIgnoringSafeArea(.top)
		}
	}

}
#endif
