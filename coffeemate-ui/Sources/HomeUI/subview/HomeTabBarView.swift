//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import Foundation
import Models
import SwiftUI

// MARK: - View

public struct HomeTabBarView: View {

	// MARK: Private properties

	private let tabBarItems: [HomeTabBarItem]

	@Binding private var selectedItem: HomeTabBarItem

	@Dependency(\.mainQueue) private var mainQueue

	// MARK: Init

	public init(
		tabBarItems: [HomeTabBarItem],
		selectedItem: Binding<HomeTabBarItem>
	) {
		self.tabBarItems = tabBarItems
		self._selectedItem = selectedItem
	}

	// MARK: Body

	public var body: some View {
		ZStack {
			Asset.Color.black.swiftUIColor

			HStack(spacing: .zero) {
				ForEach(tabBarItems) { item in
					image(for: item)
						.foregroundColor(tintColor(for: item))
						.adaptiveFrame(size: .square(24))
						.if(isSelected(item)) { view in
							view.shadow(
								color: tintColor(for: item),
								radius: 3,
								x: 0,
								y: 0
							)
						}
						.frame(maxHeight: .infinity)
						.adaptiveFrame(width: 60, height: 46)
						.contentShape(Rectangle())
						.onTapGesture {
							selectedItem = item
						}
						.frame(maxWidth: .infinity, maxHeight: .infinity)
						.animation(.easeInOut, value: selectedItem)
						.adaptivePadding(.bottom, 8)
				}
			}
			.adaptivePadding(.horizontal, 32)
		}
		.cornerRadius(20, corners: [.topLeft, .topRight])
		.shadow(
			color: Asset.Color.monteCarlo.swiftUIColor.opacity(0.5),
			radius: 14,
			x: 0,
			y: 8
		)
		.edgesIgnoringSafeArea(.bottom)
		.adaptiveFrame(height: 60)
	}

	// MARK: Private methods

	private func isSelected(_ item: HomeTabBarItem) -> Bool {
		return selectedItem == item
	}

	private func image(for item: HomeTabBarItem) -> SwiftUI.Image {
		switch item {
		case .chat:
			return Asset.Image.TabBar.chat.swiftUIImage
		case .contacts:
			return Asset.Image.TabBar.contacts.swiftUIImage
		case .profile:
			return Asset.Image.TabBar.profile.swiftUIImage
		}
	}

	private func tintColor(for item: HomeTabBarItem) -> SwiftUI.Color {
		if isSelected(item) {
			return Asset.Color.keppel.swiftUIColor
		} else {
			return Asset.Color.alabaster.swiftUIColor.opacity(0.6)
		}
	}

}

// MARK: - Previews

#if DEBUG
struct HomeTabBarView_Preview: PreviewProvider {

	private struct ContentView: View {

		@State private var selectedItem: HomeTabBarItem = .contacts

		var body: some View {
			ZStack {
				Color.black
					.edgesIgnoringSafeArea(.top)

				VStack {
					Spacer()

					HomeTabBarView(
						tabBarItems: [.chat, .contacts, .profile],
						selectedItem: $selectedItem
					)
				}
			}
		}

	}

	static var previews: some View {
		ForEachDevice([.iPhone14Pro, .iPhoneSE3rdGeneration]) {
			ContentView()
		}
	}

}
#endif
