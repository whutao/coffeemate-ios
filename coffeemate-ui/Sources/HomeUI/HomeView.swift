//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ChatListUI
import ChatRoomUI
import ComposableArchitecture
import ContactsUI
import Design
import Extensions
import Home
import NavigationTransitions
import ProfileUI
import ProfileEditorUI
import SideProfileOverviewUI
import SwiftUI

// MARK: - View

public struct HomeView: View {

	// MARK: Private properties

	private let store: StoreOf<Home>

	// MARK: Init

	public init(store: StoreOf<Home>) {
		self.store = store
	}

	// MARK: Body

	public var body: some View {
		NavigationStackStore(store.scope(
			state: \.path,
			action: Home.Action.path
		)) {
			HomeTabContainerView(store: store) {
				SwitchStore(store.scope(
					state: \.activeTab,
					action: Home.Action.tab
				)) { activeTabState in
					Group {
						switch activeTabState {
						case .chatList:
							CaseLet(
								/Home.State.Tab.chatList,
								 action: Home.Action.Tab.chatList,
								 then: ChatListView.init
							)
						case .contacts:
							CaseLet(
								/Home.State.Tab.contacts,
								 action: Home.Action.Tab.contacts,
								 then: ContactsView.init
							)
						case .profile:
							CaseLet(
								/Home.State.Tab.profile,
								 action: Home.Action.Tab.profile,
								 then: ProfileView.init
							)
						}
					}
					.background {
						Asset.Color.black.swiftUIColor
							.ignoresSafeArea()
					}
				}
			}
		} destination: { pathStore in
			SwitchStore(pathStore) { pathState in
				switch pathState {
				case .chatRoom:
					CaseLet(
						/Home.State.Path.chatRoom,
						 action: Home.Action.Path.chatRoom,
						 then: ChatRoomView.init
					)
				case .profileEditor:
					CaseLet(
						/Home.State.Path.profileEditor,
						 action: Home.Action.Path.profileEditor,
						 then: ProfileEditorView.init
					)
				case .sideProfileOverview:
					CaseLet(
						/Home.State.Path.sideProfileOverview,
						 action: Home.Action.Path.sideProfileOverview,
						 then: SideProfileOverviewView.init
					)
				}
			}
		}
		.ignoresSafeArea(.keyboard)
	}

}

// MARK: - Previews

#if DEBUG
import Mocking

struct HomeView_Preview: PreviewProvider {

	static var previews: some View {
		HomeView(store: Store(
			initialState: .init(activeTab: .contacts(.init())),
			reducer: Home.init,
			withDependencies: { dependency in
				dependency.userClient.ownProfile = {
					return .mockedJulia()
				}
			}
		))
	}

}
#endif
