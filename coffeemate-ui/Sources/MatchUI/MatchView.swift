//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import Kingfisher
import Match
import Models
import SwiftUI
import ViewComponents

// MARK: - View

public struct MatchView: View {

	// MARK: Private properties

	@Environment(\.dismiss) private var dismiss

	@Environment(\.isPresented) private var isPresented

	@ObservedObject private var viewStore: ViewStore<ViewState, Match.Action>

	private let store: StoreOf<Match>

	// MARK: Init

	public init(store: StoreOf<Match>) {
		self.store = store
		self._viewStore = .init(initialValue: .init(store, observe: ViewState.init))
	}

	// MARK: Body

	public var body: some View {
		GeometryReader { geometry in
			ZStack {
				Asset.Color.black.swiftUIColor
					.ignoresSafeArea()

				VStack(alignment: .center, spacing: .zero) {
					VStack(alignment: .center, spacing: .zero) {
						Spacer()

						Text(viewStore.headerTitle)
							.lineLimit(1)
							.adaptiveFont(.primary, .bold, ofSize: 30)
							.foregroundColor(Asset.Color.alabaster.swiftUIColor)
					}
					.frame(height: (0.12) * geometry.size.height)

					VStack(alignment: .center, spacing: .zero) {
						ZStack {
							imageView(for: viewStore.ownProfileImage)
								.zIndex(0)
								.frame(size: .square(0.4 * geometry.size.width))
								.offset(x: -0.3 * (0.4 * geometry.size.width))

							imageView(for: viewStore.sideProfileImage)
								.zIndex(1)
								.frame(size: .square(0.4 * geometry.size.width))
								.offset(x: 0.3 * (0.4 * geometry.size.width))
						}
						.frame(size: geometry.size)
					}
					.frame(height: (0.44) * geometry.size.height)

					VStack(alignment: .center, spacing: .zero) {
						Text(viewStore.sideProfileName)
							.lineLimit(1)
							.adaptiveFont(.primary, .bold, ofSize: 20)
							.foregroundColor(Asset.Color.alabaster.swiftUIColor)

						Spacer()
					}
					.frame(height: (0.24) * geometry.size.height)

					VStack(alignment: .center, spacing: .zero) {
						OpaqueButton(
							title: viewStore.sendMessageTitle,
							foregroundColor: Asset.Color.alabaster.swiftUIColor,
							backgroundColor: Asset.Color.keppel.swiftUIColor
						) {
							viewStore.send(.view(.didTapSendMessageButton))
						}
						.frame(width: (0.62) * geometry.size.width)
						.adaptiveFrame(height: 40)
						.frame(maxWidth: .infinity, maxHeight: .infinity)
					}
					.frame(height: (0.2) * geometry.size.height)
				}
				.frame(size: geometry.size)
				.overlay(alignment: .topLeading) {
					if isPresented {
						Button {
							dismiss()
						} label: {
							Asset.Image.Icon.cross.swiftUIImage
								.resizable()
								.aspectRatio(contentMode: .fill)
								.foregroundColor(Asset.Color.keppel.swiftUIColor)
								.adaptivePadding(4)
						}
						.scaleOnPress()
						.adaptiveFrame(size: .square(24))
						.offset(x: 20, y: 20)
					}
				}
			}
		}
	}

	// MARK: Private methods

	@ViewBuilder private func imageView(
		for imageRepresentation: ImageRepresentation
	) -> some View {
		GeometryReader { geometry in
			ImageView(
				imageRepresentation: imageRepresentation,
				ifInterpretedAsKFImage: { kfImage in
					kfImage
						.resizable()
						.cacheOriginalImage()
						.downsampling(size: .square(400))
						.placeholder { _ in
							LoadingSpinnerView(
								tintColor: Asset.Color.alabaster.swiftUIColor
							)
							.adaptiveFrame(size: .square(30))
						}
				},
				ifInterpretedAsImage: { swiftUIImage in
					swiftUIImage
						.resizable()
				}
			)
			.aspectRatio(contentMode: .fill)
			.frame(size: .square(geometry.size.minDimension))
			.clipShape(Capsule())
			.overlay {
				Capsule()
					.adaptiveStroke(Asset.Color.alabaster.swiftUIColor, lineWidth: 2)
			}
		}
	}

}

// MARK: - ViewState

extension MatchView {

	fileprivate struct ViewState: Equatable, Sendable {

		// MARK: Exposed properites

		fileprivate let ownProfileImage: ImageRepresentation

		fileprivate let sideProfileImage: ImageRepresentation

		fileprivate let sideProfileName: String

		fileprivate let headerTitle: String

		fileprivate let sendMessageTitle: String

		// MARK: Init

		fileprivate init(state: Match.State) {
			self.headerTitle = state.headerTitle
			self.sendMessageTitle = state.chatButtonTitle
			self.ownProfileImage = .url(state.ownProfile.avatarUrl)
			self.sideProfileImage = .url(state.sideProfile.avatarUrl)
			self.sideProfileName = state.sideProfile.nickname
		}

	}

}

// MARK: - Previews

#if DEBUG
import Mocking

struct MatchView_Preview: PreviewProvider {

	static var previews: some View {
		Color.clear.fullScreenCover(isPresented: .constant(true)) {
			MatchView(store: .init(
				initialState: Match.State(
					ownProfile: .mockedJulia(),
					sideProfile: .mockedDenis()
				),
				reducer: Match.init
			))
		}
	}

}
#endif
