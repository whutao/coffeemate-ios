//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Application
import ComposableArchitecture
import HomeUI
import SignInUI
import SignUpUI
import SplashUI
import SwiftUI

// MARK: - View

struct ApplicationScreenView: View {

	// MARK: Private properties

	private let store: StoreOf<ApplicationScreen>

	init(store: StoreOf<ApplicationScreen>) {
		self.store = store
	}

	// MARK: Body

	var body: some View {
		ZStack {
			SwitchStore(store) { screenState in
				Group {
					switch screenState {
					case .signIn:
						CaseLet(
							/ApplicationScreen.State.signIn,
							 action: ApplicationScreen.Action.signIn,
							 then: SignInView.init
						)
						.zIndex(2)
					case .signUp:
						CaseLet(
							/ApplicationScreen.State.signUp,
							 action: ApplicationScreen.Action.signUp,
							 then: SignUpView.init
						)
						.zIndex(1)
					case .home:
						CaseLet(
							/ApplicationScreen.State.home,
							 action: ApplicationScreen.Action.home,
							 then: HomeView.init
						)
						.zIndex(0)
					case .splash:
						CaseLet(
							/ApplicationScreen.State.splash,
							 action: ApplicationScreen.Action.splash,
							 then: SplashView.init
						)
						.zIndex(3)
					}
				}
			}
			.transition(.opacity.animation(.easeInOut(duration: 0.3)))
		}
	}

}
