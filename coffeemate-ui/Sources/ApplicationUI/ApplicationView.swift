//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Application
import ComposableArchitecture
import SwiftUI

// MARK: - View

public struct ApplicationView: View {

	// MARK: Private properties

	private let store: StoreOf<Application>

	// MARK: Init

	public init(store: StoreOf<Application>) {
		self.store = store
	}

	// MARK: Body

	public var body: some View {
		ApplicationToastContainerView(store: store.scope(
			state: \.toast,
			action: Application.Action.toast
		)) {
			ApplicationScreenView(store: store.scope(
				state: \.screen,
				action: Application.Action.screen
			))
		}
	}

}
