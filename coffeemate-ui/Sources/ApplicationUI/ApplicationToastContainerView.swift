//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import AlertToast
import Application
import ComposableArchitecture
import Design
import Extensions
import ToastClient
import SwiftUI

// MARK: - View

struct ApplicationToastContainerView<Content: View>: View {

	// MARK: Private properties

	private let store: StoreOf<ApplicationToast>

	private let content: () -> Content

	init(
		store: StoreOf<ApplicationToast>,
		@ViewBuilder content: @escaping () -> Content
	) {
		self.store = store
		self.content = content
	}

	// MARK: Body

	var body: some View {
		WithViewStore(store, observe: { $0 }) { viewStore in
			content().toast(
				isPresenting: viewStore.binding(
					get: \.isPresenting,
					send: { _ in .view(.didDismissCurrentToast) }
				),
				duration: viewStore.toastDuration,
				tapToDismiss: true,
				offsetY: 10,
				alert: {
					if let toast = viewStore.currentToast {
						return alertToast(from: toast)
					} else {
						/// Dummy alert view needs to be present here, but it will never be shown.
						return .init(displayMode: .hud, type: .regular)
					}
				},
				completion: {
					viewStore.send(.view(.didDismissCurrentToast))
				}
			)
		}
	}

	// MARK: - Private properties

	private func alertToast(from toast: Toast) -> AlertToast {
		return AlertToast(
			displayMode: .hud,
			type: type(for: toast),
			title: title(for: toast),
			subTitle: subtitle(for: toast),
			style: style(for: toast)
		)
	}

	private func title(for toast: Toast) -> String? {
		return toast.title
	}

	private func subtitle(for toast: Toast) -> String? {
		return toast.text
	}

	private func `type`(for toast: Toast) -> AlertToast.AlertType {
		return .regular
	}

	private func style(for toast: Toast) -> AlertToast.AlertStyle {
		let titleFont: SwiftUI.Font = .adaptiveFont(.primary, .medium, ofSize: 12)
		let subtitleFont: SwiftUI.Font = .adaptiveFont(.primary, .medium, ofSize: 10)
		switch toast.kind {
		case ._debug:
			return .style(
				backgroundColor: Asset.Color.lightningYellow.swiftUIColor,
				titleColor: Asset.Color.black.swiftUIColor,
				subTitleColor: Asset.Color.black.swiftUIColor,
				titleFont: titleFont,
				subTitleFont: subtitleFont
			)
		case .info:
			return .style(
				backgroundColor: nil,
				titleColor: nil,
				subTitleColor: nil,
				titleFont: titleFont,
				subTitleFont: subtitleFont
			)
		case .error:
			return .style(
				backgroundColor: Asset.Color.comegranate.swiftUIColor,
				titleColor: Asset.Color.alabaster.swiftUIColor,
				subTitleColor: Asset.Color.alabaster.swiftUIColor,
				titleFont: titleFont,
				subTitleFont: subtitleFont
			)
		case .success:
			return .style(
				backgroundColor: Asset.Color.malachite.swiftUIColor,
				titleColor: Asset.Color.alabaster.swiftUIColor,
				subTitleColor: Asset.Color.alabaster.swiftUIColor,
				titleFont: titleFont,
				subTitleFont: subtitleFont
			)
		case .warning:
			return .style(
				backgroundColor: nil,
				titleColor: nil,
				subTitleColor: nil,
				titleFont: titleFont,
				subTitleFont: subtitleFont
			)
		}
	}

}

// MARK: - Previews

#if DEBUG
struct ApplicationToastContainerView_Preview: PreviewProvider {

	static var previews: some View {
		ApplicationView(store: Store(
			initialState: .setupDuringSplash(toast: .init(
				toasts: [
					._debug("asdfghjkl;kjhgfds")
				]
			)),
			reducer: Application.init
		))
	}

}
#endif
