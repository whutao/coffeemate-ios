//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Contacts
import Extensions
import MatchUI
import SideProfileOverview
import SideProfileOverviewUI
import SwiftUI
import ViewComponents

// MARK: - View

public struct ContactsView: View {

	// MARK: Private properties

	private let store: StoreOf<Contacts>

	// MARK: Init

	public init(store: StoreOf<Contacts>) {
		self.store = store
	}

	// MARK: Body

	public var body: some View {
		WithViewStore(store, observe: ViewState.init) { viewStore in
			ZStack {
				Group {
					IfLetStore(store.scope(
						state: \.activeContact,
						action: Contacts.Action.contact
					)) { contactStore in
						ContactView(store: contactStore)
							.zIndex(2)
					} else: {
						switch viewStore.stub {
						case .loading:
							ContactsLoadingStubView()
								.zIndex(0)
						case .empty:
							ContactsEmptyStubView(
								title: viewStore.emptyTitle,
								description: viewStore.emptyDescription,
								isButtonLoading: viewStore.isRefreshingContacts,
								buttonTitle: viewStore.emptyRefreshButtonTitle
							) {
								viewStore.send(.view(.didTapEmptyPlaceholderRefreshButton))
							}
							.zIndex(1)
						}
					}
				}
				.transition(.opacity.animation(.easeInOut))
			}
			.disabled(!viewStore.isUserInteractionEnabled)
			.fullScreenCover(store: store.scope(
				state: \.$match,
				action: Contacts.Action.match
			)) { matchStore in
				MatchView(store: matchStore)
			}
			.sheet(store: store.scope(
				state: \.$sideProfileOverview,
				action: Contacts.Action.sideProfileOverview
			)) { sideProfileOverviewStore in
				SideProfileOverviewView(store: sideProfileOverviewStore)
			}
			.onFirstAppear {
				viewStore.send(.view(.didAppearFirstTime))
			}
		}
	}

}

// MARK: - ViewState

extension ContactsView {

	internal struct ViewState: Equatable, Sendable {

		// MARK: Exposed types

		internal enum Stub: Equatable, Sendable {
			case loading
			case empty
		}

		// MARK: Exposed properties

		internal let isUserInteractionEnabled: Bool

		internal let isRefreshingContacts: Bool

		internal let stub: Stub

		internal let emptyTitle: String

		internal let emptyDescription: String

		internal let emptyRefreshButtonTitle: String

		@PresentationState internal var sideProfileOverview: SideProfileOverview.State?

		// MARK: Init

		internal init(contactsState: Contacts.State) {
			self.isUserInteractionEnabled = contactsState.isUserInteractionEnabled
			self.isRefreshingContacts = contactsState.inProgressOperations.contains(.refreshContacts)
			self.sideProfileOverview = contactsState.sideProfileOverview
			self.stub = (contactsState.contactsRefreshTimesCount == 0 ? .loading : .empty)
			self.emptyTitle = contactsState.contactsEmptyTitle
			self.emptyDescription = contactsState.contactsEmptyDescription
			self.emptyRefreshButtonTitle = contactsState.contactsEmptyRefreshButtonTitle
		}

	}

}

// MARK: - Previews

#if DEBUG
struct ContactsView_Preview: PreviewProvider {

	static var previews: some View {
		ContactsView(store: Store(
			initialState: .init(),
			reducer: Contacts.init
		))
	}

}
#endif
