//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import SwiftUI
import ViewComponents

// MARK: - View

struct ContactsActionBarView: View {

	// MARK: Private properties

	private let messageAction: () -> Void

	private let dislikeAction: () -> Void

	private let likeAction: () -> Void

	private let profileAction: () -> Void

	// MARK: Init

	init(
		messageAction: @escaping () -> Void = {},
		dislikeAction: @escaping () -> Void = {},
		likeAction: @escaping () -> Void = {},
		profileAction: @escaping () -> Void = {}
	) {
		self.messageAction = messageAction
		self.dislikeAction = dislikeAction
		self.likeAction = likeAction
		self.profileAction = profileAction
	}

	// MARK: Body

	var body: some View {
		VStack(spacing: .zero) {
			HStack(alignment: .center, spacing: .zero) {
				Group {
					button(
						image: Asset.Image.Icon.paperplane.swiftUIImage,
						color: Asset.Color.goldSand.swiftUIColor,
						action: messageAction
					)
					.adaptiveFrame(size: .square(38))

					button(
						image: Asset.Image.Icon.cross.swiftUIImage,
						color: Asset.Color.comegranate.swiftUIColor,
						action: dislikeAction
					)
					.adaptiveFrame(size: .square(46))

					button(
						image: Asset.Image.Icon.coffee.swiftUIImage,
						color: Asset.Color.monteCarlo.swiftUIColor,
						action: likeAction
					)
					.adaptiveFrame(size: .square(46))

					button(
						image: Asset.Image.Icon.person.swiftUIImage,
						color: Asset.Color.conflowerBlue.swiftUIColor,
						action: profileAction
					)
					.adaptiveFrame(size: .square(38))
				}
				.frame(maxWidth: .infinity)
			}

			Spacer()
				.adaptiveFrame(height: 22)

			GradientSeparatorView()

			Spacer()
				.adaptiveFrame(height: 6)
		}
	}

	// MARK: Private properties

	@ViewBuilder private func button(
		image: SwiftUI.Image,
		color: SwiftUI.Color,
		action: @escaping () -> Void
	) -> some View {
		Button {
			action()
		} label: {
			TransparentCircleView(
				image: image,
				color: color
			)
		}
		.scaleOnPress()
	}

}

// MARK: - Previews

#if DEBUG
struct ContactsActionBarView_Preview: PreviewProvider {

	static var previews: some View {
		ZStack {
			Color.black
				.ignoresSafeArea()

			ContactsActionBarView()
				.padding()
		}
	}

}
#endif
