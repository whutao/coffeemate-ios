//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import SwiftUI
import ViewComponents

// MARK: - View

struct ContactsEmptyStubView: View {

	// MARK: Exposed properties

	@Dependency(\.mainQueue) private var mainQueue

	@State private var isLoadingSpinnerVisible: Bool = false

	private let title: String

	private let description: String

	private let isButtonLoading: Bool

	private let buttonTitle: String

	private let buttonAction: () -> Void

	// MARK: Init

	init(
		title: String,
		description: String,
		isButtonLoading: Bool,
		buttonTitle: String,
		buttonAction: @escaping () -> Void
	) {
		self.title = title
		self.description = description
		self.isButtonLoading = isButtonLoading
		self.buttonTitle = buttonTitle
		self.buttonAction = buttonAction
	}

	// MARK: Body

	var body: some View {
		ZStack {
			Asset.Color.black.swiftUIColor
				.ignoresSafeArea()

			VStack(alignment: .center, spacing: .zero) {
				Group {
					Text(title)
						.lineLimit(3)
						.adaptiveFont(.primary, .medium, ofSize: 16)
						.adaptivePadding(.horizontal, 10)

					Spacer()
						.adaptiveFrame(height: 20)

					Text(description)
						.lineLimit(5)
						.adaptiveFont(.primary, .regular, ofSize: 14)
				}
				.multilineTextAlignment(.center)
				.foregroundColor(Asset.Color.alabaster.swiftUIColor)

				Spacer()
					.adaptiveFrame(height: 60)

				Group {
					if isButtonLoading {
						ZStack {
							if isLoadingSpinnerVisible {
								LoadingSpinnerView(tintColor: Asset.Color.alabaster.swiftUIColor)
							} else {
								Color.clear
							}
						}
						.adaptiveFrame(size: .square(44))
						.onAppear {
							mainQueue.schedule(after: .init(.now() + 2.0)) {
								isLoadingSpinnerVisible = true
							}
						}
						.onDisappear {
							isLoadingSpinnerVisible = false
						}
					} else {
						OpaqueButton(
							title: buttonTitle,
							foregroundColor: Asset.Color.alabaster.swiftUIColor,
							backgroundColor: Asset.Color.keppel.swiftUIColor,
							action: buttonAction
						)
						.adaptiveFrame(height: 44)
					}
				}
				.adaptivePadding(.horizontal, 30)
				.transition(.scale.combined(with: .opacity).animation(.spring()))
			}
			.adaptivePadding(.horizontal, 60)
		}
	}

}

// MARK: - Previews

#if DEBUG
import Mocking

struct ContactsEmptyStubView_Preview: PreviewProvider {

	private struct ContentView: View {

		@State private var isLoading: Bool = false

		var body: some View {
			ContactsEmptyStubView(
				title: Lorem.shortSentence,
				description: Lorem.longSentence,
				isButtonLoading: isLoading,
				buttonTitle: Lorem.fullname
			) {
				isLoading = true
			}
		}

	}

	static var previews: some View {
		ContentView()
	}

}
#endif
