//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import Extensions
import SwiftUI
import ViewComponents

// MARK: - View

struct ContactsLoadingStubView: View {

	// MARK: Body

	var body: some View {
		ZStack {
			Asset.Color.black.swiftUIColor
				.ignoresSafeArea()

			VStack(alignment: .center, spacing: .zero) {
				Spacer()

				HStack(alignment: .center, spacing: .zero) {
					Spacer()

					LoadingSpinnerView(
						tintColor: Asset.Color.alabaster.swiftUIColor
					)
					.adaptiveFrame(size: .square(60))

					Spacer()
				}

				Spacer()
			}
		}
	}

}

// MARK: - Previews

#if DEBUG
struct ContactsStubView_Preview: PreviewProvider {

	static var previews: some View {
		ContactsLoadingStubView()
	}

}
#endif
