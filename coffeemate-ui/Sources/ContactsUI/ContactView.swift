//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Contacts
import Design
import Extensions
import Kingfisher
import SwiftUI
import ViewComponents

// MARK: - View

struct ContactView: View {

	// MARK: Private properties

	@Dependency(\.mainQueue) private var mainQueue

	private let store: StoreOf<Contact>

	@ObservedObject private var viewStore: ViewStoreOf<Contact>

	// MARK: Init

	init(store: StoreOf<Contact>) {
		self.store = store
		self._viewStore = .init(initialValue: .init(store, observe: { $0 }))
	}

	// MARK: Body

	var body: some View {
		ZStack {
			GeometryReader { geometry in
				KFImage(viewStore.sideProfile.avatarUrl)
					.resizable()
					.cacheOriginalImage()
					.downsampling(size: .square(1400))
					.fade(duration: 0.22)
					.placeholder {
						ZStack {
							Asset.Color.black.swiftUIColor
								.frame(size: geometry.size)
								.edgesIgnoringSafeArea(.top)

							LoadingSpinnerView(
								tintColor: Asset.Color.alabaster.swiftUIColor
							)
							.adaptiveFrame(size: .square(60))
						}
						.frame(size: geometry.size)
					}
					.aspectRatio(contentMode: .fill)
					.overlay {
						LinearGradient(
							stops: [
								.init(color: Asset.Color.black.swiftUIColor.opacity(0.9), location: 0),
								.init(color: Asset.Color.black.swiftUIColor.opacity(0.9), location: 0.1),
								.init(color: Asset.Color.black.swiftUIColor.opacity(0.4), location: 0.3),
								.init(color: Color.clear, location: 0.4),
								.init(color: Asset.Color.black.swiftUIColor.opacity(0.4), location: 0.6),
								.init(color: Asset.Color.black.swiftUIColor.opacity(0.9), location: 1)
							],
							startPoint: .top,
							endPoint: .bottom
						)
					}
					.frame(size: geometry.size)
			}
			.edgesIgnoringSafeArea(.top)

			VStack(spacing: .zero) {
				HStack(spacing: .zero) {
					Spacer()

					Group {
						if viewStore.isOptionsButtonVisible {
							Button {
								viewStore.send(.view(.didTapOptionsButton))
							} label: {
								Asset.Image.Icon.options.swiftUIImage
									.resizable()
									.aspectRatio(contentMode: .fit)
									.foregroundColor(Asset.Color.alabaster.swiftUIColor)
							}
							.scaleOnPress()
						} else {
							Color.clear
						}
					}
					.adaptiveFrame(size: .square(22))
				}
				.adaptivePadding(.horizontal, 16)

				Spacer()
					.adaptiveFrame(height: 10)

				HStack(spacing: .zero) {
					VStack(spacing: .zero) {
						GenderPriorityView(
							ownGender: viewStore.sideProfile.ownGender,
							targetGenders: viewStore.sideProfile.targetGenders
						)
						.shadow(
							color: Asset.Color.black.swiftUIColor.opacity(0.2),
							radius: 4,
							x: 0,
							y: 0
						)

						Spacer()
					}

					Spacer()

					VStack(alignment: .trailing, spacing: .zero) {
						ForEach(viewStore.sideProfile.hobbies.prefix(7)) { hobby in
							HobbyView(
								hobby: hobby,
								tintColor: Asset.Color.keppel.swiftUIColor,
								isAccented: true,
								hasTransparentBackground: true
							)
							.adaptivePadding(.vertical, 3)
							.shadow(
								color: Asset.Color.black.swiftUIColor.opacity(0.4),
								radius: 7,
								x: 0,
								y: 0
							)
						}

						Spacer()
					}
				}
				.adaptivePadding(.horizontal, 16)

				Spacer()

				VStack(alignment: .leading, spacing: .zero) {
					HStack(alignment: .bottom, spacing: .zero) {
						Text(viewStore.sideProfile.nickname)
							.lineLimit(1)
							.foregroundColor(Asset.Color.alabaster.swiftUIColor)
							.adaptiveFont(.primary, .bold, ofSize: 36)
							.multilineTextAlignment(.leading)
							.minimumScaleFactor(0.5)

						Spacer()
							.frame(width: 10)

						Text(viewStore.sideProfile.age.description)
							.lineLimit(1)
							.foregroundColor(Asset.Color.alabaster.swiftUIColor)
							.adaptiveFont(.primary, .regular, ofSize: 24)
							.padding(.bottom, 4)

						Group {
							if viewStore.sideProfile.isVerified {
								Spacer()
									.frame(width: 10)

								Asset.Image.Icon.verified.swiftUIImage
									.foregroundColor(Asset.Color.kournikova.swiftUIColor)
									.frame(size: .square(20))
									.padding(.bottom, 8)
							}
						}

						Spacer()
					}

					Spacer()
						.adaptiveFrame(height: 12)

					Text(viewStore.sideProfile.about)
						.lineLimit(4)
						.multilineTextAlignment(.leading)
						.foregroundColor(Asset.Color.alabaster.swiftUIColor)
						.adaptiveFont(.primary, .regular, ofSize: 16)
				}
				.adaptivePadding(.horizontal, 16)

				Spacer()
					.adaptiveFrame(height: 24)

				ContactsActionBarView(
					messageAction: { viewStore.send(.view(.didTapSendMessageButton)) },
					dislikeAction: { viewStore.send(.view(.didTapDislikeButton)) },
					likeAction: { viewStore.send(.view(.didTapLikeButton)) },
					profileAction: { viewStore.send(.view(.didTapProfileButton)) }
				)
				.adaptivePadding(.horizontal, 38)
				.adaptivePadding(.bottom, 24)
			}
		}
	}

}

// MARK: - Previews

#if DEBUG
import Mocking

struct ContactView_Preview: PreviewProvider {

	static var previews: some View {
		ZStack {
			Color.black.ignoresSafeArea()

			ContactView(store: Store(
				initialState: .init(isOptionsButtonVisible: false, sideProfile: .init(
					profileHash: "1234567890",
					avatarUrl: URL(string: "https://www.pakainfo.com/wp-content/uploads/2021/09/image-url-for-testing.jpg")!,
					nickname: "Julia Levashova",
					age: 21,
					about: "Хочу стать президентом мира и первым указом выдать каждому по котенку",
					rating: 4.8,
					ownGender: .female,
					targetGenders: [.female, .male],
					hobbies: [
						.init(
							hobbyId: 1,
							title: "Вязание",
							imageUrl: URL(string: "https://api.coffeemates.space:443/images/hobbies/insta_blogger.svg")!,
							isPrivileged: false
						),
						.init(
							hobbyId: 2,
							title: "Турецкие сериалы",
							imageUrl: URL(string: "https://api.coffeemates.space:443/images/hobbies/insta_blogger.svg")!,
							isPrivileged: false
						),
						.init(
							hobbyId: 3,
							title: "Животные",
							imageUrl: URL(string: "https://api.coffeemates.space:443/images/hobbies/insta_blogger.svg")!,
							isPrivileged: false
						),
						.init(
							hobbyId: 4,
							title: "Психология",
							imageUrl: URL(string: "https://api.coffeemates.space:443/images/hobbies/insta_blogger.svg")!,
							isPrivileged: false
						),
						.init(
							hobbyId: 5,
							title: "Мода",
							imageUrl: URL(string: "https://api.coffeemates.space:443/images/hobbies/insta_blogger.svg")!,
							isPrivileged: false
						),
						.init(
							hobbyId: 6,
							title: "Мода",
							imageUrl: URL(string: "https://api.coffeemates.space:443/images/hobbies/insta_blogger.svg")!,
							isPrivileged: false
						),
						.init(
							hobbyId: 7,
							title: "Мода",
							imageUrl: URL(string: "https://api.coffeemates.space:443/images/hobbies/insta_blogger.svg")!,
							isPrivileged: false
						)
					],
					gallery: [],
					isVerified: true
				)),
				reducer: Contact.init
			))
			.padding(.bottom, 60)
		}
	}

}
#endif
