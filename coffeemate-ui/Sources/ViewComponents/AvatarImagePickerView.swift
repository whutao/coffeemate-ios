//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import Foundation
import Kingfisher
import Models
import PhotosUI
import SwiftUI

// MARK: - View

public struct AvatarImagePickerView: View {

	// MARK: Private properties

	@Binding private var selectedImageRepresentation: ImageRepresentation?

	@State private var lastPickedImage: UIImage? = nil

	@State private var isPhotoPickerPresented: Bool = false

	@State private var isPhotoCropperPresented: Bool = false

	private let selectedImageMaxSizeInKB: UInt

	// MARK: Init

	public init(
		selectedImageData: Binding<Data?>,
		selectedImageMaxSizeInKB: UInt
	) {
		self._selectedImageRepresentation = Binding(
			get: {
				selectedImageData.wrappedValue.flatMap(ImageRepresentation.data)
			},
			set: { newValue in
				if case let .data(newData) = newValue {
					selectedImageData.wrappedValue = newData
				} else {
					selectedImageData.wrappedValue = nil
				}
			}
		)
		self.selectedImageMaxSizeInKB = selectedImageMaxSizeInKB
	}

	public init(
		selectedImage: Binding<ImageRepresentation?>,
		selectedImageMaxSizeInKB: UInt
	) {
		self._selectedImageRepresentation = selectedImage
		self.selectedImageMaxSizeInKB = selectedImageMaxSizeInKB
	}

	// MARK: Body

	public var body: some View {
		ZStack(alignment: .bottomTrailing) {
			Group {
				if let selectedImageRepresentation {
					ImageView(
						imageRepresentation: selectedImageRepresentation,
						ifInterpretedAsKFImage: { kfImage in
							kfImage
								.resizable()
								.cacheOriginalImage()
								.appendProcessor(DownsamplingImageProcessor(size: .square(300)))
						},
						ifInterpretedAsImage: { swiftUIImage in
							swiftUIImage
								.resizable()
						}
					)
					.aspectRatio(contentMode: .fill)
				} else {
					ZStack {
						Asset.Color.gunsmoke.swiftUIColor
							.opacity(0.6)

						Asset.Image.Icon.thickPlus.swiftUIImage
							.resizable()
							.aspectRatio(contentMode: .fit)
							.foregroundColor(Asset.Color.mercury.swiftUIColor)
							.adaptiveFrame(size: .square(34))
					}
					.onTapGesture {
						isPhotoPickerPresented = true
					}
				}
			}
			.adaptiveFrame(size: .square(108))
			.clipShape(RoundedRectangle(cornerRadius: 20))
			.overlay {
				RoundedRectangle(cornerRadius: 20)
					.adaptiveStroke(Asset.Color.monteCarlo.swiftUIColor, lineWidth: 1)
			}

			Button {
				isPhotoPickerPresented = true
			} label: {
				ZStack {
					Asset.Color.paradiso.swiftUIColor

					Asset.Image.Icon.camera.swiftUIImage
						.foregroundColor(Asset.Color.monteCarlo.swiftUIColor)
				}
				.clipShape(Capsule())
				.overlay {
					Capsule()
						.adaptiveStroke(Asset.Color.monteCarlo.swiftUIColor, lineWidth: 2)
				}
			}
			.scaleOnPress()
			.adaptiveFrame(size: .square(36))
			.offset(x: 8, y: 8)
		}
		.singleImagePickerSheet(isPresented: $isPhotoPickerPresented) { imageData in
			if let compressedImageData = imageData.compressedImageData(toFitKB: selectedImageMaxSizeInKB) {
				selectedImageRepresentation = .data(compressedImageData)
			} else {
				selectedImageRepresentation = nil
			}
		}
		.adaptiveFrame(size: .square(126))
//		.singleImagePickerSheet(
//			isPresented: $isPhotoPickerPresented
//		) { imageData in
//			lastPickedImage = UIImage(data: imageData)
//			withAnimation(.linear.delay(0.3)) {
//				isPhotoCropperPresented = true
//			}
//		}
//		.fullScreenCover(isPresented: $isPhotoCropperPresented) {
//			SingleImageCropperView(originalImage: lastPickedImage.forceUnwrapped(
//				because: "Property lastPickedImage must be set before calling photo cropper."
//			)) { uiImage in
//				let imageData = uiImage.pngData()
//				if let compressedImageData = imageData?.compressedImageData(toFitKB: selectedImageMaxSizeInKB) {
//					selectedImageRepresentation = .data(compressedImageData)
//				} else {
//					selectedImageRepresentation = nil
//				}
//			}
//		}
	}

}

// MARK: - Previews

#if DEBUG
struct PhotoPickerView_Preview: PreviewProvider {

	private struct ContentView: View {

		@State private var selectedImageData: Data? = nil

		var body: some View {
			AvatarImagePickerView(
				selectedImageData: $selectedImageData,
				selectedImageMaxSizeInKB: 100
			)
		}

	}

	static var previews: some View {
		ContentView()
	}

}
#endif
