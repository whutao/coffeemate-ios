//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import SwiftUI

// MARK: - View

public struct PositionObservingView<Content: View>: View {

	// MARK: Private properties

	private let coordinateSpace: CoordinateSpace

	@Binding private var position: CGPoint

	@ViewBuilder private let content: () -> Content

	// MARK: Init

	public init(
		coordinateSpace: CoordinateSpace,
		position: Binding<CGPoint>,
		@ViewBuilder content: @escaping () -> Content
	) {
		self.coordinateSpace = coordinateSpace
		self._position = position
		self.content = content
	}

	// MARK: Init

	public var body: some View {
		content()
			.background {
				GeometryReader { geometry in
					Color.clear.preference(
						key: PreferenceKey.self,
						value: geometry.frame(in: coordinateSpace).origin
					)
				}
			}
			.onPreferenceChange(PreferenceKey.self) { position in
				self.position = position
			}
	}

}

// MARK: - Preference key

extension PositionObservingView {

	private struct PreferenceKey: SwiftUI.PreferenceKey {

		static var defaultValue: CGPoint { .zero }

		static func reduce(value: inout CGPoint, nextValue: () -> CGPoint) {

		}
	}

}
