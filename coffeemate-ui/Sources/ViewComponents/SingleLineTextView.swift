//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import Extensions
import SwiftUI

// MARK: - View

public struct SingleLineTextView: View {

	// MARK: Private properties

	private let titleStyle: TitleStyle

	private let footerStyle: FooterStyle

	private let placeholder: String

	@Binding private var text: String

	// MARK: Init

	public init(
		title titleStyle: TitleStyle = .hidden,
		footer footerStyle: FooterStyle = .hidden,
		placeholder: String,
		text: Binding<String>
	) {
		self.titleStyle = titleStyle
		self.footerStyle = footerStyle
		self.placeholder = placeholder
		self._text = text
	}

	// MARK: Body

	public var body: some View {
		VStack(alignment: .leading, spacing: .zero) {
			if case let .text(text) = titleStyle {
				Text(text)
					.lineLimit(1)
					.foregroundColor(Asset.Color.gunsmoke.swiftUIColor)
					.adaptiveFont(.primary, .regular, ofSize: 12)
					.adaptivePadding(.bottom, 6)
					.offset(x: 20)
			}

			TextField(placeholder, text: $text)
				.foregroundColor(Asset.Color.black.swiftUIColor)
				.adaptiveFont(.primary, .regular, ofSize: 14)
				.adaptivePadding(.vertical, 10)
				.adaptivePadding(.horizontal, 20)
				.overlay {
					RoundedRectangle(cornerRadius: 10)
						.adaptiveStroke(Asset.Color.gunsmoke.swiftUIColor.opacity(0.4), lineWidth: 1)
				}
				.adaptivePadding(1)

			if case let .text(text) = footerStyle {
				HStack(spacing: .zero) {
					Spacer()

					Text(text)
						.lineLimit(1)
						.foregroundColor(Asset.Color.gunsmoke.swiftUIColor.opacity(0.4))
						.adaptiveFont(.primary, .regular, ofSize: 12)

					Spacer()
						.adaptiveFrame(width: 4)
				}
				.adaptivePadding(.top, 6)
			}
		}
	}

}

// MARK: Title style

extension SingleLineTextView {

	public enum TitleStyle {
		case hidden
		case text(String)
	}

}

// MARK: Footer style

extension SingleLineTextView {

	public enum FooterStyle {
		case hidden
		case text(String)
	}

}

// MARK: - Previews

#if DEBUG
struct SingleLineTextView_Preview: PreviewProvider {

	static var previews: some View {
		ZStack {
			Color.black.ignoresSafeArea()

			VStack {
				Group {
					SingleLineTextView(
						title: .hidden,
						placeholder: "Placeholder...",
						text: .constant(.empty)
					)

					SingleLineTextView(
						title: .text("First name"),
						placeholder: "Placeholder...",
						text: .constant("Aboba")
					)

					SingleLineTextView(
						title: .hidden,
						placeholder: "Placeholder...",
						text: .constant(.empty)
					)
				}
				.padding(.horizontal)
				.background {
					Color.white
				}
			}
		}
	}

}
#endif
