//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Extensions
import Kingfisher
import Models
import SwiftUI

#warning("Add docstrings.")

// MARK: - View

public struct ImageView<
	ImageContent: View,
	KFImageContent: View,
	FailedContent: View
>: View {

	// MARK: Private properties

	private let imageRepresentation: ImageRepresentation

	private let ifInterpretedAsKFImage: (KFImage) -> KFImageContent

	private let ifInterpretedAsImage: (SwiftUI.Image) -> ImageContent

	private let ifFailedToInterpret: () -> FailedContent

	// MARK: Init

	public init(
		imageRepresentation: ImageRepresentation,
		ifInterpretedAsKFImage: @escaping (KFImage) -> KFImageContent = { image in image },
		ifInterpretedAsImage: @escaping (SwiftUI.Image) -> ImageContent = { image in image },
		ifFailedToInterpret: @escaping () -> FailedContent = { EmptyView() }
	) {
		self.imageRepresentation = imageRepresentation
		self.ifInterpretedAsKFImage = ifInterpretedAsKFImage
		self.ifInterpretedAsImage = ifInterpretedAsImage
		self.ifFailedToInterpret = ifFailedToInterpret
	}

	// MARK: Body

	public var body: some View {
		switch imageRepresentation {
		case let .data(data):
			if let image = Image(data: data) {
				ifInterpretedAsImage(image)
			} else {
				ifFailedToInterpret()
			}
		case let .url(url):
			ifInterpretedAsKFImage(KFImage(url))
		}
	}

}

// MARK: - Previews

#if DEBUG
struct ImageView_Preview: PreviewProvider {

	static var previews: some View {
		EmptyView()
	}

}
#endif
