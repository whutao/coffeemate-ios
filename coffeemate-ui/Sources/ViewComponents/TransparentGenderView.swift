//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import Extensions
import Models
import SwiftUI

// MARK: - View

public struct TransparentGenderView: View {

	// MARK: Private properties

	private let gender: Gender

	// MARK: Init

	public init(gender: Gender) {
		self.gender = gender
	}

	// MARK: Body

	public var body: some View {
		TransparentCircleView(
			image: image(for: gender),
			color: color(for: gender)
		)
	}

	// MARK: Private methods

	private func image(for gender: Gender) -> SwiftUI.Image {
		switch gender {
		case .male:
			return Asset.Image.Gender.male.swiftUIImage
		case .female:
			return Asset.Image.Gender.female.swiftUIImage
		case .other:
			return Asset.Image.Gender.other.swiftUIImage
		}
	}

	private func color(for gender: Gender) -> SwiftUI.Color {
		switch gender {
		case .male:
			return Asset.Color.conflowerBlue.swiftUIColor.opacity(0.7)
		case .female:
			return Asset.Color.heliotrope.swiftUIColor.opacity(0.7)
		case .other:
			return Asset.Color.keppel.swiftUIColor.opacity(0.7)
		}
	}

}

// MARK: - Previews

#if DEBUG
struct TransparentGenderView_Preview: PreviewProvider {

	static var previews: some View {
		VStack {
			Group {
				TransparentGenderView(gender: .male)

				TransparentGenderView(gender: .female)

				TransparentGenderView(gender: .other)
			}
			.padding(80)
		}
	}

}
#endif
