//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import Extensions
import SwiftUI

// MARK: - View

public struct ExpandableText: View {

	// MARK: Private properties

	@State private var isExpanded: Bool = false

	@State private var isTruncated: Bool = false

	@State private var intrinsicSize: CGSize = .zero

	@State private var truncatedSize: CGSize = .zero

	@State private var moreTextSize: CGSize = .zero

	private let text: String

	private var font: SwiftUI.Font = .body

	private var color: SwiftUI.Color = .primary

	private var lineLimit: Int

	private var moreButtonText: String

	private var moreButtonColor: SwiftUI.Color

	private var expandAnimation: Animation = .default

	private var trimMultipleNewlinesWhenTruncated: Bool

	// MARK: Init

	public init(
		text: String,
		textFont: SwiftUI.Font = FontKind.primary.font(ofSize: 14, weight: .regular),
		textColor: SwiftUI.Color = Asset.Color.black.swiftUIColor,
		lineLimit: Int = 3,
		moreButtonText: String,
		moreButtonColor: SwiftUI.Color = Asset.Color.conflowerBlue.swiftUIColor,
		trimMultipleNewlinesWhenTruncated: Bool = true
	) {
		self.text = text.trimmingCharacters(in: .whitespacesAndNewlines)
		self.font = textFont
		self.color = textColor
		self.lineLimit = lineLimit
		self.moreButtonText = moreButtonText
		self.moreButtonColor = moreButtonColor
		self.trimMultipleNewlinesWhenTruncated = trimMultipleNewlinesWhenTruncated
	}

	// MARK: Body

	public var body: some View {
		content()
			.lineLimit(isExpanded ? nil : lineLimit)
			.truncationMask(size: moreTextSize, enabled: shouldShowMoreButton)
			.readSize { size in
				truncatedSize = size
				isTruncated = (truncatedSize != intrinsicSize)
			}
			.background(
				content()
					.lineLimit(nil)
					.fixedSize(horizontal: false, vertical: true)
					.hidden()
					.readSize { size in
						intrinsicSize = size
						isTruncated = truncatedSize != intrinsicSize
					}
			)
			.background(
				Text(moreButtonText)
					.font(font)
					.hidden()
					.readSize { size in
						moreTextSize = size
					}
			)
			.contentShape(Rectangle())
			.onTapGesture {
				if shouldShowMoreButton {
					withAnimation(expandAnimation) {
						isExpanded.toggle()
					}
				}
			}
			.overlay(alignment: .trailingLastTextBaseline) {
				if shouldShowMoreButton {
					Button {
						withAnimation(expandAnimation) {
							isExpanded.toggle()
						}
					} label: {
						Text(moreButtonText)
							.font(font)
							.foregroundColor(moreButtonColor)
					}
					.scaleOnPress()
				}
			}
	}

	// MARK: - private properties

	@ViewBuilder private func content() -> some View {
		Group {
			if trimMultipleNewlinesWhenTruncated, shouldShowMoreButton {
				Text(textTrimmingDoubleNewlines)
			} else {
				Text(text)
			}
		}
		.font(font)
		.foregroundColor(color)
		.frame(maxWidth: .infinity, alignment: .leading)
	}

	private var shouldShowMoreButton: Bool {
		return !isExpanded && isTruncated
	}

	private var textTrimmingDoubleNewlines: String {
		return text.replacingOccurrences(
			of: #"\n\s*\n"#,
			with: "\n",
			options: .regularExpression
		)
	}
}

// MARK: - Previews

#if DEBUG
import Mocking

struct ExpandableText_Preview: PreviewProvider {

	static var previews: some View {
		ScrollView {
			VStack {
				Group {
					ExpandableText(text: Lorem.fullname, moreButtonText: "Read more")
					ExpandableText(text: Lorem.shortSentence, moreButtonText: "Read more")
					ExpandableText(text: Lorem.longParagraph, moreButtonText: "Read more")
				}
				.background {
					Color.gray.opacity(0.2)
				}
				.padding()
				.padding(.vertical, 40)
			}
		}
		.ignoresSafeArea()
	}

}
#endif
