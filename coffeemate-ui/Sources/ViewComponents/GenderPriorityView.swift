//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import Models
import SwiftUI

// MARK: - View

public struct GenderPriorityView: View {

	// MARK: Private properties

	private let ownGender: Gender

	private let targetGenders: [Gender]

	private var topTrailingGender: Gender? {
		return targetGenders.count > 1 ? targetGenders[1] : nil
	}

	private var trailingGender: Gender? {
		return !targetGenders.isEmpty ? targetGenders[0] : nil
	}

	private var bottomTrailingGender: Gender? {
		return targetGenders.count > 2 ? targetGenders[2] : nil
	}

	// MARK: Init

	public init(
		ownGender: Gender,
		targetGenders: Set<Gender>
	) {
		self.ownGender = ownGender
		self.targetGenders = Array(targetGenders)
	}

	// MARK: Body

	public var body: some View {
		ZStack {
			Group {
				view(for: ownGender)
					.adaptiveFrame(size: .square(46))

				if let trailingGender {
					view(for: trailingGender)
						.adaptiveFrame(size: .square(24))
						.offset(x: 46)
				}

				if let topTrailingGender {
					view(for: topTrailingGender)
						.adaptiveFrame(size: .square(24))
						.offset(x: 34, y: -30)
				}

				if let bottomTrailingGender {
					view(for: bottomTrailingGender)
						.adaptiveFrame(size: .square(24))
						.offset(x: 34, y: 30)
				}
			}
			.offset(x: -22)
		}
		.adaptiveFrame(width: 90, height: 94)
	}

	// MARK: Private properties

	@ViewBuilder private func view(for gender: Gender) -> some View {
		TransparentGenderView(gender: gender)
	}

}

// MARK: - Previews

#if DEBUG
struct GenderPriorityView_Preview: PreviewProvider {

	static var previews: some View {
		ForEachDevice([.iPhone14Pro, .iPhoneSE3rdGeneration]) {
			ZStack {
				Color.black.ignoresSafeArea()

				VStack {
					Group {
						GenderPriorityView(
							ownGender: .other,
							targetGenders: []
						)

						GenderPriorityView(
							ownGender: .male,
							targetGenders: [.female]
						)

						GenderPriorityView(
							ownGender: .female,
							targetGenders: [.male, .other]
						)

						GenderPriorityView(
							ownGender: .other,
							targetGenders: [.female, .male, .other]
						)
					}
					.background(Color.white)
					.padding()
				}
			}
		}
	}

}
#endif
