//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import SwiftUI

// MARK: - View

public struct GradientSeparatorView: View {

	// MARK: Init

	public init() {

	}

	// MARK: Body

	public var body: some View {
		LinearGradient(
			stops: [
				.init(color: Color.clear, location: 0),
				.init(color: Asset.Color.monteCarlo.swiftUIColor.opacity(0.8), location: 0.35),
				.init(color: Asset.Color.monteCarlo.swiftUIColor.opacity(0.8), location: 0.65),
				.init(color: Color.clear, location: 1)
			],
			startPoint: .leading,
			endPoint: .trailing
		)
		.frame(height: 1)
	}

}

// MARK: - Previews

#if DEBUG
struct GradientSeparatorView_Preview: PreviewProvider {

	static var previews: some View {
		ZStack {
			Color.gray.ignoresSafeArea()

			GradientSeparatorView()
				.padding()
		}
	}

}
#endif
