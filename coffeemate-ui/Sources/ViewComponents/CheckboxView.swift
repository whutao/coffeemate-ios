//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import Extensions
import SwiftUI

// MARK: - View

public struct CheckboxView: View {

	// MARK: Private properties

	@Binding private var isSelected: Bool

	// MARK: Init

	public init(isSelected: Binding<Bool>) {
		self._isSelected = isSelected
	}

	// MARK: Body

	public var body: some View {
		Button {
			withAnimation {
				isSelected.toggle()
			}
		} label: {
			Group {
				if isSelected {
					Image(systemName: "checkmark")
						.resizable()
						.aspectRatio(contentMode: .fit)
						.font(.system(size: 14, weight: .heavy))
						.foregroundColor(Asset.Color.black.swiftUIColor)
						.padding(6)
				} else {
					Color.clear
				}
			}
			.transition(.opacity.animation(.linear(duration: 0.12)))
			.contentShape(RoundedRectangle(cornerRadius: 6))
			.clipShape(RoundedRectangle(cornerRadius: 6))
			.overlay {
				RoundedRectangle(cornerRadius: 6)
					.adaptiveStroke(
						Asset.Color.gunsmoke.swiftUIColor.opacity(0.7),
						lineWidth: 0.8
					)
			}
		}
		.scaleOnPress()
	}

}

// MARK: - Previews

#if DEBUG
struct CheckboxView_Preview: PreviewProvider {

	static var previews: some View {
		EmptyView()
	}

}
#endif
