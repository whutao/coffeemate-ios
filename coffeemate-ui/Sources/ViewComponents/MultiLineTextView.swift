//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import Extensions
import SwiftUI

// MARK: - View

public struct MultiLineTextView: View {

	// MARK: Private properties

	private let footerStyle: FooterStyle

	@Binding private var text: String

	// MARK: Init

	public init(
		footer footerStyle: FooterStyle = .hidden,
		text: Binding<String>
	) {
		self._text = text
		self.footerStyle = footerStyle
	}

	// MARK: Body

	public var body: some View {
		VStack(alignment: .leading, spacing: .zero) {
			TextEditor(text: $text)
				.scrollContentBackground(.hidden)
				.foregroundColor(Asset.Color.black.swiftUIColor)
				.adaptiveFont(.primary, .regular, ofSize: 14)
				.adaptivePadding(.vertical, 10)
				.adaptivePadding(.horizontal, 20)
				.overlay {
					RoundedRectangle(cornerRadius: 10)
						.adaptiveStroke(Asset.Color.gunsmoke.swiftUIColor.opacity(0.4), lineWidth: 1)
				}

			if case let .text(text) = footerStyle {
				HStack(spacing: .zero) {
					Spacer()

					Text(text)
						.lineLimit(1)
						.foregroundColor(Asset.Color.gunsmoke.swiftUIColor.opacity(0.4))
						.adaptiveFont(.primary, .regular, ofSize: 12)

					Spacer()
						.adaptiveFrame(width: 4)
				}
				.adaptivePadding(.top, 6)
			}
		}
		.adaptivePadding(1)
	}

}

// MARK: Footer style

extension MultiLineTextView {

	public enum FooterStyle {
		case hidden
		case text(String)
	}

}

// MARK: - Previews

#if DEBUG
import Mocking

struct MultiLineTextView_Preview: PreviewProvider {

	static var previews: some View {
		ZStack {
			Color.black.ignoresSafeArea()

			MultiLineTextView(
				footer: .text(Lorem.shortSentence),
				text: .constant(Lorem.longParagraph)
			)
			.background(.white)
			.frame(size: .square(300))
		}
	}

}
#endif
