//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import SwiftUI

// MARK: - ViewModifier

private struct SingleCroppedMediaPickerContainerView<Content: View>: View {

	// MARK: Private properties

	@Binding private var isPresented: Bool

	@State private var isPresentingImageCropper: Bool = false

	@State private var pickedImage: UIImage? = nil

	@State private var croppedImage: UIImage? = nil

	private let completionAction: (UIImage) -> Void

	private let content: () -> Content

	// MARK: Init

	init(
		isPresented: Binding<Bool>,
		content: @escaping () -> Content,
		completionAction: @escaping (UIImage) -> Void
	) {
		self._isPresented = isPresented
		self.completionAction = completionAction
		self.content = content
	}

	// MARK: Body

	var body: some View {
		content()
			.sheet(isPresented: $isPresented) {
				SingleImagePickerView(isPresented: $isPresented) { imageData in
					pickedImage = UIImage(data: imageData)
					isPresentingImageCropper = true
				}
			}
			.fullScreenCover(isPresented: $isPresentingImageCropper) {
				if let pickedImage {
					SingleImageCropperView(originalImage: pickedImage) { uiImage in
						croppedImage = uiImage
						isPresented = false
					}
				}
			}
	}

}

// MARK: - View+singleCroppedMediaPickerPresentation

extension View {

	@ViewBuilder public func singleCroppedMediaPickerPresentation(
		isPresented: Binding<Bool>,
		completionAction: @escaping (UIImage) -> Void
	) -> some View {
		SingleCroppedMediaPickerContainerView(isPresented: isPresented) {
			self
		} completionAction: { uiImage in
			completionAction(uiImage)
		}
	}

}
