//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import Extensions
import Models
import SwiftUI

// MARK: - View

public struct GenderPickerView: View {

	// MARK: Private properties

	private let style: Style

	private let genders: [Gender]

	private let allowsMultipleSelection: Bool

	@Binding private var selectedGenders: [Gender]

	// MARK: Init

	public init(
		style: Style,
		fromUniqueGenders genders: [Gender],
		selectedGenders: Binding<[Gender]>
	) {
		self.style = style
		self.genders = genders
		self._selectedGenders = selectedGenders
		self.allowsMultipleSelection = true
	}

	public init(
		style: Style,
		fromUniqueGenders genders: [Gender],
		selectedGenders: Binding<Set<Gender>>
	) {
		self.style = style
		self.genders = genders
		self._selectedGenders = Binding(
			get: {
				selectedGenders.wrappedValue.sorted(by: \.id)
			},
			set: { genders in
				selectedGenders.wrappedValue = Set(genders)
			}
		)
		self.allowsMultipleSelection = true
	}

	public init(
		style: Style,
		fromUniqueGenders genders: [Gender],
		selectedGender: Binding<Gender?>
	) {
		self.style = style
		self.genders = genders
		self._selectedGenders = Binding(
			get: {
				selectedGender.wrappedValue
					.flatMap({ [$0] })
					.unwrapped(or: [])
			},
			set: { genders in
				selectedGender.wrappedValue = genders.first
			}
		)
		self.allowsMultipleSelection = false
	}

	// MARK: Body

	public var body: some View {
		HStack(alignment: .center, spacing: .zero) {
			ForEach(genders) { gender in
				Button {
					if isSelected(gender) {
						if allowsMultipleSelection, selectedGenders.count > 1 {
							selectedGenders.removeAll(where: { $0 == gender })
						} else {
							return
						}
					} else {
						if allowsMultipleSelection {
							selectedGenders.append(gender)
						} else {
							selectedGenders = [gender]
						}
					}
				} label: {
					image(for: gender)
						.resizable()
						.aspectRatio(contentMode: .fit)
						.foregroundColor(tintColor(for: gender))
						.adaptiveFrame(size: .square(20))
						.adaptivePadding(.all, 11)
						.background(backgroundColor(for: gender))
						.clipShape(Capsule())
						.overlay {
							Capsule()
								.stroke(tintColor(for: gender), lineWidth: borderWidth(for: gender))
						}
				}
				.idleOnPress()
				.frame(maxWidth: .infinity, maxHeight: .infinity)
			}
		}
		.animation(.spring(), value: selectedGenders)
	}

	// MARK: Private properties

	private func isSelected(_ gender: Gender) -> Bool {
		return selectedGenders.contains(gender)
	}

	private func borderWidth(for gender: Gender) -> CGFloat {
		if isSelected(gender) {
			switch style {
			case .ownGender:
				return 2
			case .targetGender:
				return 1
			}
		} else {
			return 1
		}
	}

	private func image(for gender: Gender) -> SwiftUI.Image {
		switch gender {
		case .male:
			return Asset.Image.Gender.male.swiftUIImage
		case .female:
			return Asset.Image.Gender.female.swiftUIImage
		case .other:
			return Asset.Image.Gender.other.swiftUIImage
		}
	}

	private func tintColor(for gender: Gender) -> SwiftUI.Color {
		if isSelected(gender) {
			switch (style, gender) {
			case (.ownGender, .male):
				return Asset.Color.conflowerBlue.swiftUIColor
			case (.ownGender, .female):
				return Asset.Color.heliotrope.swiftUIColor
			case (.ownGender, .other):
				return Asset.Color.keppel.swiftUIColor
			case (.targetGender, _):
				return Asset.Color.blueChalk.swiftUIColor
			}
		} else {
			return Asset.Color.gunsmoke.swiftUIColor.opacity(0.4)
		}
	}

	private func backgroundColor(for gender: Gender) -> SwiftUI.Color {
		if isSelected(gender) {
			switch (style, gender) {
			case (.ownGender, _):
				return Color.clear
			case (.targetGender, .female):
				return Asset.Color.heliotrope.swiftUIColor
			case (.targetGender, .male):
				return Asset.Color.conflowerBlue.swiftUIColor
			case (.targetGender, .other):
				return Asset.Color.keppel.swiftUIColor
			}
		} else {
			switch style {
			case .ownGender:
				return Color.clear
			case .targetGender:
				return Asset.Color.mercury.swiftUIColor.opacity(0.7)
			}
		}
	}

}

// MARK: - Style

extension GenderPickerView {

	public enum Style {

		// MARK: Case

		case ownGender

		case targetGender

	}

}

// MARK: - Previews

#if DEBUG
struct GenderPickerView_Preview: PreviewProvider {

	private struct ContentView: View {

		@State private var ownGender: Gender? = nil

		@State private var targetGenders: [Gender] = []

		var body: some View {
			VStack {
				Group {
					GenderPickerView(
						style: .ownGender,
						fromUniqueGenders: [.female, .male, .other],
						selectedGender: $ownGender
					)

					GenderPickerView(
						style: .targetGender,
						fromUniqueGenders: [.female, .male, .other],
						selectedGenders: $targetGenders
					)
				}
				.padding(60)
			}
		}

	}

	static var previews: some View {
		ContentView()
	}

}
#endif
