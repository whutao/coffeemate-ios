//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import UIKit

// MARK: - Path

/// Creates a rounded rectangle path for the given rectangle using corner radii for each corner.
public func roundedRectanglePath(
	minXminYRadius: CGFloat = 0,
	maxXminYRadius: CGFloat = 0,
	maxXmaxYRadius: CGFloat = 0,
	minXmaxYRadius: CGFloat = 0,
	in rect: CGRect
) -> UIBezierPath {
	let path = UIBezierPath()
	path.move(to: CGPoint(x: minXminYRadius, y: 0))
	path.addLine(to: CGPoint(x: rect.width - maxXminYRadius, y: 0))
	path.addArc(
		withCenter: CGPoint(x: rect.width - maxXminYRadius, y: maxXminYRadius),
		radius: maxXminYRadius,
		startAngle: (3/2) * .pi,
		endAngle: (0) * .pi,
		clockwise: true
	)
	path.addLine(to: CGPoint(x: rect.width, y: rect.height - maxXmaxYRadius))
	path.addArc(
		withCenter: CGPoint(x: rect.width - maxXmaxYRadius, y: rect.height - maxXmaxYRadius),
		radius: maxXmaxYRadius,
		startAngle: (0) * .pi,
		endAngle: (1/2) * .pi,
		clockwise: true
	)
	path.addLine(to: CGPoint(x: minXmaxYRadius, y: rect.height))
	path.addArc(
		withCenter: CGPoint(x: minXmaxYRadius, y: rect.height - minXmaxYRadius),
		radius: minXmaxYRadius,
		startAngle: (1/2) * .pi,
		endAngle: (1) * .pi,
		clockwise: true
	)
	path.addLine(to: CGPoint(x: 0, y: minXminYRadius))
	path.addArc(
		withCenter: CGPoint(x: minXminYRadius, y: minXminYRadius),
		radius: minXminYRadius,
		startAngle: (1) * .pi,
		endAngle: (3/2) * .pi,
		clockwise: true
	)
	path.close()
	return path
}
