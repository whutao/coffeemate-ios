//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import Extensions
import SwiftUI

// MARK: - View

public struct OpaqueButton: View {

	// MARK: Private properties

	private let title: String

	private let foregroundColor: SwiftUI.Color

	private let backgroundColor: SwiftUI.Color

	private let action: () -> Void

	// MARK: Init

	public init(
		title: String,
		foregroundColor: Color = Asset.Color.alabaster.swiftUIColor,
		backgroundColor: Color = Asset.Color.conflowerBlue.swiftUIColor,
		action: @escaping () -> Void
	) {
		self.title = title
		self.foregroundColor = foregroundColor
		self.backgroundColor = backgroundColor
		self.action = action
	}

	// MARK: Body

	public var body: some View {
		Button {
			action()
		} label: {
			Text(title)
				.lineLimit(1)
				.foregroundColor(foregroundColor)
				.adaptiveFont(.primary, .medium, ofSize: 16)
				.frame(maxWidth: .infinity, maxHeight: .infinity)
				.background {
					backgroundColor
						.clipShape(RoundedRectangle(cornerRadius: 10))
				}
		}
		.scaleOnPress()
	}

}

// MARK: - Preview

#if DEBUG
struct OpaqueButton_Preview: PreviewProvider {

	static var previews: some View {
		VStack {
			OpaqueButton(title: "Press me") {

			}

			OpaqueButton(title: "Press me") {

			}
			.frame(width: 100, height: 40)

			OpaqueButton(title: "Press me") {

			}
			.frame(width: 200, height: 60)
		}
		.padding(20)
	}

}
#endif
