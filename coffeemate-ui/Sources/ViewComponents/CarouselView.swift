//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Combine
import IdentifiedCollections
import Extensions
import SwiftUI

// MARK: - View

public struct CarouselView<Item: Identifiable & Hashable, Content: View>: View {

	// MARK: Typealiases

	public typealias ContentBuilder = (_ item: Item, _ size: CGSize) -> Content

	// MARK: Private properties

	@Binding private var selectedItemIndex: Int

	private let spacing: CGFloat

	private let items: IdentifiedArrayOf<Item>

	private let content: ContentBuilder

	// MARK: Init

	public init(
		items: IdentifiedArrayOf<Item>,
		selectedItemIndex: Binding<Int>,
		spacing: CGFloat = 10,
		@ViewBuilder content: @escaping ContentBuilder
	) {
		self._selectedItemIndex = selectedItemIndex
		self.spacing = spacing
		self.items = items
		self.content = content
	}

	// MARK: Body

	public var body: some View {
		if items.isNotEmpty {
			GeometryReader { geometry in
				TabView(selection: Binding(
					get: { items[clamp(selectedItemIndex, to: ClosedRange(items.indices))].id },
					set: { selectedItemIndex = items.firstIndex(of: items[id: $0]!).unwrapped(or: .zero) }
				)) {
					ForEach(items) { item in
						content(item, CGSize(
							width: geometry.size.width - 2*spacing,
							height: geometry.size.height
						))
						.padding(.horizontal, spacing)
						.contentShape(Rectangle())
						.tag(item.id)
					}
				}
				.tabViewStyle(PageTabViewStyle(indexDisplayMode: .automatic))
			}
		} else {
			Color.clear
		}
	}

}

// MARK: - Preview

#if DEBUG
private typealias IdentifiableString = String

extension IdentifiableString: Identifiable {

	public var id: String {
		return self
	}

}

struct HomeContentView_Preview: PreviewProvider {

	private struct ContentView: View {

		@State private var selectedItemIndex: Int = 0

		@State private var items: IdentifiedArrayOf<IdentifiableString> = [
			"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K"
		]

		var body: some View {
			CarouselView(
				items: items,
				selectedItemIndex: $selectedItemIndex,
				spacing: 20
			) { letter, _ in
				Rectangle()
					.fill(.indigo)
					.overlay {
						Text("\(letter)")
							.foregroundColor(.white)
							.font(.system(size: 20, weight: .bold))
							.multilineTextAlignment(.center)
					}
					.cornerRadius(12)
					.background {
						Color.gray.opacity(0.2)
					}
			}
			.background {
				Color.gray
			}
			.frame(height: 300)
//			.onReceive(Timer.publish(every: 2, on: RunLoop.main, in: .default).autoconnect()) { _ in
//				items.remove(at: selectedItemIndex)
//			}
		}

	}

	static var previews: some View {
		ContentView()
	}

}
#endif
