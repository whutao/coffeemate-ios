//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ActivityIndicatorView
import Design
import Extensions
import SwiftUI

// MARK: - View

public struct LoadingSpinnerView: View {

	// MARK: Private properties

	private let type: ActivityIndicatorView.IndicatorType

	private let tintColor: SwiftUI.Color

	// MARK: Init

	public init(
		_ type: ActivityIndicatorView.IndicatorType = .arcs(count: 3, lineWidth: 2.1),
		tintColor: SwiftUI.Color = Asset.Color.comegranate.swiftUIColor
	) {
		self.type = type
		self.tintColor = tintColor
	}

	// MARK: Body

	public var body: some View {
		ActivityIndicatorView(
			isVisible: .constant(true),
			type: type
		)
		.tint(tintColor)
		.foregroundColor(tintColor)
	}

}

// MARK: - Previews

#if DEBUG
struct LoadingSpinnerView_Preview: PreviewProvider {

	static var previews: some View {
		VStack {
			Group {
				LoadingSpinnerView()
					.frame(size: .square(20))

				LoadingSpinnerView()
					.frame(size: .square(60))

				LoadingSpinnerView()
					.frame(size: .square(200))
			}
			.padding()
		}
	}

}
#endif
