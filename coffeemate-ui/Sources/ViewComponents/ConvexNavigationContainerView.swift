//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import Extensions
import SwiftUI

// MARK: - View

public struct ConvexNavigationContainerView<
	NavigationBarContent: View,
	Content: View
>: View {

	// MARK: Exposed typealiases

	public typealias NavigationBarContentBuilder = ConvexNavigationBarView<NavigationBarContent>.ContentBuilder

	// MARK: Private properties

	private let navigationBarContent: NavigationBarContentBuilder

	private let content: () -> Content

	// MARK: Init

	public init(
		@ViewBuilder navigationBarContent: @escaping NavigationBarContentBuilder,
		@ViewBuilder content: @escaping () -> Content
	) {
		self.navigationBarContent = navigationBarContent
		self.content = content
	}

	// MARK: Body

	public var body: some View {
		ZStack(alignment: .top) {
			ConvexNavigationBarView(content: navigationBarContent)
				.zIndex(1)

			content()
				.adaptivePadding(.top, 60)
				.frame(maxWidth: .infinity, maxHeight: .infinity)
				.zIndex(0)
		}
		.toolbar(.hidden, for: .navigationBar)
	}

}

// MARK: - Previews

#if DEBUG
struct ConvexNavigationContainerView_Preview: PreviewProvider {

	static var previews: some View {
		ForEachDevice([.iPhoneSE3rdGeneration, .iPhone14]) {
			ConvexNavigationContainerView { _ in
				Color.black
			} content: {
				Color.red
			}
		}
	}

}
#endif
