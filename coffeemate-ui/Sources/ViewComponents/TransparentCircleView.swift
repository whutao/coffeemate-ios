//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import Extensions
import SwiftUI

// MARK: - View

public struct TransparentCircleView: View {

	// MARK: Private properties

	private let image: SwiftUI.Image

	private let color: SwiftUI.Color

	// MARK: Init

	public init(image: SwiftUI.Image, color: SwiftUI.Color) {
		self.image = image
		self.color = color
	}

	// MARK: Body

	public var body: some View {
		GeometryReader { geometry in
			ZStack(alignment: .center) {
				color
					.opacity(0.3)

				image
					.resizable()
					.aspectRatio(contentMode: .fit)
					.foregroundColor(color)
					.frame(
						width: (0.48) * geometry.size.width,
						height: (0.48) * geometry.size.height,
						alignment: .center
					)
			}
			.clipShape(Capsule())
			.overlay {
				Capsule()
					.adaptiveStroke(color, lineWidth: 0.8)
			}
		}
	}

}

// MARK: - Previews

#if DEBUG
struct TransparentCircleView_Preview: PreviewProvider {

	static var previews: some View {
		VStack {
			Group {
				TransparentCircleView(
					image: Image(systemName: "paperplane"),
					color: .yellow
				)
				.frame(size: .square(30))

				TransparentCircleView(
					image: Image(systemName: "paperplane"),
					color: .red
				)
				.frame(size: .square(60))

				TransparentCircleView(
					image: Image(systemName: "paperplane"),
					color: .green
				)
				.frame(size: .square(90))
			}
			.padding()
		}
	}

}
#endif
