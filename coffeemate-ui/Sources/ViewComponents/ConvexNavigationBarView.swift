//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import Extensions
import SwiftUI

// MARK: - View

public struct ConvexNavigationBarView<Content: View>: View {

	// MARK: Typealiases

	public typealias ContentBuilder = (_ size: CGSize) -> Content

	// MARK: Private properties

	private let content: ContentBuilder

	// MARK: Init

	public init(@ViewBuilder content: @escaping ContentBuilder) {
		self.content = content
	}

	// MARK: Body

	public var body: some View {
		GeometryReader { containerGeometry in
			ZStack {
				ZStack {
					Asset.Color.black.swiftUIColor
						.padding(.top, -containerGeometry.safeAreaInsets.top)
						.padding(.bottom, containerGeometry.safeAreaInsets.top)

					Asset.Color.black.swiftUIColor
						.overlay {
							LinearGradient(
								stops: [
									.init(color: Color.clear, location: 0),
									.init(color: Color.clear, location: 0.1),
									.init(color: Asset.Color.alabaster.swiftUIColor, location: 1.0)
								],
								startPoint: .top,
								endPoint: .bottom
							)
							.opacity(0.12)
						}
						.cornerRadius(20, corners: [.bottomLeft, .bottomRight])
				}

				HStack(spacing: .zero) {
					GeometryReader { contentGeometry in
						content(contentGeometry.size)
					}
				}
				.adaptivePadding(.horizontal, 18)
				.adaptivePadding(.top, 16)
				.adaptivePadding(.bottom, 16)
			}
		}
		.adaptiveFrame(height: 80)
	}

}

// MARK: - Previews

#if DEBUG
struct ConcaveNavigationBarView_Preview: PreviewProvider {

	static var previews: some View {
		ForEachDevice([.iPhoneSE3rdGeneration, .iPhone14]) {
			VStack {
				ConvexNavigationBarView { size in
					Color.red.overlay {
						Text(size.debugDescription)
							.foregroundColor(.white)
					}
				}

				Spacer()
			}
		}
	}

}
#endif
