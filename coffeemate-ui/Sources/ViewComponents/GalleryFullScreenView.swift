//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import Extensions
import IdentifiedCollections
import Kingfisher
import Models
import SwiftUI

// MARK: - View

public struct GalleryFullScreenView: View {

	// MARK: Private properties

	@GestureState private var dragOffset: CGSize = .zero

	private var backgroundOpacity: Double {
		let dragHeight = dragOffset.height
		let screenHeight = (0.5) * UIScreen.main.bounds.height
		let progress = abs(dragHeight) / screenHeight
		return clamp(Double(1 - progress), to: 0...1)
	}

	@Binding private var selectedEntry: GalleryEntry

	@State private var selectedEntryScale: Double = 1

	private let galleryEntries: IdentifiedArrayOf<GalleryEntry>

	private let closeAction: () -> Void

	// MARK: Init

	public init(
		selectedEntry: Binding<GalleryEntry>,
		galleryEntries: IdentifiedArrayOf<GalleryEntry>,
		closeAction: @escaping () -> Void = { }
	) {
		self._selectedEntry = selectedEntry
		self.galleryEntries = galleryEntries
		self.closeAction = closeAction
	}

	public init(
		selectedEntry: Binding<GalleryEntry?>,
		galleryEntries: IdentifiedArrayOf<GalleryEntry>,
		closeAction: @escaping () -> Void = { }
	) {
		self.init(
			selectedEntry: Binding(
				get: { selectedEntry.wrappedValue.unwrapped(or: galleryEntries.first!) },
				set: { selectedEntry.wrappedValue = $0 }
			),
			galleryEntries: galleryEntries,
			closeAction: closeAction
		)
	}

	// MARK: Body

	public var body: some View {
		ZStack {
			Asset.Color.black.swiftUIColor
				.opacity(backgroundOpacity)
				.ignoresSafeArea()

			TabView(selection: $selectedEntry) {
				ForEach(galleryEntries) { entry in
					ImageView(
						imageRepresentation: entry.image,
						ifInterpretedAsKFImage: { kfImage in
							kfImage
								.resizable()
								.cacheOriginalImage()
								.downsampling(size: .square(1000))
						},
						ifInterpretedAsImage: { image in
							image
								.resizable()
						}
					)
					.aspectRatio(contentMode: .fit)
					.tag(entry)
					.offset(y: dragOffset.height)
					.scaleEffect(
						amount: selectedEntry == entry ? selectedEntryScale : 1
					)
					.onTapGesture(count: 2) {
						closeAction()
					}
					.animation(.interactiveSpring(), value: dragOffset.height)
				}
			}
			.tabViewStyle(galleryEntries.count > 1 ? PageTabViewStyle(indexDisplayMode: .always) : .init())
			.overlay(alignment: .topTrailing) {
				Button {
					closeAction()
				} label: {
					Asset.Image.Icon.cross.swiftUIImage
						.resizable()
						.aspectRatio(contentMode: .fit)
						.foregroundColor(Asset.Color.monteCarlo.swiftUIColor)
						.adaptiveFrame(size: .square(16))
						.adaptivePadding(10)
						.clipShape(Capsule())
						.contentShape(Rectangle())
				}
				.scaleOnPress()
				.offset(x: -12, y: 12)
			}
		}
//		.gesture(DragGesture()
//			.updating($dragOffset) { value, output, _ in
//				withAnimation(.interactiveSpring()) {
//					output = value.translation
//				}
//			}
//			.onEnded { value in
//				withAnimation {
//					if abs(value.translation.height) > 200 {
//						closeAction()
//					}
//				}
//			}
//		)
	}

}

// MARK: - Previews

#if DEBUG
import Mocking

struct GalleryFullScreenView_Preview: PreviewProvider {

	private struct ContentView: View {

		@State private var isPresented: Bool = false

		@State private var selectedEntry: GalleryEntry = .init(
			entryId: 0,
			image: .url(AvatarURL.stalin),
			isOwn: true
		)

		private let entries: IdentifiedArrayOf<GalleryEntry> = [
			.init(
				entryId: 0,
				image: .url(AvatarURL.stalin),
				isOwn: true
			),
			.init(
				entryId: 1,
				image: .url(AvatarURL.ladybug),
				isOwn: true
			),
			.init(
				entryId: 2,
				image: .url(AvatarURL.stalin),
				isOwn: true
			)
		]

		var body: some View {
			ZStack {
				Color.red.ignoresSafeArea()

				Button("Show") {
					withAnimation {
						isPresented = true
					}
				}
				.foregroundColor(.white)
			}
			.overlay {
				if isPresented {
					GalleryFullScreenView(
						selectedEntry: $selectedEntry,
						galleryEntries: entries,
						closeAction: {
							isPresented = false
						}
					)
					.transition(.move(edge: .bottom))
				}
			}
		}

	}

	static var previews: some View {
		ContentView()
	}

}
#endif
