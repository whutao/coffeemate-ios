//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import Extensions
import SwiftUI

// MARK: - View

public struct BirthDatePickerView: View {

	// MARK: Private properties

	@State private var isPresentingDatePicker: Bool = false

	// Initial height is decided to be 300 through the experiments.
	@State private var sheetHeight: CGFloat = 300

	@Binding private var selectedDate: Date?

	private let placeholder: String

	private let doneButtonTitle: String

	private let dateFormatter: DateFormatter

	private let dateRange: ClosedRange<Date>

	// MARK: Init

	public init(
		selectedDate: Binding<Date?>,
		dateRange: ClosedRange<Date>,
		dateDisplayFormat: String,
		placeholder: String,
		doneButtonTitle: String
	) {
		self._selectedDate = selectedDate
		self.placeholder = placeholder
		self.doneButtonTitle = doneButtonTitle
		self.dateRange = dateRange
		self.dateFormatter = {
			let formatter = DateFormatter()
			formatter.dateFormat = dateDisplayFormat
			return formatter
		}()
	}

	// MARK: Body

	public var body: some View {
		Group {
			if let selectedDate {
				Text(dateFormatter.string(from: selectedDate))
					.foregroundColor(Asset.Color.keppel.swiftUIColor)
					.adaptiveFont(.primary, .medium, ofSize: 14)
					.frame(maxWidth: .infinity, maxHeight: .infinity)
					.onTapGesture {
						isPresentingDatePicker = true
					}
					.transition(.scale)
			} else {
				Button {
					isPresentingDatePicker.toggle()
				} label: {
					HStack(spacing: .zero) {
						Spacer()
							.adaptiveFrame(size: .square(20))

						Spacer()

						Text(placeholder)
							.lineLimit(1)
							.adaptiveFont(.primary, .medium, ofSize: 14)

						Spacer()

						Asset.Image.Icon.calendar.swiftUIImage
							.resizable()
							.aspectRatio(contentMode: .fit)
							.adaptiveFrame(size: .square(20))
					}
					.foregroundColor(Asset.Color.alabaster.swiftUIColor)
					.adaptivePadding(.horizontal, 20)
					.adaptivePadding(.vertical, 10)
					.background {
						Asset.Color.monteCarlo.swiftUIColor
					}
					.clipShape(RoundedRectangle(cornerRadius: 10))
				}
				.scaleOnPress()
			}
		}
		.adaptiveFrame(height: 50)
		.transition(.scale.animation(.spring()))
		.sheet(isPresented: $isPresentingDatePicker) {
			VStack(alignment: .center, spacing: .zero) {
				HStack(spacing: .zero) {
					Spacer()

					Button {
						isPresentingDatePicker = false
					} label: {
						Text(doneButtonTitle)
							.lineLimit(1)
							.adaptiveFont(.primary, .medium, ofSize: 10)
							.foregroundColor(Asset.Color.black.swiftUIColor)
							.adaptivePadding(.horizontal, 8)
							.adaptivePadding(.vertical, 6)
							.background {
								Asset.Color.monteCarlo.swiftUIColor
							}
							.clipShape(RoundedRectangle(cornerRadius: 6))
					}
					.scaleOnPress()
				}

				Spacer()
					.adaptiveFrame(height: 6)

				HStack(alignment: .center, spacing: .zero) {
					Spacer()

					DatePicker(
						selection: Binding(
							get: { selectedDate.unwrapped(or: dateRange.upperBound) },
							set: { selectedDate = $0 }
						),
						in: dateRange,
						displayedComponents: .date,
						label: EmptyView.init
					)
					.datePickerStyle(.wheel)

					Spacer()
				}
			}
			.padding()
			.clipShape(RoundedRectangle(cornerRadius: 8))
			.readSize { size in
				sheetHeight = size.height
			}
			.presentationDetents([.height(sheetHeight)])
			.transition(.scale.animation(.spring()))
		}
	}

}

// MARK: - Previews

#if DEBUG
struct DatePickerView_Preview: PreviewProvider {

	private struct ContentView: View {

		@State private var selectedDate: Date? = nil

		var body: some View {
			ZStack {
				Color.black.ignoresSafeArea()

				VStack {
					Spacer()

					BirthDatePickerView(
						selectedDate: $selectedDate,
						dateRange: Date().addingTimeInterval(-300000)...Date(),
						dateDisplayFormat: "dd.MM.yyyy",
						placeholder: "Your bithdate day",
						doneButtonTitle: "DONE"
					)
					.padding(.horizontal, 30)

					Spacer()
				}
			}
		}

	}

	static var previews: some View {
		ContentView()
	}

}
#endif
