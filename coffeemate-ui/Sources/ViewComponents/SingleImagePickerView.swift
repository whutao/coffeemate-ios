//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import Localizable
import MediaPicker
import SwiftUI

// MARK: - View

public struct SingleImagePickerView: View {

	// MARK: Private properties

	@Binding private var isPresented: Bool

	private let selectAction: (Data) -> Void

	// MARK: Init

	public init(
		isPresented: Binding<Bool>,
		selectAction: @escaping (Data) -> Void
	) {
		self._isPresented = isPresented
		self.selectAction = selectAction
	}

	// MARK: Body

	public var body: some View {
		MediaPickerView(isPresented: $isPresented) { selectedMedia in
			Task {
				if let data = await selectedMedia?.data {
					selectAction(data)
				}
			}
		} albumEmptyBuilder: {
			ZStack {
				Color.clear.scaledToFill()

				Text(L10n.mediaPickerPhotoLibraryEmptyPlaceholder)
					.lineLimit(1)
					.adaptiveFont(.primary, .medium, ofSize: 14)
					.foregroundColor(Asset.Color.alabaster.swiftUIColor)
					.adaptivePadding(.horizontal, 60)
			}
		} albumLoadingBuilder: {
			AlbumLoadingView()
		} headerCancelBuilder: { cancelAction in
			Button {
				cancelAction()
			} label: {
				Text(L10n.mediaPickerPhotoLibraryCancel)
					.lineLimit(1)
					.adaptiveFont(.primary, .medium, ofSize: 14)
					.foregroundColor(Asset.Color.keppel.swiftUIColor)
			}
			.scaleOnPress()
		} footerDoneBuilder: { doneAction in
			Button {
				doneAction()
			} label: {
				Text(L10n.mediaPickerPhotoLibraryDone)
					.lineLimit(1)
					.adaptiveFont(.primary, .medium, ofSize: 14)
					.foregroundColor(Asset.Color.keppel.swiftUIColor)
			}
			.scaleOnPress()
		} footerDescriptionBuilder: { temporarlySelectedMedias in
			Text(L10n.mediaPickerPhotoLibrarySelectedMediasDescription(temporarlySelectedMedias.count))
				.lineLimit(1)
				.adaptiveFont(.primary, .regular, ofSize: 14)
				.foregroundColor(Asset.Color.keppel.swiftUIColor)
		} permissionRequiredBuilder: {
			GeometryReader { geometry in
				ZStack {
					Color.clear

					VStack(alignment: .center, spacing: .zero) {
						Text(L10n.mediaPickerPhotoLibraryPermissionRequiredDescription)
							.lineLimit(4)
							.multilineTextAlignment(.center)
							.adaptiveFont(.primary, .regular, ofSize: 14)
							.foregroundColor(Asset.Color.alabaster.swiftUIColor)
							.adaptivePadding(.horizontal, 60)

						Spacer()
							.adaptiveFrame(height: 30)

						OpaqueButton(
							title: L10n.mediaPickerPhotoLibraryPermissionRequiredButton,
							foregroundColor: Asset.Color.alabaster.swiftUIColor,
							backgroundColor: Asset.Color.keppel.swiftUIColor
						) {
							let openSettingsUrl = URL(string: UIApplication.openSettingsURLString).forceUnwrapped(
								because: "Application must be able to open settings URL."
							)
							UIApplication.shared.open(openSettingsUrl)
						}
						.adaptiveFrame(height: 50)
						.adaptivePadding(.horizontal, 90)
					}
					.frame(size: geometry.size)
				}
			}
		}
	}

}

// MARK: - Loading view

extension SingleImagePickerView {

	fileprivate struct AlbumLoadingView: View {

		// MARK: Private properties

		@Dependency(\.mainQueue) private var mainQueue

		@State private var isShowingLoadingIndicator: Bool = false

		// MARK: Init

		init() {
			self.isShowingLoadingIndicator = false
		}

		// MARK: Body

		var body: some View {
			ZStack {
				Color.clear.scaledToFill()

				Group {
					if isShowingLoadingIndicator {
						LoadingSpinnerView(tintColor: Asset.Color.alabaster.swiftUIColor)
							.adaptiveFrame(size: .square(40))
					}
				}
				.transition(.scale.animation(.easeInOut))
			}
			.onAppear {
				mainQueue.schedule(after: .init(.now() + 1.2)) {
					isShowingLoadingIndicator = true
				}
			}
		}

	}

}

// MARK: - View extension

extension View {

	@ViewBuilder public func singleImagePickerSheet(
		isPresented: Binding<Bool>,
		selectAction: @escaping (Data) -> Void
	) -> some View {
		sheet(isPresented: isPresented) {
			SingleImagePickerView(
				isPresented: isPresented,
				selectAction: selectAction
			)
			.presentationDetents([.fraction(0.5), .large])
			.presentationDragIndicator(.hidden)
		}
	}

}
