//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

// Reference: https://github.com/smyshlaevalex/FlowStackLayout
#warning("Find out how to make horizontal version.")

import SwiftUI

// MARK: - Layout

struct WrappedVStackLayout: Layout {

	// MARK: Layout result

	struct LayoutResult {
		let containerSize: CGSize
		let subviewPositions: [CGPoint]
	}

	// MARK: Private properteis

	private let alignment: Alignment

	private let horizontalSpacing: Double?

	private let verticalSpacing: Double?

	// MARK: Init

	init(alignment: Alignment = .center, horizontalSpacing: Double? = nil, verticalSpacing: Double? = nil) {
		self.alignment = alignment
		self.horizontalSpacing = horizontalSpacing
		self.verticalSpacing = verticalSpacing
	}

	// MARK: Exposed methods

	func makeCache(subviews: Subviews) -> LayoutResult {
		LayoutResult(containerSize: .zero, subviewPositions: [])
	}

	func updateCache(_ cache: inout LayoutResult, subviews: Subviews) {
		cache = LayoutResult(
			containerSize: .zero,
			subviewPositions: []
		)
	}

	func sizeThatFits(
		proposal: ProposedViewSize,
		subviews: Subviews,
		cache: inout LayoutResult
	) -> CGSize {
		cache = layout(
			maxWidth: proposal.replacingUnspecifiedDimensions().width,
			subviews: subviews
		)
		return cache.containerSize
	}

	func placeSubviews(
		in bounds: CGRect,
		proposal: ProposedViewSize,
		subviews: Subviews,
		cache: inout LayoutResult
	) {
		for (subview, subviewPosition) in zip(subviews, cache.subviewPositions) {
			subview.place(
				at: CGPoint(
					x: bounds.minX + subviewPosition.x,
					y: bounds.minY + subviewPosition.y
				),
				proposal: proposal
			)
		}
	}

	// MARK: Private methods

	private func layout(maxWidth: CGFloat, subviews: Subviews) -> LayoutResult {
		var containerSize: CGSize = .zero
		var currentSubviewPosition: CGPoint = .zero
		var previousHorizontalViewSpacing: ViewSpacing?
		var verticalViewSpacing = ViewSpacing()
		var subviewFrameRows: [[CGRect]] = []
		var currentSubviewFrameRow: [CGRect] = []

		for subview in subviews {
			let size = subview.sizeThatFits(.unspecified)
			let horizontalSpacing = previousHorizontalViewSpacing.flatMap {
				self.horizontalSpacing ?? subview.spacing.distance(to: $0, along: .horizontal)
			} ?? 0
			previousHorizontalViewSpacing = subview.spacing

			let subviewWidthAndSpacing = size.width + horizontalSpacing

			let newContainerWidth = currentSubviewPosition.x + subviewWidthAndSpacing
			if newContainerWidth <= maxWidth {
				currentSubviewPosition.x += horizontalSpacing

				currentSubviewFrameRow.append(CGRect(origin: currentSubviewPosition, size: size))

				containerSize.width = max(containerSize.width, newContainerWidth)
				currentSubviewPosition.x += size.width

				verticalViewSpacing.formUnion(subview.spacing)

				let newContainerHeight = currentSubviewPosition.y + size.height
				containerSize.height = max(containerSize.height, newContainerHeight)
			} else {
				currentSubviewPosition.x = 0
				currentSubviewPosition.y = containerSize.height

				let verticalSpacing = self.verticalSpacing ?? subview.spacing.distance(to: verticalViewSpacing, along: .vertical)
				verticalViewSpacing = ViewSpacing()

				currentSubviewPosition.y += verticalSpacing

				subviewFrameRows.append(currentSubviewFrameRow)
				currentSubviewFrameRow = []
				currentSubviewFrameRow.append(CGRect(origin: currentSubviewPosition, size: size))

				let newContainerHeight = currentSubviewPosition.y + size.height
				containerSize.height = max(containerSize.height, newContainerHeight)
				containerSize.width = max(containerSize.width, size.width)

				currentSubviewPosition.x += size.width
			}
		}

		subviewFrameRows.append(currentSubviewFrameRow)
		currentSubviewFrameRow = []

		subviewFrameRows = subviewFrameRows.map { rowFrames in
			let trailingSpacing = containerSize.width - (rowFrames.last?.maxX ?? 0)
			let rowHeight = rowFrames.max(by: { $0.height < $1.height })?.height ?? 10
			let xOffset: CGFloat
			switch alignment.horizontal {
			case .leading:
				xOffset = 0
			case .center:
				xOffset = trailingSpacing / 2
			case .trailing:
				xOffset = trailingSpacing
			default:
				xOffset = 0
			}

			return rowFrames.map { frame in
				let yOffset: CGFloat
				switch alignment.vertical {
				case .top:
					yOffset = 0
				case .center:
					yOffset = (rowHeight - frame.height) / 2
				case .bottom:
					yOffset = rowHeight - frame.height
				default:
					yOffset = 0
				}
				return frame.offsetBy(dx: xOffset, dy: yOffset)
			}
		}

		let subviewPositions = subviewFrameRows.flatMap { $0 } .map(\.origin)

		return LayoutResult(
			containerSize: containerSize,
			subviewPositions: subviewPositions
		)
	}

}

// MARK: - Stack

public struct WrappedVStack<Content: View>: View {

	// MARK: Private properties

	private let alignment: Alignment

	private let horizontalSpacing: Double?

	private let verticalSpacing: Double?

	private let content: () -> Content

	// MARK: Init

	public init(
		alignment: Alignment = .center,
		horizontalSpacing: Double? = nil,
		verticalSpacing: Double? = nil,
		@ViewBuilder content: @escaping () -> Content
	) {
		self.alignment = alignment
		self.horizontalSpacing = horizontalSpacing
		self.verticalSpacing = verticalSpacing
		self.content = content
	}

	public var body: some View {
		WrappedVStackLayout(
			alignment: alignment,
			horizontalSpacing: horizontalSpacing,
			verticalSpacing: verticalSpacing
		) {
			content()
		}
	}

}

// MARK: - Previews

#if DEBUG
import Mocking

struct WrappedVStack_Preview: PreviewProvider {

	private static let words: [String] = Range(0...100).map({ _ in Lorem.word })

	static var previews: some View {
		ZStack {
			Color.black.ignoresSafeArea()

			ScrollView(.vertical) {
				WrappedVStack {
					ForEach(words, id: \.self) { word in
						Text(word)
							.padding(12)
							.background {
								Color.white
							}
							.clipShape(Capsule())
					}
				}
			}
			.frame(height: 400)
		}
	}

}
#endif
