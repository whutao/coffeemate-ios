//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import Extensions
import ImageCropper
import Localizable
import SwiftUI

// MARK: - View

public struct SingleImageCropperView: View {

	// MARK: Private properties

	@Environment(\.dismiss) private var dismiss

	private let originalImage: UIImage

	private let completion: (UIImage) -> Void

	// MARK: Init

	public init(
		originalImage: UIImage,
		completion: @escaping (UIImage) -> Void
	) {
		self.originalImage = originalImage
		self.completion = completion
	}

	// MARK: Body

	public var body: some View {
		ImageCropperView(
			originalImage: originalImage,
			cropKind: .square,
			maxZoom: 5,
			gridLineColor: Asset.Color.alabaster.swiftUIColor.opacity(0.5),
			gridLineWidth: 1
		) {
			Asset.Color.black.swiftUIColor
		} navigationBarContent: { _, dismissClosure, completionClosure in
			HStack(alignment: .center, spacing: .zero) {
				Button {
					dismissClosure()
				} label: {
					Text(L10n.imageCropperNavigationBarCancel)
						.lineLimit(1)
						.adaptiveFont(.primary, .medium, ofSize: 14)
						.foregroundColor(Asset.Color.keppel.swiftUIColor)
						.padding(.vertical, 6)
						.padding(.trailing, 6)
						.contentShape(Rectangle())
				}
				.scaleOnPress()

				Spacer()

				Button {
					completionClosure()
				} label: {
					Text(L10n.imageCropperNavigationBarDone)
						.lineLimit(1)
						.adaptiveFont(.primary, .medium, ofSize: 14)
						.foregroundColor(Asset.Color.keppel.swiftUIColor)
						.padding(.vertical, 6)
						.padding(.leading, 6)
						.contentShape(Rectangle())
				}
				.scaleOnPress()
			}
			.padding(.horizontal, 24)
			.padding(.vertical, 12)
		} dismissAction: {
			dismiss()
		} completionAction: { croppedUIImage in
			completion(croppedUIImage)
			dismiss()
		}

	}

}

// MARK: - View extension

extension View {

	@ViewBuilder public func singleImageCropperFullScreenCover(
		isPresented: Binding<Bool>,
		originalImage: UIImage,
		completion: @escaping (UIImage) -> Void
	) -> some View {
		fullScreenCover(isPresented: isPresented) {
			SingleImageCropperView(
				originalImage: originalImage,
				completion: completion
			)
		}
	}

}

// MARK: - Previews

#if DEBUG
struct SingleImageCropperView_Preview: PreviewProvider {

	private struct ContentView: View {

		private let image: UIImage = Asset.Image.SignIn.background.image

		@State private var isPresented: Bool = false

		@State private var croppedImage: UIImage? = nil

		var body: some View {
			VStack {
				Group {
					if let croppedImage {
						Image(uiImage: croppedImage)
							.resizable()
							.aspectRatio(contentMode: .fit)
					}
				}
				.frame(size: .square(300))

				Button {
					isPresented = true
				} label: {
					Text("Present")
				}
			}
			.fullScreenCover(isPresented: $isPresented) {
				SingleImageCropperView(originalImage: image) { uiImage in
					croppedImage = uiImage
				}
			}
		}

	}

	static var previews: some View {
		ContentView()
	}

}
#endif
