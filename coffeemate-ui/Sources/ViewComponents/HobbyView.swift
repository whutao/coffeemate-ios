//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import Extensions
import Kingfisher
import Mocking
import Models
import SwiftUI

// MARK: - View

public struct HobbyView: View {

	// MARK: Private properties

	private let hobby: Hobby

	private let tintColor: SwiftUI.Color

	private let isAccented: Bool

	private let hasTransparentBackground: Bool

	// MARK: Init

	public init(
		hobby: Hobby,
		tintColor: SwiftUI.Color,
		isAccented: Bool,
		hasTransparentBackground: Bool = false
	) {
		self.hobby = hobby
		self.tintColor = tintColor
		self.isAccented = isAccented
		self.hasTransparentBackground = hasTransparentBackground
	}

	// MARK: Body

	public var body: some View {
		HStack(spacing: .zero) {
			Text(hobby.title)
				.lineLimit(1)
				.adaptiveFont(.primary, .regular, ofSize: 12)

			Spacer()
				.adaptiveFrame(width: 6)

			KFImage(hobby.imageUrl)
				.cacheOriginalImage()
				.setProcessors([SVGImageProcessor()])
				.resizable()
				.fade(duration: 0.33)
				.renderingMode(.template)
				.aspectRatio(contentMode: .fit)
				.adaptiveFrame(size: .square(14))
		}
		.foregroundColor(tintColor)
		.adaptivePadding(.vertical, 6)
		.adaptivePadding(.leading, 10)
		.adaptivePadding(.trailing, 8)
		.background {
			if hasTransparentBackground {
				tintColor.opacity(0.3)
			}
		}
		.clipShape(Capsule())
		.overlay {
			Capsule()
				.adaptiveStroke(tintColor, lineWidth: isAccented ? 1.8 : 0.8)
		}
		.padding(2)
		.animation(.easeInOut, value: isAccented)
	}

}

// MARK: - Previews

#if DEBUG
struct HobbyView_Preview: PreviewProvider {

	static var previews: some View {
		ZStack {
			Color.black.ignoresSafeArea()

			VStack(alignment: .center, spacing: 60) {
				Group {
					HobbyView(
						hobby: .init(
							hobbyId: 1,
							title: "Party",
							imageUrl: URL(string: "https://api.coffeemates.space:443/images/hobbies/party.svg")!,
							isPrivileged: false
						),
						tintColor: Color.black,
						isAccented: true
					)

					HobbyView(
						hobby: .init(
							hobbyId: 2,
							title: "Bars",
							imageUrl: URL(string: "https://api.coffeemates.space:443/images/hobbies/coffee.svg")!,
							isPrivileged: true
						),
						tintColor: Color.black,
						isAccented: false
					)
				}
				.foregroundColor(Color.white)
				.background(Color.white)
				.scaleEffect(amount: 2)
			}
		}
	}

}
#endif
