//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import Extensions
import IdentifiedCollections
import Models
import SwiftUI

// MARK: - View

public struct HobbyPickerView: View {

	// MARK: Private properties

	private let allHobbies: IdentifiedArrayOf<Hobby>

	private let selectionLimit: Int?

	@Binding private var selectedHobbies: IdentifiedArrayOf<Hobby>

	// MARK: Init

	public init(
		allHobbies: IdentifiedArrayOf<Hobby>,
		selectedHobbies: Binding<IdentifiedArrayOf<Hobby>>,
		selectionLimit: Int? = nil
	) {
		self.allHobbies = allHobbies
		self.selectionLimit = selectionLimit
		self._selectedHobbies = selectedHobbies
	}

	// MARK: Body

	public var body: some View {
		ScrollView {
			WrappedVStack(
				alignment: .topLeading,
				horizontalSpacing: 12,
				verticalSpacing: 8
			) {
				ForEach(allHobbies) { hobby in
					HobbyView(
						hobby: hobby,
						tintColor: selectedHobbies.contains(hobby) ? Asset.Color.paradiso.swiftUIColor : Asset.Color.gunsmoke.swiftUIColor.opacity(0.44),
						isAccented: selectedHobbies.contains(hobby)
					)
					.animation(.spring(), value: selectedHobbies)
					.onTapGesture {
						if selectedHobbies.contains(hobby) {
							selectedHobbies.remove(hobby)
						} else if let selectionLimit {
							if selectionLimit > selectedHobbies.count {
								selectedHobbies.append(hobby)
							}
						} else {
							selectedHobbies.append(hobby)
						}
					}
				}
			}
		}
		.scrollIndicators(.never, axes: .vertical)
		.scrollIndicators(.never, axes: .horizontal)
	}

}

// MARK: - Previews

#if DEBUG
struct HobbyPickerView_Preview: PreviewProvider {

	private struct ContentView: View {

		@State private var selectedHobbies: IdentifiedArrayOf<Hobby> = []

		var body: some View {
			HobbyPickerView(
				allHobbies: IdentifiedArray(uniqueElements: Hobby.allRandomMocked()),
				selectedHobbies: $selectedHobbies,
				selectionLimit: 2
			)
			.padding(.vertical, 100)
		}

	}

	static var previews: some View {
		ContentView()
	}

}
#endif
