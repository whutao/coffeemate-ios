//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import Extensions
import IdentifiedCollections
import Kingfisher
import Models
import SwiftUI

// MARK: - View

public struct GalleryCarouselView: View {

	// MARK: Private properties

	@Binding private var selectedEntry: GalleryEntry?

	private let isEntryDeleteButtonVisisble: Bool

	private let entries: IdentifiedArrayOf<GalleryEntry>

	private let entryTapAction: (GalleryEntry) -> Void

	private let entryDeleteTapAction: (GalleryEntry) -> Void

	// MARK: Init

	public init(
		entries: IdentifiedArrayOf<GalleryEntry>,
		selectedEntry: Binding<GalleryEntry?>,
		isEntryDeleteButtonVisisble: Bool = false,
		entryTapAction: @escaping (GalleryEntry) -> Void = { _ in },
		entryDeleteTapAction: @escaping (GalleryEntry) -> Void = { _ in }
	) {
		self._selectedEntry = selectedEntry
		self.entries = entries
		self.entryTapAction = entryTapAction
		self.entryDeleteTapAction = entryDeleteTapAction
		self.isEntryDeleteButtonVisisble = isEntryDeleteButtonVisisble
	}

	public init(
		entries: IdentifiedArrayOf<GalleryEntry>,
		selectedEntry: Binding<GalleryEntry>,
		isEntryDeleteButtonVisisble: Bool = false,
		entryTapAction: @escaping (GalleryEntry) -> Void = { _ in },
		entryDeleteTapAction: @escaping (GalleryEntry) -> Void = { _ in }
	) {
		self._selectedEntry = Binding(
			get: {
				selectedEntry.wrappedValue
			},
			set: { newValue in
				selectedEntry.wrappedValue = newValue.unwrapped(or: entries.first.forceUnwrapped(
					because: "Only non-empty gallery entries are allowed."
				))
			}
		)
		self.entries = entries
		self.entryTapAction = entryTapAction
		self.entryDeleteTapAction = entryDeleteTapAction
		self.isEntryDeleteButtonVisisble = isEntryDeleteButtonVisisble
	}

	// MARK: Body

	public var body: some View {
		CarouselView(
			items: entries,
			selectedItemIndex: Binding(
				get: {
					if let selectedEntryId = selectedEntry?.entryId {
						return entries.index(id: selectedEntryId).unwrapped(or: .zero)
					} else {
						return .zero
					}
				},
				set: { selectedEntry = entries[safe: $0] }
			),
			spacing: 20
		) { entry, size in
			ImageView(
				imageRepresentation: entry.image,
				ifInterpretedAsKFImage: { kfImage in
					kfImage
						.resizable()
						.cacheOriginalImage()
						.downsampling(size: .init(
							width: 600,
							height: 600
						))
						.placeholder {
							ZStack {
								Asset.Color.black.swiftUIColor

								LoadingSpinnerView(
									tintColor: Asset.Color.alabaster.swiftUIColor
								)
								.adaptiveFrame(size: .square(60))
							}
						}
				},
				ifInterpretedAsImage: { image in
					image
						.resizable()
				}
			)
			.scaledToFill()
			.frame(size: size)
			.clipShape(RoundedRectangle(cornerRadius: 20))
			.contentShape(RoundedRectangle(cornerRadius: 20))
			.onTapGesture {
				if let selectedEntry, selectedEntry.id == entry.id {
					entryTapAction(entry)
				} else {
					selectedEntry = entry
				}
			}
			.overlay(alignment: .topTrailing) {
				if isEntryDeleteButtonVisisble {
					Button {
						entryDeleteTapAction(entry)
					} label: {
						Asset.Image.Icon.cross.swiftUIImage
							.resizable()
							.aspectRatio(contentMode: .fit)
							.foregroundColor(Asset.Color.black.swiftUIColor)
							.frame(size: .square(14))
							.adaptivePadding(8)
							.background {
								Asset.Color.mercury.swiftUIColor
							}
							.clipShape(Capsule())
					}
					.scaleOnPress()
					.offset(x: -10, y: 10)
				}
			}
		}
		.frame(maxWidth: .infinity)
		.aspectRatio(1, contentMode: .fit)
	}

}

// MARK: - Previews

#if DEBUG
struct GalleryCarouselView_Preview: PreviewProvider {

	private struct ContentView: View {

		private let entries: IdentifiedArrayOf<GalleryEntry>

		@State private var selectedEntry: GalleryEntry

		init() {
			let entries: IdentifiedArrayOf<GalleryEntry> = [
				.randomMockedOwn(),
				.randomMockedOwn(),
				.randomMockedOwn(),
				.randomMockedOwn(),
				.randomMockedOwn(),
				.randomMockedOwn(),
				.randomMockedOwn(),
				.randomMockedOwn(),
				.randomMockedOwn(),
				.randomMockedOwn(),
				.randomMockedOwn(),
				.randomMockedOwn(),
				.randomMockedOwn()
			]
			self.entries = entries
			self._selectedEntry = State(initialValue: entries.first!)
		}

		var body: some View {
			GalleryCarouselView(
				entries: entries,
				selectedEntry: $selectedEntry,
				isEntryDeleteButtonVisisble: true
			)
		}

	}

	static var previews: some View {
		ContentView()
			.frame(height: UIScreen.main.bounds.width)
	}

}
#endif
