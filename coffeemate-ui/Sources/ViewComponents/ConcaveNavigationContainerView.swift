//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import Extensions
import SwiftUI

// MARK: - View

public struct ConcaveNavigationContainerView<
	NavigationBarContent: View,
	Content: View
>: View {

	// MARK: Exposed typealiases

	public typealias NavigationBarContentBuilder = (_ size: CGSize) -> NavigationBarContent

	// MARK: Private properties

	private let navigationBarContent: NavigationBarContentBuilder

	private let content: () -> Content

	// MARK: Init

	public init(
		@ViewBuilder navigationBarContent: @escaping NavigationBarContentBuilder,
		@ViewBuilder content: @escaping () -> Content
	) {
		self.navigationBarContent = navigationBarContent
		self.content = content
	}

	// MARK: Body

	public var body: some View {
		ZStack(alignment: .top) {
			Asset.Color.black.swiftUIColor
				.ignoresSafeArea(edges: .top)
				.zIndex(0)

			GeometryReader { geometry in
				navigationBarContent(geometry.size)
			}
			.adaptivePadding(.horizontal, 18)
			.adaptivePadding(.top, 16)
			.adaptivePadding(.bottom, 16)
			.adaptiveFrame(height: 80)
			.zIndex(2)

			content()
				.cornerRadius(20, corners: [.topLeft, .topRight])
				.adaptivePadding(.top, 80)
				.frame(maxWidth: .infinity, maxHeight: .infinity)
				.zIndex(1)
		}
		.toolbar(.hidden, for: .navigationBar)
	}

}

// MARK: - Previews

#if DEBUG
struct ConcaveNavigationContainerView_Preview: PreviewProvider {

	static var previews: some View {
		ForEachDevice([.iPhoneSE3rdGeneration, .iPhone14]) {
			ConcaveNavigationContainerView { _ in
				Button {
					fatalError()
				} label: {
					Color.red
				}
			} content: {
				ScrollView {
					Color.blue
						.frame(height: 1000)
				}
				.background {
					Color.white
				}
			}

		}
	}

}
#endif
