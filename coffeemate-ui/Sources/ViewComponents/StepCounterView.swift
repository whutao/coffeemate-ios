//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import Extensions
import SwiftUI

// MARK: - View

public struct StepCounterView: View {

	// MARK: Private properties

	private let currentStep: Int

	private let stepCount: Int

	// MARK: Init

	public init(
		currentStep: Int,
		stepCount: Int
	) {
		self.currentStep = clamp(currentStep, to: 0...stepCount-1)
		self.stepCount = stepCount
	}

	// MARK: Body

	public var body: some View {
		VStack(spacing: .zero) {
			HStack(spacing: .zero) {
				ForEach((0..<stepCount).map({ $0 }), id: \.self) { step in
					Circle()
						.foregroundColor(color(forStep: step))
						.adaptiveFrame(size: .square(8))
						.adaptivePadding(.horizontal, 6)
						.animation(.easeOut, value: currentStep)
				}
			}
		}
	}

	// MARK: Private methods

	private func color(forStep step: Int) -> SwiftUI.Color {
		if currentStep == step {
			return Asset.Color.monteCarlo.swiftUIColor
		} else {
			return Asset.Color.mercury.swiftUIColor
		}
	}

}

// MARK: - Previews

#if DEBUG
struct StepCounterView_Preview: PreviewProvider {

	static var previews: some View {
		StepCounterView(
			currentStep: 2,
			stepCount: 4
		)
	}

}
#endif
