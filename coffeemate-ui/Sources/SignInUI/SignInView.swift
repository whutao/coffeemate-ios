//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import SignIn
import SwiftUI
import ViewComponents

// MARK: - View

public struct SignInView: View {

	// MARK: Private properties

	private let store: StoreOf<SignIn>

	// MARK: Init

	public init(store: StoreOf<SignIn>) {
		self.store = store
	}

	// MARK: Body

	public var body: some View {
		WithViewStore(store, observe: { $0 }) { viewStore in
			GeometryReader { geometry in
				VStack(spacing: .zero) {
					VStack(spacing: .zero) {
						Spacer()

						SignInLogoView(
							leadingText: "COFFEE",
							trailingText: "MATE"
						)

						Spacer()

						VStack(alignment: .leading, spacing: .zero) {
							Text(viewStore.headerTitle)
								.adaptiveFont(.primary, .medium, ofSize: 20)
								.foregroundColor(Asset.Color.monteCarlo.swiftUIColor)

							GradientSeparatorView()
								.adaptivePadding(.vertical, 12)

							Text(viewStore.headerDescription)
								.adaptiveFont(.primary, .regular, ofSize: 16)
								.foregroundColor(Asset.Color.alabaster.swiftUIColor)
						}
					}
					.frame(height: (0.5) * geometry.size.height)

					VStack(alignment: .center, spacing: .zero) {
						Spacer()

						Group {
							if viewStore.isPerformingSignIn {
								LoadingSpinnerView(
									tintColor: Asset.Color.alabaster.swiftUIColor
								)
								.adaptiveFrame(size: .square(44))
							} else {
								SignInGoogleButton {
									viewStore.send(.view(.didTapGoogleButton))
								}
								.adaptiveFrame(size: .square(54))
							}
						}
						.transition(.scale.animation(.spring()))

						Spacer()

						SignInFooterView(text: viewStore.footerDescription)
					}
				}
				.adaptivePadding(.horizontal, 30)
			}
			.background {
				Asset.Image.SignIn.background.swiftUIImage
					.resizable(resizingMode: .stretch)
					.aspectRatio(contentMode: .fill)
					.overlay {
						Color.black
					}
					.opacity(viewStore.isPerformingSignIn ? 0.8 : 0.6)
					.blur(radius: viewStore.isPerformingSignIn ? 5.5 : 3.5)
					.padding(-20)
					.ignoresSafeArea()
					.animation(.easeInOut, value: viewStore.isPerformingSignIn)
			}
		}
	}

}

// MARK: - Previews

#if DEBUG
struct SignInView_Preview: PreviewProvider {

	static var previews: some View {
		SignInView(store: Store(
			initialState: .init(isPerformingSignIn: false),
			reducer: SignIn.init
		))
	}

}
#endif
