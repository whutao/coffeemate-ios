//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import Extensions
import SwiftUI

// MARK: - View

struct SignInLogoView: View {

	// MARK: Private properties

	private let leadingText: String

	private let trailingText: String

	// MARK: Init

	init(leadingText: String, trailingText: String) {
		self.leadingText = leadingText
		self.trailingText = trailingText
	}

	// MARK: Body

	var body: some View {
		VStack(spacing: .zero) {
			Asset.Image.SignIn.appLogo.swiftUIImage
				.resizable()
				.aspectRatio(contentMode: .fit)
				.adaptiveFrame(height: 54)
				.offset(x: 8)

			Spacer()
				.adaptiveFrame(height: 14)

			Group {
				Text(leadingText)
					.foregroundColor(Asset.Color.anakiwa.swiftUIColor)
				+
				Text(" ")
				+
				Text(trailingText)
					.foregroundColor(Asset.Color.alabaster.swiftUIColor)
			}
			.adaptiveFont(.primary, .bold, ofSize: 18)
		}
	}

}

// MARK: - Previews

#if DEBUG
struct SignInLogoView_Preview: PreviewProvider {

	static var previews: some View {
		ZStack {
			Color.gray
				.opacity(0.8)
				.ignoresSafeArea()

			SignInLogoView(leadingText: "COFFEE", trailingText: "MATE")
		}
	}

}
#endif
