//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import Extensions
import SwiftUI

// MARK: - View

struct SignInFooterView: View {

	// MARK: Private properties

	private let text: String

	// MARK: Init

	init(text: String) {
		self.text = text
	}

	// MARK: Body

	var body: some View {
		Text(.init(text))
			.lineLimit(4)
			.multilineTextAlignment(.center)
			.tint(Asset.Color.keppel.swiftUIColor)
			.foregroundColor(Asset.Color.alabaster.swiftUIColor)
			.adaptiveFont(.primary, .regular, ofSize: 10)
	}

}

// MARK: - Previews

#if DEBUG
struct SignInFooterView_Preview: PreviewProvider {

	static var previews: some View {
		ZStack {
			Color.gray.ignoresSafeArea()

			SignInFooterView(text: String(repeating: "A", count: 100))
				.padding(50)
		}
	}

}
#endif
