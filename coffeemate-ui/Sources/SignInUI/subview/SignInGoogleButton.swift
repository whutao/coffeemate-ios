//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import Extensions
import SwiftUI

// MARK: - View

struct SignInGoogleButton: View {

	// MARK: Private properties

	private let action: () -> Void

	// MARK: Init

	init(action: @escaping () -> Void) {
		self.action = action
	}

	// MARK: Body

	var body: some View {
		Button {
			action()
		} label: {
			Asset.Image.SignIn.googleButton.swiftUIImage
				.resizable()
				.aspectRatio(contentMode: .fit)
		}
		.scaleOnPress()
	}

}

// MARK: - Previews

#if DEBUG
struct SignInGoogleButton_Preview: PreviewProvider {

	static var previews: some View {
		ZStack {
			Color.gray
				.opacity(0.2)
				.ignoresSafeArea()

			SignInGoogleButton {

			}
			.adaptiveFrame(size: .square(54))
		}
	}

}
#endif
