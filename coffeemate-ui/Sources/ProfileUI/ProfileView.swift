//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import Models
import Profile
import SideProfileOverviewUI
import SwiftUI
import ViewComponents

// MARK: - View

public struct ProfileView: View {

	// MARK: Private properties

	private let store: StoreOf<Profile>

	// MARK: Init

	public init(store: StoreOf<Profile>) {
		self.store = store
	}

	// MARK: Body

	public var body: some View {
		ProfileNavigationContainerView(store: store) {
			IfLetStore(store.scope(
				state: \.ownProfileOverview,
				action: Profile.Action.ownProfileOverview
			)) { profileStore in
				SideProfileOverviewView(store: profileStore)
			} else: {
				LoadingSpinnerView(
					tintColor: Asset.Color.keppel.swiftUIColor
				)
				.adaptiveFrame(size: .square(60))
				.frame(maxWidth: .infinity, maxHeight: .infinity)
			}
		}
		.onAppear {
			store.send(.view(.didAppear))
		}
		.toolbar(.hidden, for: .navigationBar)
	}

}

// MARK: - Previews

#if DEBUG
import Mocking

struct ProfileView_Preview: PreviewProvider {

	static var previews: some View {
		ProfileView(store: Store(
			initialState: .init(ownProfileOverview: .init(
				ownProfile: .mockedJulia()
			)),
			reducer: Profile.init
		))
	}

}
#endif
