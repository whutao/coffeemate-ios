//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import Profile
import SwiftUI
import ViewComponents

// MARK: - View

struct ProfileNavigationContainerView<Content: View>: View {

	// MARK: Private properties

	@ObservedObject private var viewStore: ViewStore<ViewState, Profile.Action>

	private let store: StoreOf<Profile>

	private let content: () -> Content

	// MARK: Init

	init(
		store: StoreOf<Profile>,
		@ViewBuilder content: @escaping () -> Content
	) {
		self.store = store
		self._viewStore = .init(initialValue: ViewStore(store, observe: ViewState.init))
		self.content = content
	}

	// MARK: Body

	var body: some View {
		ConcaveNavigationContainerView { _ in
			HStack(alignment: .center, spacing: .zero) {
				Button {
					viewStore.send(.view(.didTapEditButton))
				} label: {
					Asset.Image.Icon.editPencil.swiftUIImage
						.resizable()
						.aspectRatio(contentMode: .fit)
						.foregroundColor(Asset.Color.monteCarlo.swiftUIColor)
				}
				.scaleOnPress()
				.adaptiveFrame(size: .square(28))

				Spacer()

				Text(viewStore.title)
					.lineLimit(1)
					.foregroundColor(Asset.Color.alabaster.swiftUIColor)
					.adaptiveFont(.primary, .medium, ofSize: 20)

				Spacer()

				Button {
					viewStore.send(.view(.didTapSignOutButton))
				} label: {
					SystemImage.exit
						.resizable()
						.aspectRatio(contentMode: .fit)
						.foregroundColor(Asset.Color.monteCarlo.swiftUIColor)
				}
				.scaleOnPress()
				.adaptiveFrame(size: .square(22))
			}
		} content: {
			content()
				.background {
					Asset.Color.alabaster.swiftUIColor
				}
		}
	}

}

// MARK: - ViewState

extension ProfileNavigationContainerView {

	struct ViewState: Equatable, Sendable {

		// MARK: Exposed properties

		let title: String

		// MARK: Init

		init(profileState: Profile.State) {
			self.title = profileState.headerTitle
		}

	}

}

// MARK: - Previews

#if DEBUG
struct ProfileNavigationContainerView_Preview: PreviewProvider {

	static var previews: some View {
		ProfileNavigationContainerView(store: Store(
			initialState: Profile.State.init(),
			reducer: Profile.init
		)) {
			Color.clear
				.frame(height: 1200)
		}

	}

}
#endif
