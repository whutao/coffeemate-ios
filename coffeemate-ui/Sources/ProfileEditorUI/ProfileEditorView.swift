//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import Models
import ProfileEditor
import SideProfileOverviewUI
import SwiftUI
import ViewComponents

// MARK: - View

public struct ProfileEditorView: View {

	// MARK: Private properties

	@ObservedObject private var viewStore: ViewStore<ViewState, ProfileEditor.Action>

	private let store: StoreOf<ProfileEditor>

	// MARK: Init

	public init(store: StoreOf<ProfileEditor>) {
		self.store = store
		self._viewStore = .init(initialValue: ViewStore(store, observe: ViewState.init))
	}

	// MARK: Body

	public var body: some View {
		ProfileEditorNavigationContainerView(store: store) {
			IfLetStore(store.scope(
				state: \.editorRedactor,
				action: ProfileEditor.Action.ownProfileEditorRedactor
			)) { editorRedactorStore in
				ProfileEditorRedactorView(store: editorRedactorStore)
			} else: {
				LoadingSpinnerView(
					tintColor: Asset.Color.keppel.swiftUIColor
				)
				.adaptiveFrame(size: .square(60))
				.frame(maxWidth: .infinity, maxHeight: .infinity)
			}
		}
		.ignoresSafeArea(edges: .bottom)
		.disabled(!viewStore.areUserInteractionsEnabled)
		.fullScreenCover(store: store.scope(
			state: \.$ownProfileDeleteDialog,
			action: ProfileEditor.Action.ownProfileDeleteDialog
		)) { presentationStore in
			ProfileEditorDeleteDialogView(store: presentationStore)
				.transition(.opacity.animation(.easeInOut(duration: 0.22)))
				.ignoresSafeArea()
		}
		.sheet(
			store: store.scope(
				state: \.$ownProfileOverview,
				action: ProfileEditor.Action.ownProfileOverview
			),
			content: SideProfileOverviewView.init
		)
		.onFirstAppear {
			store.send(.view(.didAppearFirstTime))
		}
	}

}

// MARK: - ViewState

extension ProfileEditorView {

	struct ViewState: Equatable, Sendable {

		// MARK: Exposed properties

		let areUserInteractionsEnabled: Bool

		// MARK: Init

		init(profileEditorState: ProfileEditor.State) {
			self.areUserInteractionsEnabled = profileEditorState.areUserInteractionsEnabled
		}

	}

}

// MARK: - Previews

#if DEBUG
import Mocking

struct ProfileEditorView_Preview: PreviewProvider {

	static var previews: some View {
		ProfileEditorView(store: Store(
			initialState: .init(
				editorRedactor: .init(
					ownProfile: .mockedJulia(),
					allHobbies: IdentifiedArray(uniqueElements: Hobby.allRandomMocked())
				),
				ownProfileDeleteDialog: nil
			),
			reducer: ProfileEditor.init
		))
	}

}
#endif
