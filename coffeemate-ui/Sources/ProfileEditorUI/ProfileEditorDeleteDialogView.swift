//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import ProfileEditor
import SwiftUI
import ViewComponents

// MARK: - View

struct ProfileEditorDeleteDialogView: View {

	// MARK: Private properties

	@StateObject private var globalFocusState = GlobalFocusState()

	private let reasonInputFieldId: UUID

	private let store: StoreOf<ProfileEditorDelete>

	// MARK: Init

	init(store: StoreOf<ProfileEditorDelete>) {
		self.store = store
		self.reasonInputFieldId = UUID()
	}

	// MARK: Body

	var body: some View {
		WithViewStore(store, observe: { $0 }) { viewStore in
			ZStack {
				Asset.Color.gunsmoke.swiftUIColor
					.opacity(0.9)
					.blur(radius: 5.5)
					.padding(-20)
					.ignoresSafeArea()

				VStack(alignment: .center, spacing: .zero) {
					HStack(alignment: .center, spacing: .zero) {
						Text(viewStore.reasonFieldTitle)
							.lineLimit(1)
							.adaptiveFont(.primary, .medium, ofSize: 14)
							.foregroundColor(Asset.Color.black.swiftUIColor)

						Spacer()
					}
					.adaptivePadding(.horizontal, 6)

					Spacer()
						.adaptiveFrame(height: 12)

					MultiLineTextView(
						footer: .hidden,
						text: viewStore.binding(
							get: \.reason,
							send: { .view(.didChangeReasonText($0)) }
						)
					)
					.adaptiveFrame(height: 200)
					.customFocus($globalFocusState.focus, equals: .uuid(reasonInputFieldId))
					.onTapGesture {
						globalFocusState.focus = .uuid(reasonInputFieldId)
					}

					Spacer()
						.adaptiveFrame(height: 12)

					HStack(alignment: .center, spacing: .zero) {
						Text(viewStore.feedbackAllowedFieldTitle)
							.lineLimit(1)
							.adaptiveFont(.primary, .regular, ofSize: 12)
							.foregroundColor(Asset.Color.black.swiftUIColor)

						Spacer()

						CheckboxView(isSelected: viewStore.binding(
							get: \.isFeedbackAllowed,
							send: { .view(.didSelectFeedbackAllowed($0)) }
						))
						.adaptiveFrame(size: .square(20))
					}
					.adaptivePadding(.horizontal, 6)

					Spacer()
						.adaptiveFrame(height: 20)

					OpaqueButton(
						title: viewStore.confirmButtonTitle,
						backgroundColor: Asset.Color.comegranate.swiftUIColor
					) {
						viewStore.send(.view(.didTapConfirmButton))
					}
					.adaptiveFrame(height: 40)
					.adaptivePadding(.horizontal, 30)
				}
				.adaptivePadding(.horizontal, 12)
				.adaptivePadding(.vertical, 22)
				.background {
					Asset.Color.alabaster.swiftUIColor
				}
				.cornerRadius(12, corners: .allCorners)
				.overlay {
					RoundedRectangle(cornerRadius: 12)
						.adaptiveStroke(Asset.Color.gunsmoke.swiftUIColor.opacity(0.5), lineWidth: 1)
				}
				.overlay(alignment: .topTrailing) {
					Button {
						viewStore.send(.view(.didTapCancelButton))
					} label: {
						Image(systemName: "plus")
							.resizable()
							.rotationEffect(.degrees(45))
							.aspectRatio(contentMode: .fit)
							.foregroundColor(Asset.Color.gunsmoke.swiftUIColor)
							.padding(2)
							.contentShape(Rectangle())
							.adaptiveFrame(size: .square(24))
					}
					.scaleOnPress()
					.offset(x: -12, y: 12)
				}
				.adaptivePadding(.horizontal, 30)
				.offset(y: -80)
			}
		}
		.onTapGesture {
			globalFocusState.focus = nil
		}
	}

}

// MARK: - Previews

#if DEBUG
import Mocking

struct ProfileEditorDeleteDialogView_Preview: PreviewProvider {

	static var previews: some View {
		ZStack {
			Text(Lorem.longParagraph)

			ProfileEditorDeleteDialogView(store: Store(
				initialState: .init(),
				reducer: ProfileEditorDelete.init
			))
		}
		.ignoresSafeArea()
	}

}
#endif
