//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import Models
import ProfileEditor
import SwiftUI
import ViewComponents

// MARK: - View

struct ProfileEditorTargetGendersPickerView: View {

	// MARK: Exposed properties

	@ObservedObject private var viewStore: ViewStore<ViewState, ProfileEditorRedactor.Action>

	private let store: StoreOf<ProfileEditorRedactor>

	// MARK: Init

	init(store: StoreOf<ProfileEditorRedactor>) {
		self.store = store
		self._viewStore = .init(initialValue: ViewStore(store, observe: ViewState.init))
	}

	// MARK: Body

	var body: some View {
		GenderPickerView(
			style: .targetGender,
			fromUniqueGenders: [.female, .male, .other],
			selectedGenders: viewStore.binding(
				get: \.targetGenders,
				send: { .view(.didUpdateTargetGenders($0)) }
			)
			.removeDuplicates()
		)
	}

}

// MARK: - ViewState

extension ProfileEditorTargetGendersPickerView {

	struct ViewState: Equatable, Sendable {

		// MARK: Exposed properties

		let targetGenders: Set<Gender>

		// MARK: Init

		init(profileEditorRedactorState: ProfileEditorRedactor.State) {
			self.targetGenders = profileEditorRedactorState.targetGenders
		}

	}

}

// MARK: - Previews

#if DEBUG
struct ProfileEditorTargetGendersPickerView_Preview: PreviewProvider {

	static var previews: some View {
		EmptyView()
	}

}
#endif
