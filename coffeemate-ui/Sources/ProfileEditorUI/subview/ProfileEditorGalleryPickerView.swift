//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import IdentifiedCollections
import Models
import ProfileEditor
import SwiftUI
import ViewComponents

// MARK: - View

struct ProfileEditorGalleryPickerView: View {

	// MARK: Private properties

	@State private var isPhotoPickerPresented: Bool = false

	@ObservedObject private var viewStore: ViewStore<ViewState, ProfileEditorRedactor.Action>

	private let store: StoreOf<ProfileEditorRedactor>

	// MARK: Init

	init(store: StoreOf<ProfileEditorRedactor>) {
		self.store = store
		self._viewStore = .init(initialValue: ViewStore(store, observe: ViewState.init))
	}

	// MARK: Body

	var body: some View {
		VStack(alignment: .center, spacing: .zero) {
			Group {
				if !viewStore.isGalleryEmpty {
					GalleryCarouselView(
						entries: viewStore.galleryEntries,
						selectedEntry: viewStore.binding(
							get: \.activeGalleryEntry,
							send: { .view(.didSetActiveGalleryEntry($0?.galleryPickerEntry)) }
						),
						isEntryDeleteButtonVisisble: true,
						entryTapAction: { _ in
							#warning("TODO: Open fullscreen gallery.")
						},
						entryDeleteTapAction: { entry in
							viewStore.send(.view(.didTapRemoveGalleryEntry(entry.galleryPickerEntry)))
						}
					)

					Spacer()
						.adaptiveFrame(height: 16)

					Text(viewStore.galleryFooter)
						.foregroundColor(Asset.Color.gunsmoke.swiftUIColor)
						.adaptiveFont(.primary, .medium, ofSize: 14)
						.frame(maxWidth: .infinity)
				}
			}
			.transition(.opacity.animation(.linear))

			Group {
				if viewStore.galleryEntries.count < viewStore.maxGalleryEntries {
					Spacer()
						.adaptiveFrame(height: 20)

					HStack(alignment: .center, spacing: .zero) {
						Spacer()

						Button {
							isPhotoPickerPresented = true
						} label: {
							Asset.Image.Icon.camera.swiftUIImage
								.resizable()
								.aspectRatio(contentMode: .fit)
								.adaptiveFrame(size: .square(22))
								.padding(8)
								.foregroundColor(Asset.Color.keppel.swiftUIColor)
								.contentShape(Rectangle())
						}
						.scaleOnPress()

						Spacer()
					}
				}
			}
			.transition(.scale.combined(with: .opacity).animation(.easeInOut))
		}
		.singleImagePickerSheet(isPresented: $isPhotoPickerPresented) { data in
			if let compressedImageData = data.compressedImageData(toFitKB: viewStore.pickedImageMaxSizeInKB) {
				viewStore.send(.view(.didAddNewGalleryEntry(imageData: compressedImageData)))
			}
		}
	}

}

// MARK: - ViewState

extension ProfileEditorGalleryPickerView {

	struct ViewState: Equatable, Sendable {

		// MARK: Expsosed properties

		let pickedImageMaxSizeInKB: UInt

		let activeGalleryEntry: GalleryEntry?

		let galleryEntries: IdentifiedArrayOf<GalleryEntry>

		let maxGalleryEntries: Int

		let galleryFooter: String

		let isGalleryEmpty: Bool

		// MARK: Init

		init(profileEditorRedactorState: ProfileEditorRedactor.State) {
			self.pickedImageMaxSizeInKB = profileEditorRedactorState.pickedPhotoMaxSizeInKB
			self.isGalleryEmpty = profileEditorRedactorState.gallery.isEmpty
			self.activeGalleryEntry = profileEditorRedactorState.activeGalleryEntry?.galleryEntry
			self.galleryEntries = IdentifiedArrayOf(
				uniqueElements: profileEditorRedactorState.gallery.map(\.galleryEntry)
			)
			self.galleryFooter = profileEditorRedactorState.galleryPiclerFooter
			self.maxGalleryEntries = profileEditorRedactorState.galleryPickerEntryLimit
		}

	}

}

// MARK: - Helper

extension GalleryEntry {

	fileprivate var galleryPickerEntry: ProfileEditorRedactor.State.GalleryPickerEntry {
		switch self.image {
		case let .data(data):
			return .newEntry(entryId, data)
		case .url:
			return .existingEntry(self)
		}
	}

}

extension ProfileEditorRedactor.State.GalleryPickerEntry {

	fileprivate var galleryEntry: GalleryEntry {
		switch self {
		case let .existingEntry(entry):
			return entry
		case let .newEntry(entryId, entryData):
			return GalleryEntry(
				entryId: entryId,
				image: .data(entryData),
				isOwn: true
			)
		}
	}

}

// MARK: - Previews

#if DEBUG
struct ProfileEditorGalleryPickerView_Preview: PreviewProvider {

	static var previews: some View {
		ScrollView {
			VStack {
				ProfileEditorSectionView(title: "Gallery", titlePadding: 34) {
					ProfileEditorGalleryPickerView(store: Store(
						initialState: .init(
							ownProfile: .mockedJulia(),
							allHobbies: IdentifiedArray(uniqueElements: Hobby.allRandomMocked())
						),
						reducer: ProfileEditorRedactor.init
					))
				}
				.background {
					Color.gray
				}

				Spacer()
			}
		}
		.frame(width: 200)
	}

}
#endif
