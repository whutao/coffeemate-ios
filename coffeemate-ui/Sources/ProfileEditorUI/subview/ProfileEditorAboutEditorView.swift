//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import Models
import ProfileEditor
import SwiftUI
import ViewComponents

// MARK: - View

struct ProfileEditorAboutEditorView: View {

	// MARK: Exposed properties

	@EnvironmentObject private var globalFocusState: GlobalFocusState

	@ObservedObject private var viewStore: ViewStore<ViewState, ProfileEditorRedactor.Action>

	private let store: StoreOf<ProfileEditorRedactor>

	// MARK: Init

	init(store: StoreOf<ProfileEditorRedactor>) {
		self.store = store
		self._viewStore = .init(initialValue: ViewStore(store, observe: ViewState.init))
	}

	// MARK: Body

	var body: some View {
		MultiLineTextView(
			footer: .text(viewStore.aboutFieldFooterLabel),
			text: viewStore.binding(
				get: \.about,
				send: { .view(.didUpdateAbout($0)) }
			)
		)
		.adaptiveFrame(height: 300)
	}

}

// MARK: - ViewState

extension ProfileEditorAboutEditorView {

	struct ViewState: Equatable, Sendable {

		// MARK: Exposed properties

		let about: String

		let aboutFieldFooterLabel: String

		// MARK: Init

		init(profileEditorRedactorState: ProfileEditorRedactor.State) {
			self.about = profileEditorRedactorState.about
			self.aboutFieldFooterLabel = profileEditorRedactorState.aboutFieldFooterLabel
		}

	}

}

// MARK: - Previews

#if DEBUG
struct ProfileEditorAboutEditorView_Preview: PreviewProvider {

	static var previews: some View {
		EmptyView()
	}

}
#endif
