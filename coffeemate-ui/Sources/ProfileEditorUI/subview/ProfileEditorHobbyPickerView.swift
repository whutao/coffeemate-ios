//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import Models
import ProfileEditor
import SwiftUI
import ViewComponents

// MARK: - View

struct ProfileEditorHobbyPickerView: View {

	// MARK: Exposed properties

	@ObservedObject private var viewStore: ViewStore<ViewState, ProfileEditorRedactor.Action>

	private let store: StoreOf<ProfileEditorRedactor>

	// MARK: Init

	init(store: StoreOf<ProfileEditorRedactor>) {
		self.store = store
		self._viewStore = .init(initialValue: ViewStore(store, observe: ViewState.init))
	}

	// MARK: Body

	var body: some View {
		VStack(alignment: .center, spacing: .zero) {
			HobbyPickerView(
				allHobbies: viewStore.allHobbies,
				selectedHobbies: viewStore.binding(
					get: \.selectedHobbies,
					send: { .view(.didUpdateSelectedHobbies($0)) }
				),
				selectionLimit: viewStore.hobbiesPickerSelectionLimit
			)
			.adaptiveFrame(height: 360)

			Spacer()
				.adaptiveFrame(height: 12)

			Text(viewStore.hobbiesPickerFooter)
				.foregroundColor(Asset.Color.gunsmoke.swiftUIColor)
				.adaptiveFont(.primary, .medium, ofSize: 14)
				.frame(maxWidth: .infinity)
		}
	}

}

// MARK: - ViewState

extension ProfileEditorHobbyPickerView {

	struct ViewState: Equatable, Sendable {

		// MARK: Exposed properties

		let allHobbies: IdentifiedArrayOf<Hobby>

		let selectedHobbies: IdentifiedArrayOf<Hobby>

		let hobbiesPickerSelectionLimit: Int

		let hobbiesPickerFooter: String

		// MARK: Init

		init(profileEditorRedactorState: ProfileEditorRedactor.State) {
			self.allHobbies = profileEditorRedactorState.allHobbies
			self.selectedHobbies = profileEditorRedactorState.selectedHobbies
			self.hobbiesPickerSelectionLimit = profileEditorRedactorState.hobbiesPickerSelectionLimit
			self.hobbiesPickerFooter = profileEditorRedactorState.hobbiesPickerFooter
		}

	}

}

// MARK: - Previews

#if DEBUG
struct ProfileEditorHobbyPickerView_Preview: PreviewProvider {

	static var previews: some View {
		EmptyView()
	}

}
#endif
