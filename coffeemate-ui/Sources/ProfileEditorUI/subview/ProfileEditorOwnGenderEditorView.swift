//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import Models
import ProfileEditor
import SwiftUI
import ViewComponents

// MARK: - View

struct ProfileEditorOwnGenderPickerView: View {

	// MARK: Exposed properties

	@ObservedObject private var viewStore: ViewStore<ViewState, ProfileEditorRedactor.Action>

	private let store: StoreOf<ProfileEditorRedactor>

	// MARK: Init

	init(store: StoreOf<ProfileEditorRedactor>) {
		self.store = store
		self._viewStore = .init(initialValue: ViewStore(store, observe: ViewState.init))
	}

	// MARK: Body

	var body: some View {
		GenderPickerView(
			style: .ownGender,
			fromUniqueGenders: [.female, .male, .other],
			selectedGender: viewStore.binding(
				get: \.ownGender,
				send: { .view(.didUpdateOwnGender($0)) }
			)
			.removeDuplicates()
		)
	}

}

// MARK: - ViewState

extension ProfileEditorOwnGenderPickerView {

	struct ViewState: Equatable, Sendable {

		// MARK: Exposed properties

		let ownGender: Gender?

		// MARK: Init

		init(profileEditorRedactorState: ProfileEditorRedactor.State) {
			self.ownGender = profileEditorRedactorState.ownGender
		}

	}

}

// MARK: - Previews

#if DEBUG
struct ProfileEditorOwnGenderPickerView_Preview: PreviewProvider {

	static var previews: some View {
		EmptyView()
	}

}
#endif
