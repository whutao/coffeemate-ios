//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import Models
import ProfileEditor
import SwiftUI
import ViewComponents

// MARK: - View

struct ProfileEditorAvatarEditorView: View {

	// MARK: Exposed properties

	@ObservedObject private var viewStore: ViewStore<ViewState, ProfileEditorRedactor.Action>

	private let store: StoreOf<ProfileEditorRedactor>

	// MARK: Init

	init(store: StoreOf<ProfileEditorRedactor>) {
		self.store = store
		self._viewStore = .init(initialValue: ViewStore(store, observe: ViewState.init))
	}

	// MARK: Body

	var body: some View {
		VStack(alignment: .center, spacing: .zero) {
			AvatarImagePickerView(
				selectedImage: viewStore.binding(
					get: \.avatarImage,
					send: { .view(.didUpdateAvatarImage($0)) }
				),
				selectedImageMaxSizeInKB: viewStore.avatarMaxSizeInKB
			)

			Spacer()
				.adaptiveFrame(height: 10)

			Text(viewStore.photoPickerTitle)
				.lineLimit(1)
				.foregroundColor(Asset.Color.black.swiftUIColor)
				.adaptiveFont(.primary, .regular, ofSize: 14)
		}
	}

}

// MARK: - ViewState

extension ProfileEditorAvatarEditorView {

	struct ViewState: Equatable, Sendable {

		// MARK: Exposed properties

		let avatarImage: ImageRepresentation?

		let avatarMaxSizeInKB: UInt

		let photoPickerTitle: String

		// MARK: Init

		init(profileEditorRedactorState: ProfileEditorRedactor.State) {
			self.avatarMaxSizeInKB = profileEditorRedactorState.pickedPhotoMaxSizeInKB
			self.avatarImage = profileEditorRedactorState.avatarImage
			self.photoPickerTitle = profileEditorRedactorState.photoPickerTitle
		}

	}

}

// MARK: - Previews

#if DEBUG
struct ProfileEditorAvatarEditorView_Preview: PreviewProvider {

	static var previews: some View {
		EmptyView()
	}

}
#endif
