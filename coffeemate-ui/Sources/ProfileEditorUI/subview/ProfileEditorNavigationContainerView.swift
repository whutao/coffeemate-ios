//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import ProfileEditor
import SwiftUI
import ViewComponents

// MARK: - View

struct ProfileEditorNavigationContainerView<Content: View>: View {

	// MARK: Private properties

	@ObservedObject private var viewStore: ViewStore<ViewState, ProfileEditor.Action>

	private let store: StoreOf<ProfileEditor>

	private let content: () -> Content

	// MARK: Init

	init(
		store: StoreOf<ProfileEditor>,
		@ViewBuilder content: @escaping () -> Content
	) {
		self.store = store
		self._viewStore = .init(initialValue: ViewStore(store, observe: ViewState.init))
		self.content = content
	}

	// MARK: Body

	var body: some View {
		ConvexNavigationContainerView { _ in
			HStack(alignment: .center, spacing: .zero) {
				Button {
					viewStore.send(.view(.didTapBackButton))
				} label: {
					Asset.Image.Icon.back.swiftUIImage
						.resizable()
						.aspectRatio(contentMode: .fit)
						.foregroundColor(Asset.Color.monteCarlo.swiftUIColor)
				}
				.scaleOnPress()
				.adaptiveFrame(size: .square(22))

				Text(viewStore.title)
					.lineLimit(1)
					.foregroundColor(Asset.Color.alabaster.swiftUIColor)
					.adaptiveFont(.primary, .medium, ofSize: 20)
					.frame(maxWidth: .infinity, maxHeight: .infinity)

				Button {
					viewStore.send(.view(.didTapPreviewButton))
				} label: {
					Asset.Image.Icon.eye.swiftUIImage
						.resizable()
						.aspectRatio(contentMode: .fit)
						.foregroundColor(Asset.Color.monteCarlo.swiftUIColor)
				}
				.scaleOnPress()
				.adaptiveFrame(size: .square(22))
			}
		} content: {
			content()
				.background {
					Asset.Color.alabaster.swiftUIColor
				}
		}
	}

}

// MARK: - ViewState

extension ProfileEditorNavigationContainerView {

	struct ViewState: Equatable, Sendable {

		// MARK: Exposed properties

		let title: String

		// MARK: Init

		init(profileEditorState: ProfileEditor.State) {
			self.title = profileEditorState.headerTitle
		}

	}

}

// MARK: - Previews

#if DEBUG
import IdentifiedCollections
import Mocking
import Models

struct ProfileEditorNavigationContainerView_Preview: PreviewProvider {

	static let store: StoreOf<ProfileEditor> = Store(
		initialState: .init(editorRedactor: ProfileEditorRedactor.State.init(
			ownProfile: .mockedJulia(),
			allHobbies: IdentifiedArrayOf(uniqueElements: Hobby.allRandomMocked())
		)),
		reducer: ProfileEditor.init
	)

	static var previews: some View {
		ProfileEditorNavigationContainerView(store: store) {
			IfLetStore(
				store.scope(
					state: \.editorRedactor,
					action: ProfileEditor.Action.ownProfileEditorRedactor
				),
				then: ProfileEditorRedactorView.init
			)
		}

	}

}
#endif
