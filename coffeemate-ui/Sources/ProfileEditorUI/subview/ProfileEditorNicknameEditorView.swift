//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import Models
import ProfileEditor
import SwiftUI
import ViewComponents

// MARK: - View

struct ProfileEditorNicknameEditorView: View {

	// MARK: Exposed properties

	@ObservedObject private var viewStore: ViewStore<ViewState, ProfileEditorRedactor.Action>

	private let store: StoreOf<ProfileEditorRedactor>

	// MARK: Init

	init(store: StoreOf<ProfileEditorRedactor>) {
		self.store = store
		self._viewStore = .init(initialValue: ViewStore(store, observe: ViewState.init))
	}

	// MARK: Body

	var body: some View {
		SingleLineTextView(
			title: .text(viewStore.nicknameFieldTitle),
			footer: .text(viewStore.nicknameFieldFooter),
			placeholder: .empty,
			text: viewStore.binding(
				get: \.nickname,
				send: { .view(.didUpdateNickname($0)) }
			)
		)
	}

}

// MARK: - ViewState

extension ProfileEditorNicknameEditorView {

	struct ViewState: Equatable, Sendable {

		// MARK: Exposed properties

		let nickname: String

		let nicknameFieldTitle: String

		let nicknameFieldFooter: String

		// MARK: Init

		init(profileEditorRedactorState: ProfileEditorRedactor.State) {
			self.nickname = profileEditorRedactorState.nickname
			self.nicknameFieldTitle = profileEditorRedactorState.nicknameFieldTitle
			self.nicknameFieldFooter = profileEditorRedactorState.nicknameFieldFooter
		}

	}

}

// MARK: - Previews

#if DEBUG
struct ProfileEditorNicknameEditorView_Preview: PreviewProvider {

	static var previews: some View {
		EmptyView()
	}

}
#endif
