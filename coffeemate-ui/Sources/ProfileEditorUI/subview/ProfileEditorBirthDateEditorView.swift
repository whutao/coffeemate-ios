//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import Models
import ProfileEditor
import SwiftUI
import ViewComponents

// MARK: - View

struct ProfileEditorBirthDateEditorView: View {

	// MARK: Exposed properties

	@ObservedObject private var viewStore: ViewStore<ViewState, ProfileEditorRedactor.Action>

	private let store: StoreOf<ProfileEditorRedactor>

	// MARK: Init

	init(store: StoreOf<ProfileEditorRedactor>) {
		self.store = store
		self._viewStore = .init(initialValue: ViewStore(store, observe: ViewState.init))
	}

	// MARK: Body

	var body: some View {
		BirthDatePickerView(
			selectedDate: viewStore.binding(
				get: \.birthDate,
				send: { .view(.didUpdateBirthDate($0)) }
			)
			.removeDuplicates(),
			dateRange: viewStore.birthDateRange,
			dateDisplayFormat: viewStore.birthDateDisplayFormat,
			placeholder: viewStore.birthDatePlaceholder,
			doneButtonTitle: viewStore.birthDateDoneButtonTitle
		)
	}

}

// MARK: - ViewState

extension ProfileEditorBirthDateEditorView {

	struct ViewState: Equatable, Sendable {

		// MARK: Exposed properties

		let birthDate: Date?

		let birthDateRange: ClosedRange<Date>

		let birthDateDisplayFormat: String

		let birthDatePlaceholder: String

		let birthDateDoneButtonTitle: String

		// MARK: Init

		init(profileEditorRedactorState: ProfileEditorRedactor.State) {
			self.birthDate = profileEditorRedactorState.birthDate
			self.birthDateRange = profileEditorRedactorState.birthDateRange
			self.birthDateDisplayFormat = profileEditorRedactorState.birthDateDisplayFormat
			self.birthDatePlaceholder = profileEditorRedactorState.birthDatePlaceholder
			self.birthDateDoneButtonTitle = profileEditorRedactorState.birthDateDoneButtonTitle
		}

	}

}

// MARK: - Previews

#if DEBUG
struct ProfileEditorBirthDateEditorView_Preview: PreviewProvider {

	static var previews: some View {
		EmptyView()
	}

}
#endif
