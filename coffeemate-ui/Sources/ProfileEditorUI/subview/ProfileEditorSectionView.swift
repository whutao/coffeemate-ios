//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import Extensions
import SwiftUI
import ViewComponents

// MARK: - View

public struct ProfileEditorSectionView<Content: View>: View {

	// MARK: Private properties

	private let title: String

	private let titlePadding: CGFloat

	private let content: () -> Content

	// MARK: Init

	public init(
		title: String,
		titlePadding: CGFloat = .zero,
		@ViewBuilder content: @escaping () -> Content
	) {
		self.title = title
		self.titlePadding = titlePadding
		self.content = content
	}

	// MARK: Body

	public var body: some View {
		VStack(spacing: .zero) {
			HStack(spacing: .zero) {
				Text(title)
					.lineLimit(1)
					.foregroundColor(Asset.Color.black.swiftUIColor)
					.adaptiveFont(.primary, .medium, ofSize: 16)

				Spacer()
			}
			.adaptivePadding(.horizontal, titlePadding)

			Spacer()
				.adaptiveFrame(height: 14)

			content()
		}
	}

}

// MARK: - Previews

#if DEBUG
struct ProfileEditorSectionView_Preview: PreviewProvider {

	static var previews: some View {
		VStack {
			Group {
				ProfileEditorSectionView(title: "Nickname") {
					SingleLineTextView(
						title: .hidden,
						placeholder: .empty,
						text: .constant(.empty)
					)
				}

				ProfileEditorSectionView(title: "Birthday date") {
					SingleLineTextView(
						title: .hidden,
						placeholder: .empty,
						text: .constant(.empty)
					)
				}
			}
			.background {
				Color.gray
			}

			Spacer()
		}
		.padding(40)
	}

}
#endif
