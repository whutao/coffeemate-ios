//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import Design
import Extensions
import Models
import ProfileEditor
import SwiftUI
import ViewComponents

// MARK: - View

struct ProfileEditorRedactorView: View {

	// MARK: Private properties

	@StateObject private var globalFocusState = GlobalFocusState()

	private let nicknameFieldId: UUID = UUID()

	private let aboutFieldId: UUID = UUID()

	private let store: StoreOf<ProfileEditorRedactor>

	@ObservedObject private var viewStore: ViewStore<ViewState, ProfileEditorRedactor.Action>

	// MARK: Init

	init(store: StoreOf<ProfileEditorRedactor>) {
		self.store = store
		self._viewStore = .init(initialValue: ViewStore(store, observe: ViewState.init))
	}

	// MARK: Body

	var body: some View {
		ScrollViewReader { scroll in
			ScrollView {
				VStack(alignment: .center, spacing: .zero) {
					ProfileEditorAvatarEditorView(store: store)

					Spacer()
						.adaptiveFrame(height: 12)

					Group {
						ProfileEditorNicknameEditorView(store: store)
							.id(nicknameFieldId)
							.customFocus($globalFocusState.focus, equals: .uuid(nicknameFieldId))
							.onTapGesture {
								guard globalFocusState.focus != .uuid(nicknameFieldId) else {
									return
								}
								globalFocusState.focus = .uuid(nicknameFieldId)
								withAnimation {
									scroll.scrollTo(nicknameFieldId, anchor: .center)
								}
							}

						ProfileEditorSectionView(title: viewStore.birthDatePickerTitle) {
							ProfileEditorBirthDateEditorView(store: store)
						}

						ProfileEditorSectionView(title: viewStore.ownGenderPickerTitle) {
							ProfileEditorOwnGenderPickerView(store: store)
						}

						ProfileEditorSectionView(title: viewStore.targetGendersPickerTitle) {
							ProfileEditorTargetGendersPickerView(store: store)
						}

						ProfileEditorSectionView(title: viewStore.aboutFieldTitle) {
							ProfileEditorAboutEditorView(store: store)
								.id(aboutFieldId)
								.customFocus($globalFocusState.focus, equals: .uuid(aboutFieldId))
								.onTapGesture {
									guard globalFocusState.focus != .uuid(aboutFieldId) else {
										return
									}
									globalFocusState.focus = .uuid(aboutFieldId)
									withAnimation {
										scroll.scrollTo(aboutFieldId, anchor: .center)
									}
								}
						}

						ProfileEditorSectionView(title: viewStore.hobbiesPickerTitle) {
							ProfileEditorHobbyPickerView(store: store)
						}
					}
					.adaptivePadding(.vertical, 12)
					.adaptivePadding(.horizontal, 34)

					ProfileEditorSectionView(
						title: viewStore.galleryPickerTitle,
						titlePadding: 34
					) {
						ProfileEditorGalleryPickerView(store: store)
					}
					.adaptivePadding(.vertical, 12)

					Spacer()
						.adaptiveFrame(height: 30)

					OpaqueButton(title: viewStore.saveButtonTitle) {
						viewStore.send(.view(.didTapSaveButton))
					}
					.disabledAndFaintIf(!viewStore.isSaveButtonEnabled)
					.adaptiveFrame(height: 40)
					.adaptivePadding(.horizontal, 70)

					Spacer()
						.adaptiveFrame(height: 30)

					OpaqueButton(
						title: viewStore.deleteAccountButtonTitle,
						backgroundColor: Asset.Color.comegranate.swiftUIColor
					) {
						viewStore.send(.view(.didTapDeleteAccount))
					}
					.adaptiveFrame(height: 40)
					.adaptivePadding(.horizontal, 70)

					Spacer()
						.adaptiveFrame(height: 30)

					Text(viewStore.versionFooterTitle)
						.lineLimit(1)
						.foregroundColor(Asset.Color.mineShaft.swiftUIColor.opacity(0.5))
						.adaptiveFont(.primary, .regular, ofSize: 12)
				}
				.adaptivePadding(.top, 30)
				.adaptivePadding(.bottom, 180)
				.contentShape(Rectangle())
				.onTapGesture {
					globalFocusState.focus = nil
				}
			}
			.scrollDismissesKeyboard(.interactively)
		}
	}

}

// MARK: - ViewState

extension ProfileEditorRedactorView {

	struct ViewState: Equatable, Sendable {

		// MARK: Exposed properties

		let birthDatePickerTitle: String

		let ownGenderPickerTitle: String

		let targetGendersPickerTitle: String

		let aboutFieldTitle: String

		let hobbiesPickerTitle: String

		let galleryPickerTitle: String

		let saveButtonTitle: String

		let isSaveButtonEnabled: Bool

		let deleteAccountButtonTitle: String

		let versionFooterTitle: String

		// MARK: Init

		init(profileEditorRedactorState: ProfileEditorRedactor.State) {
			self.birthDatePickerTitle = profileEditorRedactorState.birthDatePickerTitle
			self.ownGenderPickerTitle = profileEditorRedactorState.ownGenderPickerTitle
			self.targetGendersPickerTitle = profileEditorRedactorState.targetGendersPickerTitle
			self.aboutFieldTitle = profileEditorRedactorState.aboutFieldTitle
			self.hobbiesPickerTitle = profileEditorRedactorState.hobbiesPickerTitle
			self.galleryPickerTitle = profileEditorRedactorState.galleryPickerTitle
			self.saveButtonTitle = profileEditorRedactorState.saveButtonTitle
			self.isSaveButtonEnabled = profileEditorRedactorState.isSaveButtonEnabled
			self.deleteAccountButtonTitle = profileEditorRedactorState.deleteAccountButtonTitle
			self.versionFooterTitle = profileEditorRedactorState.versionFooterTitle
		}

	}

}

// MARK: - Previews

#if DEBUG
import Mocking

struct ProfileEditorRedactorView_Preview: PreviewProvider {

	static var previews: some View {
		ScrollView {
			ProfileEditorRedactorView(store: Store(
				initialState: .init(
					ownProfile: .mockedJulia(),
					allHobbies: IdentifiedArray(uniqueElements: Hobby.allRandomMocked())
				),
				reducer: ProfileEditorRedactor.init
			))
		}
	}

}
#endif
