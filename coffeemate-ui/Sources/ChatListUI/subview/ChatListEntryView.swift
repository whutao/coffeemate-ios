//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ChatList
import Design
import Extensions
import Kingfisher
import SwiftUI
import ViewComponents

// MARK: - View

struct ChatListEntryView: View {

	// MARK: Private properties

	private let chatEntry: ChatList.State.Entry

	// MARK: Init

	init(chatEntry: ChatList.State.Entry) {
		self.chatEntry = chatEntry
	}

	// MARK: Body

	var body: some View {
		HStack(alignment: .center, spacing: .zero) {
			ZStack {
				KFImage(chatEntry.avatarUrl)
					.resizable()
					.cacheOriginalImage()
					.appendProcessor(DownsamplingImageProcessor(size: .square(200)))
					.placeholder {
						LoadingSpinnerView(
							tintColor: Asset.Color.keppel.swiftUIColor
						)
						.adaptiveFrame(size: .square(24))
					}
					.aspectRatio(contentMode: .fill)
					.adaptiveFrame(size: .square(54))
					.clipShape(Capsule())
			}

			Spacer()
				.adaptiveFrame(width: 10)

			VStack(alignment: .leading, spacing: .zero) {
				Spacer()

				Text(chatEntry.nickname)
					.adaptiveFont(.primary, .medium, ofSize: 18)

				Spacer()
					.adaptiveFrame(height: 3)

				Group {
					if let text = chatEntry.lastMessageDisplayText {
						Text(text)
							.foregroundColor(Asset.Color.black.swiftUIColor)
							.adaptiveFont(.primary, .regular, ofSize: 14)
					} else {
						Text(chatEntry.lastMessageEmptyPlaceholder)
							.foregroundColor(Asset.Color.mineShaft.swiftUIColor.opacity(0.3))
							.adaptiveFont(.primary, .regular, ofSize: 14)
					}
				}
				.lineLimit(1)

				Spacer()
			}
			.lineLimit(1)
			.foregroundColor(Asset.Color.black.swiftUIColor)
			.adaptivePadding(.vertical, 5)

			Group {
				Spacer()
					.frame(minWidth: 12)

				if let date = chatEntry.lastMessageDate {
					VStack(alignment: .leading, spacing: .zero) {
						Text(date, format: .relative(presentation: .numeric))
							.foregroundColor(Asset.Color.black.swiftUIColor)
							.adaptiveFont(.primary, .regular, ofSize: 12)

						Spacer()
					}
				}
			}
			.adaptivePadding(.vertical, 8)
		}
		.adaptiveFrame(height: 54)
	}

}

// MARK: - Previews

#if DEBUG
import Mocking

struct ChatListEntryView_Preview: PreviewProvider {

	static var previews: some View {
		VStack {
			Group {
				ChatListEntryView(chatEntry: .init(
					chatSummary: .mockedWithLastMessage()
				))
				.background {
					Color.gray.opacity(0.2)
				}

				ChatListEntryView(chatEntry: .init(
					chatSummary: .mockedWithLastMessage()
				))
			}
			.padding(30)
		}
	}

}
#endif
