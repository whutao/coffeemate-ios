//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ChatList
import ComposableArchitecture
import Design
import Extensions
import SwiftUI
import ViewComponents

// MARK: - View

struct ChatListNavigationContainerView<Content: View>: View {

	// MARK: Private properties

	@ObservedObject private var viewStore: ViewStore<ViewState, ChatList.Action>

	private let store: StoreOf<ChatList>

	private let content: () -> Content

	// MARK: Init

	init(
		store: StoreOf<ChatList>,
		@ViewBuilder content: @escaping () -> Content
	) {
		self._viewStore = .init(initialValue: .init(store, observe: ViewState.init))
		self.store = store
		self.content = content
	}

	// MARK: Body

	var body: some View {
		ConcaveNavigationContainerView { _ in
			HStack(alignment: .center, spacing: .zero) {
				Spacer()
					.adaptiveFrame(size: .square(22))

				Text(viewStore.title)
					.lineLimit(1)
					.adaptiveFont(.primary, .medium, ofSize: 20)
					.foregroundColor(Asset.Color.alabaster.swiftUIColor)
					.frame(maxWidth: .infinity)

				Group {
					if viewStore.isChangeVisibilityAvailable {
						Button {
							viewStore.send(.view(.didTapVisibilityButton))
						} label: {
							Asset.Image.Icon.eye.swiftUIImage
								.resizable()
								.aspectRatio(contentMode: .fit)
								.foregroundColor(Asset.Color.monteCarlo.swiftUIColor)
						}
						.scaleOnPress()
					} else {
						Spacer()
					}
				}
				.adaptiveFrame(size: .square(22))
			}
		} content: {
			content()
		}
		.toolbar(.hidden, for: .navigationBar)
	}

}

// MARK: - ViewState

extension ChatListNavigationContainerView {

	struct ViewState: Equatable, Sendable {

		// MARK: Exposed properties

		let title: String

		let isChangeVisibilityAvailable: Bool

		// MARK: Init

		init(chatListState: ChatList.State) {
			self.title = chatListState.headerTitle
			self.isChangeVisibilityAvailable = chatListState.isChangeVisibilityAvailable
		}

	}

}

// MARK: - Previews

#if DEBUG
struct ChatListNavigationContainerView_Preview: PreviewProvider {

	static var previews: some View {
		ChatListNavigationContainerView(store: Store(
			initialState: .init(),
			reducer: ChatList.init
		)) {
			Color.clear
		}
	}

}
#endif
