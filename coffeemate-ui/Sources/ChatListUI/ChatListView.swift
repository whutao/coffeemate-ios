//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ChatList
import ComposableArchitecture
import Design
import Extensions
import SwiftUI
import ViewComponents

// MARK: - View

public struct ChatListView: View {

	// MARK: Private properties

	private let store: StoreOf<ChatList>

	// MARK: Init

	public init(store: StoreOf<ChatList>) {
		self.store = store
	}

	// MARK: Body

	public var body: some View {
		ChatListNavigationContainerView(store: store) {
			WithViewStore(store, observe: { $0 }) { viewStore in
				Group {
					if viewStore.isPerformingChatSummariesLoad {
						LoadingSpinnerView(
							tintColor: Asset.Color.keppel.swiftUIColor
						)
						.adaptiveFrame(size: .square(60))
						.frame(maxWidth: .infinity, maxHeight: .infinity)
					} else if viewStore.areChatSummariesEmpty {
						VStack(alignment: .center, spacing: .zero) {
							Spacer()
							Text(viewStore.emptyChatListPlaceholder)
								.lineLimit(3)
								.multilineTextAlignment(.center)
								.adaptiveFont(.primary, .medium, ofSize: 14)
								.foregroundColor(Asset.Color.black.swiftUIColor)
								.adaptivePadding(.horizontal, 30)
								.frame(maxWidth: .infinity)
							Spacer()
						}
					} else {
						ScrollView(showsIndicators: false) {
							LazyVStack(alignment: .center, spacing: .zero) {
								ForEach(viewStore.entries) { entry in
									ChatListEntryView(chatEntry: entry)
										.adaptivePadding(.vertical, 10)
										.adaptivePadding(.horizontal, 24)
										.contentShape(Rectangle())
										.onTapGesture {
											viewStore.send(.view(.didSelectChatEntry(entry.id)))
										}
								}
							}
							.adaptivePadding(.top, 14)
							.adaptivePadding(.bottom, 100)
						}
					}
				}
			}
			.background {
				Asset.Color.alabaster.swiftUIColor
			}
		}
		.onAppear {
			store.send(.view(.didAppear))
		}
	}

}

// MARK: - Previews

#if DEBUG
struct ChatListView_Preview: PreviewProvider {

	static var previews: some View {
		ChatListView(store: Store(
			initialState: .init(
				entries: []
			),
			reducer: ChatList.init,
			withDependencies: { dependency in
				dependency.chatClient = .previewValue
			}
		))
	}

}
#endif
