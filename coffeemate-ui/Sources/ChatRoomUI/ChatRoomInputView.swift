//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import ExyteChat
import Extensions
import SwiftUI
import ViewComponents

// MARK: - View

struct ChatRoomInputView: View {

	// MARK: Private properties

	@EnvironmentObject private var globalFocusState: GlobalFocusState

	@Binding private var text: String

	private let fieldId: UUID

	private let placeholder: String

	private let action: (InputViewAction) -> Void

	// MARK: Init

	init(
		text: Binding<String>,
		placeholder: String,
		action: @escaping (InputViewAction) -> Void
	) {
		self._text = text
		self.fieldId = UUID()
		self.placeholder = placeholder
		self.action = action
	}

	// MARK: Body

	var body: some View {
		HStack(alignment: .center, spacing: .zero) {
			Spacer()
				.adaptiveFrame(width: 6)

			TextField("", text: $text, axis: .vertical)
				.customFocus($globalFocusState.focus, equals: .uuid(fieldId))
				.adaptiveFont(.primary, .regular, ofSize: 12)
				.placeholder(when: text.isEmpty) {
					Text(placeholder)
						.adaptiveFont(.primary, .regular, ofSize: 12)
						.foregroundColor(Asset.Color.gunsmoke.swiftUIColor)
				}
				.foregroundColor(Asset.Color.black.swiftUIColor)
				.tint(Asset.Color.mineShaft.swiftUIColor)
				.padding(.vertical, 14)
				.padding(.horizontal, 12)
				.background {
					Asset.Color.alabaster.swiftUIColor
				}
				.cornerRadius(12, corners: .allCorners)
				.onTapGesture {
					globalFocusState.focus = .uuid(fieldId)
				}

			Spacer()
				.adaptiveFrame(width: 18)

			Button {
				action(.send)
			} label: {
				TransparentCircleView(
					image: Asset.Image.Icon.messageSend.swiftUIImage,
					color: Asset.Color.monteCarlo.swiftUIColor
				)
			}
			.scaleOnPress()
			.adaptiveFrame(size: .square(34), alignment: .bottom)
			.disabledAndFaintIf(text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)
		}
		.adaptivePadding(.horizontal, 16)
		.adaptivePadding(.vertical, 12)
		.frame(minHeight: 60)
		.background {
			Asset.Color.black.swiftUIColor
				.cornerRadius(12, corners: [.topLeft, .topRight])
				.edgesIgnoringSafeArea(.bottom)
		}
	}

}

// MARK: - Previews

#if DEBUG
struct ChatRoomInputView_Preview: PreviewProvider {

	static var previews: some View {
		EmptyView()
	}

}
#endif
