//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ChatRoom
import ComposableArchitecture
import Design
import Kingfisher
import Models
import SwiftUI
import ViewComponents

// MARK: - View

struct ChatRoomNavigationContainerView<Content: View>: View {

	// MARK: Private properties

	@ObservedObject private var viewStore: ViewStore<ViewState, ChatRoom.Action>

	private let store: StoreOf<ChatRoom>

	private let content: () -> Content

	// MARK: Init

	init(
		store: StoreOf<ChatRoom>,
		@ViewBuilder content: @escaping () -> Content
	) {
		self._viewStore = .init(initialValue: .init(store, observe: ViewState.init))
		self.store = store
		self.content = content
	}

	// MARK: Body

	var body: some View {
		ConvexNavigationContainerView { _ in
			HStack {
				Button {
					if viewStore.isCompanionAvatarInteractionEnabled {
						viewStore.send(.view(.didTapBackButton))
					}
				} label: {
					Asset.Image.Icon.back.swiftUIImage
						.resizable()
						.aspectRatio(contentMode: .fit)
						.foregroundColor(Asset.Color.monteCarlo.swiftUIColor)
				}
				.scaleOnPress()
				.adaptiveFrame(size: .square(20))

				Spacer()
					.adaptiveFrame(width: 32)

				HStack(alignment: .center, spacing: .zero) {
					KFImage(viewStore.chatCompanion.avatarUrl)
						.resizable()
						.cacheOriginalImage()
						.downsampling(size: .square(200))
						.placeholder {
							LoadingSpinnerView(
								tintColor: Asset.Color.keppel.swiftUIColor
							)
							.adaptiveFrame(size: .square(16))
						}
						.aspectRatio(contentMode: .fill)
						.adaptiveFrame(size: .square(44))
						.clipShape(Capsule())
						.onTapGesture {
							viewStore.send(.view(.didTapCompanionAvatar))
						}

					Spacer()
						.adaptiveFrame(width: 10)

					VStack(alignment: .leading, spacing: .zero) {
						Spacer()

						Text(viewStore.chatCompanion.nickname)
							.adaptiveFont(.primary, .medium, ofSize: 16)

						Spacer()
							.adaptiveFrame(height: 10)

						Spacer()
					}
					.lineLimit(1)
					.foregroundColor(Asset.Color.alabaster.swiftUIColor)

					Spacer()
				}

				Spacer()
					.adaptiveFrame(size: .square(20))
			}
		} content: {
			content()
		}
	}

}

// MARK: - ViewState

extension ChatRoomNavigationContainerView {

	struct ViewState: Equatable, Sendable {

		// MARK: Exposed properties

		let chatCompanion: ChatCompanion

		let isCompanionAvatarInteractionEnabled: Bool

		// MARK: Init

		init(chatRoomState: ChatRoom.State) {
			self.chatCompanion = chatRoomState.chatCompanion
			self.isCompanionAvatarInteractionEnabled = chatRoomState.isCompanionAvatarInteractionEnabled
		}

	}

}

// MARK: - Previews

#if DEBUG
struct ChatRoomNavigationContainerView_Preview: PreviewProvider {

	static var previews: some View {
		ChatRoomNavigationContainerView(store: Store(
			initialState: ChatRoom.State(chatId: 2, chatCompanion: .mocked()),
			reducer: ChatRoom.init
		)) {
			Color.red
		}
	}

}
#endif
