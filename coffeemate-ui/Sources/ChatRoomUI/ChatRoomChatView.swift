//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ChatRoom
import ComposableArchitecture
import Design
import Extensions
import ExyteChat
import Models
import SwiftUI
import ViewComponents

// MARK: - View

struct ChatRoomChatView: View {

	// MARK: Private properties

	private let store: StoreOf<ChatRoom>

	@StateObject private var globalFocusState = GlobalFocusState()

	// MARK: Init

	init(store: StoreOf<ChatRoom>) {
		self.store = store
	}

	// MARK: Body

	var body: some View {
		WithViewStore(store, observe: ViewState.init, send: mapViewAction(_:)) { viewStore in
			ChatView(
				messages: viewStore.messages,
				didSendMessage: { draftMessage in
					viewStore.send(.didProduceMessage(draftMessage))
				},
				messageBuilder: { message, _, _ in
					ChatRoomMessageView(message: message)
						.padding(.horizontal, 36)
						.padding(.vertical, 10)
						.contentShape(Rectangle())
						.onTapGesture {
							globalFocusState.focus = nil
						}
						.onLongPressGesture {
							// To disable message menu with "Reply" action.
						}
				},
				inputViewBuilder: { text, _, _, _, action in
					ChatRoomInputView(
						text: text,
						placeholder: viewStore.inputPlaceholder,
						action: action
					)
					.environmentObject(globalFocusState)
				}
			)
			.assetsPickerLimit(assetsPickerLimit: .zero)
			.avatarSize(avatarSize: .zero)
		}
	}

	// MARK: Private methods

	private func mapViewAction(_ action: ViewAction) -> ChatRoom.Action {
		switch action {
		case let .didProduceMessage(draftMessage):
			return .view(.didGenerateTextMessage(draftMessage.text))
		}
	}

}

// MARK: - View action

extension ChatRoomChatView {

	enum ViewAction {

		// MARK: Case

		case didProduceMessage(DraftMessage)

	}

}

// MARK: - View state

extension ChatRoomChatView {

	struct ViewState: Equatable {

		// MARK: Exposed properties

		var messages: [Message]

		var inputPlaceholder: String

		// MARK: Init

		init(chatRoom: ChatRoom.State) {
			let isFromCurrentSender: (ChatMessage) -> Bool = { chatMessage in
				return chatMessage.sender.senderId != chatRoom.chatCompanion.profileHash
			}
			let statusFor: (ChatMessage) -> Message.Status = { chatMessage in
				switch chatMessage.status {
				case .read:
					return .read
				case .sending:
					return .sending
				case .sent:
					return .sent
				}
			}
			self.messages = chatRoom.chatHistory.map { chatMessage in
				switch chatMessage.content {
				case let .text(text):
					return Message(
						id: chatMessage.messageId,
						user: .init(
							id: chatMessage.sender.senderId,
							name: chatMessage.sender.nickname,
							avatarURL: nil,
							isCurrentUser: isFromCurrentSender(chatMessage)
						),
						status: statusFor(chatMessage),
						createdAt: chatMessage.date,
						text: text
					)
				}
			}
			self.inputPlaceholder = chatRoom.inputPlaceholder
		}

	}

}

// MARK: - Previews

#if DEBUG
struct ChatRoomMessageListView_Preview: PreviewProvider {

	static var previews: some View {
		ChatRoomChatView(store: Store(
			initialState: .init(
				chatId: 1,
				chatCompanion: .init(
					profileHash: ChatSender.mockedCurrentSender.senderId,
					nickname: ChatSender.mockedCurrentSender.nickname,
					avatarUrl: URL(string: "https://upload.wikimedia.org/wikipedia/ru/f/f1/Miraculous_Ladybug.png?20200817132031")!
				),
				chatHistory: [
					.mockedFromSideSender(),
					.mockedFromSideSender(),
					.mockedFromSideSender(),
					.mockedFromCurrentSender(),
					.mockedFromCurrentSender(),
					.mockedFromCurrentSender(),
					.mockedFromSideSender(),
					.mockedFromSideSender()
				]
			),
			reducer: ChatRoom.init,
			withDependencies: { dependency in
				dependency.chatClient = .previewValue
			}
		))
	}

}
#endif
