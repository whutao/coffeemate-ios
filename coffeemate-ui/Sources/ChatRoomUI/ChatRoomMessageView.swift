//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Design
import Extensions
import ExyteChat
import SwiftUI
import ViewComponents

// MARK: - View

struct ChatRoomMessageView: View {

	// MARK: Message

	private let message: Message

	private let dateFormatter: DateFormatter = {
		let formatter = DateFormatter()
		formatter.dateFormat = "H:mm"
		return formatter
	}()

	// MARK: Init

	init(message: Message) {
		self.message = message
	}

	// MARK: Body

	var body: some View {
		VStack(alignment: .center, spacing: .zero) {
			messageBodyView(message)

			Spacer()
				.adaptiveFrame(height: 4)

			messageFooterView(message)
		}
	}

	// MARK: Private methods

	private func isFromCurrentUser(_ message: Message) -> Bool {
		return message.user.isCurrentUser
	}

	private func isRead(_ message: Message) -> Bool {
		return message.status == .read
	}

	private func messageBubbleBackgroundColor(_ message: Message) -> SwiftUI.Color {
		if isFromCurrentUser(message) {
			return Asset.Color.mercury.swiftUIColor.opacity(0.6)
		} else {
			return Asset.Color.monteCarlo.swiftUIColor.opacity(0.3)
		}
	}

	private func messageDate(for message: Message) -> String {
		return dateFormatter.string(from: message.createdAt)
	}

	@ViewBuilder private func messageBodyView(_ message: Message) -> some View {
		HStack(alignment: .center, spacing: .zero) {
			if isFromCurrentUser(message) {
				Spacer()
			}

			Text(message.text)
				.foregroundColor(Asset.Color.black.swiftUIColor)
				.adaptiveFont(.primary, .regular, ofSize: 14)
				.multilineTextAlignment(.leading)
				.adaptivePadding(16)
				.background {
					messageBubbleBackgroundColor(message)
				}
				.clipShape(MessageBubbleShape(message))

			if !isFromCurrentUser(message) {
				Spacer()
			}
		}
	}

	@ViewBuilder private func messageFooterView(_ message: Message) -> some View {
		HStack(alignment: .center, spacing: .zero) {
			if isFromCurrentUser(message) {
				Spacer()
			}

			Text(messageDate(for: message))
				.adaptiveFont(.primary, .regular, ofSize: 10)
				.foregroundColor(Asset.Color.gunsmoke.swiftUIColor)

			Spacer()
				.frame(width: 4)

			if isFromCurrentUser(message) {
				Asset.Image.Icon.messageRead.swiftUIImage
					.resizable()
					.aspectRatio(contentMode: .fit)
					.foregroundColor(isRead(message) ? Asset.Color.monteCarlo.swiftUIColor : Asset.Color.gunsmoke.swiftUIColor)
					.adaptiveFrame(size: .square(12))
			}

			if !isFromCurrentUser(message) {
				Spacer()
			}
		}
	}

}

// MARK: - View state

extension ChatRoomMessageView {

	fileprivate struct MessageBubbleShape: Shape {

		private let message: Message

		init(_ message: Message) {
			self.message = message
		}

		func path(in rect: CGRect) -> Path {
			return  Path(roundedRectanglePath(
				minXminYRadius: 16,
				maxXminYRadius: 16,
				maxXmaxYRadius: message.user.isCurrentUser ? 4 : 16,
				minXmaxYRadius: message.user.isCurrentUser ? 16 : 4,
				in: rect
			).cgPath)
		}

	}

}

// MARK: - Previews

#if DEBUG
struct ChatRoomMessageView_Preview: PreviewProvider {

	static var previews: some View {
		EmptyView()
	}

}
#endif
