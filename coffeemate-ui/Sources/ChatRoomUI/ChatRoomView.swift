//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import ComposableArchitecture
import ChatRoom
import SideProfileOverviewUI
import SwiftUI

// MARK: - View

public struct ChatRoomView: View {

	// MARK: Private properties

	@Environment(\.scenePhase) private var scenePhase

	private let store: StoreOf<ChatRoom>

	// MARK: Init

	public init(store: StoreOf<ChatRoom>) {
		self.store = store
	}

	// MARK: Body

	public var body: some View {
		WithViewStore(store, observe: { $0 }) { viewStore in
			ChatRoomNavigationContainerView(store: store) {
				ChatRoomChatView(store: store)
			}
			.onFirstAppear {
				viewStore.send(.view(.didAppearFirstTime))
			}
			.sheet(
				store: store.scope(
					state: \.$sideProfileOverview,
					action: ChatRoom.Action.sideProfileOverview
				),
				content: SideProfileOverviewView.init
			)
			.onChange(of: scenePhase) { scene in
				if case .active = scene {
					viewStore.send(.view(.didEnterForeground))
				} else if case .background = scene {
					viewStore.send(.view(.didEnterBackground))
				}
			}
		}
		.toolbar(.hidden, for: .navigationBar)
	}

}

// MARK: - Previews

#if DEBUG
struct ChatRoomView_Preview: PreviewProvider {

	static var previews: some View {
		ChatRoomView(store: Store(
			initialState: .init(
				chatId: 1,
				chatCompanion: .init(
					profileHash: "asfdgfgsfa",
					nickname: "Julia",
					avatarUrl: URL(string: "https://upload.wikimedia.org/wikipedia/ru/f/f1/Miraculous_Ladybug.png?20200817132031")!
				),
				chatHistory: []
			),
			reducer: ChatRoom.init,
			withDependencies: { dependency in
				dependency.chatClient = .previewValue
			}
		))
	}

}
#endif
