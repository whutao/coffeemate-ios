// swift-tools-version: 5.8
import PackageDescription

// MARK: - Helper

private enum _PackageTarget: String {

	// Local package dependency targets

	case constants = "Constants"
	case extensions = "Extensions"
	case log = "Log"
	case mocking = "Mocking"
	case models = "Models"
	case systemInformation = "SystemInformation"

	case toastClient = "ToastClient"

	case imageCropper = "ImageCropper"
	case mediaPicker = "MediaPicker"

	case alert = "Alert"
	case application = "Application"
	case chatList = "ChatList"
	case chatRoom = "ChatRoom"
	case contacts = "Contacts"
	case home = "Home"
	case localizable = "Localizable"
	case match = "Match"
	case profile = "Profile"
	case profileEditor = "ProfileEditor"
	case sideProfileOverview = "SideProfileOverview"
	case signIn = "SignIn"
	case signUp = "SignUp"
	case splash = "Splash"

	// 3rd party dependency targets

	case alertToast = "AlertToast"
	case activityIndicatorView = "ActivityIndicatorView"
	case composableArchitecture = "ComposableArchitecture"
	case identifiedCollections = "IdentifiedCollections"
	case exyteChat = "ExyteChat"
	case kingfisher = "Kingfisher"
	case scalingHeaderScrollView = "ScalingHeaderScrollView"
	case svgKit = "SVGKit"
	case swiftUINavigationTransitions = "NavigationTransitions"

	// Targets

	case alertUI = "AlertUI"
	case applicationUI = "ApplicationUI"
	case chatListUI = "ChatListUI"
	case chatRoomUI = "ChatRoomUI"
	case contactsUI = "ContactsUI"
	case design = "Design"
	case homeUI = "HomeUI"
	case matchUI = "MatchUI"
	case profileEditorUI = "ProfileEditorUI"
	case profileUI = "ProfileUI"
	case sideProfileOverviewUI = "SideProfileOverviewUI"
	case signInUI = "SignInUI"
	case signUpUI = "SignUpUI"
	case splashUI = "SplashUI"
	case viewComponents = "ViewComponents"

	// Exposed properties

	fileprivate var asTargetDependency: Target.Dependency {
		switch self {
		// coffeemate-common
		case .constants,
			 .extensions,
			 .log,
			 .mocking,
			 .models:
			return .product(name: rawValue, package: "coffeemate-common")
		// coffeemate-services
		case .toastClient:
			return .product(name: rawValue, package: "coffeemate-services")
		// coffeemate-image-cropper
		case .imageCropper:
			return .product(name: "ImageCropper", package: "coffeemate-image-cropper")
		// coffeemate-media-picker
		case .mediaPicker:
			return .product(name: "MediaPicker", package: "coffeemate-media-picker")
		// coffeemate-core
		case .alert,
			.application,
			.chatList,
			.chatRoom,
			.contacts,
			.home,
			.localizable,
			.match,
			.profile,
			.profileEditor,
			.sideProfileOverview,
			.signIn,
			.signUp,
			.splash:
			return .product(name: rawValue, package: "coffeemate-core")
		// 3rd party
		case .alertToast:
			return .product(name: "AlertToast", package: "AlertToast")
		case .activityIndicatorView:
			return .product(name: "ActivityIndicatorView", package: "ActivityIndicatorView")
		case .composableArchitecture:
			return .product(name: "ComposableArchitecture", package: "swift-composable-architecture")
		case .exyteChat:
			return .product(name: "ExyteChat", package: "Chat")
		case .identifiedCollections:
			return .product(name: "IdentifiedCollections", package: "swift-identified-collections")
		case .kingfisher:
			return .product(name: "Kingfisher", package: "Kingfisher")
		case .scalingHeaderScrollView:
			return .product(name: "ScalingHeaderScrollView", package: "ScalingHeaderScrollView")
		case .svgKit:
			return .product(name: "SVGKit", package: "SVGKit")
		case .swiftUINavigationTransitions:
			return .product(name: "NavigationTransitions", package: "swiftui-navigation-transitions")
		default:
			return .target(name: rawValue)
		}
	}

	var asLibrary: Product {
		return .library(name: name, targets: [name])
	}

	var name: String {
		return rawValue
	}

}

fileprivate extension Target {

	static func target(
		_ target: _PackageTarget,
		dependencies: [_PackageTarget] = [],
		resources: [Resource]? = nil
	) -> Target {
		return .target(
			name: target.name,
			dependencies: dependencies.map(\.asTargetDependency),
			resources: resources
		)
	}

	static func testTarget(
		_ target: _PackageTarget,
		dependencies: [_PackageTarget] = []
	) -> Target {
		return .testTarget(
			name: target.name,
			dependencies: dependencies.map(\.asTargetDependency)
		)
	}
}

fileprivate extension Product {

	static func library(from target: _PackageTarget) -> Product {
		return target.asLibrary
	}

}

// MARK: Package

var package = Package(
	name: "coffeemate-ui",
	platforms: [.iOS(.v16)],
	products: [
		.library(from: .alertUI),
		.library(from: .applicationUI),
		.library(from: .chatListUI),
		.library(from: .chatRoomUI),
		.library(from: .contactsUI),
		.library(from: .design),
		.library(from: .homeUI),
		.library(from: .matchUI),
		.library(from: .profileEditorUI),
		.library(from: .profileUI),
		.library(from: .sideProfileOverviewUI),
		.library(from: .signInUI),
		.library(from: .signUpUI),
		.library(from: .splashUI),
		.library(from: .viewComponents)
	],
	dependencies: [
		.package(name: "coffeemate-common", path: "coffeemate-common"),
		.package(name: "coffeemate-services", path: "coffeemate-services"),
		.package(name: "coffeemate-core", path: "coffeemate-core"),
		.package(name: "coffeemate-image-cropper", path: "coffeemate-image-cropper"),
		.package(name: "coffeemate-media-picker", path: "coffeemate-media-picker"),
		.package(url: "https://github.com/elai950/AlertToast", from: Version(1, 3, 9)),
		.package(url: "https://github.com/onevcat/Kingfisher", from: Version(7, 9, 0)),
		.package(url: "https://github.com/SVGKit/SVGKit", from: Version(3, 0, 0)),
		.package(url: "https://github.com/exyte/ScalingHeaderScrollView", from: Version(1, 0, 1)),
		.package(url: "https://github.com/exyte/Chat", from: Version(1, 1, 0)),
		.package(url: "https://github.com/exyte/ActivityIndicatorView", from: Version(1, 1, 1)),
		.package(url: "https://github.com/pointfreeco/swift-composable-architecture", from: Version(1, 1, 0)),
		.package(url: "https://github.com/pointfreeco/swift-identified-collections", from: Version(1, 0, 0)),
		.package(url: "https://github.com/davdroman/swiftui-navigation-transitions", from: Version(0, 10, 1))
	]
)

// MARK: Targets

package.targets += [
	.target(.alertUI, dependencies: [
		.alert,
		.composableArchitecture,
		.design,
		.extensions,
		.viewComponents
	]),
	.target(.applicationUI, dependencies: [
		.alertToast,
		.alertUI,
		.application,
		.design,
		.extensions,
		.homeUI,
		.signInUI,
		.signUpUI,
		.splashUI,
		.swiftUINavigationTransitions,
		.toastClient
	]),
	.target(.chatListUI, dependencies: [
		.chatList,
		.composableArchitecture,
		.design,
		.extensions,
		.viewComponents
	]),
	.target(.chatRoomUI, dependencies: [
		.chatRoom,
		.composableArchitecture,
		.design,
		.exyteChat,
		.extensions,
		.models,
		.sideProfileOverviewUI,
		.viewComponents
	]),
	.target(.contactsUI, dependencies: [
		.composableArchitecture,
		.contacts,
		.design,
		.extensions,
		.kingfisher,
		.matchUI,
		.mocking,
		.sideProfileOverview,
		.sideProfileOverviewUI,
		.viewComponents
	]),
	.target(.design, dependencies: [
		.composableArchitecture,
		.log,
		.kingfisher,
		.models,
		.svgKit
	], resources: [
		.copy("Assets/Color.xcassets"),
		.copy("Assets/Image.xcassets"),
		.copy("Fonts/Roboto-Black.ttf"),
		.copy("Fonts/Roboto-Bold.ttf"),
		.copy("Fonts/Roboto-Light.ttf"),
		.copy("Fonts/Roboto-Medium.ttf"),
		.copy("Fonts/Roboto-Regular.ttf"),
		.copy("Fonts/Roboto-Thin.ttf")
	]),
	.target(.homeUI, dependencies: [
		.chatRoomUI,
		.chatListUI,
		.composableArchitecture,
		.contactsUI,
		.design,
		.extensions,
		.home,
		.kingfisher,
		.mocking,
		.models,
		.profileUI,
		.profileEditorUI,
		.sideProfileOverviewUI,
		.swiftUINavigationTransitions,
		.viewComponents
	]),
	.target(.matchUI, dependencies: [
		.composableArchitecture,
		.design,
		.extensions,
		.kingfisher,
		.match,
		.models,
		.mocking,
		.viewComponents
	]),
	.target(.profileEditorUI, dependencies: [
		.composableArchitecture,
		.design,
		.extensions,
		.identifiedCollections,
		.mocking,
		.models,
		.profileEditor,
		.sideProfileOverview,
		.sideProfileOverviewUI,
		.viewComponents
	]),
	.target(.profileUI, dependencies: [
		.composableArchitecture,
		.design,
		.extensions,
		.mocking,
		.models,
		.profile,
		.profileEditor,
		.sideProfileOverviewUI,
		.viewComponents
	]),
	.target(.sideProfileOverviewUI, dependencies: [
		.composableArchitecture,
		.design,
		.extensions,
		.mocking,
		.models,
		.sideProfileOverview,
		.viewComponents
	]),
	.target(.signInUI, dependencies: [
		.composableArchitecture,
		.design,
		.extensions,
		.signIn,
		.viewComponents
	]),
	.target(.signUpUI, dependencies: [
		.composableArchitecture,
		.design,
		.extensions,
		.mocking,
		.signUp,
		.viewComponents
	]),
	.target(.splashUI, dependencies: [
		.composableArchitecture,
		.design,
		.extensions,
		.splash,
		.viewComponents
	]),
	.target(.viewComponents, dependencies: [
		.activityIndicatorView,
		.composableArchitecture,
		.design,
		.extensions,
		.identifiedCollections,
		.imageCropper,
		.kingfisher,
		.localizable,
		.mediaPicker,
		.mocking,
		.models
	])
]
