//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Extensions
import SwiftUI

// MARK: - View

public struct ImageCropperView<
	BackgroundContent: View,
	NavigationBarContent: View
>: View {
	
	// MARK: Exposed typealiases
	
	public typealias CompletionClosure = (UIImage) -> Void

	public typealias BackgroundContentBuilder = () -> BackgroundContent

	public typealias NavigationBarContentBuilder = (
		_ size: CGSize,
		_ dismissClosure: @escaping ActionClosure,
		_ completionClosure: @escaping ActionClosure
	) -> NavigationBarContent

	// MARK: Private methods

	@State private var scale: CGFloat = 1
	
	@State private var offset: CGSize = .zero {
		didSet {
			print(offset / scale)
		}
	}
	
	@State private var lastStoredScale: CGFloat = 1
	
	@State private var lastStoredOffset: CGSize = .zero
	
	@GestureState private var isInteracting: Bool = false
	
	private let originalImage: UIImage
	
	private let cropKind: CropKind
	
	private let gridVerticalLineCount: Int = 4
	
	private let gridHorizontalLineCount: Int = 4
	
	private let gridLineColor: Color
	
	private let gridLineWidth: CGFloat
	
	private let maxMagnificationValue: CGFloat
	
	private let backgroundContent: BackgroundContentBuilder?
	
	private let navigationBarContent: NavigationBarContentBuilder?
	
	private let coordinateSpaceName: String = "cropView.coordinateSpace"
	
	private let dismissAction: ActionClosure
	
	private let completionAction: CompletionClosure
	
	private var cropRegionWidth: CGFloat {
		return UIScreen.main.bounds.width * 0.8
	}
	
	// MARK: Init
	
	public init(
		originalImage: UIImage,
		cropKind: CropKind,
		maxZoom: CGFloat = 6,
		gridLineColor: Color = .white.opacity(0.5),
		gridLineWidth: CGFloat = 1.5,
		backgroundContent: BackgroundContentBuilder?,
		navigationBarContent: NavigationBarContentBuilder?,
		dismissAction: @escaping ActionClosure,
		completionAction: @escaping CompletionClosure
	) {
		self.originalImage = originalImage
		self.cropKind = cropKind
		self.gridLineColor = gridLineColor
		self.gridLineWidth = gridLineWidth
		self.maxMagnificationValue = maxZoom
		self.backgroundContent = backgroundContent
		self.navigationBarContent = navigationBarContent
		self.dismissAction = dismissAction
		self.completionAction = completionAction
	}
	
	// MARK: Body
	
	public var body: some View {
		ZStack {
			Group {
				if let backgroundContent {
					backgroundContent()
				} else {
					Color.black
				}
			}
			.ignoresSafeArea(edges: .all)
			
			ZStack(alignment: .top) {
				imageView(areGridsVisible: true)
					.frame(maxWidth: .infinity, maxHeight: .infinity)
					.zIndex(0)
				
				Group {
					GeometryReader { geometry in
						if let navigationBarContent {
							navigationBarContent(geometry.size, dismissAction, {
								completionAction(renderUIImage())
							})
						} else {
							HStack {
								Button {
									dismissAction()
								} label: {
									Text("Close")
								}
								
								Spacer()
								
								Button {
									DispatchQueue.main.async {
										completionAction(DispatchQueue.global(qos: .userInitiated).sync {
											renderUIImage()
										})
									}
								} label: {
									Text("Done")
								}
							}
							.padding(.horizontal, 24)
						}
					}
				}
				.frame(height: 80)
				.zIndex(1)
			}
		}
	}
	
	// MARK: Private methods
	
	private func renderUIImage() -> UIImage {
		let originalImageSize: CGSize = originalImage.size
		
		let initialCropRect: CGRect = {
			let size: CGSize = {
				if originalImageSize.width < originalImageSize.height {
					return CGSize(
						width: originalImageSize.width,
						height: originalImageSize.width / cropKind.aspectRatio
					)
				} else {
					return CGSize(
						width: originalImageSize.height * cropKind.aspectRatio,
						height: originalImageSize.height
					)
				}
			}()
			let origin = CGPoint(
				x: (0.5) * (originalImageSize.width - size.width),
				y: (0.5) * (originalImageSize.height - size.height)
			)
			return CGRect(origin: origin, size: size).integral
		}()
		
		let cropRect: CGRect = {
			let size = (1 / scale) * initialCropRect.size
			let centerOffset = (1 / scale) * CGSize(
				width: offset.width * (size.width / imageViewSize().width),
				height: offset.height * (size.height / imageViewSize().height)
			)
			let initialCropRectCenter = initialCropRect.center
			let center = CGPoint(
				x: initialCropRectCenter.x + centerOffset.width,
				y: initialCropRectCenter.y + centerOffset.height
			)
			print(initialCropRectCenter, centerOffset, center)
			return CGRect(center: center, size: size).integral
		}()
		
		print(
		   """
		   Scale = \(scale) \n
		   Offset = \(offset) \n
		   View size = \(imageViewSize()) \n
		   Original image size = \(originalImageSize)\n
		   Inital crop region = \(initialCropRect) \n
		   Crop region = \(cropRect)
		   """
		)
		
		let format = UIGraphicsImageRendererFormat()
		format.opaque = false
		
		let renderer = UIGraphicsImageRenderer(size: cropRect.size, format: format)
		return renderer.image { context in
			context.cgContext.translateBy(x: 0, y: -cropRect.origin.y)
			originalImage.draw(at: .zero)
		}
	}
	
	private func imageViewSize() -> CGSize {
		let width: CGFloat = cropRegionWidth
		let height: CGFloat = width / cropKind.aspectRatio
		return CGSize(width: width, height: height)
	}

	@ViewBuilder private func imageView(areGridsVisible: Bool) -> some View {
		GeometryReader { geometry in
			Image(uiImage: originalImage)
				.resizable()
				.aspectRatio(contentMode: .fill)
				.overlay {
					GeometryReader { imageGeometry in
						Color.clear.onChange(of: isInteracting) { interacting in
							let imageRect = imageGeometry.frame(in: .named(coordinateSpaceName))
							withAnimation {
								if imageRect.minX > 0 {
									offset.width = offset.width - imageRect.minX
									sendHapticFeedback(.medium)
								}
								if imageRect.minY > 0 {
									offset.height = offset.height - imageRect.minY
									sendHapticFeedback(.medium)
								}
								if imageRect.maxX < geometry.size.width {
									offset.width = imageRect.minX - offset.width
									sendHapticFeedback(.medium)
								}
								if imageRect.maxY < geometry.size.height {
									offset.height = imageRect.minY - offset.height
									sendHapticFeedback(.medium)
								}
							}
							if !interacting {
								lastStoredOffset = offset
							}
						}
					}
				}
				.frame(size: geometry.size)
		}
		.scaleEffect(amount: scale)
		.offset(offset)
		.animation(.interactiveSpring(), value: offset)
		.animation(.interactiveSpring(), value: scale)
		.overlay {
			if areGridsVisible {
				gridLinesOverlay()
			}
		}
		.coordinateSpace(name: coordinateSpaceName)
		.simultaneousGesture(DragGesture()
			.updating($isInteracting) { _, output, _ in
				output = true
			}
			.onChanged { value in
				offset = value.translation + lastStoredOffset
			}
		)
		.simultaneousGesture(MagnificationGesture()
			.updating($isInteracting) { _, output, _ in
				output = true
			}
			.onChanged { value in
				scale = clamp(value + lastStoredScale, to: 1...maxMagnificationValue)
			}
			.onEnded { value in
				withAnimation(.spring()) {
					if scale < 1 {
						scale = 1
					}
					lastStoredScale = scale - 1
				}
			}
		)
		.simultaneousGesture(TapGesture(count: 2)
			.onEnded {
				withAnimation(.spring()) {
					if scale > 1 {
						scale = 1
						lastStoredScale = 0
					}
				}
			}
		)
		.frame(size: imageViewSize())
		.clipShape(CropShape(cropKind))
	}
	
	@ViewBuilder private func gridLinesOverlay() -> some View {
		ZStack {
			HStack(alignment: .center, spacing: .zero) {
				ForEach(0..<gridVerticalLineCount, id: \.self) { _ in
					Rectangle()
						.fill(gridLineColor)
						.frame(width: gridLineWidth)
						.frame(maxWidth: .infinity)
				}
			}
			
			VStack(alignment: .center, spacing: .zero) {
				ForEach(0..<gridHorizontalLineCount, id: \.self) { _ in
					Rectangle()
						.fill(gridLineColor)
						.frame(height: gridLineWidth)
						.frame(maxHeight: .infinity)
				}
			}
		}
	}
	
}

// MARK: - Shape

extension ImageCropperView {
	
	fileprivate struct CropShape: Shape {
		
		// MARK: Private properties
		
		private let cropKind: CropKind
		
		// MARK: Init
		
		init(_ cropKind: CropKind) {
			self.cropKind = cropKind
		}
		
		func path(in rect: CGRect) -> Path {
			switch cropKind {
			case .circle:
				return Path(ellipseIn: rect)
			case .square, .rectangle3x4, .rectangle4x3:
				return Path(rect)
			}
		}
		
	}
	
}

// MARK: - Init

extension ImageCropperView where BackgroundContent == EmptyView, NavigationBarContent == EmptyView {
	
	public init(
		originalImage: UIImage,
		cropKind: CropKind,
		maxZoom: CGFloat = 6,
		gridLineColor: Color = .white.opacity(0.5),
		gridLineWidth: CGFloat = 1.5,
		dismissAction: @escaping ActionClosure,
		completionAction: @escaping CompletionClosure
	) {
		self.originalImage = originalImage
		self.cropKind = cropKind
		self.maxMagnificationValue = maxZoom
		self.gridLineColor = gridLineColor
		self.gridLineWidth = gridLineWidth
		self.backgroundContent = nil
		self.navigationBarContent = nil
		self.dismissAction = dismissAction
		self.completionAction = completionAction
	}
	
}

// MARK: - Previews

#if DEBUG
struct ImageCropperView_Preview: PreviewProvider {

	private struct ContentView: View {
		
		private let image = Asset.man.image
		
		@State private var croppedImage: UIImage? = nil
		
		@State private var isPresented: Bool = false
		
		var body: some View {
			GeometryReader { geometry in
				VStack {
					Group {
						if let croppedImage {
							Image(uiImage: croppedImage)
								.resizable()
						} else {
							Color.red
						}
					}
					.frame(size: .square(geometry.size.minDimension))
					.clipped()
					
					Spacer()
					
					Button {
						isPresented = true
					} label: {
						Text("Tap")
					}
					
					Spacer()
				}
			}
			.padding()
			.fullScreenCover(isPresented: $isPresented) {
				ImageCropperView(
					originalImage: image,
					cropKind: .square,
					dismissAction: {
						isPresented = false
					},
					completionAction: { uiImage in
						croppedImage = uiImage
						isPresented = false
					}
				)
			}
		}
		
	}

	static var previews: some View {
		ContentView()
	}

}
#endif
