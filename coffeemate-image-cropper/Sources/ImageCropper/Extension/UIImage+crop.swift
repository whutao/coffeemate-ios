//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import UIKit

// MARK: - UIImage+croppedCopy

extension UIImage {
	
	/// Crops a UIImage to CGRect.
	public func croppedCopy(to rect: CGRect) -> UIImage {
		UIGraphicsBeginImageContext(rect.size)
		defer {
			UIGraphicsEndImageContext()
		}
		let context = UIGraphicsGetCurrentContext()
		
		let drawRect = CGRect(
			x: -rect.origin.x,
			y: -rect.origin.y,
			width: self.size.width,
			height: self.size.height
		)
		
		context?.clip(to: CGRect(
			x: 0,
			y: 0,
			width: rect.size.width,
			height: rect.size.height
		))
		
		draw(in: drawRect)
		
		return UIGraphicsGetImageFromCurrentImageContext()!
	}
	
}
