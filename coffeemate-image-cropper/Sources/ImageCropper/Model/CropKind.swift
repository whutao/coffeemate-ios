//
//  Created by Roman Nabiullin
//
//  Copyright (c) 2023 - Present
//
//  All Rights Reserved.
//

import Foundation

// MARK: - Model

public enum CropKind: Equatable, Sendable {
	
	// MARK: Case
	
	case circle
	
	case square
	
	/// Rectangle with *aspectRatio = width/height = 4/3*.
	case rectangle4x3
	
	/// Rectangle with *aspectRatio = width/height = 3/4*.
	case rectangle3x4
	
	// MARK: Exposed methods
	
	internal var aspectRatio: Double {
		switch self {
		case .circle, .square:
			return 1
		case .rectangle4x3:
			return 4 / 3
		case .rectangle3x4:
			return 3 / 4
		}
	}
	
}
