// swift-tools-version: 5.8
import PackageDescription

let package = Package(
	name: "coffeemate-image-cropper",
	platforms: [.iOS(.v16)],
	products: [
		.library(name: "ImageCropper", targets: ["ImageCropper"])
	],
	dependencies: [
		.package(name: "coffeemate-common", path: "coffeemate-common"),
		.package(url: "https://github.com/pointfreeco/swift-identified-collections", from: Version(1, 0, 0))
	],
	targets: [
		.target(
			name: "ImageCropper",
			dependencies: [
				.product(name: "Extensions", package: "coffeemate-common"),
				.product(name: "IdentifiedCollections", package: "swift-identified-collections")
			],
			resources: [
				.copy("Resources/Assets.xcassets")
			]
		)
	]
)
